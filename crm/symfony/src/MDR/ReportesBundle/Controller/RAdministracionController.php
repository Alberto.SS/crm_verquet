<?php

namespace MDR\ReportesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


/**
 * RAdministracion controller.
 *
 * @Route("/administracion")
 */
class RAdministracionController extends Controller
{

	/**
     * @Route("/productos", name="reporte_productos")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Administracion:productos.html.twig")
     */
    public function RepProductosAction(Request $request)
    {
 		$em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepProd();
        $entities = array();


        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $OficinaVta = $searchForm->get('OfiVentas')->getData();
                $Producto = $searchForm->get('Prodname')->getData();
                $Activo = $searchForm->get('activo')->getData(); 

               	$entities = $em->getRepository('MDRPuntoVentaBundle:Productos')->findByRepProductos($OficinaVta, $Producto, $Activo);
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelProductos($entities);
					}
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:administracion:productos.html.twig', array(
            'productos' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de productos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepProd()
    {       
        $form = $this->createFormBuilder()
        		->add('OfiVentas', 'entity', array(
                    'label' => 'Oficina de Ventas',
                    'class' => 'MDRPuntoVentaBundle:OficinasVentas',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'rep_ofivta','class' => "form-control"),
                ))
                ->add('Prodname', 'entity', array(
                    'label' => 'Nombre Producto',
                    'class' => 'MDRPuntoVentaBundle:Productos',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'rep_prodname','class' => "form-control"),
                ))
                ->add('activo', 'choice', array(
			   		'choices'   => array('1' => 'Activo', '0' => 'Inactivo'),
					'label'         => 'Activo',
					'empty_value'   => '-----------',
					'required'  => false,
					'attr' => array('class' => "form-control"),
				))                
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()')	
                	))					
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))				
                ->getForm();

        return $form;
    }

function createExcelProductos($listados, $titulo = 'Productos'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "OriVta")
				->setCellValue('B1', "Producto")
                ->setCellValue('C1', "Nombre")
                ->setCellValue('D1', "Precio")
				->setCellValue('E1', "Activo")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
					        
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getOficinaVenta()->getDescripcion())
						->setCellValue('B'.$numRow, $listado->getMaterialPrecio())
						->setCellValue('C'.$numRow, $listado->getProducto()->getDescripcion())
						->setCellValue('D'.$numRow, $listado->getPrecio())
						->setCellValue('E'.$numRow, $listado->getActivo() == 1 ? 'Activo' : 'Inactivo') 
                    ;
					$numRow ++;	
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}

    /**
     * @Route("/usuarios", name="reporte_usuarios")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Administracion:usuarios.html.twig")
     */
    public function RepUsuariosAction(Request $request)
    {
 		$em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepUsuarios();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $Activo = $searchForm->get('activo')->getData();
                if($Activo || $Activo == "0" ){
                		$entities = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findByActivo($Activo);
                }else{
                		$entities = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAll();
                } 
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelUsuarios($entities);
					}             
              
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:administracion:usuarios.html.twig', array(
            'usuarios' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de usuarios.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepUsuarios()
    {       
        $form = $this->createFormBuilder()
                ->add('activo', 'choice', array(
			   		'choices'   => array('1' => 'Activo', '0' => 'Inactivo'),
					'label'         => 'Activo',
					'empty_value'   => '-----------',
					'required'  => false,
					'attr' => array('class' => "form-control"),
				))                
                ->add(
                	'Consultar', 'submit' , array(
                	'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()')	
                	))					
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))
                ->getForm();

        return $form;
    }

function createExcelUsuarios($listados, $titulo = 'Usuarios'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Id")
				->setCellValue('B1', "Nombre")
                ->setCellValue('C1', "A. Paterno")
                ->setCellValue('D1', "A. Materno")
				->setCellValue('E1', "Usuario")
				->setCellValue('F1', "Perfil")
				->setCellValue('G1', "Activo")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
					        
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getUNumero())
						->setCellValue('B'.$numRow, $listado->getUNombre())
						->setCellValue('C'.$numRow, $listado->getUApPaterno())
						->setCellValue('D'.$numRow, $listado->getUApMaterno())
						->setCellValue('E'.$numRow, $listado->getuClave())
						->setCellValue('F'.$numRow, $listado->getTipoUsuario()->__toString())
						->setCellValue('G'.$numRow, $listado->getActivo() == 1 ? 'Activo' : 'Inactivo') 
                    ;
					$numRow ++;	
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}
	
 	/**
     * @Route("/rep_efectividad", name="reporte_efectividad")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Administracion:efectividad.html.twig")
     */
    public function RepEfectividadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepEfectividad();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $OfiVentas = $searchForm->get('OfiVentas')->getData();

                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByRepEfectividad($fechaInicio, $fechaFin, $OfiVentas);
					if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelEfectividad($entities);
					}                 
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Administracion:efectividad.html.twig', array(
            'efectividad' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de Efectividad.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepEfectividad()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('OfiVentas', 'entity', array(
                    'label' => 'Oficina de Ventas',
                    'class' => 'MDRPuntoVentaBundle:OficinasVentas',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'rep_ofivta','class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()') 
                    ))
                ->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))
                ->getForm();

        return $form;
    }

function createExcelEfectividad($listados, $titulo = 'Efectividad'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Tipo de pago")
				->setCellValue('B1', "OrVta")
                ->setCellValue('C1', "Falta")
                ->setCellValue('D1', "OriVta")
				->setCellValue('E1', "Estatus")
				->setCellValue('F1', "Fecha de Cobro")
				->setCellValue('G1', "Cliente")
				->setCellValue('H1', "Total")
				->setCellValue('I1', "Usuario")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
					            $fechaCobro = '';
					foreach ($listado->getCobros() as $cobro) {
						if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
						{
							$fechaCobro = $cobro->getFechaCobro()->format('d/m/Y H:i:s');
						}
					}		
					foreach ($listado->getProductos() as $detalle) {		        
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getPago()->getDescripcion())
						->setCellValue('B'.$numRow, $listado->getIdPedido())
						->setCellValue('C'.$numRow, $listado->getFecha()->format('Y/m/d H:i:s'))
						->setCellValue('D'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
						->setCellValue('E'.$numRow, $listado->getEstatus()->getDescripcion())
						->setCellValue('F'.$numRow, $fechaCobro )
						->setCellValue('G'.$numRow, $listado->getCliente()->__toString()) 
						->setCellValue('H'.$numRow, $listado->getTotalPago()) 
						->setCellValue('I'.$numRow, $listado->getUsuario()->getUNombre().' '.$listado->getUsuario()->getUApPaterno().' '.$listado->getUsuario()->getUApMaterno()) 
                    ;
					$numRow ++;	
					}
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}
	
    /**
     * @Route("/rep_filtro", name="reporte_filtro")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Administracion:filtro.html.twig")
     */
    public function RepFiltroAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepFiltro();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $OfiVentas = $searchForm->get('OfiVentas')->getData();
                $Tpago = $searchForm->get('tpago')->getData();

                $entities = $em->getRepository('MDRPuntoVentaBundle:Productos')->findByRepFiltro($fechaInicio, $OfiVentas, $Tpago);
                
               
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Administracion:filtro.html.twig', array(
            'filtro' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de Efectividad.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepFiltro()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Fecha de Alta',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('class' => "form-control"),
                ))                
                ->add('OfiVentas', 'entity', array(
                    'label' => 'Oficina de Ventas',
                    'class' => 'MDRPuntoVentaBundle:OficinasVentas',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'rep_ofivta','class' => "form-control"),
                ))
                ->add('tpago', 'choice', array(
			   		'choices'   => array('1' => 'TDC', '2' => 'COD'),
					'label'         => 'Tipo Pago',
					'empty_value'   => '-----------',
					'required'  => false,
					'attr' => array('class' => "form-control"),
				))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()') 
                    ))
                ->add(
                    'Excel', 'button' , array(
                    'attr' => array('class' => 'btn btn-success') 
                    ))
                ->getForm();

        return $form;
    }


    /**
     * @Route("/rep_flash", name="reporte_flash")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Administracion:flash.html.twig")
     */
    public function RepFlashAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepFlash();
        $entities = array();

        function addtimedate($date,$time){

           $hrs = substr($time,0,-3);
           $min = substr($time,3);

           return $date->add(new \DateInterval('PT0'.$hrs.'H'.$min.'M'));

        }

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $time1 = $searchForm->get('time1')->getData();
                $time2 = $searchForm->get('time2')->getData();
                $fechaInicio = addtimedate($fechaInicio,$time1);
                $fechaFin = addtimedate($fechaFin,$time2);

                $entities = $em->getRepository('MDRPuntoVentaBundle:ClienteFlash')->findByLlamadas($fechaInicio, $fechaFin);
                if($searchForm->get('Excel')->isClicked())
                {
                    return $this->createExcelFlash($entities);
                }
               
            }
        }
        //\Doctrine\Common\Util\Debug::dump(count($entities)); 
        return $this->render('MDRReportesBundle:Administracion:flash.html.twig', array(
            'llamadasflash' => $entities,
            'form_rep' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para generar reporte de Efectividad.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepFlash()
    {       
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('time1', 'choice', array(
                    'choices'     => array('00:00' => '00:00 am','00:30' => '00:30 am','01:00' => '01:00 am','01:30' => '01:30 am',
                                            '02:00' => '02:00 am','02:30' => '02:30 am','03:00' => '03:00 am','03:30' => '03:30 am',
                                            '04:00' => '04:00 am','04:30' => '04:30 am','05:00' => '05:00 am','05:30' => '05:30 am',
                                            '06:00' => '06:00 am','06:30' => '06:30 am','07:00' => '07:00 am','07:30' => '07:30 am',
                                            '08:00' => '08:00 am','08:30' => '08:30 am','09:00' => '09:00 am','09:30' => '09:30 am',
                                            '10:00' => '10:00 am','10:30' => '10:30 am','11:00' => '11:00 am','11:30' => '11:30 am',
                                            '12:00' => '12:00 pm','12:30' => '12:30 pm','13:00' => '01:00 pm','13:30' => '01:30 pm',
                                            '14:00' => '02:00 pm','14:30' => '02:30 pm','15:00' => '03:00 pm','15:30' => '03:30 pm',
                                            '16:00' => '04:00 pm','16:30' => '04:30 pm','17:00' => '05:00 pm','17:30' => '05:30 pm',
                                            '18:00' => '06:00 pm','18:30' => '06:30 pm','19:00' => '07:00 pm','19:30' => '07:30 pm',
                                            '20:00' => '08:00 pm','20:30' => '08:30 pm','21:00' => '09:00 pm','21:30' => '09:30 pm',
                                            '22:00' => '10:00 pm','22:30' => '10:30 pm','23:00' => '11:00 pm','23:30' => '11:30 pm'),
                    'empty_value' => '----------',
                    'label' => 'Hora inicio',
                    'attr' => array('class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('time2', 'choice', array(
                    'choices'     => array('00:00' => '00:00 am','00:30' => '00:30 am','01:00' => '01:00 am','01:30' => '01:30 am',
                                            '02:00' => '02:00 am','02:30' => '02:30 am','03:00' => '03:00 am','03:30' => '03:30 am',
                                            '04:00' => '04:00 am','04:30' => '04:30 am','05:00' => '05:00 am','05:30' => '05:30 am',
                                            '06:00' => '06:00 am','06:30' => '06:30 am','07:00' => '07:00 am','07:30' => '07:30 am',
                                            '08:00' => '08:00 am','08:30' => '08:30 am','09:00' => '09:00 am','09:30' => '09:30 am',
                                            '10:00' => '10:00 am','10:30' => '10:30 am','11:00' => '11:00 am','11:30' => '11:30 am',
                                            '12:00' => '12:00 pm','12:30' => '12:30 pm','13:00' => '01:00 pm','13:30' => '01:30 pm',
                                            '14:00' => '02:00 pm','14:30' => '02:30 pm','15:00' => '03:00 pm','15:30' => '03:30 pm',
                                            '16:00' => '04:00 pm','16:30' => '04:30 pm','17:00' => '05:00 pm','17:30' => '05:30 pm',
                                            '18:00' => '06:00 pm','18:30' => '06:30 pm','19:00' => '07:00 pm','19:30' => '07:30 pm',
                                            '20:00' => '08:00 pm','20:30' => '08:30 pm','21:00' => '09:00 pm','21:30' => '09:30 pm',
                                            '22:00' => '10:00 pm','22:30' => '10:30 pm','23:00' => '11:00 pm','23:30' => '11:30 pm'),
                    'empty_value' => '----------',
                    'label' => 'Hora Fin',
                    'attr' => array('class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'loadPedidos()') 
                    ))
                ->add(
                    'Excel', 'submit' , array(
                    'attr' => array('class' => 'btn btn-success') 
                    ))
                ->getForm();

        return $form;
    }

    function createExcelFlash($listados, $titulo = 'Flash'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Número Telefónico")
                ->setCellValue('B1', "Fecha")
                ->setCellValue('C1', "Cliente")
                ->setCellValue('D1', "DNIS")
                ->setCellValue('E1', "Agente")
                ->setCellValue('F1', "Interes")
                ->setCellValue('G1', "Observaciones")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getTelefono()->getNumero())
                        ->setCellValue('B'.$numRow, $listado->getFecha()->format('Y/m/d H:i:s'))
                        ->setCellValue('C'.$numRow, $listado->getCliente()->getNombreCompleto())
                        ->setCellValue('D'.$numRow, $listado->getDnis())
                        ->setCellValue('E'.$numRow, $listado->getUsuario()->__toString())
                        ->setCellValue('F'.$numRow, $listado->getComentarios())
                    ;

                    if($listado->getTelefono()->getEsListaNegra() == 1){
                        $sheet
                            ->setCellValue('G'.$numRow,"EN LISTA NEGRA");
                    }else{
                        $sheet
                            ->setCellValue('G'.$numRow,"");
                    } 
                        
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

}
