<?php

namespace MDR\ReportesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\ComentariosPedido;
use MDR\PuntoVentaBundle\Form\ComentariosPedidoType;

/**
 * ComentariosPedido controller.
 *
 * @Route("/comentariospedido")
 */
class ComentariosPedidoController extends Controller
{
    /**
     * @Route("/", name="comentariospedido")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
	  $searchForm = $this->createSearchForm();
	  
	  $entities = [];
        if($request->isMethod('POST'))
        {
        $searchForm->submit($request);	
        $em = $this->getDoctrine()->getManager();
		$fechaInicio = $searchForm->get('fechaInicio')->getData();
        $fechaFin = $searchForm->get('fechaFin')->getData();
        $entities = $em->getRepository('MDRPuntoVentaBundle:ComentariosPedido')->findAllCompleteByFechas($fechaInicio, $fechaFin);
		
			 if(count($entities)<=0){
					$this->get('session')->getFlashBag()->add(
						'error',  $this->get('translator')->trans('busquedaSinResultados')
					);
				}
		
		 if($searchForm->get('ExcelDetalles')->isClicked())
            {
                return $this->createExcelDetalles($entities);
            }
		}
        return array(
            'entities' => $entities,
			'searchForm'    => $searchForm->createView(),
        );
    }

public function createSearchForm()
    {
        $fechaInicio = new \DateTime();
        $fechaInicio->sub(new \DateInterval('P2D'));
        $fechaFin = new \DateTime();

        $form = $this->createFormBuilder()
        ->add('fechaInicio', 'date', array(
            'label' => 'Inicio',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => $fechaInicio,                    
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('fechaFin', 'date', array(
            'label' => 'Fin',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => $fechaFin,
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('Buscar', 'submit')
        ->add('ExcelDetalles', 'submit', array('label' => 'Generar Excel'))
        
        ->getForm();

        return $form;
    }


    function createExcelDetalles($comentarios, $titulo = 'Comentarios'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Fecha")
				->setCellValue('B1', "Tipo de LLamada")
                ->setCellValue('C1', "SubTipo de LLamada")
                ->setCellValue('D1', "Pedido")
				->setCellValue('E1', "MTotal")
				->setCellValue('F1', "Capturado Por (usuario)")
                ->setCellValue('G1', "Capturado Por (nombre)")
                ->setCellValue('H1', "Usuario (usuario)")
                ->setCellValue('I1', "Usuario (nombre)")
                ->setCellValue('J1', "Comentario")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($comentarios as $comentario) {
					        
                    $sheet
                        ->setCellValue('A'.$numRow, $comentario->getFecha()->format('d/m/Y H:i:s'))
						->setCellValue('B'.$numRow, $comentario->getTipoLlamada()->getTipoLlamada()->getDescripcion())
						->setCellValue('C'.$numRow, $comentario->getTipoLlamada()->getDescripcionSubTipo())
						->setCellValue('D'.$numRow, $comentario->getPedido()->__toString())
						->setCellValue('E'.$numRow, $comentario->getPedido()->getTotalPago())
                        ->setCellValue('F'.$numRow, $comentario->getPedido()->getUsuario()->getUClave()) 
						->setCellValue('G'.$numRow, $comentario->getPedido()->getUsuario()->getUNombre().' '.$comentario->getPedido()->getUsuario()->getUApPaterno().' '.$comentario->getPedido()->getUsuario()->getUApMaterno()) 
                        ->setCellValue('H'.$numRow, $comentario->getUsuario()->getUClave())
                        ->setCellValue('I'.$numRow, $comentario->getUsuario()->getUNombre().' '.$comentario->getUsuario()->getUApPaterno().' '.$comentario->getUsuario()->getUApMaterno()) 
                        ->setCellValue('J'.$numRow, $comentario->getComentario())
                    ;
					$numRow ++;	
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}
}
