<?php

namespace MDR\ReportesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Ventas controller.
 *
 * @Route("/ventas")
 */
class VentasController extends Controller
{
    /**
     * @Route("/", name="reportes_ventas")
     * @method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $searchForm = $this->createSearchForm();

        $entities = [];
        if($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $em = $this->getDoctrine()->getManager();
            $fechaInicio = $searchForm->get('fechaInicio')->getData();
            $fechaFin = $searchForm->get('fechaFin')->getData();
            $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findAllCompleteByFechas($fechaInicio, $fechaFin);
            if($searchForm->get('ExcelDetalles')->isClicked())
            {
                return $this->createExcelDetalles($entities);
            }
           if($searchForm->get('ExcelResumen')->isClicked())
            {
                return $this->createExcelResumen($entities);
            }
			if($searchForm->get('ExcelPedidosClientes')->isClicked())
            {
                return $this->createExcelPedidosClientes($entities);
            }
            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'entities'      => $entities,
            'searchForm'    => $searchForm->createView(),
        );
    }

    /**
     * @Route("/efectivas", name="reportes_ventas_efectivas")
     * @method({"GET","POST"})
     * @Template("MDRReportesBundle:Ventas:index.html.twig")
     */
    public function efectivasAction(Request $request)
    {
        $searchForm = $this->createSearchForm();

        $entities = [];
        if($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $em = $this->getDoctrine()->getManager();
            $fechaInicio = $searchForm->get('fechaInicio')->getData();
            $fechaFin = $searchForm->get('fechaFin')->getData();
            $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByFechaCobradoComplete($fechaInicio, $fechaFin);
            if($searchForm->get('ExcelDetalles')->isClicked())
            {
                return $this->createExcelDetalles($entities);
            }
            if($searchForm->get('ExcelResumen')->isClicked())
            {
                return $this->createExcelResumen($entities);
            }
			if($searchForm->get('ExcelPedidosClientes')->isClicked())
            {
                return $this->createExcelPedidosClientes($entities);
            }
            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'entities'      => $entities,
            'searchForm'    => $searchForm->createView(),
        );
    }

    public function createSearchForm()
    {
        $fechaInicio = new \DateTime();
        $days = 2;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));

        $form = $this->createFormBuilder()
        ->add('fechaInicio', 'date', array(
            'label' => 'Inicio',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => $fechaInicio,
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('fechaFin', 'date', array(
            'label' => 'Fin',
            "read_only" => true,
            'widget' => 'single_text',
            'format' => 'dd/MM/yyyy',
            'data' => new \DateTime(),
            'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
        ))
        ->add('Buscar', 'submit')
        ->add('ExcelDetalles', 'submit', array('label' => 'Excel Detalles'))
        ->add('ExcelResumen', 'submit', array('label' => 'Excel Resumen'))
		->add('ExcelPedidosClientes', 'submit', array('label' => 'Excel Pedidos - Clientes'))
        
        ->getForm();

        return $form;
    }

    function createExcelDetalles($pedidos, $titulo = 'ventas-detalles'){
        set_time_limit(240);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "No.")
                ->setCellValue('B1', "Usuario")
                ->setCellValue('C1', "PedidoSAP")
                ->setCellValue('D1', "DID")
                ->setCellValue('E1', "Canal")
                ->setCellValue('F1', "Oficina")
                ->setCellValue('G1', "Tipo de Pago")
                ->setCellValue('H1', "Fecha")
                ->setCellValue('I1', "Cliente")
                ->setCellValue('J1', "Estatus")
                ->setCellValue('K1', "Fecha Cobro")
                ->setCellValue('L1', "Total")
                ->setCellValue('M1', "Cantidad")
                ->setCellValue('N1', "Producto")
                ->setCellValue('O1', "PU")
                ->setCellValue('P1', "Sub Total")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($pedidos as $pedido) 
        {
            if(count($pedido->getProductos()) > 0)
            {
                $fechaCobro = '';
                foreach ($pedido->getCobros() as $cobro) {
                    if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
                    {
                        $fechaCobro = $cobro->getFechaCobro()->format('Y/m/d H:i:s');
                    }
                }
                foreach ($pedido->getProductos() as $detalle) {
                    $sheet
                        ->setCellValue('A'.$numRow, $pedido->getIdPedido())
                        ->setCellValue('B'.$numRow, $pedido->getUsuario()->getUClave())
                        ->setCellValue('C'.$numRow, $pedido->getPedidoSAP())
                        ->setCellValue('D'.$numRow, $pedido->getDnis())
                        ->setCellValue('E'.$numRow, $detalle->getProductoPrecio()->getCanalDistribucionDesc())
                        ->setCellValue('F'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
                        ->setCellValue('G'.$numRow, $pedido->getPago()->getDescripcion())
                        ->setCellValue('H'.$numRow, $pedido->getFecha()->format('Y/m/d H:i:s'))
                        ->setCellValue('I'.$numRow, $pedido->getCliente()->__toString())
                        ->setCellValue('J'.$numRow, $pedido->getEstatus()->getDescripcion())
                        ->setCellValue('K'.$numRow, $fechaCobro)
                        ->setCellValue('L'.$numRow, $pedido->getTotalPago())
                        ->setCellValue('M'.$numRow, $detalle->getCantidad())
                        ->setCellValue('N'.$numRow, $detalle->getProductoPrecio()->getProducto()->getDescripcion())
                        ->setCellValue('O'.$numRow, $detalle->getProductoPrecio()->getPrecio())
                        ->setCellValue('P'.$numRow, $detalle->getCantidad() * $detalle->getProductoPrecio()->getPrecio())
                    ;
                    $numRow ++;
                }
                $detalle = $pedido->getProductos()[0];
                foreach ($pedido->getDescuentos() as $descuento) {
                    $sheet
                        ->setCellValue('A'.$numRow, $pedido->getIdPedido())
                        ->setCellValue('B'.$numRow, $pedido->getUsuario()->getUClave())
                        ->setCellValue('C'.$numRow, $pedido->getPedidoSAP())
                        ->setCellValue('D'.$numRow, $pedido->getDnis())
                        ->setCellValue('E'.$numRow, $detalle->getProductoPrecio()->getCanalDistribucionDesc())
                        ->setCellValue('F'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
                        ->setCellValue('G'.$numRow, $pedido->getPago()->getDescripcion())
                        ->setCellValue('H'.$numRow, $pedido->getFecha()->format('Y/m/d H:i:s'))
                        ->setCellValue('I'.$numRow, $pedido->getCliente()->__toString())
                        ->setCellValue('J'.$numRow, $pedido->getEstatus()->getDescripcion())
                        ->setCellValue('K'.$numRow, $fechaCobro)
                        ->setCellValue('L'.$numRow, $pedido->getTotalPago())
                        ->setCellValue('M'.$numRow, 1)
                        ->setCellValue('N'.$numRow, 'Descuento '.$descuento->getClave())
                        ->setCellValue('O'.$numRow, $descuento->getImporte() * -1)
                        ->setCellValue('P'.$numRow, $descuento->getImporte() * -1)
                    ;
                    $numRow ++;
                }
                $sheet
                    ->setCellValue('A'.$numRow, $pedido->getIdPedido())
                    ->setCellValue('B'.$numRow, $pedido->getUsuario()->getUClave())
                    ->setCellValue('C'.$numRow, $pedido->getPedidoSAP())
                    ->setCellValue('D'.$numRow, $pedido->getDnis())
                    ->setCellValue('E'.$numRow, $detalle->getProductoPrecio()->getCanalDistribucionDesc())
                    ->setCellValue('F'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
                    ->setCellValue('G'.$numRow, $pedido->getPago()->getDescripcion())
                    ->setCellValue('H'.$numRow, $pedido->getFecha()->format('Y/m/d H:i:s'))
                    ->setCellValue('I'.$numRow, $pedido->getCliente()->__toString())
                    ->setCellValue('J'.$numRow, $pedido->getEstatus()->getDescripcion())
                    ->setCellValue('K'.$numRow, $fechaCobro)
                    ->setCellValue('L'.$numRow, $pedido->getTotalPago())
                    ->setCellValue('M'.$numRow, '1')
                    ->setCellValue('N'.$numRow, 'Gastos Envío')
                    ->setCellValue('O'.$numRow, $pedido->getGastosEnvio()->getPrecio())
                    ->setCellValue('P'.$numRow, $pedido->getGastosEnvio()->getPrecio())
                ;
                $numRow ++;
            }
        }
    
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    }

    function createExcelResumen($pedidos, $titulo = 'ventas-resumen'){
        set_time_limit(240);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "No.")
                ->setCellValue('B1', "Usuario")
                ->setCellValue('C1', "PedidoSAP")
                ->setCellValue('D1', "DID")
                ->setCellValue('E1', "Canal")
                ->setCellValue('F1', "Oficina")
                ->setCellValue('G1', "Tipo de Pago")
                ->setCellValue('H1', "Fecha")
                ->setCellValue('I1', "Cliente")
                ->setCellValue('J1', "Tel Pedido")
                ->setCellValue('K1', "Tel Casa")
                ->setCellValue('L1', "Tel Oficina")
                ->setCellValue('M1', "Tel Móvil")
                ->setCellValue('N1', "Estatus")
                ->setCellValue('O1', "Fecha Cobro")
                ->setCellValue('P1', "Total")
                ->setCellValue('Q1', "Gastos Envío")
                ->setCellValue('R1', "Descuento")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($pedidos as $pedido) 
        {
            $fechaCobro = '';
            foreach ($pedido->getCobros() as $cobro) {
                if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
                {
                    $fechaCobro = $cobro->getFechaCobro()->format('Y/m/d H:i:s');
                }
            }
            $sheet
                ->setCellValue('A'.$numRow, $pedido->getIdPedido())
                ->setCellValue('B'.$numRow, $pedido->getUsuario()->getUClave())
                ->setCellValue('C'.$numRow, $pedido->getPedidoSAP())
                ->setCellValue('D'.$numRow, $pedido->getDnis())
                ->setCellValue('G'.$numRow, $pedido->getPago()->getDescripcion())
                ->setCellValue('H'.$numRow, $pedido->getFecha()->format('Y/m/d H:i:s'))
                ->setCellValue('I'.$numRow, $pedido->getCliente()->__toString())
                ->setCellValue('J'.$numRow, $pedido->getTelefono())
                ->setCellValue('K'.$numRow, $pedido->getDireccion()->getTelCasa())
                ->setCellValue('L'.$numRow, $pedido->getDireccion()->getTelOficina())
                ->setCellValue('M'.$numRow, $pedido->getDireccion()->getTelCel())
                ->setCellValue('N'.$numRow, $pedido->getEstatus()->getDescripcion())
                ->setCellValue('O'.$numRow, $fechaCobro)
                ->setCellValue('P'.$numRow, $pedido->getTotalPago())
                ->setCellValue('Q'.$numRow, $pedido->getGastosEnvio()->getPrecio())
                ->setCellValue('R'.$numRow, $pedido->calculaDescuento())
            ;
            if(count($pedido->getProductos()) > 0)
            {
                $detalle = $pedido->getProductos()[0];
                $sheet
                    ->setCellValue('E'.$numRow, $detalle->getProductoPrecio()->getCanalDistribucionDesc())
                    ->setCellValue('F'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
                    ;
            }
            $numRow ++;
        }
    
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    }
	
	function createExcelPedidosClientes($pedidos, $titulo = 'Pedidos-Clientes'){
        set_time_limit(240);
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->setActiveSheetIndex(0)
				->setCellValue('A1', "DID")
				->setCellValue('B1', "Tpago")
                ->setCellValue('C1', "Ordvta")
                ->setCellValue('D1', "Origvta")
                ->setCellValue('E1', "Estatus")
                ->setCellValue('F1', "Fecha")
				->setCellValue('G1', "Cliente")
				->setCellValue('H1', "Sexo")
				->setCellValue('I1', "Tel casa")
				->setCellValue('J1', "Tel oficina")
				->setCellValue('K1', "Celular")
				->setCellValue('L1', "Calle")
				->setCellValue('M1', "Cp")
				->setCellValue('N1', "Colonia")
				->setCellValue('O1', "Municipio")
				->setCellValue('P1', "Estado")
				->setCellValue('Q1', "Entrecalles")
				->setCellValue('R1', "Indicaciones")
				->setCellValue('S1', "Cantidad")
				->setCellValue('T1', "Clave")
				->setCellValue('U1', "Producto")
				->setCellValue('V1', "Precio")
				->setCellValue('W1', "Gastos de Envio")
				->setCellValue('X1', "Usuario")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($pedidos as $pedido) 
        {
            $fechaCobro = '';
            foreach ($pedido->getCobros() as $cobro) {
                if($cobro->getRespuesta()->getIdTipoRespuesta() == 1)
                {
                    $fechaCobro = $cobro->getFechaCobro()->format('Y/m/d H:i:s');
                }
            }
			foreach ($pedido->getProductos() as $detalle) {
            $sheet
                ->setCellValue('A'.$numRow, $pedido->getDnis())
                ->setCellValue('B'.$numRow, $pedido->getPago()->getDescripcion())
                ->setCellValue('C'.$numRow, $pedido->getIdPedido())
                ->setCellValue('E'.$numRow, $pedido->getEstatus()->getDescripcion())
                ->setCellValue('F'.$numRow, $pedido->getFecha()->format('Y/m/d H:i:s'))
                ->setCellValue('G'.$numRow, $pedido->getCliente()->__toString())
                ->setCellValue('H'.$numRow, $pedido->getCliente()->getSexoString())
                ->setCellValue('I'.$numRow, $pedido->getDireccion()->getTelCasa())
                ->setCellValue('J'.$numRow, $pedido->getDireccion()->getTelOficina())
                ->setCellValue('K'.$numRow, $pedido->getDireccion()->getTelCel())
                ->setCellValue('L'.$numRow, $pedido->getDireccion()->getCalle())
				->setCellValue('M'.$numRow, $pedido->getDireccion()->getCp())
				->setCellValue('N'.$numRow, $pedido->getDireccion()->getColonia())
				->setCellValue('O'.$numRow, $pedido->getDireccion()->getMunicipio())
				->setCellValue('P'.$numRow, $pedido->getDireccion()->getEstado())
				->setCellValue('Q'.$numRow, $pedido->getDireccion()->getEntreCalles())
				->setCellValue('R'.$numRow, $pedido->getDireccion()->getReferencia())
				->setCellValue('S'.$numRow, $detalle->getCantidad())
				->setCellValue('T'.$numRow, $detalle->getProductoPrecio()->getMaterialPrecio())
				->setCellValue('U'.$numRow, $detalle->getProductoPrecio()->getProducto()->getDescripcion())
				->setCellValue('V'.$numRow, $detalle->getProductoPrecio()->getPrecio())
				->setCellValue('W'.$numRow, $pedido->getGastosEnvio()->getPrecio())
				->setCellValue('X'.$numRow, $pedido->getUsuario()->getUClave())
            ;
			}
            if(count($pedido->getProductos()) > 0)
            {
                $detalle = $pedido->getProductos()[0];
                $sheet
                  /*  ->setCellValue('F'.$numRow, $detalle->getProductoPrecio()->getCanalDistribucionDesc())*/
                    ->setCellValue('D'.$numRow, $detalle->getProductoPrecio()->getOficinaVenta()->getDescripcion())
                    ;
            }
            $numRow ++;
        }
    
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}

    /**
     * @Route("/resumen/{id}/{tipo}", name="reportes_ventas_resumen", defaults={"tipo" = "html"})
     * @method({"GET"})
     * @Template()
     */
    public function resumenAction(Request $request, $id, $tipo)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        
        if (!$entity) {
            $this->get('session')->getFlashBag()->add(
                'error', 'No se encontro el pedido con id: '.$id
            );
            return $this->render('::base.html.twig');
        }

        if($tipo == 'pdf')
        {
            $htmlVenta = $this->renderView('MDRReportesBundle:ventas:resumen.html.twig', array('entity' => $entity));
            
            $response = new Response($this->get('knp_snappy.pdf')->getOutputFromHtml($htmlVenta), 200, array(
                'Content-Type' => 'application/pdf'
                    )
            );
            $response->headers->set('Content-Disposition', 'attachment; filename="venta'.$id.'.pdf"');

            return $response;
        }
        else
        {
            return array(
                'entity'    => $entity
                );
        }
    }
}
