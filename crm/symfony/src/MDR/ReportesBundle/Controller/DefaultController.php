<?php

namespace MDR\ReportesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MDRReportesBundle:Default:index.html.twig', array('name' => $name));
    }
}
