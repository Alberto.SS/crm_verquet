<?php

namespace MDR\CobranzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;


/**
 * Cobranza controller.
 *
 * @Route("/cobranza")
 */

class CobranzaController extends Controller
{


    /**
     * Pagina Principal
     *
     * @Route("/", name="cobraza")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findAll();

        return array(
            'entities' => $entities,
        );
    }


    /**
     * Pagina de Posfechados
     *
     * @Route("/posfechados", name="posfechados")
     * @Method({"GET","POST"})
     * @Template("MDRCobranzaBundle:Cobranza:posfechados.html.twig")
     */
    public function PFindexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchForm1();
        $entities = array();
        $usuario = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $terminal = $searchForm->get('terminal')->getData();
                $OfiVentas = $searchForm->get('OfiVentas')->getData();
               
                $usuario = $this->get('security.context')->getToken()->getUser();
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByFechasAndTerminal($fechaInicio, $fechaFin, $terminal, $OfiVentas);
               
            }

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
       
       
        return $this->render('MDRCobranzaBundle:Cobranza:posfechados.html.twig', array(
            'entities' => $entities,
            'usuario' => $usuario,
            'form_pf' => $searchForm->createView()
        ));
           
    }
    
    /**
     * Pagina de Cierre
     *
     * @Route("/cierre", name="cierre")
     * @Method({"GET","POST"})
     * @Template("MDRCobranzaBundle:Cobranza:cierre.html.twig")
     */
    public function CRindexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchForm2();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $estatus = $searchForm->get('estatus')->getData();
                $terminal = $searchForm->get('terminal')->getData();
                
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByFechasAndEstatus($fechaInicio, $fechaFin, $estatus, $terminal);
               
            }

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }


            return $this->render('MDRCobranzaBundle:Cobranza:cierre.html.twig', array(
                'cierre' => $entities,
                'form_cr' => $searchForm->createView()
            ));

       
    }
        


    /**
     * Pagina de Cancelados
     *
     * @Route("/cancelados", name="cancelados")
     * @Method({"GET","POST"})
     * @Template("MDRCobranzaBundle:Cobranza:cancelados.html.twig")
     */
    public function CLindexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchForm3();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $terminal = $searchForm->get('terminal')->getData();               
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByPedidosCancelados($terminal);              

                

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
                            
            }


        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRCobranzaBundle:Cobranza:cancelados.html.twig', array(
            'pcancelados' => $entities,
            'form_cc' => $searchForm->createView()
        ));

    }

    /**
     * Pagina de Pauta Seca
     *
     * @Route("/pauta_seca", name="pauta_seca")
     * @Method({"GET","POST"})
     * @Template("MDRCobranzaBundle:Cobranza:pauta.html.twig")
     */
    public function pautaSecaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchFormPauta();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                    
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByPedidosPautaSeca($fechaInicio,$fechaFin);              

                

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
                            
            }


        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRCobranzaBundle:Cobranza:pauta.html.twig', array(
            'pauta' => $entities,
            'form_ps' => $searchForm->createView()
        ));

    }


    /**
     * Pagina de Bitacora
     *
     * @Route("/bitacora", name="bitacora")
     * @Method({"GET","POST"})
     * @Template("MDRCobranzaBundle:Cobranza:bitacora.html.twig")
     */
    public function BTindexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchForm4();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $pedido = $searchForm->get('idPedido')->getData();
                $area = "Cobranza";
                $entities = $em->getRepository('MDRPuntoVentaBundle:Bitacora')->findByOperaciones($fechaInicio, $fechaFin, $area, $pedido);
                    if($searchForm->get('Excel')->isClicked())
                    {
                        return $this->createExcelBit($entities);
                    }               
            }            

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRCobranzaBundle:Cobranza:bitacora.html.twig', array(
            'bitacora' => $entities,
            'form_bt' => $searchForm->createView()
        ));
    }       

function createExcelBit($listados, $titulo = 'BitacoraCobranza'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Fecha")
                ->setCellValue('B1', "Usuario")
                ->setCellValue('C1', "Area")
                ->setCellValue('D1', "Pedido")
                ->setCellValue('E1', "Descripcion de la Operacion")
                ->setCellValue('F1', "Estatus de la Operacion")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {        
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getFecha()->format('d/m/Y H:i:s'))
                        ->setCellValue('B'.$numRow, $listado->getPedido()->getUsuario()->__toString())
                        ->setCellValue('C'.$numRow, $listado->getAreaOperacion())
                        ->setCellValue('D'.$numRow, $listado->getPedido()->__toString())
                        ->setCellValue('E'.$numRow, str_replace( array("<b>", "</b>"),' ',$listado->getDescripcionOperacion()))
                        ->setCellValue('F'.$numRow, $listado->getEstatusOperacion())
                    ;
                    $numRow ++; 
                    
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }


     /**
     * Edits an existing Pedidos Pago entity.
     *
     * @Route("/{id}/editDireccionTarjeta", name="edit_direccion_tarjeta")
     * @Method({"GET", "POST"})
     * @Template("MDRCobranzaBundle:Cobranza:edit.html.twig")
     */
    public function editDireccionTarjetaAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $direccion = array();
        $entities = $em->getRepository('MDRPuntoVentaBundle:PagoTarjeta')->findByPedido($id);
        $direccionPedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        $direccion1 = $em ->getRepository('MDRPuntoVentaBundle:Direcciones')->find($direccionPedido->getDireccion());        
        $direccion2 = $em ->getRepository('MDRPuntoVentaBundle:Direcciones')->findByCliente($direccionPedido->getCliente());        
        $direccion['direcciones'] = $direccion2;
        $direccion['direccion'] = $direccion1;
        $direccion['OriPedido'] = $direccionPedido;
        
        $entity = null;
        if(count($entities) > 0)
        {
            $entity = $entities[0];
        }
        if(strlen($entity->getTitular()) == 0)
        {
            $entity->setTitular($entity->getPedido()->getCliente()->getNombreCompleto());
        }
        if(!$entity)
        {
            $this->get('session')->getFlashBag()->add(
                'error', 'No se encontro información de pago con Tarjeta para el pedido: '.$id
            );
            return $this->render('MDRCobranzaBundle:Cobranza:index.html.twig');
        }
        if($entity->getPedido()->getEstatus()->getIdEstatus() == 17 || $entity->getPedido()->getEstatus()->getIdEstatus() == 16 || $entity->getPedido()->getEstatus()->getIdEstatus() == 1  )
        {
            
        }else{
            $this->get('session')->getFlashBag()->add(
                'error', 'El pedido no tiene el estatus correcto para modificar datos de cobro'
            );
            return $this->render('MDRCobranzaBundle:Cobranza:index.html.twig');
        }
         
            

        return array(            
            'pagoTarjeta' => $entity,
            'direccion' => $direccion,
            );

    }    

    /**
    * Crear un formulario para buscar pedidos posfechados o no cobrados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm1()
    {
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('terminal', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Terminales',
                    'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                            ->where("e.idTerminal IN(:terminalesIds)")
                            ->setParameter('terminalesIds', [1,2,3,4,5]);
                            },
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'pf_terminal','class' => "form-control"),
                ))
                ->add('OfiVentas', 'entity', array(
                    'label' => 'Oficina de Ventas',
                    'class' => 'MDRPuntoVentaBundle:OficinasVentas',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'pf_ofiVtas','class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->setAction($this->generateUrl('posfechados'))
                ->setMethod('POST')

                ->getForm();

        return $form;
    }


    /**
    * Crear un formulario para buscar pedidos cobrados, en cierre y conciliacion.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm2()
    {
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(                    
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('estatus', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Estatus',
                    'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                            ->where("e.idEstatus IN(:estatusIds)")
                            ->setParameter('estatusIds', [2,3,4]);
                            },
                    'required' => true,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'cc_status','class' => "form-control"),
                ))
                ->add('terminal', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Terminales',
                    'required' => true,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'cc_terminal','class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->setAction($this->generateUrl('cierre'))
                ->setMethod('POST')

                ->getForm();

        return $form;
    }

    /**
    * Crear un formulario para buscar pedidos cobrados el dia en curso para poder cancelarlos y mostrar tambien los pedidos 
    * que estan cancelados y la causa de su cancelacion.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm3()
    {
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
               ->add('terminal', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:Terminales',
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('id' => 'cc_terminal','class' => "form-control"),
                ))
                
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->setAction($this->generateUrl('cancelados'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
    * Crear un formulario para buscar en bitacora las operaciones realizadas por un usuario.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm4()
    {
             
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    "required" => false,
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    "required" => false,
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('idPedido', null, array(
                    'label' => '# Pedido',
                    "read_only" => false,
                    "required" => false,
                    'attr' => array('class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->add('Excel', 'submit', array(
                      'label' => 'Excel',
                      'attr' => array('class' => 'btn btn-success'),
                      ))    
                ->setAction($this->generateUrl('bitacora'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
    * Crear un formulario para buscar en pedidos en estatus pauta seca.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormPauta()
    {
             
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    "required" => false,
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    "required" => false,
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    )) 
                ->setAction($this->generateUrl('pauta_seca'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

}


