<?php

namespace MDR\CobranzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MDRCobranzaBundle:Default:index.html.twig', array('name' => $name));
    }
}
