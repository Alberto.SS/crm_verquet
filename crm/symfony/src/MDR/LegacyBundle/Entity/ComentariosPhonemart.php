<?php

namespace MDR\LegacyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ComentariosPhonemart
 */
class ComentariosPhonemart implements JsonSerializable
{
    /**
     * @var string
     */
    private $tipo;

    /**
     * @var string
     */
    private $subTipo;

    /**
     * @var string
     */
    private $comentario;

    /**
     * @var string
     */
    private $usuario;

    /**
     * @var \DateTime
     */
    private $fAlta;

    /**
     * @var integer
     */
    private $idComentario;

    /**
     * @var \MDR\LegacyBundle\Entity\Pedidosphonemart
     */
    private $pedido;


    /**
     * Set tipo
     *
     * @param string $tipo
     * @return ComentariosPhonemart
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set subTipo
     *
     * @param string $subTipo
     * @return ComentariosPhonemart
     */
    public function setSubTipo($subTipo)
    {
        $this->subTipo = $subTipo;

        return $this;
    }

    /**
     * Get subTipo
     *
     * @return string 
     */
    public function getSubTipo()
    {
        return $this->subTipo;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ComentariosPhonemart
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return ComentariosPhonemart
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set fAlta
     *
     * @param \DateTime $fAlta
     * @return ComentariosPhonemart
     */
    public function setFAlta($fAlta)
    {
        $this->fAlta = $fAlta;

        return $this;
    }

    /**
     * Get fAlta
     *
     * @return \DateTime 
     */
    public function getFAlta()
    {
        return $this->fAlta;
    }

    /**
     * Get idComentario
     *
     * @return integer 
     */
    public function getIdComentario()
    {
        return $this->idComentario;
    }

    /**
     * Set pedido
     *
     * @param \MDR\LegacyBundle\Entity\Pedidosphonemart $pedido
     * @return ComentariosPhonemart
     */
    public function setPedido(\MDR\LegacyBundle\Entity\Pedidosphonemart $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\LegacyBundle\Entity\Pedidosphonemart 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    public function jsonSerialize()
    {
        return array(
            'tipo'          => $this->tipo,
            'subTipo'       => $this->subTipo,
            'comentario'    => $this->comentario,
            'usuario'       => $this->usuario,
            'fAlta'         => $this->fAlta,
            'idComentario'  => $this->idComentario,                       
        );

    }
}
