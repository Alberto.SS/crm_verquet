<?php

namespace MDR\LegacyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * MensajeriaPrhonemart
 */
class MensajeriaPrhonemart implements JsonSerializable
{
    /**
     * @var string
     */
    private $mensajeria;

    /**
     * @var \DateTime
     */
    private $fSalida;

    /**
     * @var integer
     */
    private $idMensajeria;

    /**
     * @var \MDR\LegacyBundle\Entity\Pedidosphonemart
     */
    private $pedido;


    /**
     * Set mensajeria
     *
     * @param string $mensajeria
     * @return MensajeriaPrhonemart
     */
    public function setMensajeria($mensajeria)
    {
        $this->mensajeria = $mensajeria;

        return $this;
    }

    /**
     * Get mensajeria
     *
     * @return string 
     */
    public function getMensajeria()
    {
        return $this->mensajeria;
    }

    /**
     * Set fSalida
     *
     * @param \DateTime $fSalida
     * @return MensajeriaPrhonemart
     */
    public function setFSalida($fSalida)
    {
        $this->fSalida = $fSalida;

        return $this;
    }

    /**
     * Get fSalida
     *
     * @return \DateTime 
     */
    public function getFSalida()
    {
        return $this->fSalida;
    }

    /**
     * Get idMensajeria
     *
     * @return integer 
     */
    public function getIdMensajeria()
    {
        return $this->idMensajeria;
    }

    /**
     * Set pedido
     *
     * @param \MDR\LegacyBundle\Entity\Pedidosphonemart $pedido
     * @return MensajeriaPrhonemart
     */
    public function setPedido(\MDR\LegacyBundle\Entity\Pedidosphonemart $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\LegacyBundle\Entity\Pedidosphonemart 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    public function jsonSerialize()
    {
        return array(
            'mensajeria'    => $this->mensajeria,
            'fSalida'       => $this->fSalida,
            'idMensajeria'  => $this->idMensajeria,                       
        );

    }
}
