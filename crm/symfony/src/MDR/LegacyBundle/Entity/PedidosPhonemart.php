<?php

namespace MDR\LegacyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * PedidosPhonemart
 */
class PedidosPhonemart implements JsonSerializable
{
    /**
     * @var string
     */
    private $Pago;

    /**
     * @var string
     */
    private $did;

    /**
     * @var string
     */
    private $estatus;

    /**
     * @var string
     */
    private $origVta;

    /**
     * @var \DateTime
     */
    private $fAlta;

    /**
     * @var \DateTime
     */
    private $hAlta;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $sexo;

    /**
     * @var \DateTime
     */
    private $fNacim;

    /**
     * @var string
     */
    private $telCasa;

    /**
     * @var string
     */
    private $telOficina;

    /**
     * @var string
     */
    private $celular;

    /**
     * @var string
     */
    private $calle;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $municipio;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $entreCalles;

    /**
     * @var string
     */
    private $indicaciones;

    /**
     * @var string
     */
    private $numExt;

    /**
     * @var integer
     */
    private $cant1;

    /**
     * @var string
     */
    private $clav1;

    /**
     * @var string
     */
    private $prod1;

    /**
     * @var string
     */
    private $prec1;

    /**
     * @var integer
     */
    private $cant2;

    /**
     * @var string
     */
    private $clav2;

    /**
     * @var string
     */
    private $prod2;

    /**
     * @var string
     */
    private $prec2;

    /**
     * @var integer
     */
    private $cant3;

    /**
     * @var string
     */
    private $clav3;

    /**
     * @var string
     */
    private $prod3;

    /**
     * @var string
     */
    private $prec3;

    /**
     * @var integer
     */
    private $cant4;

    /**
     * @var string
     */
    private $clav4;

    /**
     * @var string
     */
    private $prod4;

    /**
     * @var string
     */
    private $prec4;

    /**
     * @var integer
     */
    private $cant5;

    /**
     * @var string
     */
    private $clav5;

    /**
     * @var string
     */
    private $prod5;

    /**
     * @var string
     */
    private $prec5;

    /**
     * @var integer
     */
    private $cant6;

    /**
     * @var string
     */
    private $clav6;

    /**
     * @var string
     */
    private $prod6;

    /**
     * @var string
     */
    private $prec6;

    /**
     * @var string
     */
    private $gEnvio;

    /**
     * @var string
     */
    private $total;

    /**
     * @var string
     */
    private $usuario;

    /**
     * @var integer
     */
    private $pedido;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mensajeria;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $comentarios;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cobros;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mensajeria = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comentarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cobros = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add mensajeria
     *
     * @param \MDR\LegacyBundle\Entity\MensajeriaPrhonemart $mensajeria
     * @return PedidosPhonemart
     */
    public function addMensajerium(\MDR\LegacyBundle\Entity\MensajeriaPrhonemart $mensajeria)
    {
        $this->mensajeria[] = $mensajeria;

        return $this;
    }

    /**
     * Remove mensajeria
     *
     * @param \MDR\LegacyBundle\Entity\MensajeriaPrhonemart $mensajeria
     */
    public function removeMensajerium(\MDR\LegacyBundle\Entity\MensajeriaPrhonemart $mensajeria)
    {
        $this->mensajeria->removeElement($mensajeria);
    }

    /**
     * Get mensajeria
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMensajeria()
    {
        return $this->mensajeria;
    }

    /**
     * Add comentarios
     *
     * @param \MDR\LegacyBundle\Entity\ComentariosPhonemart $comentarios
     * @return PedidosPhonemart
     */
    public function addComentario(\MDR\LegacyBundle\Entity\ComentariosPhonemart $comentarios)
    {
        $this->comentarios[] = $comentarios;

        return $this;
    }

    /**
     * Remove comentarios
     *
     * @param \MDR\LegacyBundle\Entity\ComentariosPhonemart $comentarios
     */
    public function removeComentario(\MDR\LegacyBundle\Entity\ComentariosPhonemart $comentarios)
    {
        $this->comentarios->removeElement($comentarios);
    }

    /**
     * Get comentarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Add cobros
     *
     * @param \MDR\LegacyBundle\Entity\CobrosPhonemart $cobros
     * @return PedidosPhonemart
     */
    public function addCobro(\MDR\LegacyBundle\Entity\CobrosPhonemart $cobros)
    {
        $this->cobros[] = $cobros;

        return $this;
    }

    /**
     * Remove cobros
     *
     * @param \MDR\LegacyBundle\Entity\CobrosPhonemart $cobros
     */
    public function removeCobro(\MDR\LegacyBundle\Entity\CobrosPhonemart $cobros)
    {
        $this->cobros->removeElement($cobros);
    }

    /**
     * Get cobros
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCobros()
    {
        return $this->cobros;
    }
    /**
     * Set Pago
     *
     * @param string $pago
     * @return PedidosPhonemart
     */
    public function setPago($pago)
    {
        $this->Pago = $pago;

        return $this;
    }

    /**
     * Get Pago
     *
     * @return string 
     */
    public function getPago()
    {
        return $this->Pago;
    }

    /**
     * Set did
     *
     * @param string $did
     * @return PedidosPhonemart
     */
    public function setDid($did)
    {
        $this->did = $did;

        return $this;
    }

    /**
     * Get did
     *
     * @return string 
     */
    public function getDid()
    {
        return $this->did;
    }

    /**
     * Set estatus
     *
     * @param string $estatus
     * @return PedidosPhonemart
     */
    public function setEstatus($estatus)
    {
        $this->estatus = $estatus;

        return $this;
    }

    /**
     * Get estatus
     *
     * @return string 
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * Set origVta
     *
     * @param string $origVta
     * @return PedidosPhonemart
     */
    public function setOrigVta($origVta)
    {
        $this->origVta = $origVta;

        return $this;
    }

    /**
     * Get origVta
     *
     * @return string 
     */
    public function getOrigVta()
    {
        return $this->origVta;
    }

    /**
     * Set fAlta
     *
     * @param \DateTime $fAlta
     * @return PedidosPhonemart
     */
    public function setFAlta($fAlta)
    {
        $this->fAlta = $fAlta;

        return $this;
    }

    /**
     * Get fAlta
     *
     * @return \DateTime 
     */
    public function getFAlta()
    {
        return $this->fAlta;
    }

    /**
     * Set hAlta
     *
     * @param \DateTime $hAlta
     * @return PedidosPhonemart
     */
    public function setHAlta($hAlta)
    {
        $this->hAlta = $hAlta;

        return $this;
    }

    /**
     * Get hAlta
     *
     * @return \DateTime 
     */
    public function getHAlta()
    {
        return $this->hAlta;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return PedidosPhonemart
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return PedidosPhonemart
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     * @return PedidosPhonemart
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Set fNacim
     *
     * @param \DateTime $fNacim
     * @return PedidosPhonemart
     */
    public function setFNacim($fNacim)
    {
        $this->fNacim = $fNacim;

        return $this;
    }

    /**
     * Get fNacim
     *
     * @return \DateTime 
     */
    public function getFNacim()
    {
        return $this->fNacim;
    }

    /**
     * Set telCasa
     *
     * @param string $telCasa
     * @return PedidosPhonemart
     */
    public function setTelCasa($telCasa)
    {
        $this->telCasa = $telCasa;

        return $this;
    }

    /**
     * Get telCasa
     *
     * @return string 
     */
    public function getTelCasa()
    {
        return $this->telCasa;
    }

    /**
     * Set telOficina
     *
     * @param string $telOficina
     * @return PedidosPhonemart
     */
    public function setTelOficina($telOficina)
    {
        $this->telOficina = $telOficina;

        return $this;
    }

    /**
     * Get telOficina
     *
     * @return string 
     */
    public function getTelOficina()
    {
        return $this->telOficina;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return PedidosPhonemart
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string 
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set calle
     *
     * @param string $calle
     * @return PedidosPhonemart
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return PedidosPhonemart
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     * @return PedidosPhonemart
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return PedidosPhonemart
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return PedidosPhonemart
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set entreCalles
     *
     * @param string $entreCalles
     * @return PedidosPhonemart
     */
    public function setEntreCalles($entreCalles)
    {
        $this->entreCalles = $entreCalles;

        return $this;
    }

    /**
     * Get entreCalles
     *
     * @return string 
     */
    public function getEntreCalles()
    {
        return $this->entreCalles;
    }

    /**
     * Set indicaciones
     *
     * @param string $indicaciones
     * @return PedidosPhonemart
     */
    public function setIndicaciones($indicaciones)
    {
        $this->indicaciones = $indicaciones;

        return $this;
    }

    /**
     * Get indicaciones
     *
     * @return string 
     */
    public function getIndicaciones()
    {
        return $this->indicaciones;
    }

    /**
     * Set numExt
     *
     * @param string $numExt
     * @return PedidosPhonemart
     */
    public function setNumExt($numExt)
    {
        $this->numExt = $numExt;

        return $this;
    }

    /**
     * Get numExt
     *
     * @return string 
     */
    public function getNumExt()
    {
        return $this->numExt;
    }

    /**
     * Set cant1
     *
     * @param integer $cant1
     * @return PedidosPhonemart
     */
    public function setCant1($cant1)
    {
        $this->cant1 = $cant1;

        return $this;
    }

    /**
     * Get cant1
     *
     * @return integer 
     */
    public function getCant1()
    {
        return $this->cant1;
    }

    /**
     * Set clav1
     *
     * @param string $clav1
     * @return PedidosPhonemart
     */
    public function setClav1($clav1)
    {
        $this->clav1 = $clav1;

        return $this;
    }

    /**
     * Get clav1
     *
     * @return string 
     */
    public function getClav1()
    {
        return $this->clav1;
    }

    /**
     * Set prod1
     *
     * @param string $prod1
     * @return PedidosPhonemart
     */
    public function setProd1($prod1)
    {
        $this->prod1 = $prod1;

        return $this;
    }

    /**
     * Get prod1
     *
     * @return string 
     */
    public function getProd1()
    {
        return $this->prod1;
    }

    /**
     * Set prec1
     *
     * @param string $prec1
     * @return PedidosPhonemart
     */
    public function setPrec1($prec1)
    {
        $this->prec1 = $prec1;

        return $this;
    }

    /**
     * Get prec1
     *
     * @return string 
     */
    public function getPrec1()
    {
        return $this->prec1;
    }

    /**
     * Set cant2
     *
     * @param integer $cant2
     * @return PedidosPhonemart
     */
    public function setCant2($cant2)
    {
        $this->cant2 = $cant2;

        return $this;
    }

    /**
     * Get cant2
     *
     * @return integer 
     */
    public function getCant2()
    {
        return $this->cant2;
    }

    /**
     * Set clav2
     *
     * @param string $clav2
     * @return PedidosPhonemart
     */
    public function setClav2($clav2)
    {
        $this->clav2 = $clav2;

        return $this;
    }

    /**
     * Get clav2
     *
     * @return string 
     */
    public function getClav2()
    {
        return $this->clav2;
    }

    /**
     * Set prod2
     *
     * @param string $prod2
     * @return PedidosPhonemart
     */
    public function setProd2($prod2)
    {
        $this->prod2 = $prod2;

        return $this;
    }

    /**
     * Get prod2
     *
     * @return string 
     */
    public function getProd2()
    {
        return $this->prod2;
    }

    /**
     * Set prec2
     *
     * @param string $prec2
     * @return PedidosPhonemart
     */
    public function setPrec2($prec2)
    {
        $this->prec2 = $prec2;

        return $this;
    }

    /**
     * Get prec2
     *
     * @return string 
     */
    public function getPrec2()
    {
        return $this->prec2;
    }

    /**
     * Set cant3
     *
     * @param integer $cant3
     * @return PedidosPhonemart
     */
    public function setCant3($cant3)
    {
        $this->cant3 = $cant3;

        return $this;
    }

    /**
     * Get cant3
     *
     * @return integer 
     */
    public function getCant3()
    {
        return $this->cant3;
    }

    /**
     * Set clav3
     *
     * @param string $clav3
     * @return PedidosPhonemart
     */
    public function setClav3($clav3)
    {
        $this->clav3 = $clav3;

        return $this;
    }

    /**
     * Get clav3
     *
     * @return string 
     */
    public function getClav3()
    {
        return $this->clav3;
    }

    /**
     * Set prod3
     *
     * @param string $prod3
     * @return PedidosPhonemart
     */
    public function setProd3($prod3)
    {
        $this->prod3 = $prod3;

        return $this;
    }

    /**
     * Get prod3
     *
     * @return string 
     */
    public function getProd3()
    {
        return $this->prod3;
    }

    /**
     * Set prec3
     *
     * @param string $prec3
     * @return PedidosPhonemart
     */
    public function setPrec3($prec3)
    {
        $this->prec3 = $prec3;

        return $this;
    }

    /**
     * Get prec3
     *
     * @return string 
     */
    public function getPrec3()
    {
        return $this->prec3;
    }

    /**
     * Set cant4
     *
     * @param integer $cant4
     * @return PedidosPhonemart
     */
    public function setCant4($cant4)
    {
        $this->cant4 = $cant4;

        return $this;
    }

    /**
     * Get cant4
     *
     * @return integer 
     */
    public function getCant4()
    {
        return $this->cant4;
    }

    /**
     * Set clav4
     *
     * @param string $clav4
     * @return PedidosPhonemart
     */
    public function setClav4($clav4)
    {
        $this->clav4 = $clav4;

        return $this;
    }

    /**
     * Get clav4
     *
     * @return string 
     */
    public function getClav4()
    {
        return $this->clav4;
    }

    /**
     * Set prod4
     *
     * @param string $prod4
     * @return PedidosPhonemart
     */
    public function setProd4($prod4)
    {
        $this->prod4 = $prod4;

        return $this;
    }

    /**
     * Get prod4
     *
     * @return string 
     */
    public function getProd4()
    {
        return $this->prod4;
    }

    /**
     * Set prec4
     *
     * @param string $prec4
     * @return PedidosPhonemart
     */
    public function setPrec4($prec4)
    {
        $this->prec4 = $prec4;

        return $this;
    }

    /**
     * Get prec4
     *
     * @return string 
     */
    public function getPrec4()
    {
        return $this->prec4;
    }

    /**
     * Set cant5
     *
     * @param integer $cant5
     * @return PedidosPhonemart
     */
    public function setCant5($cant5)
    {
        $this->cant5 = $cant5;

        return $this;
    }

    /**
     * Get cant5
     *
     * @return integer 
     */
    public function getCant5()
    {
        return $this->cant5;
    }

    /**
     * Set clav5
     *
     * @param string $clav5
     * @return PedidosPhonemart
     */
    public function setClav5($clav5)
    {
        $this->clav5 = $clav5;

        return $this;
    }

    /**
     * Get clav5
     *
     * @return string 
     */
    public function getClav5()
    {
        return $this->clav5;
    }

    /**
     * Set prod5
     *
     * @param string $prod5
     * @return PedidosPhonemart
     */
    public function setProd5($prod5)
    {
        $this->prod5 = $prod5;

        return $this;
    }

    /**
     * Get prod5
     *
     * @return string 
     */
    public function getProd5()
    {
        return $this->prod5;
    }

    /**
     * Set prec5
     *
     * @param string $prec5
     * @return PedidosPhonemart
     */
    public function setPrec5($prec5)
    {
        $this->prec5 = $prec5;

        return $this;
    }

    /**
     * Get prec5
     *
     * @return string 
     */
    public function getPrec5()
    {
        return $this->prec5;
    }

    /**
     * Set cant6
     *
     * @param integer $cant6
     * @return PedidosPhonemart
     */
    public function setCant6($cant6)
    {
        $this->cant6 = $cant6;

        return $this;
    }

    /**
     * Get cant6
     *
     * @return integer 
     */
    public function getCant6()
    {
        return $this->cant6;
    }

    /**
     * Set clav6
     *
     * @param string $clav6
     * @return PedidosPhonemart
     */
    public function setClav6($clav6)
    {
        $this->clav6 = $clav6;

        return $this;
    }

    /**
     * Get clav6
     *
     * @return string 
     */
    public function getClav6()
    {
        return $this->clav6;
    }

    /**
     * Set prod6
     *
     * @param string $prod6
     * @return PedidosPhonemart
     */
    public function setProd6($prod6)
    {
        $this->prod6 = $prod6;

        return $this;
    }

    /**
     * Get prod6
     *
     * @return string 
     */
    public function getProd6()
    {
        return $this->prod6;
    }

    /**
     * Set prec6
     *
     * @param string $prec6
     * @return PedidosPhonemart
     */
    public function setPrec6($prec6)
    {
        $this->prec6 = $prec6;

        return $this;
    }

    /**
     * Get prec6
     *
     * @return string 
     */
    public function getPrec6()
    {
        return $this->prec6;
    }

    /**
     * Set gEnvio
     *
     * @param string $gEnvio
     * @return PedidosPhonemart
     */
    public function setGEnvio($gEnvio)
    {
        $this->gEnvio = $gEnvio;

        return $this;
    }

    /**
     * Get gEnvio
     *
     * @return string 
     */
    public function getGEnvio()
    {
        return $this->gEnvio;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return PedidosPhonemart
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return PedidosPhonemart
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Get pedido
     *
     * @return integer 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    public function jsonSerialize()
    {
        return array(
            'Pago'          => $this->Pago,
            'did'           => $this->did,
            'estatus'       => $this->estatus,
            'origVta'       => $this->origVta,
            'fAlta'         => $this->fAlta,
            'hAlta'         => $this->hAlta,
            'nombre'        => $this->nombre,
            'mail'          => $this->mail,
            'sexo'          => $this->sexo,
            'fNacim'        => $this->fNacim,
            'telCasa'       => $this->telCasa,
            'telOficina'    => $this->telOficina,
            'celular'       => $this->celular,
            'calle'         => $this->calle,
            'cp'            => $this->cp,
            'colonia'       => $this->colonia,
            'municipio'     => $this->municipio,
            'estado'        => $this->estado,
            'entreCalles'   => $this->entreCalles,
            'indicaciones'  => $this->indicaciones,
            'numExt'        => $this->numExt,
            'cant1'         => $this->cant1,
            'clav1'         => $this->clav1,
            'prod1'         => $this->prod1,
            'prec1'         => $this->prec1,
            'cant2'         => $this->cant2,
            'clav2'         => $this->clav2,
            'prod2'         => $this->prod2,
            'prec2'         => $this->prec2,
            'cant3'         => $this->cant3,
            'clav3'         => $this->clav3,
            'prod3'         => $this->prod3,
            'prec3'         => $this->prec3,
            'cant4'         => $this->cant4,
            'clav4'         => $this->clav4,
            'prod4'         => $this->prod4,
            'prec4'         => $this->prec4,
            'cant5'         => $this->cant5,
            'clav5'         => $this->clav5,
            'prod5'         => $this->prod5,
            'prec5'         => $this->prec5,
            'cant6'         => $this->cant6,
            'clav6'         => $this->clav6,
            'prod6'         => $this->prod6,
            'prec6'         => $this->prec6,
            'gEnvio'        => $this->gEnvio,
            'total'         => $this->total,
            'usuario'       => $this->usuario,
            'pedido'        => $this->pedido,
            'mensajeria'    => $this->mensajeria->toArray(),
            'comentarios'   => $this->comentarios->toArray(),
            'cobros'        => $this->cobros->toArray(),
                       
        );

    }



}
