<?php

namespace MDR\LegacyBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * @Route("/sesionLegacy")
 */
class SessionController extends Controller
{
    //Route("/login")
    /**
     */
    public function loginAction(Request $request)
    {
    	$usuNumero = null;
    	$usuarioLegacy = null;
        $usuario = $this->get('security.context')->getToken()->getUser();
                
        if($usuario == 'anon.')
        {
            $usuario = null;
            if(!empty($_SESSION))
            {
                if(!empty($_SESSION['usuario']))
                {
                    $usuarioLegacy = get_object_vars($_SESSION['usuario']);
                    if(!empty($usuarioLegacy['usuNumero']))
                    {
                        $usuNumero = $usuarioLegacy['usuNumero'];
                    }
                }
            }
            if($usuNumero)
            {
                $emMDR = $this->getDoctrine()->getManager();
                $usuario = $emMDR->getRepository('MDRPuntoVentaBundle:Usuario')->find($usuNumero);
            }
        }

    	if($usuario)
    	{
    		if(count($usuario->getRoles()) > 0)
    		{
    			$algo = $usuario->getRoles()[0]->getRole();
    		}
    		$token = new UsernamePasswordToken(
                $usuario,
                null,
                'secured_area',
                $usuario->getRoles());
            // give it to the security context
            $this->container->get('security.context')->setToken($token);

            $user = $this->get('security.context')->getToken()->getUser();
            $url = $request->headers->get('referer');
            if(!$url)
            {
                $key = '_security.secured_area.target_path';
                if ($this->container->get('session')->has($key)) {
                    $url = $this->container->get('session')->get($key);
                    $this->container->get('session')->remove($key);
                }
            }

            if(!$url)
            {
            	return $this->redirect($this->container->get('router')->getContext()->getBaseUrl().'/../../MDRElastix/software.php');
            }
    		return $this->redirect($url);
    	}
    	else
    	{
            return $this->redirect($this->container->get('router')->getContext()->getBaseUrl().'/../../MDRElastix');
    	}
	}

}
