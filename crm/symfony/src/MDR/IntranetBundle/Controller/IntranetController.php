<?php

namespace MDR\IntranetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

use MDR\PuntoVentaBundle\Entity\IntranetNoticias;
use MDR\PuntoVentaBundle\Entity\IntranetMensajeInbox;
use MDR\PuntoVentaBundle\Entity\IntranetInbox;
use MDR\PuntoVentaBundle\Entity\IntranetTalleres;
use MDR\PuntoVentaBundle\Entity\IntranetCorreos;
use MDR\PuntoVentaBundle\Entity\Usuario;
use MDR\PuntoVentaBundle\Entity\IntranetRegistrosTalleres;

/**
 * Intranet controller.
 *
 * @Route("")
 */

class IntranetController extends Controller
{
    /**
     * Pagina Principal
     *
     * @Route("/", name="main_intranet")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $OpinionForm = $this->createOpinionForm();      
        $prevfc = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(1)->getValor();
        $prevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(2)->getValor();
        $showfc = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(6)->getValor();
        $showem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(7)->getValor();

        return array(            
            'form_com' => $OpinionForm->createView(),
            'prevfc' => $prevfc,
            'prevem' => $prevem,
            'showfc' => $showfc,
            'showem' => $showem,
        );
    }

    /**
    * Crear un formulario para buscar pedidos posfechados o no cobrados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createOpinionForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('area', 'entity', array(
                    'class' => 'MDRPuntoVentaBundle:IntranetCorreos',
                    'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                            ->where("e.Activo IN(:estatusIds)")
                            ->setParameter('estatusIds', [1]);
                            },
                    'required' => false,
                    'empty_value' => "-----------",
                    'attr' => array('class' => "form-control"),
                ))
                ->add('comentarios', 'textarea', array( 
                    'label' => 'Comentarios:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'placeholder' => 'Tú opinión es importante ayúdanos a mejorar')
                ))  
                ->add('Enviar', 'button' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'SendMessage()')   
                    ))
                ->setAction($this->generateUrl('main_intranet'))
                ->getForm();

        return $form;
    }

    /**
     * Crear nuevas noticias
     *
     * @Route("/newNoticia", name="new_noticia")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Noticias:index.html.twig")
     */
    public function newNoticiaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newNoticia = $this->newNoticiaForm();
        $respuesta['estatus'] = 0;

        if ($request->isMethod('POST'))
        {
            $newNoticia->submit($request);
            if ($newNoticia->isValid())
            {
                $imagen = $newNoticia->get('archivo')->getData();
                $tituloNoticia = $newNoticia->get('titulo')->getData();
                $noticia = $newNoticia->get('cuerpo')->getData();
                $uRL = $newNoticia->get('url')->getData();
                $fechaInicio = $newNoticia->get('fechaInicio')->getData();
                $fechaFin = $newNoticia->get('fechaFin')->getData();
                $tipoNoticia = $newNoticia->get('tiponoticia')->getData();                
                $usuario = $this->get('security.context')->getToken()->getUser();

                    try{

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES');
                        if($env == "Dev"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas');  

                            $rute .= "ImagenesIntranetqas/";
                        }else if($env == "Prd"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');
                            
                            $rute .= "ImagenesIntranetprd/";
                        }

                        $setNoticia = new IntranetNoticias();
                        $setNoticia -> setTituloNoticia($tituloNoticia);
                        $setNoticia -> setNoticia($noticia);
                        $setNoticia -> setURL($uRL); 
                        $setNoticia -> setFechaInicio($fechaInicio); 
                        $setNoticia -> setFechaFin($fechaFin); 
                        $setNoticia -> setTipoNoticia($tipoNoticia);
                        $setNoticia -> setUsuario($usuario); 

                        if($imagen){
                            $name = str_replace(" ", "", $imagen->getClientOriginalName());
                            $newNoticia->get('archivo')->getData()->move($upload_folder,$name);
                            $setNoticia -> setImagen($rute.$name); 
                        }else {                                                 
                            $setNoticia -> setImagen('/MDR/bundles/mdrcobranza/images/logoMDR.png');
                        }
                        $em->persist($setNoticia);
                        $em->flush();       

                        $this->get('session')->getFlashBag()->add(
                            'success',  "Se ha registrado con exito la noticia con número <b>".$setNoticia->getIdnoticia()."</b>"
                        );

                        return $this->redirect($this->generateUrl('new_noticia'));

                    }catch(\SoapFault $ex){
                        $this->get('logger')->error('ERROR: NEW NOTICIA');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar la noticia"
                        );
                    }catch(\Exception $ex){
                        $this->get('logger')->error('ERROR: NEW NOTICIA');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar la noticia"
                        );
                    }
            }
        }        
            

        return array(
            'form_nnews' => $newNoticia->createView()
        );
        
    } 

    /**
    * Crear un formulario para buscar pedidos posfechados o no cobrados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function newNoticiaForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('archivo', 'file', array(
                    'label'     => 'Imagen:',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateNotImgExtension(this.value,this.id)')
                ))
                ->add('titulo', null, array(
                    'label' => '* Titulo Noticia:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 40),
                ))
                ->add('cuerpo', 'textarea', array( 
                    'label' => '* Descripción de la Noticia:',
                    "required" => true,
                    'attr' => array('class' => "form-control", 'placeholder' => 'Información sobre la noticia','maxlength' => 650)
                ))
                ->add('url', null, array(
                    'label' => 'URL (en caso de usar información externa):',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add('fechaInicio', 'date', array(                    
                    'label' => '* Fecha de Inicio (cuando iniciara a verse la noticia)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => '* Fecha de Fin (cuando dejara de verse la noticia)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('tiponoticia', 'choice', array(                   
                    'label' => '* Relevancia de la Noticia:',
                    "required" => true,
                    'choices'  => array(
                        'small-box bg-aqua' => 'Noticia Relevante',
                        'small-box bg-green' => 'Noticia Informativa', 
                        'small-box bg-yellow' => 'Noticia Importante', 
                        'small-box bg-red' => 'Noticia de Sumamente Importante'
                    ),
                    'empty_value' => '----------------------------------------',
                    'attr' => array('class' => "form-control")
                ))
                ->add('Guardar', 'submit' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                    ))
                ->add('Eliminar', 'button' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick'=>'ClearImage()')   
                    ))
                ->add('Cancelar', 'button' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'clearForm()')   
                    ))
                ->add('Vista', 'button' , array(                    
                    'label' => 'Vista Previa',
                    'attr' => array('class' => 'btn btn-info', 'onclick' => 'showPrev()')   
                    ))
                ->setAction($this->generateUrl('new_noticia'))
                ->getForm();

        return $form;
    }

    /**
     * @Route("/noticiasJSON", name="noticias_json")
     * @Template()
     */
    public function noticiasJSONAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['noticias'] = '';

        try{    
            $Noticias = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->getNoticiasActivas();
            if(count($Noticias)>0){
                $respuesta['estatus'] = 1;
                $respuesta['noticias'] = $Noticias;
            }else{
                $respuesta['estatus'] = 2;
                $respuesta['noticias'] = "Aún no se han registrado noticias para el día de hoy.";    
            }
            
        }catch(\SoapFault $ex){
            $respuesta['noticias'] = "Aún no hay noticias registradas, intente más tarde.";
        }catch(\Exception $ex){
            $respuesta['noticias'] = "Aún no hay noticias registradas, intente más tarde.";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/getNoticia", name="get_info_noticia")
     * @Template()
     */
    public function getNoticiaAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('idNoticia');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['Info'] = '';

        try{    
            $Noticias = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->find($id);
            if(count($Noticias)>0){
                $respuesta['estatus'] = 1;
                $respuesta['Info'] = $Noticias;
            }else{
                $respuesta['Info'] = "La noticia puedo ser eliminada o registrada de forma erronea";
            }
            
        }catch(\SoapFault $ex){
            $respuesta['Info'] = "Error en carga de Noticia";
        }catch(\Exception $ex){
            $respuesta['Info'] = "Error en carga de Noticia";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Edición del empleado del mes
     *
     * @Route("/editEmpleadoMes", name="edit_emp_mes")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:PanelControl:empleadoMes.html.twig")
     */
    public function editEmpleadoMesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empleadoForm = $this->updateEmpleadoForm();
        $prevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(2)->getValor();
        $showem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(7)->getValor();

        if ($request->isMethod('POST'))
        {
            $empleadoForm->submit($request);
            if ($empleadoForm->isValid())
            {
                try{
                        $imagenprev = $empleadoForm->get('archivo1')->getData();
                        $imagenfull = $empleadoForm->get('archivo2')->getData();

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES');  

                        if($env == "Dev"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas');  

                            $rute .= "ImagenesIntranetqas/";
                        }else if($env == "Prd"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');
                           
                            $rute .= "ImagenesIntranetprd/";
                        }

                        if($imagenprev && $imagenfull){

                            $name1 = "Empprev".".".$imagenprev->guessExtension();
                            $empleadoForm->get('archivo1')->getData()->move($upload_folder,$name1);
                            $Updateprevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(2);
                            $Updateprevem -> setValor($rute.$name1);

                            $name2 = "EmpFull".".".$imagenfull->guessExtension();
                            $empleadoForm->get('archivo2')->getData()->move($upload_folder,$name2);
                            $Updateshowem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(7);
                            $Updateshowem -> setValor($rute.$name2);

                            $em->persist($Updateprevem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_emp_mes')); 

                        }else if($imagenprev){
                            $name = "Empprev".".".$imagenprev->guessExtension();
                            $empleadoForm->get('archivo1')->getData()->move($upload_folder,$name);
                            $Updateprevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(2);
                            $Updateprevem -> setValor($rute.$name);
                            $em->persist($Updateprevem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_emp_mes')); 

                        }else if($imagenfull){

                            $name = "EmpFull".".".$imagenfull->guessExtension();
                            $empleadoForm->get('archivo2')->getData()->move($upload_folder,$name);
                            $Updateshowem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(7);
                            $Updateshowem -> setValor($rute.$name);
                            $em->persist($Updateshowem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_emp_mes')); 

                        }else{
                            $this->get('session')->getFlashBag()->add(
                                'error',  "Aún no se han seleccionado archivos"
                            );
                            return $this->redirect($this->generateUrl('edit_emp_mes'));
                        }


                }catch(\SoapFault $ex){
                        $this->get('logger')->error('ERROR: UPDATE IMG EMPLEADO MES');
                        $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar la(s) imagen(es)"
                    );
                }catch(\Exception $ex){
                        $this->get('logger')->error('ERROR: UPDATE IMG EMPLEADO MES');
                        $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar la(s) imagen(es)"
                    );
                }        

            }   

        }    

        return array(            
            'form_emp_mes' => $empleadoForm->createView(),
            'prevem' => $prevem,
            'showem' => $showem,
        );
    }

    /**
    * Crear un formulario para carga de imagenes de empleado del mes.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function updateEmpleadoForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('archivo1', 'file', array(
                    'label'     => 'Imagen previa',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateImgExtension(this.value,this.id)')
                ))
                ->add('archivo2', 'file', array(
                    'label'     => 'Imagen de consulta',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateImgExtension(this.value,this.id)')
                ))               
                ->add(
                    'Guardar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=> 'showLoading("Actualizando")')   
                ))
                ->add(
                    'previo', 'button' , array(
                    'label'     => 'Vista Previa',                        
                    'attr' => array('class' => 'btn btn-info', 'onclick' => 'showPrev()')   
                ))
                ->add(
                    'cancel', 'reset' , array(
                    'label'     => 'Cancelar',                        
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'revertPrev()')   
                ))
                ->setAction($this->generateUrl('edit_emp_mes'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

    /**
     * Edición de cumpleaños
     *
     * @Route("/editCumpleAnnios", name="edit_cumple_annios")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:PanelControl:Cumpleanios.html.twig")
     */
    public function editCumpleAnniosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cumpleForm = $this->updateCumpleForm();      
        $prevfc = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(1)->getValor();
        $showfc = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(6)->getValor();

        if ($request->isMethod('POST'))
        {
            $cumpleForm->submit($request);
            if ($cumpleForm->isValid())
            {
                try{
                        $imagenprev = $cumpleForm->get('archivo1')->getData();
                        $imagenfull = $cumpleForm->get('archivo2')->getData();

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES'); 
                         

                        if($env == "Dev"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas');  

                            $rute .= "ImagenesIntranetqas/";
                        }else if($env == "Prd"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');
                           
                            $rute .= "ImagenesIntranetprd/";
                        }

                        if($imagenprev && $imagenfull){

                            $name1 = "FelizCumPrev".".".$imagenprev->guessExtension();
                            $cumpleForm->get('archivo1')->getData()->move($upload_folder,$name1);
                            $Updateprevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(1);
                            $Updateprevem -> setValor($rute.$name1);

                            $name2 = "FelizCumFull".".".$imagenfull->guessExtension();
                            $cumpleForm->get('archivo2')->getData()->move($upload_folder,$name2);
                            $Updateshowem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(6);
                            $Updateshowem -> setValor($rute.$name2);

                            $em->persist($Updateprevem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_cumple_annios')); 

                        }else if($imagenprev){
                            $name = "FelizCumPrev".".".$imagenprev->guessExtension();
                            $cumpleForm->get('archivo1')->getData()->move($upload_folder,$name);
                            $Updateprevem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(1);
                            $Updateprevem -> setValor($rute.$name);
                            $em->persist($Updateprevem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_cumple_annios')); 

                        }else if($imagenfull){

                            $name = "FelizCumFull".".".$imagenfull->guessExtension();
                            $cumpleForm->get('archivo2')->getData()->move($upload_folder,$name);
                            $Updateshowem = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(6);
                            $Updateshowem -> setValor($rute.$name);
                            $em->persist($Updateshowem);
                            $em->flush();       

                            $this->get('session')->getFlashBag()->add(
                                'success',  "Actualización Exitosa"
                            );

                            return $this->redirect($this->generateUrl('edit_cumple_annios')); 

                        }else{
                            $this->get('session')->getFlashBag()->add(
                                'error',  "Aún no se han seleccionado archivos"
                            );
                            return $this->redirect($this->generateUrl('edit_cumple_annios'));
                        }


                }catch(\SoapFault $ex){
                        $this->get('logger')->error('ERROR: UPDATE IMG FELIZ CUMPLEANNIOS');
                        $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar la(s) imagen(es)"
                    );
                }catch(\Exception $ex){
                        $this->get('logger')->error('ERROR: UPDATE IMG FELIZ CUMPLEANNIOS');
                        $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar la(s) imagen(es)"
                    );
                }        

            }   

        }

        return array(            
            'form_cum' => $cumpleForm->createView(),
            'prevfc' => $prevfc,
            'showfc' => $showfc,
        );
    } 

    /**
    * Crear un formulario para carga de imagenes de empleado del mes.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function updateCumpleForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('archivo1', 'file', array(
                    'label'     => 'Imagen previa',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateImgExtension(this.value,this.id)')
                ))
                ->add('archivo2', 'file', array(
                    'label'     => 'Imagen de consulta',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateImgExtension(this.value,this.id)')
                ))               
                ->add(
                    'Guardar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=> 'showLoading("Actualizando")') 
                ))
                ->add(
                    'previo', 'button' , array(
                    'label'     => 'Vista Previa',                        
                    'attr' => array('class' => 'btn btn-info', 'onclick' => 'showPrev()')   
                ))
                ->add(
                    'cancel', 'reset' , array(
                    'label'     => 'Cancelar',                        
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'revertPrev()')   
                ))
                ->setAction($this->generateUrl('edit_cumple_annios'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    } 

    /**
     * Consulta de tiempoes de entrega y Bines.
     *
     * @Route("/TiemposEntregaBines", name="bines_entregas")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:EntregaBines.html.twig")
     */
    public function ConsultaBINTEAction()
    {
        
        $ConsultaTiemposEntrega = $this->createTiemposForm(); 
        $ConsultaBines = $this->createBinesForm(); 

        return array(            
            'form_TE' => $ConsultaTiemposEntrega->createView(),
            'form_BIN' => $ConsultaBines->createView(),
        );
    } 

    /**
    * Crear un formulario para consulta de tiempo de entrega.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createTiemposForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('codigo', null, array(
                    'label' => '* CP:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 10),
                ))
                ->add('tipopago', 'choice', array(                   
                    'label' => 'Tipo de Pago:',
                    "required" => true,
                    'choices'  => array(
                        1 => 'TDC',
                        2 => 'COD',
                    ),
                    'empty_value' => '----------------------------------------',
                    'attr' => array('class' => "form-control")
                ))      
                ->add(
                    'Consultar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=> 'showConsulta("TEN")') 
                ))
                ->getForm();

        return $form;
    } 

    /**
    * Crear un formulario para consulta de Bines.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createBinesForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('bin', null, array(
                    'label' => '* Bin de Tarjeta:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 6, 'placeholder' => 'Primeros 6 digitos de la tarjeta'),
                ))  
                ->add(
                    'Consultar', 'button' , array(
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=> 'showConsulta("BIN")') 
                ))
                ->getForm();

        return $form;
    }

    /**
     * @Route("/getTiemposEntrega", name="get_tiempos_entrega")
     * @Template()
     */
    public function getTiemposEntregaAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $CP = $request->get('cp');
        $TP = $request->get('tpago');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['Info'] = '';

        try{
            $existCP = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($CP);
            if(count($existCP)>0){

                $TiempoEntrega = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findExisteCoberturaIntranet($TP,$CP);
                if(count($TiempoEntrega)>0){
                    $respuesta['estatus'] = 1;
                    $respuesta['Info'] = $TiempoEntrega;
                }else{
                   $respuesta['Info'] = 'No se encontro cobertura de envio para el CP: <b>'.$CP.'</b>'; 
                }
            }else{
                   $respuesta['Info'] = 'Este CP: <b>'.$CP.'</b> NO EXISTE';  
            }    
            
        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error INTRANET TIEMPOS DE ENTREGA CONSULTA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['Info'] = "No es posible obtener información intente más tarde";
        }catch(\Exception $ex){            
            $this->get('logger')->error('Error INTRANET TIEMPOS DE ENTREGA CONSULTA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['Info'] = "No es posible obtener información intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/getBines", name="get_bines")
     * @Template()
     */
    public function getBinesAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $BIN = $request->get('bin');       
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['Info'] = '';

        try{
            
                $InformacionTDC = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($BIN);
                if(count($InformacionTDC)>0){
                    $respuesta['estatus'] = 1;
                    $respuesta['Info'] = $InformacionTDC;
                }else{
                   $respuesta['Info'] = 'No existe información vinculada al BIN: <b>'.$BIN.'</b>'; 
                }
            
        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error INTRANET BINES CONSULTA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['Info'] = "No es posible obtener información intente más tarde";
        }catch(\Exception $ex){            
            $this->get('logger')->error('Error INTRANET BINES CONSULTA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['Info'] = "No es posible obtener información intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Consulta de noticias en Responsive Desing.
     *
     * @Route("/Noticias", name="noticias_responsive_desing")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Noticias:Consulta.html.twig")
     */
    public function NoticiasResponsiveAction()
    {
        $em = $this->getDoctrine()->getManager();
        $Noticias = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->getNoticiasActivas();
        if(count($Noticias)<=0){
                $this->get('session')->getFlashBag()->add(
                'error',  "Aún no hay noticias Activas"
                );
        }    

        return array(            
            'Noticias' => $Noticias,
        );
    }

    /**
     * Consulta de INBOX.
     *
     * @Route("/Inbox", name="show_inbox")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Inbox:index.html.twig")
     */
    public function InboxAction()
    {
        $em = $this->getDoctrine()->getManager();

        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
        $mails = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInbox($idUser);

        return array(            
            'Inbox' => $mails,
        );
    }

    /**
     * Lectura de correo..
     *
     * @Route("/{id}/openmail", name="show_message")
     * @Method("GET")
     * @Template("IntranetBundle:Inbox:readMail.html.twig")
     */
    public function ReadMensajeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $CC = "";
        $data = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($id);
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
        $estatusInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetEstatusInbox')->find(2);
        $InboxOpen = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInboxToDelete($id,$idUser);
        $InboxOpen[0] -> setEstatus($estatusInbox);
        $em->persist($InboxOpen[0]);
        $em->flush();
        $user_cc = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->findByMensaje($id);
        if(count($user_cc)>0){
            foreach ($user_cc as $usuario) {

                $CC .= $usuario->getReceptor()->getUNombre()." ".$usuario->getReceptor()->getUApPaterno();
                if($usuario->getReceptor()->getUApMaterno()){
                    $CC .= " ".$usuario->getReceptor()->getUApMaterno().", ";
                }else{
                    $CC .= ", ";
                }
            
            }
        }

        return array(            
            'Info' => $data,
            'CC'   => $CC ,
        );
    }

    /**
     * Nuevo correo..
     *
     * @Route("/newmail", name="new_message")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Inbox:new.html.twig")
     */
    public function NewMailAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = array();
        $user = $this->get('security.context')->getToken()->getUser()->getTipoUsuario()->getIdTipoUsuario();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();

        if($user == 14 || $user == 17 ){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findToInbox($idUser);
        }else if($user == 26){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAll();
        }else{
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAllToInbox($idUser);
        }

        return array(            
            'usuarios' => $usuarios,
        );
    }

    /**
     * @Route("/sendMensaje", name="send_mensaje")
     * @Method({"GET","POST"})
     */
    public function sendMensajeAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $usuarios_receptor = $request->get('destinatario');
        $user_emisor = $this->get('security.context')->getToken()->getUser();
        $asunto = $request->get('asunto');
        $cuerpo =  $request->get('mensaje');     
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

        try{

                      
                $newMensaje = new IntranetMensajeInbox();
                $newMensaje -> setAsunto($asunto);
                $newMensaje -> setCuerpo($cuerpo);
                $newMensaje -> setFecha(new \DateTime());
                $newMensaje -> setEmisor($user_emisor);
                $em->persist($newMensaje);

                $estatusInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetEstatusInbox')->find(1);

                foreach ($usuarios_receptor as $id_usuario) {

                    $usuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id_usuario);

                    $newInbox = new IntranetInbox();
                    $newInbox -> setEstatus($estatusInbox);
                    $newInbox -> setMensaje($newMensaje);
                    $newInbox -> setReceptor($usuario);
                    $em->persist($newInbox);

                }
                    
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                'success',  "Mensaje enviado con exito"
                );

            $respuesta['estatus'] = 1;    
            $respuesta['mensaje'] = $this->generateUrl('show_inbox');

        }catch(\SoapFault $ex){

            $this->get('logger')->error('Error SEND MENSAJE:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Se presento un error en el envio del mensaje, intente más tarde';
            

        }catch(\Exception $ex){            

            $this->get('logger')->error('Error SEND MENSAJE:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Se presento un error en el envio del mensaje, intente más tarde';

        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
         
    }

    /**
     * @Route("/deleteMensaje", name="delete_mensaje")
     * @Template()
     */
    public function deleteMensajeAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $Mensajes = $request->get('Mensajes');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

        $estatusInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetEstatusInbox')->find(3);


        try{    
                foreach ($Mensajes as $id_mensaje) {
                    $UpdateInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->find($id_mensaje);
                    $UpdateInbox -> setEstatus($estatusInbox);
                    $UpdateInbox -> setFechaEliminado(new \DateTime());
                    $em->persist($UpdateInbox);
                }
                    $em->flush();

                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = "El mensaje o mensajes ha sido eliminados con exito";

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error DELETE MENSAJE INBOX1:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Se ha presentado un error, porfavor intente más tarde";
        }catch(\Exception $ex){  
            $this->get('logger')->error('Error DELETE MENSAJE INBOX1:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Se ha presentado un error, porfavor intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

     /**
     * @Route("/deleteMensajeRead", name="delete_mensaje_rd")
     * @Template()
     */
    public function deleteMensajeReadAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $idMensaje = $request->get('Mensajes');
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

        $estatusInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetEstatusInbox')->find(3);

        try{    
                    $InboxDelete = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInboxToDelete($idMensaje,$idUser);
                    if(count($InboxDelete)>0){
                        foreach ($InboxDelete as $id_mensaje) {
                            $id_mensaje -> setEstatus($estatusInbox);
                            $id_mensaje -> setFechaEliminado(new \DateTime());
                            $em->persist($id_mensaje);
                        }    
                        $em->flush();
                        $respuesta['estatus'] = 1;
                        $respuesta['mensaje'] = "El mensaje ha sido eliminado con exito";
                    }else{
                        $respuesta['mensaje'] = "El mensaje No ha sido encontrado";
                    }

                

        }catch(\SoapFault $ex){  
            $this->get('logger')->error('Error DELETE MENSAJE INBOX2:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Se ha presentado un error, porfavor intente más tarde";
        }catch(\Exception $ex){  
            $this->get('logger')->error('Error DELETE MENSAJE INBOX2:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Se ha presentado un error, porfavor intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Apertura para reenvio..
     *
     * @Route("/{id}/Reenviar", name="reenviar_mensaje")
     * @Method("GET")
     * @Template("IntranetBundle:Inbox:reenvioInbox.html.twig")
     */
    public function ReenvioInboxAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($id);
        $user = $this->get('security.context')->getToken()->getUser()->getTipoUsuario()->getIdTipoUsuario();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();

        if($user == 14 || $user == 17 ){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findToInbox($idUser);
        }else if($user == 26){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAll();
        }else{
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAllToInbox($idUser);
        }

        return array(            
            'Info' => $data,
            'usuarios' => $usuarios,
        );
    }

    /**
     * APertura para Responder..
     *
     * @Route("/{id}/Responder", name="responder_mensaje")
     * @Method("GET")
     * @Template("IntranetBundle:Inbox:responderInbox.html.twig")
     */
    public function ResponderInboxAction($id)
    {
        $em = $this->getDoctrine()->getManager();        
        $CC = "";
        $data = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($id);
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
        $user_cc = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getCCRespuesta($id,$idUser);
        if(count($user_cc)>0){
            foreach ($user_cc as $usuario) {

                $CC .= $usuario->getReceptor()->getUNombre()." ".$usuario->getReceptor()->getUApPaterno();
                if($usuario->getReceptor()->getUApMaterno()){
                    $CC .= " ".$usuario->getReceptor()->getUApMaterno().", ";
                }else{
                    $CC .= ", ";
                }
            
            }
        }

        
            $CC .= $data->getEmisor()->getUNombre()." ".$data->getEmisor()->getUApPaterno();
            if($data->getEmisor()->getUApMaterno()){
                $CC .= " ".$data->getEmisor()->getUApMaterno().".";
            }else{
               $CC .= ".";
            } 
        

        return array(            
            'Info' => $data,
            'CC'   => $CC ,
        );
    }

    /**
     * @Route("/responderMensaje", name="responder_inbox")
     * @Method({"GET","POST"})
     */
    public function responderMensajeAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $user_emisor = $this->get('security.context')->getToken()->getUser();
        $asunto = $request->get('asunto');
        $cuerpo =  $request->get('mensaje');
        $idMsjprev = $request->get('idmensaje');    
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();

        try{

                      
                $newMensaje = new IntranetMensajeInbox();
                $newMensaje -> setAsunto($asunto);
                $newMensaje -> setCuerpo($cuerpo);
                $newMensaje -> setFecha(new \DateTime());
                $newMensaje -> setEmisor($user_emisor);
                $em->persist($newMensaje);

                $estatusInbox = $em->getRepository('MDRPuntoVentaBundle:IntranetEstatusInbox')->find(1);
                $MjsOrigen = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($idMsjprev);
                $usuarioOrigen = $MjsOrigen -> getEmisor();

                $RespuestaOrigen = new IntranetInbox();
                $RespuestaOrigen -> setEstatus($estatusInbox);
                $RespuestaOrigen -> setMensaje($newMensaje);
                $RespuestaOrigen -> setReceptor($usuarioOrigen);
                $em->persist($RespuestaOrigen);


                
                $user_cc = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getCCRespuesta($idMsjprev,$idUser);

                foreach ($user_cc as $id_usuario) {
                    $id = $id_usuario ->  getReceptor() -> getUNumero();
                    $usuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);

                    $newInbox = new IntranetInbox();
                    $newInbox -> setEstatus($estatusInbox);
                    $newInbox -> setMensaje($newMensaje);
                    $newInbox -> setReceptor($usuario);
                    $em->persist($newInbox);

                }
                    
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                'success',  "Mensaje enviado con exito"
                );

            $respuesta['estatus'] = 1;    
            $respuesta['mensaje'] = $this->generateUrl('show_message',['id' => $idMsjprev]);

        }catch(\SoapFault $ex){

            $this->get('logger')->error('Error RESPONDER MENSAJE:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Se presento un error en el envio del mensaje, intente más tarde';
            

        }catch(\Exception $ex){            

            $this->get('logger')->error('Error RESPONDER MENSAJE:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Se presento un error en el envio del mensaje, intente más tarde';

        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
         
    }

    /**
     * Consulta de INBOX Enviados.
     *
     * @Route("/InboxSend", name="show_inbox_send")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Inbox:enviados.html.twig")
     */
    public function InboxSendAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createEnviadosForm();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero(); 
        $mails = array();

       
        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {   
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();             
                $mails = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInboxsend($idUser,$fechaInicio,$fechaFin);
            }
                
            if(count($mails)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'form_enviados' => $searchForm->createView(),            
            'Inbox' => $mails,
        );
    }

    /**
    * Crear un formulario para buscar pedidos Enviados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEnviadosForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('Consultar', 'submit' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'consulta()')   
                    ))
                ->setAction($this->generateUrl('show_inbox_send'))
                ->getForm();

        return $form;
    }

    /**
     * Consulta de INBOX Eliminado.
     *
     * @Route("/InboxDelete", name="show_inbox_delete")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Inbox:eliminados.html.twig")
     */
    public function InboxDeleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createEliminadosForm();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero(); 
        $mails = array();

       
        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {   
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();             
                $mails = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInboxdelete($idUser,$fechaInicio,$fechaFin);
            }
                
            if(count($mails)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'form_eliminados' => $searchForm->createView(),            
            'Inbox' => $mails,
        );
    }

    /**
    * Crear un formulario para buscar pedidos Eliminados.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEliminadosForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('Consultar', 'submit' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'consulta()')   
                    ))
                ->setAction($this->generateUrl('show_inbox_delete'))
                ->getForm();

        return $form;
    }

    /**
     * Consulta de INBOX Eliminado.
     *
     * @Route("/InboxLeidos", name="show_inbox_leidos")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Inbox:leidos.html.twig")
     */
    public function InboxLeidosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createLeidosForm();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero(); 
        $mails = array();

       
        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {   
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();             
                $mails = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getInboxRead($idUser,$fechaInicio,$fechaFin);
            }
                
            if(count($mails)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'form_leidos' => $searchForm->createView(),            
            'Inbox' => $mails,
        );
    }

    /**
    * Crear un formulario para buscar pedidos Leidos.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createLeidosForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('Consultar', 'submit' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'consulta()')   
                    ))
                ->setAction($this->generateUrl('show_inbox_leidos'))
                ->getForm();

        return $form;
    }


    /**
     * Lectura de correo..
     *
     * @Route("/{id}/openInbox", name="show_message_con")
     * @Method("GET")
     * @Template("IntranetBundle:Inbox:readInbox.html.twig")
     */
    public function ReadInboxAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $CC = "";
        $data = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($id);
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
        $user_cc = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->findByMensaje($id);
        if(count($user_cc)>0){
            foreach ($user_cc as $usuario) {

                $CC .= $usuario->getReceptor()->getUNombre()." ".$usuario->getReceptor()->getUApPaterno();
                if($usuario->getReceptor()->getUApMaterno()){
                    $CC .= " ".$usuario->getReceptor()->getUApMaterno().", ";
                }else{
                    $CC .= ", ";
                }
            
            }
        }

        return array(            
            'Info' => $data,
            'CC'   => $CC ,
        );
    }

     /**
     * Apertura para reenvio..
     *
     * @Route("/{id}/ReenviarSend", name="reenviar_inbox")
     * @Method("GET")
     * @Template("IntranetBundle:Inbox:reenvioInboxSend.html.twig")
     */
    public function ReenvioInboxSendAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('MDRPuntoVentaBundle:IntranetMensajeInbox')->find($id);
        $user = $this->get('security.context')->getToken()->getUser()->getTipoUsuario()->getIdTipoUsuario();
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();

        if($user == 14 || $user == 17 ){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findToInbox($idUser);
        }else if($user == 26){
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAll();
        }else{
            $usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findAllToInbox($idUser);
        }

        return array(            
            'Info' => $data,
            'usuarios' => $usuarios,
        );
    }

    /**
     * @Route("/getTotalInbox", name="total_inbox")
     * @Template()
     */
    public function totalInboxJSONAction(Request $request)
    {       
        
        $em = $this->getDoctrine()->getManager();

        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['num'] = 0;
        $inbox = array();

        try{    

            $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
            $mails = $em->getRepository('MDRPuntoVentaBundle:IntranetInbox')->getTotalInbox($idUser);

            $totalInbox = count($mails);
            $respuesta['estatus'] = 1;
            $respuesta['num'] = $totalInbox;
            $respuesta['inbox'] = $mails;

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error GET NUM TOTAL INBOX:');
            $this->get('logger')->error($ex->getMessage());
        }catch(\Exception $ex){
             $this->get('logger')->error('Error GET NUM TOTAL INBOX:');
            $this->get('logger')->error($ex->getMessage());
        }  

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Admnistrador de noticias.
     *
     * @Route("/AdminNoticias", name="admin_news")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Noticias:administrador.html.twig")
     */
    public function AdminNoticiasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createAdminNewsForm();
        $editForm = $this->createAdminNewsEditForm();
        $noticias = array();
        $noticias = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->getNoticiasActivas();
       
        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            $editForm->submit($request);
            if ($searchForm->isValid())
            {   
                $option = $searchForm->get('activas')->getData();
                if($option == 2){                     
                    $noticias = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->getNoticasInactivas();
                }

                if(count($noticias)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }  
            }else if($editForm->isValid()){

            }
           
        }

        return array(
            'form_consulta' => $searchForm->createView(),   
            'form_edit' => $editForm->createView(),            
            'Noticias' => $noticias,
        );
            
    }

    /**
    * Crear un formulario para buscar Noticias.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createAdminNewsForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('activas', 'choice', array(                  
                    'label' => '* Activas/Desactivas:',
                    "required" => true,
                    'choices'     => array(1 => 'Activas', 2 => 'Desactivas'),
                    'empty_value' => '-----------------------',
                    'attr' => array('class' => "form-control")
                ))
                ->add('Consultar', 'submit' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'consulta()')   
                    ))
                ->setAction($this->generateUrl('admin_news'))
                ->getForm();

        return $form;
    }

    /**
    * Crear un formulario editar Noticias.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createAdminNewsEditForm()
    {
               
       $form = $this->createFormBuilder()
                ->add('archivo', 'file', array(
                    'label'     => 'Imagen:',
                    'required'  => false,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateNotImgExtension(this.value,this.id)')
                ))
                ->add('titulo', null, array(
                    'label' => '* Titulo Noticia:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 40),
                ))
                ->add('cuerpo', 'textarea', array( 
                    'label' => '* Descripción de la Noticia:',
                    "required" => true,
                    'attr' => array('class' => "form-control", 'placeholder' => 'Información sobre la noticia','maxlength' => 650)
                ))
                ->add('url', null, array(
                    'label' => 'URL (en caso de usar información externa):',
                    "required" => false,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add('fechaInicio', 'date', array(                    
                    'label' => '* Fecha de Inicio (cuando iniciara a verse la noticia)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',                
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => '* Fecha de Fin (cuando dejara de verse la noticia)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('tiponoticia', 'choice', array(                   
                    'label' => '* Relevancia de la Noticia:',
                    "required" => true,
                    'choices'  => array(
                        'small-box bg-aqua' => 'Noticia Relevante',
                        'small-box bg-green' => 'Noticia Informativa', 
                        'small-box bg-yellow' => 'Noticia Importante', 
                        'small-box bg-red' => 'Noticia de Sumamente Importante'
                    ),
                    'empty_value' => '----------------------------------------',
                    'attr' => array('class' => "form-control")
                ))
                ->add('Guardar', 'button' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                    ))
                ->add('Eliminar', 'button' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick'=>'ClearImage()')   
                    ))
                ->add('Cancelar', 'button' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'clearForm()')   
                    ))
                ->add('Vista', 'button' , array(                    
                    'label' => 'Vista Previa',
                    'attr' => array('class' => 'btn btn-info', 'onclick' => 'showPrev()')   
                    ))
                ->setAction($this->generateUrl('admin_news'))
                ->getForm();

        return $form;
    } 

    /**
     * @Route("/updateNoticia", name="update_noticia")
     * @Template()
     */
    public function updateNoticiaAction(Request $request)
    {       
        
        $em = $this->getDoctrine()->getManager();

        $archivo = $request->files->get('archivo');
        $titulo = $request->request->get('titulo');
        $cuerpo = $request->request->get('cuerpo');
        $tipo = $request->request->get('tipo');
        $url = $request->request->get('url');
        $inicio = $request->request->get('inicio');
        $fin = $request->request->get('fin');
        $idNoticia = $request->request->get('idNoticia');
        $usuario = $this->get('security.context')->getToken()->getUser();

        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = "";
        $inbox = array();

        try{ 
                $UpdateNoticia = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->find($idNoticia);

            if(count($UpdateNoticia) == 1){

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES'); 
                         

                        if($env == "Dev"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas');  

                            $rute .= "ImagenesIntranetqas/";
                        }else if($env == "Prd"){
                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');
                           
                            $rute .= "ImagenesIntranetprd/";
                        }

                        if($archivo){
                            $name = str_replace(" ", "", $archivo->getClientOriginalName());
                            $request->files->get('archivo')->move($upload_folder,$name);
                            $UpdateNoticia -> setImagen($rute.$name); 
                        }

                $UpdateNoticia -> setTituloNoticia($titulo);
                $UpdateNoticia -> setNoticia($cuerpo);
                $UpdateNoticia -> setURL($url); 
                $UpdateNoticia -> setFechaInicio(new \Datetime($inicio)); 
                $UpdateNoticia -> setFechaFin(new \Datetime($fin)); 
                $UpdateNoticia -> setTipoNoticia($tipo);
                $UpdateNoticia -> setUsuario($usuario); 

                $em->persist($UpdateNoticia);
                $em->flush();  
                 
                 $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha actualizado con exito la Noticia'
                    );

                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = "Se ha actualizado con exito la Noticia";                 

            }else{

                $respuesta['estatus'] = 0;
                $respuesta['mensaje'] = "La noticia no existe o ha sido eliminada";

            }

            

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error UPDATE NOTICIA');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Hubo un error en la actualización de la noticia, intente más tarde';
        }catch(\Exception $ex){
            $this->get('logger')->error('Error UPDATE NOTICIA');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Hubo un error en la actualización de la noticia, intente más tarde';
        }  

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/deleteNoticia", name="delete_noticia")
     * @Template()
     */
    public function deleteNoticiaAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $idNoticia = $request->get('idNoticia');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

       


        try{    
                $Noticia = $em->getRepository('MDRPuntoVentaBundle:IntranetNoticias')->find($idNoticia);

                $em->remove($Noticia);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha eliminado con exito la Noticia'
                    );

                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = "Se ha eliminado la Noticia";

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error DELETE NOTICIA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar la noticia, intente más tarde";
        }catch(\Exception $ex){  
            $this->get('logger')->error('Error DELETE NOTICIA:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar la noticia, intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Consulta de Lista Negra de Direcciones
     *
     * @Route("/ListaNegra", name="direcciones_ln")
     * @Template("IntranetBundle:Default:direcciones.html.twig")
     */
    public function ListaNegraDirAction()
    {
        $em = $this->getDoctrine()->getManager();   
        $Direcciones = $em->getRepository('MDRPuntoVentaBundle:ListaNegraDirecciones')->findAll();

        return array( 
            'Direcciones' => $Direcciones,
        );
    }     

    /**
     * Vista de perfil
     *
     * @Route("/PerfilUsuario", name="perfil_usuario")
     * @Template("IntranetBundle:Default:perfil.html.twig")
     */
    public function PerfilUsuarioAction()
    {
        $em = $this->getDoctrine()->getManager(); 
        $usuario = $this->get('security.context')->getToken()->getUser();

        return array( 
            'User' => $usuario,
        );
    }

    /**
     * Cambiar Avatar
     *
     * @Route("/Avatar", name="select_avatar")
     * @Template("IntranetBundle:Default:avatars.html.twig")
     */
    public function AvatarAction()
    {
        $em = $this->getDoctrine()->getManager(); 
        $Avatars = $em->getRepository('MDRPuntoVentaBundle:IntranetAvatar')->findAll();

        return array( 
            'avatar' => $Avatars,
        );
    }

    /**
     * @Route("/updateAvatar", name="update_avatar")
     * @Template()
     */
    public function updateAvatarAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $idAvatar = $request->get('idAvatar');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

       


        try{    
                $Avatar = $em->getRepository('MDRPuntoVentaBundle:IntranetAvatar')->find($idAvatar);
                $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
                $Usuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($idUser);

                $Usuario -> setAvatarIntranet($Avatar);
                $em->persist($Usuario);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha actualizado su avatar'
                    );

                $respuesta['estatus'] = 1;

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error UPDATE AVATAR:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar la noticia, intente más tarde";
        }catch(\Exception $ex){  
            $this->get('logger')->error('Error UPDATE AVATAR:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar la noticia, intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    } 

    /**
     * Vista de perfil
     *
     * @Route("/MDRCorreos", name="correos_mdr")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:correosmdr.html.twig")
     */
    public function MDRCorreosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); 
        $correos = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->findAll();
        $newForm = $this->createNewCorreoForm();

        if ($request->isMethod('POST'))
        {
            $newForm->submit($request);
            if ($newForm->isValid())
            {   
                $area = $newForm->get('area')->getData();
                $correo = trim($newForm->get('correo')->getData());
                
                $existe = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->findArea($area);
                
                if(count($existe) == 0){

                    $newCorreo = new IntranetCorreos();
                    $newCorreo -> setArea($area);
                    $newCorreo -> setCorreo($correo);
                    $newCorreo -> setActivo(true);
                    $em->persist($newCorreo);
                    $em->flush();

                     $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha registrado con exito el correo'
                    );
                    return $this->redirect($this->generateUrl('correos_mdr')); 
                }else{
                     $this->get('session')->getFlashBag()->add(
                        'error',  'El area ya existe'
                    );
                }

                
            }
           
        }


        return array(
            'form_new' => $newForm->createView(),      
            'correos' => $correos,
        );
    }

    /**
    * Crear un formulario nuevo Correo.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewCorreoForm()
    {
               
       $form = $this->createFormBuilder()
                ->add('area', null, array(
                    'label' => '* Area:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add('correo', null, array(
                    'label' => '* E-mail:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 250),
                ))
                ->add('Guardar', 'submit' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                    ))
                ->add('Cancelar', 'button' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'clearForm()')   
                    ))
                ->setAction($this->generateUrl('correos_mdr'))
                ->getForm();

        return $form;
    } 

    /**
     * @Route("/activaCorreo", name="_ActivaCorreo")
     */
    public function activaCorreoAction(Request $request)
    {
        $id = $request->get('idCorreo');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
    
        $CorreoMDR = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->find($id);
        if(!$CorreoMDR){
            $respuesta['mensaje'] = 'No existe el correo o ha sido eliminado.';    
        }else{
            $CorreoMDR->setActivo($valor);
            $em->persist($CorreoMDR);
            $em->flush();
                    $this->get('session')->getFlashBag()->add(
                        'success',  'El correo ha sido activado'
                    );
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/eliminaCorreo", name="_EliminaCorreo")
     */
    public function eliminaCorreoAction(Request $request)
    {
        $id = $request->get('idCorreo');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
    
        $CorreoMDR = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->find($id);
        if(!$CorreoMDR){
            $respuesta['mensaje'] = 'No existe el correo o ya ha sido eliminado.';    
        }else{
            $em->remove($CorreoMDR);
            $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'success',  'El correo ha sido eliminado'
                    );
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }  

    /**
     * @Route("/getCorreo", name="_getCorreo")
     */
    public function getCorreoAction(Request $request)
    {
        $id = $request->get('idCorreo');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
    
        $CorreoMDR = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->find($id);
        if(!$CorreoMDR){
            $respuesta['mensaje'] = 'No existe el correo o ya ha sido eliminado.';    
        }else{            
            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = $CorreoMDR; 
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }  

    /**
     * @Route("/updateCorreo", name="_UpdateCorreo")
     */
    public function updateCorreoAction(Request $request)
    {
        $id = $request->get('idCorreo');
        $area = $request->get('area');
        $correo = $request->get('correo');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
    
        $CorreoMDR = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->find($id);
        if(!$CorreoMDR){
            $respuesta['mensaje'] = 'No existe el correo o ha sido eliminado.';    
        }else{
                $DflArea = $CorreoMDR -> getArea();
                if($DflArea == $area){
                    $existe = array();
                }else{
                    $existe = $em->getRepository('MDRPuntoVentaBundle:IntranetCorreos')->findArea($area); 
                }

                if(count($existe) == 0){
                    $CorreoMDR -> setArea($area);
                    $CorreoMDR -> setCorreo($correo);
                    $em->persist($CorreoMDR);
                    $em->flush();
                            $this->get('session')->getFlashBag()->add(
                                'success',  'El correo ha sido actualizado'
                            );
                    $respuesta['estado'] = 1;
                }else{
                    $respuesta['mensaje'] = 'El area ya existe';
                }    
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Pagina de alta talleres y apoyos
     *
     * @Route("/newTalleresApoyos", name="talleres_apoyos")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:talleres.html.twig")
     */
    public function TalleresAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newForm = $this->createNewTallerForm();

        if ($request->isMethod('POST'))
        {
            $newForm->submit($request);
            if ($newForm->isValid())
            {   
               
                try{

                $Nombre = $newForm->get('titulo')->getData();
                $Descripcion = $newForm->get('cuerpo')->getData();
                $FechaInicio = $newForm->get('fechaInicio')->getData();
                $FechaFin = $newForm->get('fechaFin')->getData();
                $usuario = $newForm->get('Usuario')->getData();
                $Tipo = $newForm->get('tipo')->getData();
                $user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($usuario); 


                    $newTaller = new IntranetTalleres();
                    $newTaller -> setNombre($Nombre);
                    $newTaller -> setDescripcion($Descripcion);
                    $newTaller -> setFechaInicio($FechaInicio);
                    $newTaller -> setFechaFin($FechaFin);
                    $newTaller -> setUsuario($user);
                    if($Tipo){
                        $newTaller -> setTipo(true);
                    }else{
                        $newTaller -> setTipo(false);
                    }

                    $em->persist($newTaller);
                    $em->flush();

                     $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha registrado con exito el Taller/Apoyo'
                    );
                    return $this->redirect($this->generateUrl('talleres_apoyos')); 

                }catch(\SoapFault $ex){
                    $this->get('logger')->error('ERROR: NEW TALLER/APOYO');
                    $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar Taller/Apoyo"
                    );
                }catch(\Exception $ex){
                    $this->get('logger')->error('ERROR: NEW TALLER/APOYO');
                    $this->get('logger')->error($ex->getMessage());

                    $this->get('session')->getFlashBag()->add(
                        'error',  "Hubo un error al guardar Taller/Apoyo"
                    );
                }
                
            }
           
        } 


        return array(            
            'form_new' => $newForm->createView(),
        );
    }

    /**
    * Crear un formulario nuevo Taller.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createNewTallerForm()
    {
               
       $form = $this->createFormBuilder()
                ->add('titulo', null, array(
                    'label' => '* Nombre:',
                    "required" => true,
                    'attr' => array('class' => "form-control",'maxlength' => 200),
                ))                
                ->add('cuerpo', 'textarea', array( 
                    'label' => '* Descripción del Taller o Apoyo:',
                    "required" => true,
                    'attr' => array('class' => "form-control", 'placeholder' => 'Descripción del Taller o Apoyo')
                ))
                ->add('Usuario', 'entity', array(
                    'label' => '* Usuario que lo impartira:',
                    'class' => 'MDRPuntoVentaBundle:Usuario',
                    'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                            ->where("e.tipoUsuario IN(:Tipo)")
                            ->setParameter('Tipo', [3,4]);
                            },
                    'required' => true,
                    'empty_value' => '----------------------------------------',
                    'attr' => array('class' => "form-control"),
                ))                
                ->add('tipo', 'choice', array(                   
                    'label' => '* Tipo:',
                    "required" => true,
                    'choices'  => array(
                        1 => 'Taller',
                        0 => 'Apoyo',
                    ),
                    'empty_value' => '----------------------------------------',
                    'attr' => array('class' => "form-control")
                ))                
                ->add('fechaInicio', 'date', array(                    
                    'label' => '* Fecha de Inicio (cuando inician inscripciones)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => '* Fecha de Fin (cuando terminan inscripciones)',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('Guardar', 'submit' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                ))
                ->add('Cancelar', 'reset' , array(                 
                    'attr' => array('class' => 'btn btn-danger', 'onclick' => 'clearForm()')   
                ))
                ->setAction($this->generateUrl('talleres_apoyos'))
                ->getForm();

        return $form;
    }

    /**
     * Administrador de talleres y Apoyos
     *
     * @Route("/AdministradorTalleresApoyos", name="admin_talleres")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:adminTalleres.html.twig")
     */
    public function TalleresApoyosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager(); 
        $Talleres = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findTalleresActivos();
        $searchForm = $this->createAdminTalleresForm();
       
        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {   
                $option = $searchForm->get('activas')->getData();
                if($option == 2){                     
                    $Talleres = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findTalleresInactivos();
                }

                if(count($Talleres)<=0){
                    $this->get('session')->getFlashBag()->add(
                        'error',  $this->get('translator')->trans('busquedaSinResultados')
                    );
                }  
            }
           
        }
        return array(
            'form_consulta' => $searchForm->createView(),    
            'Talleres' => $Talleres,
        );
    }

    /**
    * Crear un formulario para buscar Noticias.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createAdminTalleresForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('activas', 'choice', array(                  
                    'label' => '* Activas/Desactivas:',
                    "required" => true,
                    'choices'     => array(1 => 'Activas', 2 => 'Desactivas'),
                    'empty_value' => '-----------------------',
                    'attr' => array('class' => "form-control")
                ))
                ->add('Consultar', 'submit' , array(                   
                    'attr' => array('class' => 'btn btn-block btn-info', 'onclick'=>'consulta()')   
                    ))
                ->setAction($this->generateUrl('admin_talleres'))
                ->getForm();

        return $form;
    }

    /**
     * @Route("/deleteTaller", name="delete_taller")
     * @Template()
     */
    public function deleteTallerAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $idTaller = $request->get('idTaller');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';

        try{    

                $existenRegistros = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findAllRegistros($idTaller);
                if(count($existenRegistros)>0){
                    foreach ($existenRegistros as $exist) {
                            $em->remove($exist);
                            $em->flush();                      
                    } 
                }

                $Taller = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->find($idTaller);

                $em->remove($Taller);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha eliminado con exito el Taller/Apoyo'
                    );

                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = "Se ha eliminado la Taller/Apoyo";

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error DELETE TALLER:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar el Taller/Apoyo, intente más tarde";
        }catch(\Exception $ex){  
            $this->get('logger')->error('Error DELETE TALLER:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "No ha sido posible eliminar el Taller/Apoyo, intente más tarde";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Show de talleres y Apoyos
     *
     * @Route("/TalleresApoyos", name="show_talleres_apoyos")
     * @Template("IntranetBundle:Default:showTalleres.html.twig")
     */
    public function showTalleresApoyosAction()
    {
        $em = $this->getDoctrine()->getManager(); 
        $Talleres = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findTalleresActivos();
        //\Doctrine\Common\Util\Debug::dump($Talleres); 
        return array( 
            'Talleres' => $Talleres,
        );
    }

    /**
     * @Route("/getInfoTaller", name="get_info_taller")
     * @Template()
     */
    public function getInfoTallerAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('idTaller');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['Info'] = '';
        $respuesta['users'] = '';

        try{    
            $Taller = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->find($id);            
            $Usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findUsuariosTalleres();
            if(count($Taller)>0){
                $respuesta['estatus'] = 1;
                $respuesta['Info'] = $Taller;
                $respuesta['users'] = $Usuarios; 
            }else{
                $respuesta['Info'] = "El taller o Apoyo pudo ser eliminada o registrada de forma erronea";
            }
            
        }catch(\SoapFault $ex){
            $respuesta['Info'] = "Error en carga de Taller o Apoyo";
        }catch(\Exception $ex){
            $respuesta['Info'] = "Error en carga de Taller o Apoyo";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/registroTaller", name="registro_taller")
     * @Template()
     */
    public function registroTallerAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $idTaller = $request->get('idTaller');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';
        $usuario = $this->get('security.context')->getToken()->getUser();
        $idUsuario = $this->get('security.context')->getToken()->getUser()->getUNumero();
        //\Doctrine\Common\Util\Debug::dump($idTaller);
        try{ 

            $existe = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findByesRegistrado($idTaller,$idUsuario);
                if(count($existe)>0){
                    $respuesta['mensaje'] = 'Ya se había registrado anteriormente al taller ';
                }else{
                    $Taller = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->find($idTaller);

                    $registroTaller = new IntranetRegistrosTalleres();
                    $registroTaller -> setUsuario($usuario);
                    $registroTaller -> setTaller($Taller);
                    $em->persist($registroTaller);
                    $em->flush();
                    
                    $this->get('session')->getFlashBag()->add(
                                'success',  'Se ha sido registrado con exito'
                            );

                    $respuesta['estatus'] = 1;
                    $respuesta['mensaje'] = 'Registro Exitoso';
                }
            
            
        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error REGISTRO AL TALLER:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Error en registro al Taller o Apoyo";
        }catch(\Exception $ex){
            $this->get('logger')->error('Error REGISTRO AL TALLER:');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = "Error en registro al Taller o Apoyo";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * @Route("/updateTaller", name="update_taller")
     * @Template()
     */
    public function updateTallerAction(Request $request)
    {       
        
        $em = $this->getDoctrine()->getManager();

        $idTaller = $request->request->get('id');
        $fechaInicio = $request->request->get('date1');
        $fechaFin = $request->request->get('date2');
        $Nombre = $request->request->get('titulo');
        $Descripcion = $request->request->get('cuerpo');
        $usuario = $request->request->get('usuario');
        $Tipo = $request->request->get('tipo');

        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = "";

        try{ 
                $UpdateTaller = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->find($idTaller);
                $user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($usuario);

            if(count($UpdateTaller) > 0){

                $UpdateTaller -> setNombre($Nombre);
                $UpdateTaller -> setDescripcion($Descripcion);
                $UpdateTaller -> setFechaInicio(new \Datetime($fechaInicio)); 
                $UpdateTaller -> setFechaFin(new \Datetime($fechaFin)); 
                $UpdateTaller -> setUsuario($user); 
                $UpdateTaller -> setTipo($Tipo);

                $em->persist($UpdateTaller);
                $em->flush();  
                 
                 $this->get('session')->getFlashBag()->add(
                        'success',  'Se ha actualizado con exito el Taller o Apoyo'
                    );

                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = "Se ha actualizado con exito el Taller o Apoyo";                 

            }else{
                
                $respuesta['mensaje'] = "El taller o Apoyo no existe o ha sido eliminada";

            }

            

        }catch(\SoapFault $ex){
            $this->get('logger')->error('Error UPDATE Taller/Apoyo');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Hubo un error en la actualización del Taller o Apoyo, intente más tarde';
        }catch(\Exception $ex){
            $this->get('logger')->error('Error UPDATE Taller/Apoyo');
            $this->get('logger')->error($ex->getMessage());
            $respuesta['mensaje'] = 'Hubo un error en la actualización del Taller o Apoyo, intente más tarde';
        }  

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

    /**
     * Reporte de registros a TALLERES
     *
     * @Route("/TalleresReportes", name="reportes_talleres")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:reporteTalleres.html.twig")
     */
    public function TalleresReportesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $searchForm = $this->createSearchFormRepTalleres();
        $entities = array();


        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $idTaller = $searchForm->get('taller')->getData();

                $entities = $em->getRepository('MDRPuntoVentaBundle:IntranetTalleres')->findAllRegistros($idTaller);
                    if($searchForm->get('Excel')->isClicked())
                    {
                        return $this->createExcelTalleres($entities);
                    }
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }

        return array(
            'Inscritos' => $entities,
            'form_rep' => $searchForm->createView()
        );
    }

    /**
    * Crear un formulario para generar reporte de talleres.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRepTalleres()
    {       
        $form = $this->createFormBuilder()
                ->add('taller', 'entity', array(
                    'label' => 'Talleres',
                    'class' => 'MDRPuntoVentaBundle:IntranetTalleres',
                    'required' => true,
                    'empty_value' => "-----------",
                    'attr' => array('class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning btn_consultar', 'onclick'=>'load()') 
                    ))                  
                ->add('Excel', 'submit', array(
                      'label' => 'Excel',
                      'attr' => array('class' => 'btn btn-success'),
                      ))                
                ->getForm();

        return $form;
    }

    function createExcelTalleres($listados, $titulo = 'Lista_de_Inscritps'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Taller")
                ->setCellValue('B1', "Nombre")
                ->setCellValue('C1', "APP")
                ->setCellValue('D1', "APM")
            ;
            
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {
                            
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getTaller()->getNombre())
                        ->setCellValue('B'.$numRow, $listado->getUsuario()->getUNombre())
                        ->setCellValue('C'.$numRow, $listado->getUsuario()->getUApPaterno())
                        ->setCellValue('D'.$numRow, $listado->getUsuario()->getUApMaterno())
                    ;
                    $numRow ++; 
                }
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    
    }

    /**
     * Carga de Reglamento
     *
     * @Route("/CargaReglamento", name="carga_reglamento")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:cargaReglamento.html.twig")
     */
    public function CargaReglamentoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newForm = $this->createcagraReglamentoForm();
        $reglamento = $env = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(4); 

        if ($request->isMethod('POST'))
        {
            $newForm->submit($request);
            if ($newForm->isValid())
            {
                $filePDF = $newForm->get('archivo')->getData();

                    try{

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES');
                        $updateReglamento = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(4);

                        if($env == "Dev"){

                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas'); 
                            $rute .= "ImagenesIntranetqas/DocumentosMDRqas/";
                            $upload_folder .= '\DocumentosMDRqas';

                        }else if($env == "Prd"){

                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');                            
                            $rute .= "ImagenesIntranetprd/DocumentosMDRprd/";
                            $upload_folder .= '\DocumentosMDRprd';

                        }

                        if($filePDF){
                            $name = "Reglamento.pdf";
                            $newForm->get('archivo')->getData()->move($upload_folder,$name);
                            $updateReglamento -> setValor($rute.$name); 
                        }

                        $em->persist($updateReglamento);
                        $em->flush();       

                        $this->get('session')->getFlashBag()->add(
                            'success',  "Se ha actualizado el <b>REGLAMENTO</b>"
                        );

                        return $this->redirect($this->generateUrl('carga_reglamento'));

                    }catch(\SoapFault $ex){
                        $this->get('logger')->error('ERROR: UPDATE REGLAMENTO');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar el REGLAMENTO"
                        );
                    }catch(\Exception $ex){
                        $this->get('logger')->error('ERROR: UPDATE REGLAMENTO');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar el REGLAMENTO"
                        );
                    } 
               
            } 
            
        }

        return array(
            'form_new' => $newForm->createView(),
            'reglamento' => $reglamento,
        );
    }

    /**
    * Crear un formulario para carga de reglamento.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createcagraReglamentoForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('archivo', 'file', array(
                    'label'     => 'Imagen:',
                    'required'  => true,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateNotExtension(this.value,this.id)')
                ))
                ->add('Guardar', 'submit' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                ))

                ->add('Cancelar', 'reset' , array(                  
                    'attr' => array('class' => 'btn btn-danger', 'onclick'=>'cancel()')   
                ))
                ->setAction($this->generateUrl('carga_reglamento'))
                ->getForm();

        return $form;
    }

    /**
     * Carga de Gaceta
     *
     * @Route("/CargaGaceta", name="carga_gaceta")
     * @Method({"GET","POST"})
     * @Template("IntranetBundle:Default:cargaGaceta.html.twig")
     */
    public function CargaGacetaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $newForm = $this->createcagraGacetaForm();
        $gaceta = $env = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(5); 

        

        if ($request->isMethod('POST'))
        {
            $newForm->submit($request);
            if ($newForm->isValid())
            {
                $filePDF = $newForm->get('archivo')->getData();

                    try{

                        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
                        $rute = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('LEGACY_PATH_IMAGES');
                        $updateGaceta = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find(5);

                        if($env == "Dev"){

                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_qas'); 
                            $rute .= "ImagenesIntranetqas/DocumentosMDRqas/";
                            $upload_folder .= '\DocumentosMDRqas';

                        }else if($env == "Prd"){

                            $upload_folder = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('path_file_images_intranet_prd');                            
                            $rute .= "ImagenesIntranetprd/DocumentosMDRprd/";
                            $upload_folder .= '\DocumentosMDRprd';

                        }
                        if($filePDF){
                            $name = "Gaceta.pdf";
                            $newForm->get('archivo')->getData()->move($upload_folder,$name);
                            $updateGaceta -> setValor($rute.$name); 
                        }

                        $em->persist($updateGaceta);
                        $em->flush();       

                        $this->get('session')->getFlashBag()->add(
                            'success',  "Se ha actualizado la <b>GACETA</b>"
                        );

                        return $this->redirect($this->generateUrl('carga_gaceta'));

                    }catch(\SoapFault $ex){
                        $this->get('logger')->error('ERROR: UPDATE GACETA');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar la GACETA"
                        );
                    }catch(\Exception $ex){
                        $this->get('logger')->error('ERROR: UPDATE GACETA');
                        $this->get('logger')->error($ex->getMessage());

                        $this->get('session')->getFlashBag()->add(
                            'error',  "Hubo un error al guardar la GACETA"
                        );
                    } 
               
            } 
            
        }

        return array(
            'form_new' => $newForm->createView(),
            'gaceta' => $gaceta,
        );
    }

    /**
    * Crear un formulario para carga de gaceta.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createcagraGacetaForm()
    {
               
        $form = $this->createFormBuilder()
                ->add('archivo', 'file', array(
                    'label'     => 'Imagen:',
                    'required'  => true,
                    'attr' => array('class' => 'form-control', 'onchange' => 'validateNotExtension(this.value,this.id)')
                ))
                ->add('Guardar', 'submit' , array(                  
                    'attr' => array('class' => 'btn btn-primary', 'onclick'=>'save()')   
                ))

                ->add('Cancelar', 'reset' , array(                  
                    'attr' => array('class' => 'btn btn-danger', 'onclick'=>'cancel()')   
                ))
                ->setAction($this->generateUrl('carga_gaceta'))
                ->getForm();

        return $form;
    }

    /**
     * @Route("/getInfoGacetaReglamento", name="get_info_gaceta_reglamento")
     * @Template()
     */
    public function getInfoGacetaReglamentoAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['Info'] = '';

        try{    
            $Información = $em->getRepository('MDRPuntoVentaBundle:IntranetMain')->find($id);
            if(count($Información)>0){
                $respuesta['estatus'] = 1;
                $respuesta['Info'] = $Información;
            }else{
                $respuesta['Info'] = "No hay información, intente más tarde";
            }
            
        }catch(\SoapFault $ex){
            $respuesta['Info'] = "Error en carga de Datos";
        }catch(\Exception $ex){
            $respuesta['Info'] = "Error en carga de Datos";
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }



    /**
     * @Route("/updateSkin", name="update_skin_user")
     * @Template()
     */
    public function updateSkinAction(Request $request)
    {       
        $em = $this->getDoctrine()->getManager();
        $skin = $request->get('skin');
        $idUser = $this->get('security.context')->getToken()->getUser()->getUNumero();
        $respuesta = array();

        try{    
            $UpdateSkinUsuario = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($idUser);
            if(count($UpdateSkinUsuario)>0){
                $UpdateSkinUsuario -> setTemaIntranet($skin); 
                $em->persist($UpdateSkinUsuario);
                $em->flush();  
            }

        }catch(\SoapFault $ex){
            $this->get('logger')->error('ERROR: UPDATE SKIN');
            $this->get('logger')->error($ex->getMessage());

        }catch(\Exception $ex){
            $this->get('logger')->error('ERROR: UPDATE SKIN');
            $this->get('logger')->error($ex->getMessage());
        }  
       
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }

}
