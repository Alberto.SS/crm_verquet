<?php

namespace MDR\IntranetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

use MDR\PuntoVentaBundle\Entity\IntranetNoticias;
use MDR\PuntoVentaBundle\Entity\IntranetContenidos;
use MDR\PuntoVentaBundle\Entity\IntranetMenus;
use MDR\PuntoVentaBundle\Form\IntranetMenusType;

/**
 * @Route("/Admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/menu", name="intranet_admin_menu")
     * @Template()
     */
    public function MenuAction()
    {
        $menu = new IntranetMenus();
        $formMenu   = $this->createCreateFormMenu($menu);
        return array(
            'formMenu'  => $formMenu->createView(),
        );
    }

    /**
     * @Route("/menuJSON", name="intranet_menu_json")
     * @Template()
     */
    public function MenuJSONAction()
    {
        $em = $this->getDoctrine()->getManager();
        $menus = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->findAll();
        return new JsonResponse($menus);
    }

    /**
     * @Route("/contenido", name="intranet_contenido")
     * @Method("POST")
     */
    public function ContenidoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $query = $request->request->get('query');
        $querys = explode('/', $query);
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->findByUrls($querys);
        if($menu != null)
        {
            $respuesta['menu'] = $menu;
            $contenido = $menu->getContenido();
            if($contenido != null)
            {
                $plantilla = $contenido->getPlantilla();
                $comentarios = $em->getRepository('MDRPuntoVentaBundle:IntranetComentarios')->findByContenido($contenido);
                if($plantilla != null)
                {
                    $respuesta['estado'] = 1;
                    $respuesta['mensaje'] = 'Página encontrada se envía contenido';
                    $respuesta['contenido'] = $contenido;
                    $respuesta['comentarios'] = $comentarios;
                    $respuesta['idPlantilla'] = $contenido->getPlantilla()->getIdPlantilla();
                    $respuesta['archivosLista'] = [];
                    $path = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS');
                    $pathWeb = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS_WEB');
                    $path = $path . DIRECTORY_SEPARATOR . $contenido->getIdContenido();
                    if(file_exists($path))
                    {
                        $archivos = scandir($path);
                        $respuesta['pathArchivosLista'] = $pathWeb;
                        foreach ($archivos as $archivo) {
                            $pathArchivo = $path . DIRECTORY_SEPARATOR .$archivo;
                            if(is_file($pathArchivo))
                            {
                                $tamanio = filesize($pathArchivo)/1024;
                                $respuesta['archivosLista'][] = ['nombre' => $archivo, 'tamanio' => $tamanio];
                            }
                        }
                        $archivoExcel = $path . DIRECTORY_SEPARATOR .'tabla.xls';
                        if(!file_exists($archivoExcel)){
                            $archivoExcel = $path . DIRECTORY_SEPARATOR .'tabla.xlsx';
                            if(!file_exists($archivoExcel)){
                                $archivoExcel = null;
                            }
                        }
                        if($archivoExcel != null)
                        {
                            $respuesta['datosExcel'] = $this->LeerExcel($archivoExcel);
                        }
                    }
                }
            }
        
        }


        return new JsonResponse($respuesta);
    }

    public function LeerExcel($path)
    {
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject($path);
        $sheet = $phpExcelObject->setActiveSheetIndex(0);

        $cols = [];
        $rows = [];
        $contCols = 0;
        $hayDatos = true;

        while ($hayDatos) {
            $letra = $this->LetrasExcel($contCols + 1);
            $hayDatos = strlen($sheet->getCell($letra.'1')) > 0;
            if($hayDatos)
            {
                $cols[] = $sheet->getCell($letra.'1')->getValue();
                $contCols++;
            }
        }

        $contRows = 2;
        $hayDatos = true;
        while ($hayDatos) {
            $hayDatos = strlen($sheet->getCell('A'.$contRows)) > 0;
            if($hayDatos)
            {
                $fila = [];
                for ($i = 0; $i < $contCols; $i++) {
                    $letra = $this->LetrasExcel($i + 1);
                    $dato = '';
                    try {
                        $dato = $sheet->getCell($letra.$contRows)->getValue();
                    } catch (\Exception $ex) {
                    }
                    $fila[] = $dato;
                }
                $rows[] = $fila;
                $contRows++;
            }
        }

        return ['cols' => $cols, 'rows' => $rows];
    }

    private function LetrasExcel($num)
    {
        // Z - 26, AA - 27
        $div = floor(($num - 1) / 26);
        $mod = $num - $div * 26;
        
        $letra = strtoupper(base_convert($mod + 9, 10, 36));
        if($div > 0)
        {
            return $this->LetrasExcel($div).$letra;
        }

        return $letra;
    }

    /**
     * @Route("/plantilla", name="intranet_plantilla")
     * @Method("POST")
     */
    public function PlantillaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Plantilla no encontrada';

        $id = $request->request->get('id');
        $plantilla = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->find($id);
        if($plantilla != null)
        {
            $respuesta['plantilla'] = $plantilla;
            $nombreView = 'IntranetBundle:js:'.$plantilla->getNombre().'.html.twig';
            if ( $this->get('templating')->exists($nombreView) ) {
                $html = $this->renderView($nombreView, array());
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Plantilla encontrada se envía contenido';
                $respuesta['html'] = $html;
            }
        }
        

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/plantillas", name="intranet_plantillas")
     * @Method("GET")
     */
    public function PlantillasAction(Request $request)
    {
        return new JsonResponse($this->GetPlantillasHtml());
    }

    public function GetPlantillasHtml()
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'No se encontraron plantillas';

        $plantillas = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->findAll();
        $cont = 0;
        $respuesta['plantillas'] = [];
        foreach ($plantillas as $key => $plantilla) {
            $cont ++;
            $nombreView = 'IntranetBundle:js:'.$plantilla->getNombre().'.html.twig';
            if ( $this->get('templating')->exists($nombreView) ) {
                $html = $this->renderView($nombreView, array());
                $respuesta['plantillas'][$plantilla->getIdPlantilla()] = $html;
            }
        }
        if(count($cont) > 0)
        {
            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = 'Plantillas encontradas, se envía contenido';
        }

        return $respuesta;
    }

    /**
     * @Route("/contenidoGuardar", name="intranet_contenido_guardar")
     * @Method("POST")
     */
    public function GuardarContenidoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $imagen = $request->files->get('imagen');
        $fondo = $request->files->get('fondo');
        $video = $request->files->get('video');
        $idMenu = $request->request->get('idMenu');
        $idPlantilla = $request->request->get('idPlantilla');

        $html1 = $request->request->get('html1');
        $html2 = $request->request->get('html2');
        $html3 = $request->request->get('html3');
        $html4 = $request->request->get('html4');

        if($idMenu == 0 || $idPlantilla == 0)
        {
            $respuesta['mensaje'] = 'Plantilla o Menú sin definir';
            return new JsonResponse($respuesta);
        }
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenu);
        $plantilla = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->find($idPlantilla);

        if($menu != null && $plantilla != null)
        {
            $contenido = $menu->getContenido();

            $extension = $contenido->getImagen();
            $extensiones = explode('|', $extension);
            $extensionesRand = explode('|', $extension);

            $rand = '#'.rand();
            if($imagen != null)
            {
                $extensiones[0] = $imagen->guessClientExtension();
                if(strlen($extensiones[0]) == 0) {
                    $extensiones[0] = $imagen->getExtension();
                }
                $extensionesRand[0] =  $extensiones[0].$rand;
            }
            if($fondo != null)
            {
                $extensiones[1] = $fondo->guessClientExtension();
                if(strlen($extensiones[1]) == 0) {
                    $extensiones[1] = $fondo->getExtension();
                }
                $extensionesRand[1] =  $extensiones[1].$rand;
            }
            // if($video != null)
            // {
            //     $extensiones[2] = $video->guessClientExtension();
            //     if(strlen($extensiones[2]) == 0) {
            //         $extensiones[2] = $video->getExtension();
            //     }
            // }

            $contenido->setPlantilla($plantilla);
            $menu->setContenido($contenido);
            $contenido->setImagen(implode('|', $extensionesRand));
            $contenido->setHtml1($html1);
            $contenido->setHtml2($html2);
            $contenido->setHtml3($html3);
            $contenido->setHtml4($html4);
            $contenido->setFechaEdicion(new \DateTime());
            $contenido->setUsuarioEdita($user);
            $em->flush();
            
            $path = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS');
            //Validar existencia de carpeta
            if(!file_exists($path.DIRECTORY_SEPARATOR.$contenido->getIdContenido()))
            {
                mkdir($path.DIRECTORY_SEPARATOR.$contenido->getIdContenido());
            }
            if($imagen != null)
            {
                $imagen->move($path.DIRECTORY_SEPARATOR.$contenido->getIdContenido(), '__logo.'.$extensiones[0]);
            }
            if($fondo != null)
            {
                $fondo->move($path.DIRECTORY_SEPARATOR.$contenido->getIdContenido(), '__fondo.'.$extensiones[1]);
            }
            if($video != null)
            {
                $video->move($path.DIRECTORY_SEPARATOR.$contenido->getIdContenido(), '__video.mp4');
            }
            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = 'Página actualizada correctamente';
            $respuesta['contenido'] = $contenido;
        }

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/contenidoGuardarArchivo", name="intranet_contenido_guardar_archivo")
     * @Method("POST")
     */
    public function GuardarContenidoArchivoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $archivo = $request->files->get('archivo');
        $nombreArchivo = $request->files->get('nombreArchivo');
        $idMenu = $request->request->get('idMenu');
        $idTemp = $request->request->get('idTemp');
        $idPlantilla = $request->request->get('idPlantilla');

        if($idMenu == 0)
        {
            $respuesta['mensaje'] = 'Plantilla o Menú sin definir';
            return new JsonResponse($respuesta);
        }
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenu);
        $plantilla = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->find($idPlantilla);
        $contenido = $menu->getContenido();

        if($plantilla != null)
        {
            $path = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS');
            $path = $path . DIRECTORY_SEPARATOR . $contenido->getIdContenido();
            //Validar existencia de carpeta
            if(!file_exists($path))
            {
                mkdir($path);
            }
            if($nombreArchivo == null || strlen($nombreArchivo) == 0)
            {
                $nombreArchivo = $archivo->getClientOriginalName();
            }
            $archivo->move($path, $nombreArchivo);

            $contenido->setFechaEdicion(new \DateTime());
            $contenido->setUsuarioEdita($user);
            $em->flush();

            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = 'Archivo Cargado correctamente';
            $respuesta['idTemp'] = $idTemp;
        }

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/contenidoGuardarArchivos", name="intranet_contenido_guardar_archivos")
     * @Method("POST")
     */
    public function GuardarContenidoArchivosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $idMenu = $request->request->get('idMenu');
        $idPlantilla = $request->request->get('idPlantilla');
        $archivos = $request->files->get('archivos');
        $idArchivos = $request->request->get('idArchivos');

        if($idMenu == 0)
        {
            $respuesta['mensaje'] = 'Plantilla o Menú sin definir';
            return new JsonResponse($respuesta);
        }
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenu);
        $plantilla = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->find($idPlantilla);
        $contenido = $menu->getContenido();

        if($plantilla != null)
        {
            $path = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS');
            $path = $path . DIRECTORY_SEPARATOR . $contenido->getIdContenido();
            //Validar existencia de carpeta
            if(!file_exists($path))
            {
                mkdir($path);
            }
            $respuesta['idArchivos'] = [];
            for ($i = 0; $i < count($archivos); $i++) { 
                $archivo = $archivos[$i];
                $archivo->move($path, $archivo->getClientOriginalName());
                $respuesta['idArchivos'][] = $idArchivos[$i];
            }
            $contenido->setFechaEdicion(new \DateTime());
            $contenido->setUsuarioEdita($user);
            $em->flush();

            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = 'Archivo Cargado correctamente';
        }


        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/borrarContenidoArchivoAction", name="intranet_contenido_borrar_archivo")
     * @Method("POST")
     */
    public function BorrarContenidoArchivoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $idMenu = $request->request->get('idMenu');
        $archivo = $request->request->get('archivo');

        if($idMenu == 0)
        {
            $respuesta['mensaje'] = 'Plantilla o Menú sin definir';
            return new JsonResponse($respuesta);
        }
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($idMenu);
        $contenido = $menu->getContenido();

        if($contenido == null)
        {
            $respuesta['mensaje'] = 'Contenido sin definir';
            return new JsonResponse($respuesta);
        }
        $path = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS');
        $path = $path . DIRECTORY_SEPARATOR . $contenido->getIdContenido() . DIRECTORY_SEPARATOR . $archivo;
        
        //Validar existencia de archivo
        if(file_exists($path))
        {
            unlink($path);
            $respuesta['estado'] = 1;
            $respuesta['mensaje'] = 'Archivo Eliminado correctamente';
        } else {
            $respuesta['estado'] = 0;
            $respuesta['mensaje'] = 'No se encontro archivo ';
        }

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/menuEliminar", name="intranet_menu_eliminar")
     * @Method("POST")
     */
    public function EliminarMenuAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta['estado'] = 0;
        $respuesta['mensaje'] = 'Página no encontrada';

        $id = $request->request->get('idContenido');
        $menu = $em->getRepository('MDRPuntoVentaBundle:IntranetMenus')->find($id);
        $contenido = $menu->getContenido();
        if($contenido != null)
        {
            $comentarios = $em->getRepository('MDRPuntoVentaBundle:IntranetComentarios')->findByContenido($contenido);
            foreach ($comentarios as $comentario) {
                $em->remove($comentario);
            }
            $em->remove($contenido);
        }
        if($menu != null)
        {
            $em->remove($menu);
        }
        $em->flush();
        $respuesta['estado'] = 1;
        $respuesta['mensaje'] = 'Página eliminada correctamente';

        return new JsonResponse($respuesta);
    }

    /**
     * @Route("/", name="intranet_index")
     * @Template()
     */
    public function IndexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $plantillas = $em->getRepository('MDRPuntoVentaBundle:IntranetPlantillas')->findBy([], ['nombre' =>'ASC']);
        $pathIntranetContenidos = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('PATH_INTRANET_CONTENIDOS_WEB');
        $res = $this->GetPlantillasHtml();
        $plantillasHtml = [];
        if($res['estado'] == 1)
        {
            $plantillasHtml = $res['plantillas'];
        }

        return array(
            'plantillas'                => $plantillas,
            'plantillasHtml'            => $plantillasHtml,
            'pathIntranetContenidos'    => $pathIntranetContenidos,
        );
    }
  

    //Creación de formularios

    /**
     * Creates a form to create a IntranetMenus entity.
     *
     * @param IntranetMenus $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateFormMenu(IntranetMenus $entity)
    {
        $form = $this->createForm(new IntranetMenusType(), $entity, array(
            'action' => $this->generateUrl('intranetmenus_create'),
            'method' => 'POST',
            'tipoRequest'   => 'ajax',
            'attr'          => [
                'id'    => 'menu_form'
            ]
        ));

        return $form;
    }

}
