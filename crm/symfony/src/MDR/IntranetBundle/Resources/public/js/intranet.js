var configToolbar = [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                  { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                  { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                  '/',
                  { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                  '/',
                  { name: 'paragraph', groups: [ 'list' ], items: [ 'NumberedList', 'BulletedList' ] },
                  { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                  { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                  '/',
                  { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                  { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                  { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                  { name: 'others', items: [ '-' ] },
                  { name: 'about', items: [ 'About' ] }
                ];
        var configContent = 
          'h1 h2 h3 p blockquote strong em;' +
          'a[!href];' +
          'img(left,right)[!src,alt,width,height];' +
          'table tr th td caption;' +
          'span{!font-family};' +
          'span{!color};' +
          'span(!marker);' +
          'del ins';
CKEDITOR.disableAutoInline = true;
var contenidoBD = null;
var archivosContenido = [];
var pathArchivosLista = '';
var datosExcel = {cols: [], rows: []};
var menuBD = null;
var idPlantilla = 0;

var AppRouter = Backbone.Router.extend({

  routes: {
    "c/*query":                "getContent",  // #productos
    "notFound/*query":         "notFound",
  },

  notFound: function(query)
  {
    var html = NotFoundTemplate();
    $("#contenido_intranet").html(html);
    $("#breadcrumbMenu").hide().html("");
    $("#panelAdmin").hide();
  },

  getContent: function(query) {
    if(query == null)
    {
      window.location = '#notFound/' + query;
      return;
    }
    showLoading('Cargando Información', '#contenido_intranet');
    $.post(urls.intranet.contenido, {query: query}, function(data, textStatus, xhr) {
      contenidoBD = data.contenido;
      if(contenidoBD === undefined)
      {
        window.location = '#notFound/' + query;
        return;
      }
      $("#panelAdmin").show();
      comentariosIntranet = new ComentariosIntranetCollection(data.comentarios);
      archivosContenido = data.archivosLista;
      pathArchivosLista = data.pathArchivosLista;
      if(data.datosExcel !== undefined)
      {
        datosExcel = data.datosExcel;
      } else {
        datosExcel = {cols: [], rows: []};
      }
      menuBD = data.menu;
      if(data.estado == 1)
      {
        idPlantilla = contenidoBD.plantilla.idPlantilla;
        if(isAdmin)
        {
          $("#plantilla").val(idPlantilla);
        }
        refreshContent();
      } else {
        if(isAdmin)
        {
          idPlantilla = $("#plantilla").val();
          contenidoBD = {
            idContenido: 0,
            imagen: '',
            html1: '',
            html2: '',
            html3: '',
            html4: '',
            fechaCreacion: '',
            fechaEdicion: '',
            usuarioCrea: '',
            usuarioEdita: '',
            comentariosActivo: false
          };
        } else if(data.contenido === undefined) {
          idPlantilla = 0;
          contenidoBD = null;
          alert(data.mensaje);
        }
        refreshContent();
      }
    }).always(function(){
      hideLoading('#contenido_intranet');
    });
  }

});

function showContent(plantilla, plantillaHtml, contenido)
{
  if (contenido === undefined)  contenido = contenidoBD;
  if (archivosContenido === undefined)  archivosContenido = [];
  if (contenido != null && plantillaHtml != null)
  {
    contenido.menu = _.find(menus, function(menu) { return menu.idMenu == menuBD.idMenu });
    var template = _.template(plantillaHtml);
    var html = template(contenido);
    $("#contenido_intranet").html(html);
    archivosIntranet = new ArchivosIntranetCollection();
    for (var i = 0; i < archivosContenido.length; i++) {
      var pathArchivo = pathArchivosLista + '/' + contenido.idContenido + '/' + archivosContenido[i].nombre;
      archivosIntranet.add({nombre: archivosContenido[i].nombre, tamanio: archivosContenido[i].tamanio, data: pathArchivo, path: pathArchivo, estatus: 1});
    }
    if($("#ComentariosIntranet").length > 0)
    {
      appComentariosIntranet = new ComentariosIntranetView;
    }
    var breadcrumbTemplate = _.template($("#BreadcrumbMenusTemplate").html());
    var menusPadres = getPadresMenu(menuBD.idMenu);
    var html = breadcrumbTemplate({menus: menusPadres});
    $("#breadcrumbMenu").show().html(html);
  } else {
    $("#contenido_intranet").html("");
    $("#breadcrumbMenu").show().html("");
  }
}

function editHtml(activar){
    var items = $("[data-edit='html']");
    for (var i = items.length - 1; i >= 0; i--)
    {
      if(activar)
      {
        $(items[i]).attr("contenteditable", "true");
        CKEDITOR.inline( items[i] );
      } else {
        $(items[i]).removeAttr("contenteditable");
      }
    }
    if(activar)
    {
      $("#btnCancelar").show();
      $("#btnGuardar").show();
      $("#btnEditar").hide();
      $(".optEditable").show();
      $("input[data-edit=img]").show().change(function() {
        var that = this;
        if(this.files.length > 0)
        {
          var f = this.files[0];
          if (!f.type.match('image.*')) {
            alert("El archivo debe ser una imagen");
            $(this).val('');
            return;
          }
          var reader = new FileReader();

          // Closure to capture the file information.
          reader.onload = (function(theFile) {
            return function(e) {
              if($(that).attr("data-nombre") == 'fondo')
              {
                var div = $("div[data-nombre=" + $(that).attr("data-nombre") + "]");
                div.css('background', 'url(' + e.target.result + ')');
              } else {
                var img = $("img[data-nombre=" + $(that).attr("data-nombre") + "]");
                img.attr('src', e.target.result);
              }
            };
          })(f);

          // Read in the image file as a data URL.
          reader.readAsDataURL(f);
        }
      });
      $("input[data-edit=video]").show().change(function() {
        var that = this;
        if(this.files.length > 0)
        {
          var f = this.files[0];
          if (!f.type.match('video.*')) {
            alert("El archivo debe ser video");
            $(this).val('');
            return;
          }
        }
      });
    } else {
      $("#btnCancelar").hide();
      $("#btnGuardar").hide();
      $("#btnEditar").show();
      $(".optEditable").hide();
      var editors = CKEDITOR.instances;
      for (var i = 1; i < 5; i++) {
        if(editors["editor"+i] !== undefined)
        {
          editors["editor"+i].destroy();
        }
      }
      refreshContent();
    }
  }

  function getContenidoHtml()
  {
    var items = $("[data-nombre]");
    var contenido = _.clone(contenidoBD);
    var formData = new FormData();

    for (var i = items.length - 1; i >= 0; i--) {
      var $item = $(items[i]);
      var valor = null;
      if($item.attr("data-edit") == "html")
      {
        valor = $item.html();
      } else if(["img", "video"].indexOf($item.attr("data-edit")) >= 0) {
        if($($item).prop('tagName') == "INPUT")
        {
          if($item[0].files.length > 0)
          {
            valor = $item[0].files[0];
          } else {
            valor = '';
          }
        }
      } else {
        valor = $item.val();
      }
      if(valor != null)
      {
        contenido[$item.attr("data-nombre")] = valor;
        formData.append($item.attr("data-nombre"), valor);
      }
    }
    contenido.imagen = contenidoBD.imagen;
    return {contenido: contenido, formData: formData};
  }

  function saveHtml(){
    var data = getContenidoHtml();
    var contenido = data.contenido;
    var formData = data.formData;
    contenido.idMenu = menuBD.idMenu;
    contenido.idPlantilla = $("#plantilla").val();
    formData.append('idMenu', menuBD.idMenu);
    formData.append('idPlantilla', $("#plantilla").val());

    jQuery.ajax({
      url: urls.intranet.contenidoGuardar,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      type: 'POST',
      success: function(data){
        if(data.estado == 1)
        {
          contenidoBD = data.contenido;
          editHtml(false);
        }
        flashMessage(getFlashType(data.estado), data.mensaje);
      }
    });

  }

  function eliminar(){
    menu = _.find(menus, function(menu) { return menu.idMenu == menuBD.idMenu });
    if(menu.menus.length > 0)
    {
      alert('Esté menú contiene otros elementos, primero debe eliminarlos.');
      return;
    }
    bootbox.confirm("¿Seguro de eliminar el menú y su contenido?", function(data){
      if(data)
      {
        if(menuBD != null)
        {
          var id = $().val();
          $.post(urls.intranet.menuEliminar, {idContenido: menuBD.idMenu}, function(data, textStatus, xhr) {
            flashMessage(getFlashType(data.estado), data.mensaje);
            if(data.estado == 1)
            {
              window.location = '#c/';
            }
          });
        }
      }
    });
  }

  function reporteComentarios() {
    var plantilla = _.find(plantillas, function (item){ return item.nombre == "ReporteComentarios"});
    var plantillaHtml = plantillasHtml[plantilla.idPlantilla];
    showContent(plantilla, plantillaHtml, contenidoBD);
  }

  function plantillaChange()
  {
    var contenido = {};
    if($("#btnCancelar").is(":visible"))
    {
      contenido = getContenidoHtml();
      contenido = contenido.contenido;
    }
    else {
      contenido = contenidoBD;
    }
    idPlantilla = $("#plantilla").val();
    refreshContent(contenido);
    if($("#btnCancelar").is(":visible"))
    {
      editHtml(true);
    }
  }

  function refreshContent(contenido)
  {
    var plantillaHtml = null;
    var plantilla = null;
    if(idPlantilla > 0)
    {
      plantillaHtml = plantillasHtml[idPlantilla];
      plantilla = _.find(plantillas, function(plantilla){ return plantilla.idPlantilla == idPlantilla });
    }
    showContent(plantilla, plantillaHtml, contenido);
  }