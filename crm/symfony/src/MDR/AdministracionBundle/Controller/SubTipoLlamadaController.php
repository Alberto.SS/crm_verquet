<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\SubTipoLlamada;
use MDR\PuntoVentaBundle\Form\SubTipoLlamadaType;

/**
 * SubTipoLlamada controller.
 *
 * @Route("/subtipollamada")
 */
class SubTipoLlamadaController extends Controller
{

    /**
     * Lists all SubTipoLlamada entities.
     *
     * @Route("/", name="subtipollamada")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->findAllOrder();
		
		 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }


        return array(
            'entities' => $entities,
        );
    }

    /**
     * Export Excel all Sub Tipo Llamadas entities.
     *
     * @Route("/exportExcel", name="subtipollamada_excel_export")
     * @Method("GET")
     * @Template()
     */
    public function exportExcelAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->findAllOrder();

        return $this->createExcel($entities, 'TiposLlamada');
    }

    /**
     * Creates a new SubTipoLlamada entity.
     *
     * @Route("/", name="subtipollamada_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:SubTipoLlamada:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new SubTipoLlamada();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('subtipollamada_show', array('id' => $entity->getIdSubTipo())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a SubTipoLlamada entity.
     *
     * @param SubTipoLlamada $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubTipoLlamada $entity)
    {
        $form = $this->createForm(new SubTipoLlamadaType(), $entity, array(
            'action' => $this->generateUrl('subtipollamada_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new SubTipoLlamada entity.
     *
     * @Route("/new", name="subtipollamada_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SubTipoLlamada();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SubTipoLlamada entity.
     *
     * @Route("/{id}", name="subtipollamada_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubTipoLlamada entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SubTipoLlamada entity.
     *
     * @Route("/{id}/edit", name="subtipollamada_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubTipoLlamada entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SubTipoLlamada entity.
    *
    * @param SubTipoLlamada $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SubTipoLlamada $entity)
    {
        $form = $this->createForm(new SubTipoLlamadaType(), $entity, array(
            'action' => $this->generateUrl('subtipollamada_update', array('id' => $entity->getIdSubTipo())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing SubTipoLlamada entity.
     *
     * @Route("/{id}", name="subtipollamada_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:SubTipoLlamada:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubTipoLlamada entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('subtipollamada_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a SubTipoLlamada entity.
     *
     * @Route("/{id}", name="subtipollamada_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubTipoLlamada entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('subtipollamada'));
    }

    /**
     * Creates a form to delete a SubTipoLlamada entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('subtipollamada_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top: -54px;','class' => 'btn btn-danger')))
            ->getForm()
        ;
    }

    private function createExcel($entities, $titulo = 'Dids'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Tipo.")
                ->setCellValue('B1', "Sub Tipo")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($entities as $entity) {
            $sheet
                ->setCellValue('A'.$numRow, $entity->getTipoLlamada()->getDescripcion())
                ->setCellValue('B'.$numRow, $entity->getDescripcionSubTipo())
            ;
            $numRow ++;
        }
        
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    }
}
