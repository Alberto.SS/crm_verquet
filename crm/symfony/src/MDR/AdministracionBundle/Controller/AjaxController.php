<?

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

    /**
 * ProductoPrecios controller.
 *
 * @Route("/ProductoPreciosAjax")
 */

class AjaxController extends Controller
{
    
     /**
     * @Route("/actual", name="_ActualizarProductoPrecio")
     */
    public function ActualizarProductoPrecio(Request $request)
    {
        $id = $request->get('idProductoPrecio');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
            //$respuesta['mensaje'] = '';
    
        $productoPrecio = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->find($id);
        if(!$productoPrecio){
            $respuesta['mensaje'] = 'No se encontro el Producto Precio';    
        }else{
            $productoPrecio->setActivo($valor);
            $em->persist($productoPrecio);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    
     /**
     * @Route("/tarjetas", name="_ActualizarListaNegraTarjetas")
     */
    public function ActualizarListaNegraTarjetas(Request $request)
    {
            
        $id = $request->get('idListaNegra');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
            //$respuesta['mensaje'] = '';
    
        $ListaNegraTarjetas = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->find($id);
        if(!$ListaNegraTarjetas){
            $respuesta['mensaje'] = 'No se encontro el Numero de tarjeta';  
        }else{
            $ListaNegraTarjetas->setActivo($valor);
            $em->persist($ListaNegraTarjetas);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * @Route("/usuarios", name="_ActualizarUsuarios")
     */
    public function ActualizarUsuarios(Request $request)
    {
            
        $id = $request->get('uNumero');

        $valorDat = $request->get('valor');
        
        
if($valorDat == 'false'){
    $valor = 0;
}
else{
    $valor = 1;
}

        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
            //$respuesta['mensaje'] = '';
    
        $Usuarios = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find($id);
        if(!$Usuarios){
            $respuesta['mensaje'] = 'No se encontro al Usuario';    
        }else{
            $Usuarios->setActivo($valor);
            $em->persist($Usuarios);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
         /**
     * @Route("/descuentos", name="_ActualizarDescuentos")
     */
    public function ActualizarDescuentos(Request $request)
    {
            
        $id = $request->get('idDescuento');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
            //$respuesta['mensaje'] = '';
    
        $Descuentos= $em->getRepository('MDRPuntoVentaBundle:Descuentos')->find($id);
        if(!$Descuentos){
            $respuesta['mensaje'] = 'No se encontro el id Del descuento contacte al Administrador del sistema'; 
        }else{
            $Descuentos->setActivo($valor);
            $em->persist($Descuentos);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

        /**
     * @Route("/horarios", name="_ActualizarHorarios")
     */
    public function ActualizarHorarios(Request $request)
    {
            
        $id = $request->get('idhorario');

        $valorDat = $request->get('valor');
        
        
if($valorDat == 'false'){
    $valor = 0;
}
else{
    $valor = 1;
}

        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
          //  $respuesta['mensaje'] = '';
    
        $Horarios = $em->getRepository('MDRPuntoVentaBundle:Horarios')->find($id);
        if(!$Horarios){
            $respuesta['mensaje'] = 'No se encontro el Horario';    
        }else{
            $Horarios->setActivo($valor);
            $em->persist($Horarios);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/gastos", name="_ActualizarGastosEnvio")
     */
    public function ActualizarGastosEnvio(Request $request)
    {
        $id = $request->get('idGastosEnvio');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
            //$respuesta['mensaje'] = '';
    
        $GastoEnvio = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id);
        if(!$GastoEnvio){
            $respuesta['mensaje'] = 'No se encontro el Gasto de Envio';    
        }else{
            $GastoEnvio->setActivo($valor);
            $em->persist($GastoEnvio);
            $em->flush();
       
            $respuesta['estado'] = 1;
        }
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/ADReglas", name="_ActivarReglas")
    */
    public function ActualizarReglasEnvio(Request $request)
    {
        $action = $request->get('ident');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{
            $Reglas = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->findAll();
            if(count($Reglas)>0){
                if($action == "A"){

                    foreach ($Reglas as $reglaad) {
                        $reglaad-> setActivo(true);
                        $em->persist($reglaad);
                        $em->flush();
                    }
                    $respuesta['estado'] = 1;
                    $respuesta['mensaje'] = "Se han Activado todas las reglas";
                }else if($action == "D"){

                    foreach ($Reglas as $reglaad) {
                        $reglaad-> setActivo(false);
                        $em->persist($reglaad);
                        $em->flush();
                    }
                    $respuesta['estado'] = 1;
                    $respuesta['mensaje'] = "Se han Desactivado todas las reglas";
                }
                
            }else{
                    $respuesta['mensaje'] = "ERROR: error en BD de Reglas";
            }
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLAS:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLAS';
        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLAS:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLAS';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/ADRegla", name="_ActivarRegla")
    */
    public function ActualizarReglaEnvio(Request $request)
    {
        $id = $request->get('idRegla');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Regla = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find($id);
            if(!$Regla){
                $respuesta['mensaje'] = 'No se encontro la Regla en la BD';    
            }else{
                $Regla->setActivo($valor);
                $em->persist($Regla);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo la regla: '.$Regla->getDescripcion();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTIVACION/DESACTIVACION DE REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTIVACION/DESACTIVACION DE REGLA';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/actualizaPagoRegla", name="_ActualizarTipoPagoRegla")
    */
    public function ActualizarTipoPagoReglaEnvio(Request $request)
    {
        $id = $request->get('idRegla');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Regla = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find($id);
            if(!$Regla){
                $respuesta['mensaje'] = 'No se encontro la Regla en la BD';    
            }else{
                $Regla->setIdPago($valor);
                $em->persist($Regla);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo el tipo de pago de la regla: '.$Regla->getDescripcion();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTUALIZACIÓN TIPO PAGO REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACIÓN TIPO PAGO REGLA';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTUALIZACIÓN TIPO PAGO REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACIÓN TIPO PAGO REGLA';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
    * @Route("/actualizaValorRegla", name="_ActualizarValorRegla")
    */
    public function ActualizarValorReglaEnvio(Request $request)
    {
        $id = $request->get('idRegla');
        $valor = $request->get('valor');
        $em = $this->getDoctrine()->getManager();        

        $respuesta['estado'] = 0;
        
        try{

            $Regla = $em->getRepository('MDRPuntoVentaBundle:ReglasFraude')->find($id);
            if(!$Regla){
                $respuesta['mensaje'] = 'No se encontro la Regla en la BD';    
            }else{
                $Regla->setValor($valor);
                $em->persist($Regla);
                $em->flush();
           
                $respuesta['estado'] = 1;
                $respuesta['mensaje'] = 'Se a actualizo la regla: '.$Regla->getDescripcion();
            }
            
        }catch(\SoapFault $ex){

                $this->get('logger')->error('Error ACTUALIZACIÓN VALOR REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACIÓN VALOR REGLA';

        }catch(\Exception $ex){   

                $this->get('logger')->error('Error ACTUALIZACIÓN VALOR REGLA:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = 'Error ACTUALIZACIÓN VALOR REGLA';
        }
        
                            
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
