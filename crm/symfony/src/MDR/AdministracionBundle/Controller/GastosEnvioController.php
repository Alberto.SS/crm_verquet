<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\GastosEnvio;
use MDR\PuntoVentaBundle\Form\GastosEnvioType;

/**
 * GastosEnvio controller.
 *
 * @Route("/gastosenvio")
 */
class GastosEnvioController extends Controller
{

    /**
     * Lists all GastosEnvio entities.
     *
     * @Route("/", name="gastosenvio")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->findAll();
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
		
        foreach ($entities as $gastoEnvio) {
            $gastoEnvio->deleteForm = $this->createDeleteForm($gastoEnvio->getIdGastosEnvio())->createView();
        }

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new GastosEnvio entity.
     *
     * @Route("/", name="gastosenvio_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:GastosEnvio:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new GastosEnvio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);



        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setActivo(true);
            if($entity->getPrecio() == 0){
                $entity->setPrecio(0);
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('gastosenvio'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a GastosEnvio entity.
     *
     * @param GastosEnvio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GastosEnvio $entity)
    {
        $form = $this->createForm(new GastosEnvioType(), $entity, array(
            'action' => $this->generateUrl('gastosenvio_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new GastosEnvio entity.
     *
     * @Route("/new", name="gastosenvio_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new GastosEnvio();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a GastosEnvio entity.
     *
     * @Route("/{id}", name="gastosenvio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GastosEnvio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
     

    /**
     * Displays a form to edit an existing GastosEnvio entity.
     *
     * @Route("/{id}/edit", name="gastosenvio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GastosEnvio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
     

    /**
    * Creates a form to edit a GastosEnvio entity.
    *
    * @param GastosEnvio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GastosEnvio $entity)
    {
        $form = $this->createForm(new GastosEnvioType(), $entity, array(
            'action' => $this->generateUrl('gastosenvio_update', array('id' => $entity->getIdGastosEnvio())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing GastosEnvio entity.
     *
     * @Route("/{id}", name="gastosenvio_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:GastosEnvio:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GastosEnvio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('gastosenvio_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
     
    /**
     * Deletes a GastosEnvio entity.
     *
     * @Route("/{id}", name="gastosenvio_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GastosEnvio entity.');
            }

            //$em->remove($entity);
            $entity->setActivo(false);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('gastosenvio'));
    }

    /**
     * Creates a form to delete a GastosEnvio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gastosenvio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('checkbox','checkbox')
            ->getForm()
        ;
    }
}
