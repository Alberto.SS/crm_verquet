<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Dids;
use MDR\PuntoVentaBundle\Form\DidsType;

/**
 * Dids controller.
 *
 * @Route("/dids/{canalDistribucion}")
 */
class DidsController extends Controller
{

    /**
     * Lists all Dids entities.
     *
     * @Route("/", name="dids")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($canalDistribucion)
    {
        $em = $this->getDoctrine()->getManager();

        //Buscar Dids por la descripción del canal de ventas
        //Crear repositorio para query (DidsRepository)
        $entities = $em->getRepository('MDRPuntoVentaBundle:Dids')->findByDescCanalDistribucion($canalDistribucion);
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }

        return array(
            'entities' => $entities,
			'canal' => $canalDistribucion,
        );
    }

    /**
     * Export Excel all Dids entities.
     *
     * @Route("/exportExcel", name="dids_excel_export")
     * @Method("GET")
     * @Template()
     */
    public function exportExcelAction($canalDistribucion)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Dids')->findByDescCanalDistribucion($canalDistribucion);

        return $this->createExcel($entities, 'Dids');
    }

    /**
     * Creates a new Dids entity.
     *
     * @Route("/", name="dids_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Dids:new.html.twig")
     */
    public function createAction(Request $request, $canalDistribucion)
    {
        $entity = new Dids();
        $form = $this->createCreateForm($entity, $canalDistribucion);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			$canal = $em->getRepository('MDRPuntoVentaBundle:CanalDistribucion')->findByDescripcion($canalDistribucion);
            $entity->setCanalDistribucion($canal[0]);
			$organizacionVenta = $em->getRepository('MDRPuntoVentaBundle:OrganizacionVentas')->find('0100');
            $entity->setOrganizacionVentas($organizacionVenta);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('dids_show', array('id' => $entity->getIdDid(), 'canalDistribucion' => $canalDistribucion)));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
			'canal' => $canalDistribucion,
        );
    }

    /**
     * Creates a form to create a Dids entity.
     *
     * @param Dids $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Dids $entity, $canalDistribucion)
    {
        $form = $this->createForm(new DidsType(), $entity, array(
            'action' => $this->generateUrl('dids_create', array('canalDistribucion' => $canalDistribucion)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Dids entity.
     *
     * @Route("/new", name="dids_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($canalDistribucion)
    {
        $entity = new Dids();
        $form   = $this->createCreateForm($entity , $canalDistribucion);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
			'canal' => $canalDistribucion,
        );
    }

    /**
     * Finds and displays a Dids entity.
     *
     * @Route("/{id}", name="dids_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($canalDistribucion, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Dids')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dids entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $canalDistribucion);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
			'canal' => $canalDistribucion,
        );
    }

    /**
     * Displays a form to edit an existing Dids entity.
     *
     * @Route("/{id}/edit", name="dids_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($canalDistribucion, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Dids')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dids entity.');
        }

        $editForm = $this->createEditForm($entity, $canalDistribucion);
        $deleteForm = $this->createDeleteForm($id, $canalDistribucion);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'canal' => $canalDistribucion,
        );
    }

    /**
    * Creates a form to edit a Dids entity.
    *
    * @param Dids $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Dids $entity , $canalDistribucion)
    {
        $form = $this->createForm(new DidsType(), $entity, array(
            'action' => $this->generateUrl('dids_update', array('id' => $entity->getIdDid(), 'canalDistribucion' => $canalDistribucion)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Dids entity.
     *
     * @Route("/{id}", name="dids_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Dids:edit.html.twig")
     */
    public function updateAction(Request $request, $canalDistribucion, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Dids')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dids entity.');
        }

        $deleteForm = $this->createDeleteForm($id, $canalDistribucion);
        $editForm = $this->createEditForm($entity, $canalDistribucion);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('dids_edit', array('id' => $id, 'canalDistribucion' => $canalDistribucion) ));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'canal' => $canalDistribucion,
        );
    }
    /**
     * Deletes a Dids entity.
     *
     * @Route("/{id}", name="dids_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $canalDistribucion, $id)
    {
		echo $id.'<br><br>'.$canalDistribucion;
        $form = $this->createDeleteForm($id, $canalDistribucion);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Dids')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Dids entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('dids', array('id' => $id, 'canalDistribucion' => $canalDistribucion)));
    }

    /**
     * Creates a form to delete a Dids entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id, $canalDistribucion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('dids_delete', array('id' => $id, 'canalDistribucion' => $canalDistribucion)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }

    private function createExcel($entities, $titulo = 'Dids'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Número.")
                ->setCellValue('B1', "Canal Distribución")
                ->setCellValue('C1', "Oficina Ventas")
            ;
        
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
        foreach ($entities as $entity) {
            $sheet
                ->setCellValue('A'.$numRow, $entity->getNumero())
                ->setCellValue('B'.$numRow, $entity->getCanalDistribucion()->getDescripcion())
                ->setCellValue('C'.$numRow, $entity->getOficinaVentas()->getDescripcion())
            ;
            $numRow ++;
        }
        
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
    }
}
