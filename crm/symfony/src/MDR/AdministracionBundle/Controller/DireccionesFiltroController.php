<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DidsMenuController extends Controller
{
	
	    /**
     * Lists all Dids entities.
     *
     * @Route("/dids", name="didsmenu")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:CanalDistribucion')->findAll();
		
			 if(count($entities)<=0){
					$this->get('session')->getFlashBag()->add(
						'error',  $this->get('translator')->trans('busquedaSinResultados')
					);
				}

        return array(
            'entities' => $entities,

        );
    }
}
