<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\BinTerminal;
use MDR\PuntoVentaBundle\Form\BinTerminalType;

/**
 * BinTerminal controller.
 *
 * @Route("/binterminal")
 */
class BinTerminalController extends Controller
{

    /**
     * Lists all BinTerminal entities.
     *
     * @Route("/", name="binterminal")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->findAllComplete();
		
			 if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
			
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new BinTerminal entity.
     *
     * @Route("/", name="binterminal_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:BinTerminal:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new BinTerminal();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->findByIdBin($entity->getIdBin());
            if(count($entities)>0){
                $this->get('session')->getFlashBag()->add(
                    'warning',  'El Bin que intenta crear ya existe, en la parte de abajo se muestra el BIN'
                );
                return $this->redirect($this->generateUrl('binterminal_show', array('id' => $entity->getIdBin())));
            }else{
                $em->persist($entity);
                $em->flush(); 
                $this->get('session')->getFlashBag()->add(
                    'success',  'Creado correctamente'
                );
                return $this->redirect($this->generateUrl('binterminal_show', array('id' => $entity->getIdBin())));
            }        
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a BinTerminal entity.
     *
     * @param BinTerminal $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BinTerminal $entity)
    {
        $form = $this->createForm(new BinTerminalType(), $entity, array(
            'action' => $this->generateUrl('binterminal_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new BinTerminal entity.
     *
     * @Route("/new", name="binterminal_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new BinTerminal();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a BinTerminal entity.
     *
     * @Route("/{id}", name="binterminal_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BinTerminal entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing BinTerminal entity.
     *
     * @Route("/{id}/edit", name="binterminal_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BinTerminal entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a BinTerminal entity.
    *
    * @param BinTerminal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BinTerminal $entity)
    {
        $form = $this->createForm(new BinTerminalType(), $entity, array(
            'action' => $this->generateUrl('binterminal_update', array('id' => $entity->getIdBin())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing BinTerminal entity.
     *
     * @Route("/{id}", name="binterminal_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:BinTerminal:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BinTerminal entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('binterminal_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a BinTerminal entity.
     *
     * @Route("/{id}", name="binterminal_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BinTerminal entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('binterminal'));
    }

    /**
     * Creates a form to delete a BinTerminal entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('binterminal_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar','attr' => array('style' => 'margin-top:7px;' ,'class' => "btn btn-danger")))
            ->getForm()
        ;
    }
}
