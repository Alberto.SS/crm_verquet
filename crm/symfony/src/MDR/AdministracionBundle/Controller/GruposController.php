<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\Grupos;
use MDR\PuntoVentaBundle\Form\GruposType;

/**
 * Grupos controller.
 *
 * @Route("/grupos")
 */
class GruposController extends Controller
{

    /**
     * Lists all Grupos entities.
     *
     * @Route("/", name="grupos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Grupos')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Grupos entity.
     *
     * @Route("/", name="grupos_create")
     * @Method("POST")
     * @Template("MDRPuntoVentaBundle:Grupos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Grupos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('grupos_show', array('id' => $entity->getidGrupo())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Grupos entity.
     *
     * @param Grupos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Grupos $entity)
    {
        $form = $this->createForm(new GruposType(), $entity, array(
            'action' => $this->generateUrl('grupos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'));

        return $form;
    }

    /**
     * Displays a form to create a new Grupos entity.
     *
     * @Route("/new", name="grupos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Grupos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Grupos entity.
     *
     * @Route("/{id}", name="grupos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Grupos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Grupos entity.
     *
     * @Route("/{id}/edit", name="grupos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Grupos entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Grupos entity.
    *
    * @param Grupos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Grupos $entity)
    {
        $form = $this->createForm(new GruposType(), $entity, array(
            'action' => $this->generateUrl('grupos_update', array('id' => $entity->getidGrupo())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar'));

        return $form;
    }
    /**
     * Edits an existing Grupos entity.
     *
     * @Route("/{id}", name="grupos_update")
     * @Method("PUT")
     * @Template("MDRPuntoVentaBundle:Grupos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:Grupos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Grupos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('grupos_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Grupos entity.
     *
     * @Route("/{id}", name="grupos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MDRPuntoVentaBundle:Grupos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Grupos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('grupos'));
    }

    /**
     * Creates a form to delete a Grupos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('grupos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
