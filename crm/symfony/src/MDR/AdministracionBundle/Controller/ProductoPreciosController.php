<?php

namespace MDR\AdministracionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MDR\PuntoVentaBundle\Entity\ProductoPrecios;
use MDR\PuntoVentaBundle\Form\ProductoPreciosType;

/**
 * ProductoPrecios controller.
 *
 * @Route("/ProductoPrecios")
 */
class ProductoPreciosController extends Controller
{

    /**
     * Lists all ProductoPrecios entities.
     *
     * @Route("/", name="productoprecios")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createSearchForm();
		$entities = array();
		if ($request->isMethod('POST'))
        {
            $form->handleRequest($request);
             if ($form->isValid()) {
				$arrayQ=array();
				$arrayQ['canalDistribucion']= $form['canalDistribucion']->getData();
				$arrayQ['oficinaVenta']= $form['oficinaVentas']->getData();
				
                if($form['activo']->getData() != null){
                    $arrayQ['activo']= $form['activo']->getData();
                }
				if($form['producto']->getData() != null){
					$arrayQ['producto']= $form['producto']->getData();
				}
		        $entities = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->findBy($arrayQ);
				
					 if(count($entities)<=0){
						$this->get('session')->getFlashBag()->add(
							'error',  $this->get('translator')->trans('busquedaSinResultados')
						);
					}
				
				
			}
		}
        return array(
            'entities' => $entities,
			'form' => $form -> createView(),
        );
    }
    /**
     * Finds and displays a ProductoPrecios entity.
     *
     * @Route("/{id}", name="productoprecios_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductoPrecios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
/**
     * Crear formulario para crear combos de seleccion .
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSearchForm()
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()

            ->add('canalDistribucion', 'entity', array(
                'class'         => 'MDRPuntoVentaBundle:CanalDistribucion',
                'label'         => 'Canal de Distribución',
                'empty_value'   => '-----------',
                'required'      => true,
                'attr' => array('class' => "form-control"),
            ))
			
			->add('oficinaVentas', 'entity', array(
                'class'         => 'MDRPuntoVentaBundle:OficinasVentas',
                'label'         => 'Oficina de Ventas',
                'empty_value'   => '-----------',
                'required'      => true,
                'attr' => array('class' => "form-control"),
            ))
			
			->add('producto', 'entity', array(
                'class'         => 'MDRPuntoVentaBundle:Productos',
                'label'         => 'Producto',
                'empty_value'   => '-----------',
                'required'      => false,
                'attr' => array('class' => "form-control"),
            ))
			
		   ->add('activo', 'choice', array(
		   		'choices'   => array('1' => 'Activo', '0' => 'Inactivo'),
				'label'         => 'Activo',
				'empty_value'   => '-----------',
				'required'  => false,
				'attr' => array('class' => "form-control"),
			))
			
            ->add('Enviar', 'submit', array(
                'label'     => 'Buscar',
				'attr' => array('class' => "btn btn-primary"),
                ))
            ->getForm();

        return $form;
    }
	
	//$request->get('estado')
}