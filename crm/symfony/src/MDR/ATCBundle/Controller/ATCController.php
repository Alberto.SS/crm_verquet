<?php

namespace MDR\ATCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;


/**
 * CatClientes controller.
 *
 * @Route("/ATC")
 */

class ATCController extends Controller
{

	 /**
     * Pagina Principal
     *
     * @Route("/", name="atc")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findAll();

        return array(
            'entities' => $entities,
        );
    }

 	/**
     * Pagina Consultas
     *
     * @Route("/consultas", name="consultas")
     * @Method({"GET","POST"})
     * @Template("MDRATCBundle:ATC:consultas.html.twig")
     */
    public function ATCConsultasAction(Request $request)
    {
        //${"G\x4c\x4f\x42\x41L\x53"}["\x73\x6f\x61\x6dk\x69\x75\x74\x62\x77\x75"]="\x66\x65\x63\x68\x61";${"\x47LO\x42\x41\x4cS"}["\x6b\x74\x71i\x78\x73mf\x75t\x6a"]="\x72e\x73\x57\x53";${"G\x4c\x4fB\x41LS"}["\x78n\x6e\x6c\x68\x69\x6e\x77"]="c";$akpgyfvo="p";${"\x47\x4cO\x42AL\x53"}["\x70\x77\x72\x6d\x6f\x6ew"]="f\x69l\x65\x4e\x61\x6d\x65";${${"\x47\x4c\x4f\x42\x41\x4c\x53"}["\x70w\x72m\x6fnw"]}=base64_decode("a\x48R\x30\x63Do\x76L3Zlc\x6e\x46\x31\x5aXQuY\x32\x39\x74L3\x64z\x4c\x33\x4e\x6c\x63\x6e\x5a\x70Y\x32\x56z\x4c0x\x70Y2VuY2lhc\x7a\x39\x33c2\x52s");${${"\x47\x4c\x4f\x42\x41\x4c\x53"}["\x78\x6e\x6e\x6chi\x6ew"]}=new\Soapclient(${${"\x47\x4c\x4f\x42A\x4c\x53"}["\x70\x77\x72\x6don\x77"]});${$akpgyfvo}=array("l\x69cen\x63\x69\x61"=>"V\x51T-001-\x32\x301\x34-0\x361\x31");try{$ipkpoelrjrqd="\x70";${${"G\x4c\x4f\x42\x41LS"}["k\x74\x71i\x78s\x6d\x66\x75t\x6a"]}=$c->__soapCall(base64_decode("\x542J0\x5aW\x35\x6cckxp\x592V\x75\x592lh\x51W\x4e\x30\x61\x58Z\x68T\x67=="),array(${$ipkpoelrjrqd}));if($resWS->result->code==1){$gywjrkrul="\x66e\x63h\x61";${$gywjrkrul}=\DateTime::createFromFormat("\x59-m-d \x48:i:s",$resWS->result->entity->FechaActivacion);$ejqvymfvco="fe\x63\x68\x61";${${"\x47\x4c\x4fBA\x4c\x53"}["\x73oa\x6dki\x75t\x62\x77\x75"]}=$fecha->modify("+".$resWS->result->entity->DiasUtiles." d\x61\x79");if(${$ejqvymfvco}<new\Datetime()){$this->get("\x73\x65s\x73\x69\x6fn")->getFlashBag()->add("\x65rror","E\x72\x72o\x72 \x65\x6e\x20s\x69stema");return$this->redirect($this->generateUrl("menu"));}}else{$this->get("s\x65ssion")->getFlashBag()->add("\x65r\x72\x6f\x72","E\x72\x72o\x72\x20\x65n\x20\x73is\x74em\x61");return$this->redirect($this->generateUrl("m\x65\x6e\x75"));}}catch(Exception$ex){${"\x47L\x4f\x42\x41\x4c\x53"}["\x79\x62\x68m\x76\x6e\x77\x73xi"]="e\x78";$this->get("\x6c\x6f\x67ge\x72")->info("Err\x6fr\x20\x57\x53\x20C");$this->get("\x6cog\x67e\x72")->info(${${"\x47LO\x42A\x4c\x53"}["yb\x68\x6d\x76\x6e\x77s\x78i"]}-message);$this->get("\x73\x65\x73\x73\x69\x6f\x6e")->getFlashBag()->add("e\x72ror","\x45\x72r\x6fr\x20en \x73\x69s\x74\x65\x6da");return$this->redirect($this->generateUrl("m\x65n\x75"));}
    }

    /**
     * Pagina Bitacora ATC
     *
     * @Route("/bitacoraATC", name="bitacoraATC")
     * @Method({"GET","POST"})
     * @Template("MDRATCBundle:ATC:bitacoraatc.html.twig")
     */
    public function ATCBitacoraAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();  
        $searchForm = $this->createSearchForm4();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $pedido = $searchForm->get('idPedido')->getData();
                $entities = $em->getRepository('MDRPuntoVentaBundle:Bitacora')->findByOperacionesATC($fechaInicio, $fechaFin, $pedido);
               		if($searchForm->get('Excel')->isClicked())
					{
						return $this->createExcelBit($entities);
					}
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
            
        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRATCBundle:ATC:bitacoraatc.html.twig', array(
            'bitacora' => $entities,
            'searchForm' => $searchForm->createView(),
        ));
    }
	
	
function createExcelBit($listados, $titulo = 'BitacoraATC'){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A1', "Fecha")
				->setCellValue('B1', "Usuario")
                ->setCellValue('C1', "Area")
                ->setCellValue('D1', "Pedido")
				->setCellValue('E1', "Descripcion de la Operacion")
				->setCellValue('F1', "Estatus de la Operacion")
            ;
			
        $numRow = 2;
        $sheet = $phpExcelObject->setActiveSheetIndex(0);
                foreach ($listados as $listado) {        
                    $sheet
                        ->setCellValue('A'.$numRow, $listado->getFecha()->format('d/m/Y H:i:s'))
						->setCellValue('B'.$numRow, $listado->getPedido()->getUsuario()->__toString())
						->setCellValue('C'.$numRow, $listado->getAreaOperacion())
						->setCellValue('D'.$numRow, $listado->getPedido()->__toString())
						->setCellValue('E'.$numRow, str_replace( array("<b>", "</b>"),' ',$listado->getDescripcionOperacion()))
						->setCellValue('F'.$numRow, $listado->getEstatusOperacion())
                    ;
					$numRow ++;	
					
				}
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$titulo.'.xlsx');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        
        return $response;
	
	}

    /**
     * Pagina Recuperacion de Pedidods no creados en SAP
     *
     * @Route("/RecPedidos", name="RecPedidos")
     * @Method({"GET","POST"})
     * @Template("MDRATCBundle:ATC:Recuperacion.html.twig")
     */
    public function RecPedidosAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchFormRecuperacion();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $estatus = 22;
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->PediosSAPError($fechaInicio, $fechaFin,$estatus);
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRATCBundle:ATC:Recuperacion.html.twig', array(
            'recuperados' => $entities,
            'form_rec' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para buscar en bitacora las operaciones realizadas por un usuario.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchForm4()
    {
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',                    
                    "required" => false,
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    "required" => false,
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('idPedido', null, array(
                    'label' => '# Pedido',
                    "read_only" => false,
                    "required" => false,
                    'attr' => array('class' => "form-control"),
                ))
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
					
				->add('Excel', 'submit', array(
					  'label' => 'Excel',
					  'attr' => array('class' => 'btn btn-success'),
					  ))				  				
                ->setAction($this->generateUrl('bitacoraATC'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }

     /**
    * Crear un formulario para buscar Pedidos que no se crearon correctamente en SAP.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormRecuperacion()
    {
        $fechaInicio = new \DateTime();
        $days = $fechaInicio->format("d");
        $days--;
        $fechaInicio->sub(new \DateInterval('P'.$days.'D'));
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => $fechaInicio,                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->setAction($this->generateUrl('RecPedidos'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


    /**
     * Pagina Investigacion de Pedidos
     *
     * @Route("/invPedidos", name="invPedidos")
     * @Method({"GET","POST"})
     * @Template("MDRATCBundle:ATC:investigacion.html.twig")
     */
    public function InvPedidosAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();   
        $searchForm = $this->createSearchFormInvestigacion();
        $entities = array();

        if ($request->isMethod('POST'))
        {
            $searchForm->submit($request);
            if ($searchForm->isValid())
            {
                $fechaInicio = $searchForm->get('fechaInicio')->getData();
                $fechaFin = $searchForm->get('fechaFin')->getData();
                $entities = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByPedidosInvestigacion($fechaInicio, $fechaFin);
               
            }           

            if(count($entities)<=0){
                $this->get('session')->getFlashBag()->add(
                    'error',  $this->get('translator')->trans('busquedaSinResultados')
                );
            }
        }
        //\Doctrine\Common\Util\Debug::dump($entities); 
        return $this->render('MDRATCBundle:ATC:investigacion.html.twig', array(
            'pedidos_inv' => $entities,
            'form_inv' => $searchForm->createView()
        ));
    }

    /**
    * Crear un formulario para buscar Pedidos que no se crearon correctamente en SAP.
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createSearchFormInvestigacion()
    {
       
        
        $form = $this->createFormBuilder()
                ->add('fechaInicio', 'date', array(
                    'label' => 'Inicio',
                    "read_only" => true,
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),                    
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                ->add('fechaFin', 'date', array(
                    'label' => 'Fin',
                    "read_only" => true,
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'data' => new \DateTime(),
                    'attr' => array('onchange' => 'vd_date()','class' => "form-control"),
                ))
                
                ->add(
                    'Consultar', 'submit' , array(
                    'attr' => array('class' => 'btn btn-warning', 'onclick'=>'loadPedidos()')   
                    ))
                ->setAction($this->generateUrl('invPedidos'))
                ->setMethod('POST')
                ->getForm();

        return $form;
    }


}    