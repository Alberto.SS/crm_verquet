<?php

namespace MDR\ATCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MDR\PuntoVentaBundle\Entity\Bitacora;
use MDR\PuntoVentaBundle\Entity\Clientes;
use MDR\PuntoVentaBundle\Entity\ComentariosPedido;
use MDR\PuntoVentaBundle\Entity\Direcciones;
use MDR\PuntoVentaBundle\Entity\Estatus;
use MDR\PuntoVentaBundle\Entity\EstatusTicket;
use MDR\PuntoVentaBundle\Entity\Pedidos;
use MDR\PuntoVentaBundle\Entity\PedidosCancelados;
use MDR\PuntoVentaBundle\Entity\PedidosTickets;
use MDR\PuntoVentaBundle\Entity\TiposCancelacion;






class ATCOperacionesController extends Controller
{

    /**
     * @Route("/clientesJSON", name="clientes_json")
     * @Method("GET")
     */
    public function clientesJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Clientes')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/formaspagoJSON", name="pagos_json")
     * @Method("GET")
     */
    public function formaspagoJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TipoPagos')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/tipollamadasJSON", name="tipo_llamadas_json")
     * @Method("GET")
     */
    public function tipollamadasJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TipoLlamada')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/estatusJSON", name="estatus_json")
     * @Method("GET")
     */
    public function estatusJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Estatus')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/subtipollamadasJSON", name="subtipo_llamadas_json")
     * @Method("GET")
     */
    public function subtipollamadasJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/estatusTicketJSON", name="estatus_ticket_json")
     * @Method("GET")
     */
    public function estatusTicketJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->findall();

        } catch (Exception $ex) {
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    

    /**
     * @Route("/pedidoConsulta", name="pedido_consulta")
     * @Method("POST")
     */
    public function pedidoConsultaAction(Request $request)
    {
        $ids = $request->get('ApedidoConsulta');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['estatus'] = 0;
            $respuesta['pedido'] = '';
            $Ncliente=0;
            $APP=0;
            $APM=0;
            $idCliente=0;
            $idPedido=0;
            $idTipoPago=0;
            $NTarjeta=0;
            $fecha1=0;
            $fecha2=0;

            if($id['Ncliente'] != ""){
                $Ncliente = $id['Ncliente'];
            }
            if($id['App'] != ""){
                $APP = $id['App'];
            }
            if($id['Apm'] != ""){
                $APM = $id['Apm'];
            }
            if($id['idPedido'] != ""){
                $idPedido = $id['idPedido'];
            }
            if($id['NTarjeta'] != ""){
                $NTarjeta = $id['NTarjeta'];
            }
            if($id['Tipo'] != ""){
                $idTipoPago = $id['Tipo'];
            }
            $NumGuia = $id['Nguia'];
            if($id['date1']){
                $fecha1 = $id['date1'];
            }
            if($id['date2']){
                $fecha2 = $id['date2'];    
            }



            $pedidos = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByPedidosCliente($Ncliente,$APP,$APM,$idPedido,$NTarjeta,$idTipoPago,$NumGuia,$fecha1,$fecha2);
            if(count($pedidos)>0){
                $respuesta['estatus'] = 1;
                $respuesta['pedido'] = $pedidos;
            }else{
                $respuesta['pedido'] = 'SIN RESULTADOS DE LA BUSQUEDA';
            }
        }
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/ComentariosPedidoATC",name="comentarios_pedido_ATC")
     * @Method({"GET", "POST"})
     */
    public function ComentariosPedidoATCAction(Request $request)
    {

        $ids = $request->get('ApedidoATC');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['code'] = 0;
            $respuesta['pedidoSAP'] ="Solo se almacenan comentarios del pedido: ".$id['idPedido'];

        foreach ($ids as $id) {

                            if($id['Operacion'] == "SO"){

                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);                            
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                             //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se almacenan <b>COMENTARIOS</b> del pedido ".$id['idPedido']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora);
                                $respuesta['code'] = 1;
                                $em->persist($Comentarios);
                                $em->flush(); 

                            }
                            /*else if($id['Operacion'] == "EC"){
                                
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(23);
                                $pedido -> setEstatus($estatus);
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                             //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se almacena pedido <b>EN ESPERA DE CAMBIO FISICO</b> del pedido ".$id['idPedido']." ".$id['Comentarios']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora);
                                $respuesta['code'] = 1;
                                $em->persist($Comentarios);
                                $em->flush(); 


                            }else if($id['Operacion'] == "ER"){
                                
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(24);
                                $pedido -> setEstatus($estatus);
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                             //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se almacena pedido <b>EN ESPERA DE REEMBOLSO</b>  del pedido ".$id['idPedido']." ".$id['Comentarios']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora);
                                $respuesta['code'] = 1;
                                $em->persist($Comentarios);
                                $em->flush(); 


                            }*/

        }
                //return new Response('WS consumido');
                $response = new Response(json_encode($respuesta));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
        }
    }

    


    /**
     * @Route("/GetComentariosPedido",name="get_comentarios_pedido")
     * @Method({"GET", "POST"})
     */
    public function GetComentariosPedidoATCAction(Request $request)
    {

        $id = $request->get('pedido');

        $em = $this->getDoctrine()->getManager();
        $comentarios = $em->getRepository('MDRPuntoVentaBundle:ComentariosPedido')->findByPedido($id ,  array('fecha' =>'DESC','idComentario' =>'DESC'));
        if(count($comentarios)>0){
            $respuesta = $comentarios;
        }else{
            $respuesta = 0;
        }      

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/GetTicketsPedido",name="get_tickets_pedido")
     * @Method({"GET", "POST"})
     */
    public function GetTicketsPedidoATCAction(Request $request)
    {

        $id = $request->get('pedido');

        $em = $this->getDoctrine()->getManager();
        $tickets = $em->getRepository('MDRPuntoVentaBundle:PedidosTickets')->findByTicketsPedido($id);
        if(count($tickets)>0){
            $respuesta = $tickets;
        }else{
            $respuesta = 0;
        }      

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * @Route("/GetDirreccionesCliente",name="get_direcciones_clientes")
     * @Method({"GET", "POST"})
     */
    public function GetDirreccionesClienteAction(Request $request)
    {

        $id = $request->get('idCliente');

        $em = $this->getDoctrine()->getManager();
        $direcciones = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->findByCliente($id);
        $respuesta = $direcciones;
             

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdateDireccion",name="update_direccion_ATC")
     * @Method({"GET", "POST"})
     */
    public function UpdateDireccionAction(Request $request)
    {

        $ids = $request->get('AupdateDirec');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] ="";
            $respuesta['desc'] ="";

        foreach ($ids as $id) {

                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                if(count($pedido)==1){
                                    $direccion= $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($id['Direccion']);
                                    if(count($direccion)==1){
                                        $pedido->setDireccion($direccion);
                                        $respuesta['desc'] = $direccion -> __toString();
                                        $respuesta['estatus'] = 1;
                                        $em->persist($pedido);
                                        $em->flush();   
                                    }else{
                                        $respuesta['mensaje'] ="No existe la dirección";
                                    }
                                     
                                }else{
                                    $respuesta['mensaje'] ="No existe el pedido";
                                }
                            $respuestas[] = $respuesta;
                                                     

        }
                //return new Response('WS consumido');
                $response = new Response(json_encode($respuesta));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
        }
    }


    /**
     * @Route("/DtlPedidoInvestigacion",name="detalles_pedidos_investigacion")
     * @Method({"GET", "POST"})
     */
    public function DtlPedidoInvestigacionAction(Request $request)
    {

        $id = $request->get('idPedido');
        $em = $this->getDoctrine()->getManager();
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] ="";
        $respuesta['Pedido'] ="";
        $respuesta['Tickets'] ="";




                try { 

                    $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByIdPedido($id);
                    if (count($Pedido)>0) {
                                $respuesta['Pedido'] = $Pedido;             
                                $Tickets = $em->getRepository('MDRPuntoVentaBundle:PedidosTickets')->findByTicketsPedido($id);
                                if(count($Tickets)>0){
                                    $respuesta['Tickets'] = $Tickets;
                                    $respuesta['estatus'] = 1;
                                }else{
                                    $respuesta['mensaje'] ="No tiene tickets este pedido"; 
                                } 
                    } else {
                            $respuesta['mensaje'] ="No existe el pedido";                    
                    }
                                                            

                }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error Pedido INVESTIGACION:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='Error en Pedido';

                }catch(\Exception $ex){   

                    $this->get('logger')->error('Error Pedido INVESTIGACION:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='Error en Pedido';

                }
        


       

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/CrearTickets",name="crear_ticket")
     * @Method({"GET", "POST"})
     */
    public function CrearTicketsAction(Request $request)
    {

        $id = $request->get('idPedido');
        $idticket = $request->get('ticket');
        $comentarios = $request->get('comentarios');
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] ="";
        $respuesta['Pedido'] = $id;




                try { 

                        $PedidoOri = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);

                        if(count($PedidoOri)>0) {
                                $EstatusTicket = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->find($idticket);

                                $NuevoTicket = new PedidosTickets();
                                $NuevoTicket -> setComentarios($comentarios);
                                $NuevoTicket -> setUsuario($user);
                                $NuevoTicket -> setPedido($PedidoOri);
                                $NuevoTicket -> setEstatusTicket($EstatusTicket);
                                $NuevoTicket -> setFecha(new \DateTime());
                                $em->persist($NuevoTicket);
                                
                                $respuesta['estatus'] = 1;
                                $respuesta['mensaje'] ="Ticket generado con exito ";
                                
                                $em->flush();

                        }else{
                           $respuesta['mensaje'] ="No existe el pedido";                 
                        }
                                                        
                                                            

                }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error NUEVO TICKET:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se puede crear el Ticket';

                }catch(\Exception $ex){   

                    $this->get('logger')->error('Error NUEVO TICKET:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se puede crear el Ticket';

                }
        


       

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/CierreInvestigacion",name="cierre_investigacion")
     * @Method({"GET", "POST"})
     */
    public function CierreInvestigacionAction(Request $request)
    {

        $id = $request->get('idPedido');
        $idticket = $request->get('Testatus');
        $comentarios = $request->get('Comentarios');
        $operacion = $request->get('Operacion');
        $motivoCancel = $request->get('Motivo');
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] ="";
        $respuesta['Pedido'] = $id;




                try { 


                    if( $operacion == 'LP' ){

                        $PedidoOri = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);

                        if(count($PedidoOri)>0) {
                                $EstatusTicket = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->find($idticket);

                                $NuevoTicket = new PedidosTickets();
                                $NuevoTicket -> setComentarios($comentarios);
                                $NuevoTicket -> setUsuario($user);
                                $NuevoTicket -> setPedido($PedidoOri);
                                $NuevoTicket -> setEstatusTicket($EstatusTicket);
                                $NuevoTicket -> setFecha(new \DateTime());
                                $em->persist($NuevoTicket);


                                $TipoPago = $PedidoOri -> getPago(); 

                                //TDC
                                if($TipoPago == "TDC"){

                                    $newEstatus =  $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(1);
                                    $PedidoOri -> setEstatus($newEstatus);
                                    $em->persist($PedidoOri);

                                    //Registramos en bitacora                       
                                    $registroBitacora = new Bitacora();
                                    $registroBitacora -> setFecha(new \DateTime());                  
                                    $registroBitacora -> setUsuario($user);
                                    $registroBitacora -> setAreaOperacion("ATC");
                                    $registroBitacora -> setPedido($PedidoOri);                    
                                    $registroBitacora -> setDescripcionOperacion("El pedido <b>EN INVESTIGACIÓN</b> ".$PedidoOri." ha sido <b>LIBERADO TDC</b>");
                                    $registroBitacora -> setEstatusOperacion("LIBERADO");
                                    $em->persist($registroBitacora);

                                    $respuesta['estatus'] = 1;
                                    $respuesta['mensaje'] ="Pedido Liberado con exito";
                                
                                //COD    
                                }else if($TipoPago == "COD"){

                                    $webServicesController = new \MDR\PuntoVentaBundle\Controller\WebServicesController();
                                    $webServicesController->setContainer($this->container);
                                    $respuestaSap = $webServicesController->crearPedido($PedidoOri);

                                    //\Doctrine\Common\Util\Debug::dump($respuestaSap); 

                                    if($respuestaSap['code'] == 1){

                                        //Registro en bitacora                          
                                            $registroBitacora = new Bitacora();
                                            $registroBitacora -> setFecha(new \DateTime());                  
                                            $registroBitacora -> setUsuario($user);
                                            $registroBitacora -> setAreaOperacion("ATC");
                                            $registroBitacora -> setPedido($PedidoOri);                    
                                            $registroBitacora -> setDescripcionOperacion("El pedido <b>EN INVESTIGACIÓN</b> ".$PedidoOri." ha sido <b>LIBERADO COD</b> con numero SAP  ".$PedidoOri->getPedidoSAP());
                                            $registroBitacora -> setEstatusOperacion("LIBERADO");
                                            $em->persist($registroBitacora);

                                            $respuesta['estatus'] = 1;
                                            $respuesta['mensaje'] =" Pedido Liberado con exito";

                                    }else if($respuestaSap['code'] == 0){
                                        if($respuestaSap['message'] == "El pedido ya se ha creado en SAP"){

                                            //Registro en bitacora                          
                                                $registroBitacora = new Bitacora();
                                                $registroBitacora -> setFecha(new \DateTime());                  
                                                $registroBitacora -> setUsuario($user);
                                                $registroBitacora -> setAreaOperacion("ATC");
                                                $registroBitacora -> setPedido($PedidoOri);                    
                                                $registroBitacora -> setDescripcionOperacion("El pedido <b>EN INVESTIGACIÓN</b> ".$PedidoOri." ha sido <b>LIBERADO COD</b> con numero SAP  ".$PedidoOri->getPedidoSAP());
                                                $registroBitacora -> setEstatusOperacion("LIBERADO");
                                                $em->persist($registroBitacora);

                                                $respuesta['estatus'] = 1;
                                                $respuesta['mensaje'] ="Pedido Liberado con exito";

                                        }else{ 

                                            //Registro en bitacora                                                          
                                                $registroBitacora = new Bitacora();
                                                $registroBitacora -> setFecha(new \DateTime());                  
                                                $registroBitacora -> setUsuario($user);
                                                $registroBitacora -> setAreaOperacion("ATC");
                                                $registroBitacora -> setPedido($PedidoOri);                        
                                                $registroBitacora -> setDescripcionOperacion("El pedido <b>EN INVESTIGACIÓN</b> ".$PedidoOri.", ha sido <b>LIBERADO COD</b> pero no se ha generado en SAP");
                                                $registroBitacora -> setEstatusOperacion("ERROR");
                                                $em->persist($registroBitacora);

                                                $respuesta['estatus'] = 1;
                                                $respuesta['mensaje'] ="Pedido Liberado con exito";

                                        }
                                    }




                                }
                                                             
                                

                        }else{
                           $respuesta['mensaje'] ="No existe el pedido";                 
                        }


                    }else if( $operacion == 'DL' ){

                            $PedidoOri = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);

                        if(count($PedidoOri)>0) {

                                $newEstatus =  $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(5);
                                $PedidoOri -> setEstatus($newEstatus);
                                $em->persist($PedidoOri);
                            //Creacion del ticket
                                $EstatusTicket = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->find($idticket);
                                $NuevoTicket = new PedidosTickets();
                                $NuevoTicket -> setComentarios($comentarios);
                                $NuevoTicket -> setUsuario($user);
                                $NuevoTicket -> setPedido($PedidoOri);
                                $NuevoTicket -> setEstatusTicket($EstatusTicket);
                                $NuevoTicket -> setFecha(new \DateTime());
                                $em->persist($NuevoTicket);    

                            //Registro en la tabla PedidosCancelados
                                $PedidosCancelados =  new PedidosCancelados();
                                $PedidosCancelados ->setPedido($PedidoOri);
                                $TipoCancelacion = $em->getRepository('MDRPuntoVentaBundle:TiposCancelacion')->find($motivoCancel);
                                $PedidosCancelados ->setTipoCancelacion($TipoCancelacion); 
                                $comentario = "El pedido ".$PedidoOri." ,ha sido <b>CANCELADO</b>.";                       
                                $PedidosCancelados -> setComentariosCancelacion($comentarios);
                                $PedidosCancelados -> setPedidoSAP(" ");                        
                                $em->persist($PedidosCancelados);

                            //Registro en bitacora                                                          
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());                  
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $registroBitacora -> setPedido($PedidoOri);                        
                                $registroBitacora -> setDescripcionOperacion("El pedido <b>EN INVESTIGACIÓN</b> ".$PedidoOri.", ha sido <b>CANCELADO</b>");
                                $registroBitacora -> setEstatusOperacion("CANCELADO");
                                $em->persist($registroBitacora);

                                $respuesta['estatus'] = 1;
                                $respuesta['mensaje'] ="Pedido CANCELADO";

                        }else{
                           $respuesta['mensaje'] ="No existe el pedido";                 
                        }    

                    }

                    $em->flush();    
                                                        
                                                            

                }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error CIERRE INVESTIGACION:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se a podido cerrar la investigación';

                }catch(\Exception $ex){   

                    $this->get('logger')->error('Error CIERRE INVESTIGACION:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se a podido cerrar la investigación';

                }
        


       

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
