var cp = '';
var cmbColonias = null;
var cmbEstado = null;
var cmbDelegacion = null;
var delegaciones = {};
var idDelegacion = '';
var idColonia = '';
var colonias = {};
var coloniasEstado = {};

document.addEventListener("DOMContentLoaded", function(){
	cmbEstado = document.getElementById("mdr_puntoventabundle_direcciones_estado");
	cmbDelegacion = document.getElementById("mdr_puntoventabundle_direcciones_municipio");
	cmbColonias = document.getElementById("mdr_puntoventabundle_direcciones_colonia");
	txtCP = document.getElementById("mdr_puntoventabundle_direcciones_cp");

	$(cmbEstado).select2({
		allowClear: true
	});
	$(cmbDelegacion).select2({
		allowClear: true
	});
	$(cmbColonias).select2({
		allowClear: true,
		minimumInputLength: 3
	});

	$(cmbEstado).on("change", changeEstado);
	$(cmbDelegacion).on("change", changeDelegacion);
	$(cmbColonias).on("change", changeColonias);
	txtCP.addEventListener("change", cargarDatosCP);
});

function changeEstado()
{
	var id = this.value;
	clearSelect(cmbDelegacion);
	$(cmbDelegacion).select2("val","");
	$(cmbColonias).select2({
		minimumInputLength: 3
	});
	getDelegacionesByEstado(id, handleDelegaciones, {select: cmbDelegacion}, handleColonias, {select: cmbColonias});
}

function changeDelegacion()
{
	var id = this.value;
	var idEstado = cmbEstado.value;
	clearSelect(cmbColonias);
	$(cmbColonias).select2("val","");

	if(parseInt(id) > 0)
	{
		$(cmbColonias).select2({
			minimumInputLength: 0
		});
		getColoniasByDelegacion(id, handleColonias, {select: cmbColonias});
	}
	else if(parseInt(idEstado) > 0)
	{
		$(cmbColonias).select2({
			minimumInputLength: 3
		});
		handleColonias(coloniasEstado[idEstado], {select: cmbColonias});
	}
}

function changeColonias()
{
	var id = this.value;
	var idMunicipio = cmbDelegacion.value;
	var idEstado = cmbEstado.value;
	var encontrado = false;
	if(parseInt(idMunicipio) > 0)
	{
		for (var i = 0; i < colonias[idMunicipio].length && !encontrado; i++) {
			if(id == colonias[idMunicipio][i].idColonia)
			{
				encontrado = true;
				txtCP.value = colonias[idMunicipio][i].cp;
			}
		}
	}
	else if(parseInt(idEstado) > 0)
	{
		for (var i = 0; i < coloniasEstado[idEstado].length && !encontrado; i++) {
			if(id == coloniasEstado[idEstado][i].idColonia)
			{
				encontrado = true;
				$(cmbDelegacion).select2('val', coloniasEstado[idEstado][i].municipio.idMunicipio);
				txtCP.value = coloniasEstado[idEstado][i].cp;
				idColonia = id;
				fireEvent(cmbDelegacion, "change");
			}
		}
	}
}

function getDelegacionesByEstado(idEstado, callback, args, callbackColonia, argsColonia)
{
	if(delegaciones[idEstado] == undefined)
	{
		showLoading("Cargando municipios", $("#mdr_puntoventabundle_direcciones_municipio").parent());
		showLoading("Cargando colonias", $("#mdr_puntoventabundle_direcciones_colonia").parent());
		$.post(urls.sepomex.delegacionesByCP, {idEstado:idEstado}, function(data, textStatus, xhr) {
			if(data.code == 1)
			{
				delegaciones[idEstado] = data.entities;
				coloniasEstado[idEstado] = data.colonias;
			}
			else
			{
				delegaciones[idEstado] = [];
				coloniasEstado[idEstado] = [];
			}
			for (var i = 0; i < coloniasEstado[idEstado].length; i++) {
				var idMunicipio = coloniasEstado[idEstado][i].municipio.idMunicipio;
				if(colonias[idMunicipio] == undefined)
				{
					colonias[idMunicipio] = [];
				}
				colonias[idMunicipio].push(coloniasEstado[idEstado][i]);
			}
			callback(delegaciones[idEstado], args);
			if(idDelegacion == 0)
			{
				callbackColonia(coloniasEstado[idEstado], argsColonia);
			}
		}).always(function(){
			hideLoading($("#mdr_puntoventabundle_direcciones_municipio").parent());
			hideLoading($("#mdr_puntoventabundle_direcciones_colonia").parent());
		});
	}
	else
	{
		callback(delegaciones[idEstado], args);
		if(coloniasEstado[idEstado] != undefined)
		{
		callbackColonia(coloniasEstado[idEstado], argsColonia);
		}
	}
}

function getColoniasByDelegacion(idDelegacion, callback, args)
{
	if(colonias[idDelegacion] == undefined)
	{
		showLoading("Cargando colonias", $("#mdr_puntoventabundle_direcciones_colonia").parent());
		$.post(urls.sepomex.coloniasByCP, {idDelegacion:idDelegacion}, function(data, textStatus, xhr) {
			if(data.code == 1)
			{
				colonias[idDelegacion] = data.entities;
			}
			else
			{
				colonias[idDelegacion] = array();
			}
			callback(colonias[idDelegacion], args);
		}).always(function(){
			hideLoading($("#mdr_puntoventabundle_direcciones_colonia").parent());
		});
	}
	else
	{
		callback(colonias[idDelegacion], args);
	}
}

function handleDelegaciones(entities, args)
{
	var select = args.select;
	clearSelect(select);
	addOption("", 0, select);
	for (var i = 0; i < entities.length; i++) {
		addOption(entities[i].nombre, entities[i].idMunicipio, select);
	}
	if(idDelegacion > 0)
	{
		$(select).select2("val", idDelegacion);
		fireEvent(select, "change");
	}
	idDelegacion = 0;
}

function handleColonias(entities, args)
{
	var select = args.select;
	var oldCP = cp;
	clearSelect(select);
	addOption("", 0, select);
	for (var i = 0; i < entities.length; i++) {
		addOption(entities[i].nombre + " - " + entities[i].cp, entities[i].idColonia, select);
	}
	cp = '';
	if(idColonia > 0)
	{
		$(select).select2("val", idColonia);
		fireEvent(select, "change");
	}
	else
	{
		if(oldCP.trim().length > 0)
		{
			$(cmbColonias).select2("search", oldCP);
		}
	}
	idColonia = 0;
}

function cargarDatosCP()
{
	cp = this.value;
	getColonias(cp, function(entities){
		if(entities.length > 0)
		{
			clearSelect(cmbColonias);
			clearSelect(cmbDelegacion);
			$(cmbColonias).select2("val","");
			$(cmbDelegacion).select2("val","");
			cmbEstado.value = '';
			cmbEstado.value = entities[0].municipio.estado.idEstado;
			addOption(entities[0].municipio.nombre, entities[0].municipio.idMunicipio, cmbDelegacion);
			idDelegacion = entities[0].municipio.idMunicipio;
			fireEvent(cmbEstado, "change");
		}
	});
}

function getColonias(cp, callback)
{
	$.post(urls.sepomex.byCP, {cp: cp}, function(data, textStatus, xhr) {
		if(data.code == 1)
		{
			callback(data.entities);
		}
		else
		{
			data.entities(new Array());
		}
		    
	});
}