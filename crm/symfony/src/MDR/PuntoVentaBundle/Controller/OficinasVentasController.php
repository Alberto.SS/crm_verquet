<?php

namespace MDR\PuntoVentaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/oficinasVentas")
 **/
class OficinasVentasController extends Controller
{

    /**
     * @Route("/preciosParaPedido/{id}", name="oficinas_pedido_precios")
     * @Method("GET")
     */
    public function getOficinasVentasAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:OficinasVentas')->findByPedido($id);
        $response = new Response(json_encode(array(
            'code' => 1,
            'message' => "",
            'entities' => $entities
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
