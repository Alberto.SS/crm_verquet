<?php

namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Nuxiba controller.
 *
 * @Route("/nuxiba")
 */
class NuxibaController extends Controller
{
    /**
     * @Route("/conexion", name="nuxiba_conexion")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
        $maxDigitosTelCliente = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('MAX_DIGITOS_TEL_CLIENTE');
        
        return array(
            'sinMenu'               => true,
            'maxDigitosTelCliente'  => $maxDigitosTelCliente,
            'env'                   => $env,
        );
    }

    /**
     * Remote Inf Nuxiba
     *
     * @Route("/REMOTE-INF.xml", name="nuxiba_remote_inf")
     * @Method("GET")
     */
    public function nuxibaRemoteInfAction()
    {
        $content = $this->renderView('MDRPuntoVentaBundle:Pedidos:REMOTE-INF.xml.twig');
        $response = new Response($content);
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

}
