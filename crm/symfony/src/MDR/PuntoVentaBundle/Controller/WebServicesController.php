<?php
namespace MDR\PuntoVentaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MDR\PuntoVentaBundle\Entity\Clientes;
use MDR\PuntoVentaBundle\Entity\ClienteFlash;
use MDR\PuntoVentaBundle\Entity\Pedidos;
use MDR\PuntoVentaBundle\Entity\PedidosOrdenCobro;
use MDR\PuntoVentaBundle\Entity\Bitacora;
use MDR\PuntoVentaBundle\Entity\Usuario;
use MDR\PuntoVentaBundle\Entity\ListaNegraTarjetas;
use MDR\PuntoVentaBundle\Entity\Telefonos;
use MDR\PuntoVentaBundle\Entity\TiposCancelacion;
use MDR\PuntoVentaBundle\Entity\PedidosCancelados;
use MDR\PuntoVentaBundle\Entity\PagoTarjeta;
use MDR\PuntoVentaBundle\Entity\ProductosPredido;
use MDR\PuntoVentaBundle\Entity\PedidosTickets;
use MDR\PuntoVentaBundle\Entity\ComentariosPedido;
use MDR\PuntoVentaBundle\Entity\Direcciones;
use MDR\PuntoVentaBundle\Entity\TipoTarjeta;
use MDR\PuntoVentaBundle\Entity\Terminales;
use MDR\PuntoVentaBundle\Entity\Bancos;
use MDR\PuntoVentaBundle\Entity\Promociones;
use MDR\PuntoVentaBundle\Entity\BinTerminal;
use MDR\PuntoVentaBundle\Entity\PedidoDescuentos;
use MDR\LegacyBundle\Entity\PedidosPhonemart;
use MDR\LegacyBundle\Entity\ComentariosPhonemart;
use MDR\PuntoVentaBundle\Entity\ReglasFraude;



class WebServicesController extends Controller
{
     public function cobrar($pedido)
    {
        $serverURL = 'http://10.11.20.185/Phonemart';
        $serverScript = 'webServicesServerMSI.php';
        $metodoALlamar = 'cobrar';

        $respuesta = [];
        $respuesta['code'] = 0;
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();

        if(!$user)
        {
            $respuesta['message'] = 'Debe iniciar sesión.';
            return $respuesta;
        }
        if (!$pedido) {
            $respuesta['message'] = 'Pedido vacio';
            return $respuesta;
        }
        /*else
        {
            $cobroExitoso = $em->getRepository("MDRPuntoVentaBundle:PedidosOrdenCobro")->findSuccessByPedido($pedido);
        }*/

        if(!in_array($pedido->getEstatus()->getIdEstatus(), [1, 12]))
        {
            $respuesta['message'] = 'El pedido tiene estatus: '.$pedido->getEstatus()->getDescripcion();
        }

        $pagoTC = $pedido->getPagosTarjeta();
        if(count($pagoTC) == 1)
        {
            $pagoTC = $pagoTC[0];
        }
        else
        {
            $pagoTC = null;
            $respuesta['message'] = 'No se encontro información para realizar el pago';
            return $respuesta;
        }
        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
        if($env == 'Dev')
        {
            $testResWS = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('TEST_RES_COBRO_WS');

            $params = array(
                'idAp'             => 'CRM01',
                'terminal'         => trim($pagoTC->getTerminal()->getCodigo()),
                'cliente'          => $pedido->getCliente()->__toString(),
                'tdc'              => $pagoTC->getNumeroTarjeta(),
                'marca'            => strtoupper(substr($pagoTC->getTipoTarjeta()->getDescripcion(), 0,1)),
                'emisor'           => $pagoTC->getBanco()->getNombre(),
                'fechaVencimiento' => $pagoTC->getVigencia()->format('m/y'),
                'cvv'              => $pagoTC->getCodigoSeguridad(),
                'importe'          => $pedido->getTotalPago(),
                'cp'               => $pedido->getDireccion()->getCp(),
                'calle'            => $pedido->getDireccion()->getCalle(),
                'telefono'         => $pedido->getDireccion()->getTelCasa(),
                'email'            => $pedido->getCliente()->getEmail(),
                'meses'            => $pagoTC->getPromocion()->getCodigo(),
                'usuario'          => $user->getUClave(),
                'idVenta'          => $pedido->getIdPedido(),
                );

            $resWS = new \StdClass;
            if($testResWS == 1)
            {
                $resWS->resultado = 'Aceptado';
                $resWS->auth = '123456';
                $resWS->mensaje = 'El cobro fue exitoso';
            }
            else
            {
                $resWS->resultado = 'Denegado';
                $resWS->auth = '';
                $resWS->mensaje = 'El cobro fue denegado por el banco emisor.33 Tarjeta expirada.';
            }
        }
        else
        {
            $client = new \Soapclient('http://10.11.20.185/Phonemart/webServicesServerMSI.php?wsdl', array(
                'trace'     => 1,
            ));
            
            $params = array(
                'idAp'             => 'CRM01',
                'terminal'         => trim($pagoTC->getTerminal()->getCodigo()),
                'cliente'          => $pedido->getCliente()->__toString(),
                'tdc'              => $pagoTC->getNumeroTarjeta(),
                'marca'            => strtoupper(substr($pagoTC->getTipoTarjeta()->getDescripcion(), 0,1)),
                'emisor'           => $pagoTC->getBanco()->getNombre(),
                'fechaVencimiento' => $pagoTC->getVigencia()->format('m/Y'),
                'cvv'              => $pagoTC->getCodigoSeguridad(),
                'importe'          => $pedido->getTotalPago(),
                'cp'               => $pedido->getDireccion()->getCp(),
                'calle'            => $pedido->getDireccion()->getCalle(),
                'telefono'         => $pedido->getDireccion()->getTelCasa(),
                'email'            => $pedido->getCliente()->getEmail(),
                'meses'            => $pagoTC->getPromocion()->getCodigo(),
                'usuario'          => $user->getUClave(),
                'idVenta'          => $pedido->getIdPedido(),
                );
            $this->get('logger')->debug('params:');
            $this->get('logger')->debug(print_r($params, true));
            
            try {
                $resWSErr = new \StdClass;
                $resWSErr->resultado = 'Error';
                $resWSErr->auth = '';
                $resWS = $client->__soapCall("cobrar", $params);
                $this->get('logger')->debug('res:');
                $this->get('logger')->debug(print_r($resWS, true));
            }catch(\SoapFault $ex){
                $this->get('logger')->error('Error de conexión con WS de cobro.');
                $this->get('logger')->error($ex->getMessage());
                $resWS = $resWSErr;
                $resWS->mensaje = 'Error de conexión con WS de cobro.';
            }catch(\Exception $ex){
                $this->get('logger')->error('Error con WS de cobro.');
                $this->get('logger')->error($ex->getMessage());
                $resWS = $resWSErr;
                $resWS->mensaje = 'Error con WS de cobro.';
            }
            if($client != null)
            {
                try {
                    $this->get('logger')->debug($client->__getLastRequest());
                    $this->get('logger')->debug($client->__getLastResponse());
                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                }
            }
        }
//Registro en PedidosOrdenCobro
        $cobro = new PedidosOrdenCobro();
        $cobro->setPedido($pedido);
        $cobro->setUsuario($user);
        $numTarjeta = $pedido->getPagosTarjeta();
         if(count($numTarjeta) == 1)
        {
            $numTarjeta = $numTarjeta[0];
        }
        $Tarjetapp = $numTarjeta->getNumeroTarjeta();
        $cobro->setNumeroTarjeta($Tarjetapp);
        $cobro->setComentarios($resWS->mensaje);
        $cobro->setFechaCobro(new \DateTime());
        $respuesta['message'] = $resWS->mensaje;
//Registro en bitacora
        $registroBitacora = new Bitacora();
        $registroBitacora -> setFecha(new \DateTime());
        $user = $this->get('security.context')->getToken()->getUser();
        //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
        $registroBitacora -> setUsuario($user);
        $registroBitacora -> setAreaOperacion("Cobranza");
        $registroBitacora -> setPedido($pedido);
        
        $cobrado = false;
        if(isset($resWS->resultado))
        {
           if($resWS->resultado == 'Aceptado')
            {
                $cobrado = true;
            }
        }
        if($cobrado)
        {
            $respuesta['code'] = 1;
            $cobro->setNumeroAutorizacion($resWS->auth);
            $tipoRespuesta = $em->getRepository("MDRPuntoVentaBundle:TipoRespuesta")->find(1);
            $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(2);
            $pedido->setEstatus($estatus);
            $respuesta['message'] = "Se a realizado el cobro del pedido ".$pedido->getIdPedido()." con exito.\nCon el numero de autorizacion ".$resWS->auth.".\nPor un monto de:".$pedido->getTotalPago().".";
            $respuesta['autorizacion'] = $resWS->auth;
        //Registro en bitacora
        $registroBitacora -> setDescripcionOperacion("Se a realizado el cobro del pedido con exito con el numero de autorizacion <b>".$resWS->auth."</b>");
        $registroBitacora -> setEstatusOperacion("Realizado con exito");
        $em->persist($registroBitacora);    
        }
        else
        {   
            $cobro->setNumeroAutorizacion("");
            $tipoRespuesta = $em->getRepository("MDRPuntoVentaBundle:TipoRespuesta")->find(5);
            //Cambiamos el status del pedido a "ERROR DE COBRO"
            $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(17);
            $pedido->setEstatus($estatus);
        //Registro en bitacora     
        $registroBitacora -> setDescripcionOperacion("Error en el intento del cobro: ".$resWS->mensaje);
        $registroBitacora -> setEstatusOperacion("ERROR");
        $em->persist($registroBitacora);
        }
        $cobro->setRespuesta($tipoRespuesta);
        $em->persist($cobro);
        $em->flush();
        $respuesta['entity'] = $cobro;
        $respuesta['pedido'] = $pedido;

        return $respuesta;
    }

    /**
     * @Route("/cobro", name="cobro")
     * @Method({"GET", "POST"})
     */
    public function cobroAction(Request $request)
    {
        $id = $request->get('idPedido');

        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        $respuesta = $this->cobrar($pedido);

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    public function estructuraPedidosWS($items)
    {
        $params = [];
        $params = [];
        $params['IFlagReenvio'] = '';
        $params['IHeaderInput'] = array('item' => array());
        $params['IItemsInput']  = array('item' => array());
        foreach ($items as $item) {
            $param = $this->estructuraPedidoWS($item['pedido'], $item['comentario']);
            if(is_array($param['IItemsInput']['item']))
            {
                $itemsInput = $param['IItemsInput']['item'];
            }
            else
            {
                $itemsInput = array($param['IItemsInput']['item']);
            }
            $params['IHeaderInput']['item'][] = $param['IHeaderInput']['item'];
            //$headersInput = $param['IHeaderInput']['item'];
            // if(is_array($param['IHeaderInput']['item']))
            // {
            //     $headersInput = $param['IHeaderInput']['item'];
            // }
            // else
            // {
            //     $headersInput = array($param['IHeaderInput']['item']);
            // }
            foreach ($itemsInput as $itemInput) {
                $params['IItemsInput']['item'][] = $itemInput;
            }
            // foreach ($headersInput as $headerInput) {
            //     $params['IHeaderInput']['item'][] = $headerInput;
            // }
        }

        return $params;
    }
    public function estructuraPedidoWS($pedido, $comentario = '')
    {
        $em = $this->getDoctrine()->getManager();
        $estados = $em->getRepository('MDRPuntoVentaBundle:Estados')->findall();

        $pagoTC = null;
        if(count($pedido->getPagosTarjeta()) > 0)
        {
            $pagoTC = $pedido->getPagosTarjeta()[0];
        }
        $cobro = null;
        if(count($pedido->getCobros()) > 0)
        {
            $cobro = $pedido->getCobros()[0];
        }
        $detallesPedido = array();
        $cont = 0;

        foreach ($pedido->getProductos() as $detallePedido) {
           
            $detallesPedido['item'][] = array(
                //'item' => array(
                    //ID_PEDIDO_CRM
                    'Bstkd'     => $detallePedido->getPedido()->getIdPedido(),
                    //POSICIÓN
                    'Posnr'     => $detallePedido->getPosicion(),
                    //CANTIDAD
                    'Kwmeng'    => $detallePedido->getCantidad(),
                    //MATERIAL_PRECIO
                    'MatnrCte'  => $detallePedido->getProductoPrecio()->getMaterialPrecio(),
                    //MATERIAL_SAP
                    'MatnrSap'  => $detallePedido->getProductoPrecio()->getProducto()->getCodigoSAP(),
                    //DESCRIPCION
                    'Arktx'     => $detallePedido->getProductoPrecio()->getProducto()->getDescripcion(),
                    //PRECIO
                    'Netwr'     => $detallePedido->getProductoPrecio()->getPrecio(),
                //)
            );
        }
        $claveDescuento = '';
        $importeDescuento = 0;
        if(count($pedido->getDescuentos()) > 0)
        {
            $descuento = $pedido->getDescuentos()[0];
            $claveDescuento = $descuento->getClave();
            $importeDescuento = $descuento->getImporteSinIva();
        }
        if(strlen($claveDescuento) > 5)
        {
            $claveDescuento = substr($claveDescuento, 0, 5);
        }
        $claveEstado = '';
        foreach ($estados as $estado) {
            if( $pedido->getDireccion()->getEstado() == $estado->getNombre())
            {
                $claveEstado = $estado->getClaveSAP();
            }
        }
        $params = array(
        	//utf8_decode(mb_substr($cad,0,40,'UTF-8'));
            'IFlagReenvio' => '',
            'IHeaderInput' => array(
                'item'  => array(
                    //Número de pedido del cliente
                    'Bstkd'     => $pedido->getIdPedido(),
                    //Fecha del pedido de compras efectuado por el cliente
                    'Bstdk'         => $pedido->getFecha()->format('Ymd'),
                    // Oficina de ventas
                    'Vkbur'         => $pedido->getProductos()[0]->getProductoPrecio()->getOficinaVenta()->getClaveSAP(),
                    // Organización de ventas
                    'SalesOrg'      => $pedido->getProductos()[0]->getProductoPrecio()->getOrganizacionVentas(),
                    // Canal de distribucióin
                    'DistrChan'     => $pedido->getProductos()[0]->getProductoPrecio()->getCanalDistribucion(),
                    // Vía de pago
                    'Zlsch'         => $pedido->getPago()->getClaveSAP(),
                    // Grupo de clientes 1
                    'Kvgr1'         => $pedido->getEstatus()->getClaveSAP(),
                    // Nombre 1 40 carac.
                    'Name1'         => mb_substr($pedido->getcliente()->getNombreCompleto(),0,40,'UTF-8'),
                    // Nombre 2
                    'Name2'         => '',
                    // Dirección de correo electrónico 241 carac.
                    'SmtpAddr'      => mb_substr($pedido->getCliente()->getEmail(),0,241,'UTF-8'),
                    // Primer número teléfono: Prefijo + número 30 carc.
                    'TelNumber'     => substr($pedido->getDireccion()->getTelCasa(),0,30),
                    // Primer nº teléfono móvil: Prefijo + conexión 30 carc.
                    'MobNumber'     => substr($pedido->getDireccion()->getTelCel(),0,30),
                    // Calle 60 carc.
                    'Street'        => mb_substr($pedido->getDireccion()->getCalle(),0,60,'UTF-8'),
                    // Número Exterior 10 carc.
                    'NumeroExterior'=> mb_substr($pedido->getDireccion()->getNoExt(),0,10,'UTF-8'),
                    // Número Interior 10 carc.
                    'NumeroInterior'=> mb_substr($pedido->getDireccion()->getNoInt(),0,10,'UTF-8'),
                    // Código postal de la población 10 carc.
                    'PostCode1'     => substr($pedido->getDireccion()->getCp(),0,10),
                    // Población 40 carc.
                    'City1'         => mb_substr($pedido->getDireccion()->getMunicipio(),0,40,'UTF-8'),
                    // Distrito 40 carc.
                    'City2'         => mb_substr($pedido->getDireccion()->getColonia(),0,40,'UTF-8'),
                    // Región (Estado federal, "land", provincia, condado)
                    'Region'        => $claveEstado,
                    // Calle 2 40 carc.
                    'StrSuppl1'     => mb_substr($pedido->getDireccion()->getEntreCalles(),0,40,'UTF-8'),
                    // Calle 3
                    'StrSuppl2'     => '',
                    // Valor neto en moneda de documento
                    'Genvio'        => $pedido->getGastosEnvio()->getPrecioSinIva(),
                    // Valor neto en moneda de documento
                    'Total'         => $pedido->getSubTotal(),
                    // Línea de texto 132 carc
                    'Indicaciones'  => mb_substr($pedido->getDireccion()->getReferencia(),0,132,'UTF-8'),
                    // Grupo de clientes 2
                    'Kvgr2'         => $pedido->getMensajeria()->getClaveSAP(),
                    // Ruta
                    'Route'         => $pedido->getMensajeria()->getRuta(),
                    // 20 caract.
                    'Banco'         => $pagoTC == null ? '' : $pagoTC->getTerminal()->getClaveSAP(),
                    // 20 caract.
                    'Tipo'          => $pagoTC == null ? '' : mb_substr($pagoTC->getTipoTarjeta()->getDescripcion(),0,20,'UTF-8'),
                    // 20 caract.
                    'Plazo'         => $pagoTC == null ? '' : (int)$pagoTC->getPromocion()->getCodigo(),
                    // Tarjertas de pago: total de autorizaciones pendientes
                    'Mcobro'        => $pedido->getTotalPago(),
                    // Tarjetas de pago: Número de tarjeta
                    'Tarjeta'       => $pagoTC == null ? '' : $pagoTC->getNumeroTarjeta(),
                    // Tarjetas de pago: núm.autorización
                    'Refbanco'      => $cobro == null ? '' : $cobro->getNumeroAutorizacion(),
                    // 20 caract.
                    'Fcobro'        => $pagoTC == null ? '' : $pagoTC->getFechaCobro()->format('d/m/Y'),
                    // RefDoc
                    'RefDoc'        => $pedido->getPedidoSAP(),
                    // Usuario 35 carc.
                    'UsuarioCc'     => mb_substr($pedido->getUsuario()->getUClave(),0,35,'UTF-8'),
                    // Comentarios 150 carc.
                    'Comentarios'   => mb_substr($comentario,0,150,'UTF-8'),
                    // Código del descuento 5 caracteres
                    'TypDesc'       => substr($claveDescuento,0,5),
                    // Importe del descuento
                    'ImpDesc'       => $importeDescuento,
                    // Campo para pipes :(
                    'Fill1'         => '',
                    )
                ),
            'IItemsInput' => $detallesPedido,
            );

        return $params;
    }

    public function crearPedido($pedido, $comentario = "")
    {
        $items = [];
        $items[] = array('pedido' => $pedido, 'comentario' => $comentario);
        $respuestas = $this->crearPedidos($items);
        if($respuestas['code'] == 0)
        {
            return array('code' => 0, 'message' => $respuestas['message'],'pedido' => $pedido->getIdPedido());
        }
        else
        {
            return $respuestas['items'][$pedido->getIdPedido()];
        }
    }

    public function crearPedidos($itemsIn)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta = array('code' => 0, 'message' => '', 'items' => array());
        $respuestas = [];

        try{
            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(15);
            $ids = [];
            $items = [];
            foreach ($itemsIn as $item) {
                $idPedido = $item['pedido']->getIdPedido();
                $ids[] = $idPedido;
                $items[$idPedido]['comentario'] = $item['comentario'];
                $respuestas[$idPedido] = array('code' => 0, 'message' => 'Error WS', 'pedidoSAP' => $item['pedido']->getIdPedido());
            }
            $pedidos = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findCompleteByIds($ids);
            foreach ($pedidos as $pedido) {
                $pedido->setEstatus($estatus);
                $items[$pedido->getIdPedido()]['pedido'] = $pedido;
            }

            $userLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_USER');
            $passLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_PASS');
            $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
            
            $fileName = dirname(__DIR__).'\\Resources\\WSDL\\WSMDR_013'.$env.'.wsdl';
            $client = new \Soapclient($fileName, array(
                'login'     => $userLogin,
                'password'  => $passLogin,
                'trace'     => 1,
                ));

            $params = $this->estructuraPedidosWS($items);
            $this->get('logger')->info('params');
            $this->get('logger')->info(print_r($params, true));
            $resWS = $client->__soapCall('ZSdmxCc013', array($params));
            $this->get('logger')->info('resWS');
            $this->get('logger')->info(print_r($resWS, true));

            $respuesta['code'] = 1;
            if(is_array($resWS->EOutputSapsalesorder->item))
            {
                $resWSOutputItems = $resWS->EOutputSapsalesorder->item;
            }
            else
            {
                $resWSOutputItems = array();
                $resWSOutputItems[] = $resWS->EOutputSapsalesorder->item;
            }
            $resWSReturnItems = array();
            if(property_exists($resWS->EReturn, 'item'))
            {
                if(is_array($resWS->EReturn->item))
                {
                    $resWSReturnItems = $resWS->EReturn->item;
                }
                else
                {
                    $resWSReturnItems = array($resWS->EReturn->item);
                }
            }

            foreach ($resWSOutputItems as $resWSOutputItem) {
                $idPedido = $resWSOutputItem->Bstkd;
                $this->get('logger')->info('Bstkd: '.$idPedido);
                if(array_key_exists($idPedido, $respuestas))
                {
                    $itemRespuesta = &$respuestas[$idPedido];
                    if($resWSOutputItem->PedidoSap == 'ERROR')
                    {
                        $itemRespuesta['code'] = 0;
                        
                        $resWSReturnItem = null;
                        foreach ($resWSReturnItems as $returnItem) {
                            if($returnItem->Bstkd == $idPedido)
                            {
                                $resWSReturnItem = $returnItem;
                            }
                        }
                        if($resWSReturnItem != null)
                        {
                            if(strlen($resWSReturnItem->ZmessageV1) > 0)
                            {
                                $itemRespuesta['code'] = 1;
                                $itemRespuesta['message'] = 'Ya estaba creado el pedido en SAP';
                                $itemRespuesta['pedidoSAP'] = $resWSReturnItem->ZmessageV1;
                            }
                            else
                            {
                                $itemRespuesta['message'] = $resWSReturnItem->Zmessage;
                            }
                        }
                    }
                    else
                    {
                        $itemRespuesta['code'] = 1;
                        $itemRespuesta['message'] = '';
                        $itemRespuesta['pedidoSAP'] = $resWSOutputItem->PedidoSap;
                    }
                }
                else
                {
                    $this->get('logger')->info('respuesta: no encontrada');
                }
            }

            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
            foreach ($items as $item) {
                $code = 0;
                $idPedido = $item['pedido']->getIdPedido();
                if(array_key_exists($idPedido, $respuestas))
                {
                    $itemRespuesta = &$respuestas[$idPedido];
                    if($itemRespuesta['code'] == 1)
                    {
                        $code = 1;
                        $item['pedido']->setPedidoSAP($itemRespuesta['pedidoSAP']);
                    }
                }
                if($code == 0)
                {
                    $item['pedido']->setEstatus($estatus);
                }
                $em->persist($item['pedido']);
            }
            $this->get('logger')->info('respuestas: ');
            $this->get('logger')->info(print_r($respuestas, true));
            $em->flush();
        }catch(\SoapFault $ex){
            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
            $pedido->setEstatus($estatus);
            $em->persist($pedido);
            $em->flush();
            $this->get('logger')->error($ex->getMessage());
            $respuesta['message'] = 'Sin conexion al WS SAP';
        }catch(\Exception $ex){
            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
            $pedido->setEstatus($estatus);
            $em->persist($pedido);
            $em->flush();
            $this->get('logger')->error($ex->getMessage());
            $respuesta['message'] = 'Sin conexion al WS SAP';
        }
        if($client != null)
        {
            $this->get('logger')->info($client->__getLastRequest());
            $this->get('logger')->info($client->__getLastResponse());
        }
        
        $respuesta['items'] = $respuestas;
        return $respuesta;
    }

    /**
     * @Route("/crearPedidoSAP", name="crear_pedido_SAP")
     * @Method({"GET", "POST"})
     */
    public function crearPedidoSAPAction(Request $request)
    {
        $id = $request->get('idPedido');

        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        $respuesta = $this->crearPedido($pedido);

        $this->get('logger')->info('Respuesta WS');
        $this->get('logger')->info(print_r($respuesta, true));
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/crearPedidosSAP", name="crear_pedidos_SAP")
     * @Method({"GET", "POST"})
     */
    public function crearPedidosSAPAction(Request $request)
    {
        $ids = $request->get('idsPedidos');

        $em = $this->getDoctrine()->getManager();
        $items = array();
        $pedidos = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByIdPedido($ids);
        foreach ($pedidos as $pedido) {
            $items[] = array('pedido' => $pedido, 'comentario' => '');
        }
        
        $respuesta = $this->crearPedidos($items);

        $this->get('logger')->info('Respuesta WS');
        $this->get('logger')->info(print_r($respuesta, true));
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    public function crearPedidoCambioFisico($pedido, $flag, $comentario)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta = [];
        $respuesta['code'] = 0;

        $userLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_USER');
        $passLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_PASS');
        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');

        $fileName = dirname(__DIR__).'\\Resources\\WSDL\\WSMDR_016'.$env.'.wsdl';
        $client = new \Soapclient($fileName, array(
            'login'     => $userLogin,
            'password'  => $passLogin,
            'trace'     => 1,
            ));
        
        $params = $this->estructuraPedidoWS($pedido, $comentario);
        //$params['IFlagReenvio'] = $flag;
        $params['ITipoPedido'] = $flag;
        $this->get('logger')->info('params');
        $this->get('logger')->info(print_r($params, true));
        try{
            $resWS = $client->__soapCall('ZSdmxCc016', array($params));
        }
        catch(\SoapFault $ex){
            $this->get('logger')->error($ex->getMessage());
            $respuesta['flujo'] = [];
            $respuesta['message'] = 'Sin respuesta del WS, intente de nuevo en el boton SAP';
        }catch(\Exception $ex){
            $this->get('logger')->error($ex->getMessage());
            $respuesta['flujo'] = [];
            $respuesta['message'] = 'Sin respuesta del WS, intente de nuevo en el boton SAP';
        }

        $this->get('logger')->info($client->__getLastRequest());
        $this->get('logger')->info($client->__getLastResponse());

        $this->get('logger')->info('resWS');
        $this->get('logger')->info(print_r($resWS, true));

        $pedidoSAP = $resWS->EOutputSapsalesorder->item->PedidoSap;
        if($pedidoSAP == 'ERROR')
        {
            $messageError = '';
            $items = [];
            if(is_array($resWS->EReturn->item))
            {
                $items = $resWS->EReturn->item;
            }
            else
            {
                $items[] = $resWS->EReturn->item;
            }
            foreach ($items as $item) {
                $messageError .= $item->Zmessage.'\n';
            }
            //$estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
            //$pedido->setEstatus($estatus);
            $respuesta['message'] = $messageError;
        }
        else
        {
            $respuesta['code'] = 1;
            $respuesta['pedidoSAP'] = $pedidoSAP;
        }
        //$em->persist($pedido);
        //$em->flush();
        
        return $respuesta;
    }

    public function crearPedidoDevolucion($pedido, $motivo, $comentario = '')
    {
        $em = $this->getDoctrine()->getManager();
        $this->get('logger')->info('crearPedidoDevolucion');
        $respuesta = [];
        $respuesta['code'] = 0;

        $userLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_USER');
        $passLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_PASS');
        $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');

        $fileName = dirname(__DIR__).'\\Resources\\WSDL\\WSMDR_015'.$env.'.wsdl';
        $client = new \Soapclient($fileName, array(
            'login'     => $userLogin,
            'password'  => $passLogin,
            ));
        
        $params = $this->estructuraPedidoWS($pedido, $comentario);
        $params['IMotivo'] = $motivo;
        $this->get('logger')->info('params');
        $this->get('logger')->info(print_r($params, true));
        $resWS = $client->__soapCall('ZSdmxCc015', array($params));
        $this->get('logger')->info('resWS');
        $this->get('logger')->info(print_r($resWS, true));

        $pedidoSAP = $resWS->EOutputSapsalesorder->item->PedidoSap;
        if($pedidoSAP == 'ERROR')
        {
            $messageError = '';
            foreach ($resWS->EReturn->item as $item) {
                $messageError .= $item->Zmessage.'\n';
            }
            //$estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(8);
            //$pedido->setEstatus($estatus);
            $respuesta['message'] = $messageError;
        }
        else
        {
            $respuesta['code'] = 1;
            $respuesta['pedidoSAP'] = $pedidoSAP;
            $pedido->setPedidoSAP($pedidoSAP);
        }
        //$em->persist($pedido);
        //$em->flush();
        
        return $respuesta;
    }

    /**
     * @Route("/crearPedidoCambioFisicoSAP", name="crear_pedido_cambio_fisico_SAP")
     * @Method({"GET", "POST"})
     */
    public function crearPedidoCambioFisicoSAPAction(Request $request)
    {
        $ids = $request->get('ApedidoATC');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {

                    if($id['Operacion'] == "C"){
                        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $respuesta = $this->crearPedidoCambioFisico($pedido,$id['Operacion'],$id['Comentarios']);
                        if(count($respuesta)>0){
                            if($respuesta['code'] == 1){
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                                $em->persist($Comentarios);
                            /*//Cambio Estatus del Pedido a 
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(7);
                                $pedido -> setEstatus($Estatus);
                                $em->persist($pedido);*/
                            //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se realiza <b>AVISO DE CAMBIO FISICO</b> del pedido ".$id['idPedido']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora);                                
                                $em->flush();
                            }    
                        }
                    }else if($id['Operacion'] == "R"){
                        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $respuesta = $this->crearPedidoCambioFisico($pedido,$id['Operacion'],$id['Comentarios']);
                        if(count($respuesta)>0){
                            if($respuesta['code'] == 1){
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                                $em->persist($Comentarios);
                            /*//Cambio Esratus del Pedido a 
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(7);
                                $pedido -> setEstatus($Estatus);
                                $em->persist($pedido);*/
                           //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se realiza <b>AVISO DE REEMBOLSO</b> del pedido ".$id['idPedido']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora); 
                                $em->flush();
                            }  
                        }

                    }else if($id['Operacion'] == "E"){
                        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $respuesta = $this->crearPedidoCambioFisico($pedido,$id['Operacion'],$id['Comentarios']);
                        if(count($respuesta)>0){
                            if($respuesta['code'] == 1){
                            //Registro en ComentariosPedido
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['Tsllamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($pedido);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                                $em->persist($Comentarios);
                            /*//Cambio Esratus del Pedido a 
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(7);
                                $pedido -> setEstatus($Estatus);
                                $em->persist($pedido);*/
                            //Registro en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("ATC");
                                $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($pedido);
                                $registroBitacora -> setDescripcionOperacion("Se realiza <b>AVISO DE REENVIO</b> del pedido ".$id['idPedido']);
                                $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                $em->persist($registroBitacora);                                
                                $em->flush(); 
                            }    
                        }
                    }    

        }
        //return new Response('WS consumido');
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

     public function getFlujoPedido($idPedido)
    {
        $em = $this->getDoctrine()->getManager();
        $respuesta = [];
        $respuesta['code'] = 0;
        $respuesta['flujo'] = [];
        $respuesta['guia'] = '';
        $respuesta['estatusDesc'] = '';
        $respuesta['estatusGlobal'] = '';
        $respuesta['message'] = 'Sin flujo en SAP';

        try{
            $userLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_USER');
            $passLogin = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('WS_SAP_PASS');
            $env = $em->getRepository('MDRPuntoVentaBundle:Parametros')->getValor('_ENV_');
            
            $fileName = dirname(__DIR__).'\\Resources\\WSDL\\WSMDR_020'.$env.'.wsdl';
            $client = new \Soapclient($fileName, array(
           
            'login'     => $userLogin,
            'password'  => $passLogin,
            'trace'     => 1,
            ));
            $params = array(
            'ITpedidoCc' => array(
                'item' => array($idPedido),
            ));

            try{
                $resWS = $client->__soapCall('ZSdmxCc020', array($params));
                $this->get('logger')->info(print_r($resWS, true));

                if(property_exists($resWS->EToutputFlujo, 'item'))
                {
                    $respuesta['message'] = '';
                    $respuesta['code'] = 1;
                    if(is_array($resWS->EToutputFlujo->item))
                    {
                        $respuesta['flujo'] = $resWS->EToutputFlujo->item;
                    }
                    else
                    {
                        $respuesta['flujo'] = array($resWS->EToutputFlujo->item);
                    }
                    $estatusDesc = '';
                    $estatusGlobal = '';
                    $guia = '';
                    foreach ($respuesta['flujo'] as $flujo) {
                        if(strlen($flujo->TipoDocSig) > 0)
                        {
                            $estatusDesc = $flujo->TipoDocSig;
                        }
                        if(strlen($flujo->EstatusGlobal) > 0)
                        {
                            $estatusGlobal = $flujo->EstatusGlobal;
                        }
                        if(strlen($flujo->Guia) > 0)
                        {
                            $guia = $flujo->Guia;
                        }
                    }
                    $respuesta['guia'] = $guia;
                    $respuesta['estatusDesc'] = $estatusDesc;
                }
            }catch(\SoapFault $ex){
                $this->get('logger')->error($ex->getMessage());
                $respuesta['message'] = 'Sin respuesta del WS, intente de nuevo en el boton SAP';
            }catch(\Exception $ex){
                $this->get('logger')->error($ex->getMessage());
                $respuesta['message'] = 'Sin respuesta del WS, intente de nuevo en el boton SAP';
            }
            $this->get('logger')->info($client->__getLastRequest());
            $this->get('logger')->info($client->__getLastResponse());
        }catch(\SoapFault $ex){
            $this->get('logger')->error($ex->getMessage());
            $respuesta['message'] = 'Sin conexion al WS SAP';
        }catch(\Exception $ex){
            $this->get('logger')->error($ex->getMessage());
            $respuesta['message'] = 'Sin conexion al WS SAP';
        }
        

        return $respuesta;
    }

    

    /**
     * @Route("/getFlujoPedidoSAP", name="flujo_pedido_SAP")
     * @Method({"GET", "POST"})
     */
    public function getFlujoPedidoSAPAction(Request $request)
    {
        $id = $request->get('idPedido');

        $em = $this->getDoctrine()->getManager();
        $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id);
        $respuesta = $this->getFlujoPedido($id);
        $respuesta['pedido'] = $pedido;
        if(strlen($respuesta['guia']) > 0)
        {
            $pedido->setNumeroGuia($respuesta['guia']);
            $em->persist($pedido);
            $em->flush();
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdateTerminal",name="terminales_update")
     * @Template()
     */
    public function UpdateTerminalAction(Request $request)
    {

        $ids = $request->get('ApedidosUpdate');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['id'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

            $strQuery = "SELECT p FROM MDRPuntoVentaBundle:PagoTarjeta p
                        JOIN p.pedido pe
                        where p.pedido = :pedido";
            $query = $em->createQuery($strQuery);
            $query->setParameter(':pedido', $id['id']);
            $pagos = $query->getResult();            
                  
            if(count($pagos) == 1)
            {
                $pago = $pagos[0];
                $terminal_inicial = $pago->getTerminal();

                try {
                    if($pago->getTerminal() != $id['terminal'])
                    {
                        $terminal = $em->getRepository('MDRPuntoVentaBundle:Terminales')->find($id['terminal']);
                        $pago->setTerminal($terminal);

                        $registroBitacora = new Bitacora();
	                    $registroBitacora -> setFecha(new \DateTime());
	                    $user = $this->get('security.context')->getToken()->getUser();
                        //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
	                    $registroBitacora -> setUsuario($user);
	                    $registroBitacora -> setAreaOperacion("Cobranza");
	                    $pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['id']);
	                    $registroBitacora -> setPedido($pedido);
	                    $registroBitacora -> setDescripcionOperacion("Actualización de Terminal en el pedido  <b>".$id['id']."</b> cambio terminal <b>".$terminal_inicial."  ->  ".$terminal->getDescripcion()."</b>");
	                    $registroBitacora -> setEstatusOperacion("Realizado con exito");
	                    $em->persist($registroBitacora);

	                    $respuesta['estatus'] = 1;
                    	$em->flush();                       
                    }
                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro pedido '.$id["id"].'.';
            }
            $respuestas[] = $respuesta;
        }

        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdatePrecioPP",name="precio_update")
     * @Template()
     */
    public function UpdatePrecioAction(Request $request)
    {

        $ids = $request->get('ApedidosUpdate');
        $em = $this->getDoctrine()->getManager();
       	$respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['Pedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';
            $newSubtotal = 0;
            $newTotal = 0;

            $productoPedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['Pedido']);
            $genvios = (double)$productoPedido->getGastosEnvio()->getPrecio();
            $subtotal = (double)$productoPedido->getSubTotal();
            $totalpago = (double)$productoPedido->getTotalPago();
            $cantidad = $id['cantidad'];
            $precio = (double)$em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->find($id['Aproducto'])->getPrecio();
            $nprecio = (double)$em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->find($id['Nproducto'])->getPrecio();

             

                    $newSubtotal = $subtotal-($cantidad*$precio);   
                    $newSubtotal = $newSubtotal + ($cantidad*$nprecio);
                    $newTotal = $newSubtotal+$genvios;

                    if($newTotal > 0 ){
                        
                        if($productoPedido){
                                try { 

                                     //ACTUALIZAMOS COSTOS DEL PEDIDO
                                     $productoPedido->setSubTotal($newSubtotal);
                                     $productoPedido->setTotalPago($newTotal);
                                     $em->persist($productoPedido);
                                     //ACTUALIZAMOS RELACION PEDIDO-PRODUCTO
                                     $productoP = $em->getRepository('MDRPuntoVentaBundle:ProductosPedido')->find($id['idDetallePedido']);
                                     $Nproducto = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->find($id['Nproducto']);
                                     $productoP -> setProductoPrecio($Nproducto);
                                     $em->persist($productoP);
                                     //REGISTRAMOS EN BITACORA
                                     $registroBitacora = new Bitacora();
                                     $registroBitacora -> setFecha(new \DateTime());
                                     $user = $this->get('security.context')->getToken()->getUser();
                                     //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                     $registroBitacora -> setUsuario($user);
                                     $registroBitacora -> setAreaOperacion($id['area']);
                                     $PedidoTotales = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['Pedido']);
                                     $registroBitacora -> setPedido($PedidoTotales);
                                     $registroBitacora -> setDescripcionOperacion("Actualización de Precio del producto  <b>".$id['descripcion']."</b> Del pedido <b>".$id['Pedido']."</b> Nuevo precio: <b>".$nprecio."  ->  ".$precio."</b> Nuevo Subtotal: <b>".$newSubtotal."</b> Nuevo Total <b>".$newTotal);
                                     $registroBitacora -> setEstatusOperacion("Realizado con exito");
                                     $em->persist($registroBitacora);
                                     $respuesta['estatus'] = 1;
                                     $em->flush();
                                     $newSubtotal = 0;
                                     $newTotal = 0;

                                } catch (\Exception $ex) {
                                    $this->get('logger')->error($ex->getMessage());
                                    $respuesta['mensaje'] = $ex->getMessage();
                                }

                        }else{
                            $respuesta['mensaje'] = 'No se encontro el pedido '.$id["Pedido"].'.';
                        }

                    }else{
                        $respuesta['mensaje'] = 'El cambio de precio en el producto no es viable el total seria de '.$newTotal.'.';
                    }

               
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdateGastosEnvioPP",name="envio_update")
     * @Template()
     */
    public function UpdateGastosEnvioAction(Request $request)
    {

        $ids = $request->get('ApedidosUpdate');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idpedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';
            $newTotal = 0;

            $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idpedido']);
            $newTotal =  $id['Totalpago']-$id['OGastosEnvio'];
            $newTotal = $newTotal + $id['nGastosEnvio'];

        if($newTotal > 0 ){
            if($Pedido)
            {
                try {
                	//Actualizamos el nuevo Gastos de envio                	
                	$GEnvio = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->find($id['idGN']);
                	$Pedido -> setGastosEnvio($GEnvio);
                	//Actualizamos el nuevo total
                    $Pedido -> setTotalPago($newTotal);
                    $em -> persist($Pedido);

                    $registroBitacora = new Bitacora();
                    $registroBitacora -> setFecha(new \DateTime());
                    $user = $this->get('security.context')->getToken()->getUser();
                    //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                    $registroBitacora -> setUsuario($user);
                    $registroBitacora -> setAreaOperacion("Cobranza");
                    $registroBitacora -> setPedido($Pedido);
                    $registroBitacora -> setDescripcionOperacion("Actualización de Precio <b>GASTOS DE ENVIO</b>,  Del pedido <b>".$id['idpedido']."</b> Nuevo Gasto Envio: <b>".$id['OGastosEnvio']."  ->  ".$id['nGastosEnvio']);
                    $registroBitacora -> setEstatusOperacion("Realizado con exito");
                    $em->persist($registroBitacora);

                    $respuesta['estatus'] = 1;
                    $em->flush();


                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }

            $newSubtotal = 0;
            $newTotal = 0;
            }else{
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idpedido"].'.';
            }
        }else{
            $respuesta['mensaje'] = 'El cambio de GASTOS DE ENVIO en el producto no es viable el total seria de '.$newTotal.'.';
        }
            $respuestas[] = $respuesta;
        }

        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdateCantProductosPedido",name="update_cantidad_producto")
     * @Template()
     */
    public function UpdateCantProductosPedidoAction(Request $request)
    {

        $ids = $request->get('ApedidosUpdate');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;

            //Modifico el subtotal y total del pedido
            $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
        if(count($Pedido)>0){

            $Pedido -> setSubTotal($id['Sub']);
            $Pedido -> setTotalPago($id['total']);
            $em->persist($Pedido);
            //Modificio la cantidad del producto-pedido
            $productoPedido = $em->getRepository('MDRPuntoVentaBundle:ProductosPedido')->find($id['Detalle']);
        if(count($productoPedido)>0){

            $productoPedido -> setCantidad($id['Cant']);
            $ProductoPrecio = $productoPedido -> getProductoPrecio();
            $Producto = $ProductoPrecio -> getProducto();
            $DescripcionProducto = $Producto -> getDescripcion();
            $em->persist($productoPedido);
            //Registro en bitacora
            $registroBitacora = new Bitacora();
            $registroBitacora -> setFecha(new \DateTime());
            $user = $this->get('security.context')->getToken()->getUser();
            //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
            $registroBitacora -> setUsuario($user);
            $registroBitacora -> setAreaOperacion($id['area']);
            $registroBitacora -> setPedido($Pedido);
            $registroBitacora -> setDescripcionOperacion("Se actualizo la cantidad del producto <b>".$DescripcionProducto."</b> del pedido <b>".$id['idPedido']."</b> de <b>".$id['ACant']."</b> a <b>".$id['Cant']."</b>");
            $registroBitacora -> setEstatusOperacion("Realizado con exito");
            $em->persist($registroBitacora);
            $respuesta['estatus'] = 1;
            $em->flush();

        }else{
            $respuesta['mensaje'] = 'El Producto ya no existe en este pedido';
        }    
        }else{
            $respuesta['mensaje'] = 'El pedido no existe';
        }
        
            $respuestas[] = $respuesta;
        }

        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/preciosJSON", name="precios_json")
     * @Template()
     */
    public function getPreciosProductoJsonAction(Request $request)
    {
        $ids = $request->get('ApedidotoPrice');
        $em = $this->getDoctrine()->getManager();        
        $respuestas = array();

        foreach ($ids as $id) {  
            $respuesta = array();
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $Precios = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->findByPrecios($id['idProducto'],$id['OfVtn'],$id['CanDistri'],$id['OrgVtn'],$id['Pdetalle']);

            if(count($Precios)>0){
                    $respuesta['estatus'] = 1;
                    $respuesta['mensaje'] = $Precios;
            }
            $respuestas[] = $respuesta;                     
        }
       
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }


	/**
     * @Route("/terminalesJSON", name="terminales_json")
     * @Method("GET")
     */
    public function terminalesJsonAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
        $message = '';
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Terminales')->findByTerminalesActualizacion();
            $code = 1;

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $code = 0;
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            //'code' => $code,
            //'message' => $message,
            /*'value' =>*/$entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/AllterminalesJSON", name="all_terminales_json")
     * @Method("GET")
     */
    public function AllterminalesJsonAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
        $message = '';
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Terminales')->findall();
            $code = 1;

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $code = 0;
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            //'code' => $code,
            //'message' => $message,
            /*'value' =>*/$entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/GastosEnvioJSON", name="costos_envio_json")
     * @Method("GET")
     */
    public function GastosEnvioJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
        $message = '';
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:GastosEnvio')->findByGastosTDC();
            $code = 1;

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $code = 0;
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            //'code' => $code,
            //'message' => $message,
            /*'value' =>*/$entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/terminalesFisicasJSON", name="terminalesfisicas_json")
     * @Method("GET")
     */
    public function terminalesFisicasJSON()
    {
        
        $em = $this->getDoctrine()->getManager();        
        $message = '';
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Terminales')->findByTerminalesFisicas();
            $code = 1;

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $code = 0;
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            //'code' => $code,
            //'message' => $message,
            /*'value' =>*/$entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/respuestasJSON", name="tiporespuestas_json")
     * @Method("GET")
     */
    public function respuestasJsonAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->findByRespuestasCancelacion();

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/respuestascancelacionJSON", name="respuestascancelaciones_json")
     * @Method("GET")
     */
    public function respuestascancelacionJsonAction()
    {
        
        $em = $this->getDoctrine()->getManager();
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TiposCancelacion')->findall();

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $message = $ex->getMessage();
        }

        $response = new Response(json_encode(
            $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    

	/**
     * @Route("/Supervisor", name="validar_supervisor")
     * @Template()
     */
    public function validateSupervisorAction(Request $request)
    {
    	$ids = $request->get('bioSupervisor');
        $em = $this->getDoctrine()->getManager();        
       	$respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['user'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

						
            $Supervisor = $em->getRepository('MDRPuntoVentaBundle:Usuario')->findBySupervisor($id['user'],strtoupper($id['pass']));
            
            if (count($Supervisor) != 0) {
            	$respuesta['estatus'] = 1;
            	$respuesta['mensaje'] = $Supervisor;
            }else{
            	$respuesta['mensaje'] = 'Error en usuario o contraseña';
            }

             $respuestas[] = $respuesta;
                       
        }
       
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;    
       	
    }

    /**
     * @Route("/CobroManual", name="cobro_manual")
     * @Template()
     */
    public function CobroManualAction(Request $request)
    {

        $ids = $request->get('AcobroManual');
        $em = $this->getDoctrine()->getManager();
       	$respuestas = array();
        $user = $this->get('security.context')->getToken()->getUser();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $Cobromanual = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
             
           
            if($Cobromanual)
            {
                try {
                        //Comprobamos que no exista un cobro previo.
                        $Cobros = $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByOrdenPedido($id['idPedido']);

                        if(count($Cobros)>0){

                            //Cambio a estatus de cobrado
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(2);
                                $Cobromanual -> setEstatus($Estatus);
                                $Monto = $Cobromanual-> getTotalPago();
                                $em->persist($Cobromanual);
                                //\Doctrine\Common\Util\Debug::dump(); 
                                $em->flush();
                                if(count($Cobros)==1){
                                    $respuesta['mensaje'] = "El Pedido:".$id['idPedido'].", Ya tiene un cobro.</br>terminal: <b>".$Cobromanual->getPagosTarjeta()[0]->getTerminal()->getDescripcion().".</b></br> Con Número de autorización: <b>".$Cobros[0]->getNumeroAutorizacion().".</b> </br></br>Recarge la pagína para actualizar la información de los pedidos y cancele el ultimo cobro realizado";
                                }else{
                                    $mensaje = null;
                                    $totalcobros = count($Cobros);
                                    for($x=0;$x<=$totalcobros-1;$x++){
                                        $mensaje .= "</br>terminal: <b>".$Cobromanual->getPagosTarjeta()[0]->getTerminal()->getDescripcion().".</b> Con Número de autorización: <b>".$Cobros[$x]->getNumeroAutorizacion().".</b>";
                                    }
                                    $respuesta['mensaje'] = "El Pedido:".$id['idPedido'].", Ya tiene registrados los cobros.".$mensaje;
                                }

                        }else{

                             //Cambio a estatus de cobrado
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(2);
                                $Cobromanual -> setEstatus($Estatus);
                                $Monto = $Cobromanual-> getTotalPago();
                                $em->persist($Cobromanual);
                                //Cambio a la nueva terminal fisica
                                $NuevaTerminal = $em->getRepository('MDRPuntoVentaBundle:Terminales')->find($id['Nterminal']);
                                $pagosTC = $Cobromanual->getPagosTarjeta();
                                 if(count($pagosTC) == 1)
                                {
                                 $pagoTC = $pagosTC[0];
                                 $pagoTC ->setTerminal($NuevaTerminal);
                                 $em->persist($pagoTC);
                                
                                //Obtenemos el nuero de tarjeta
                                $numTarjeta = $pagosTC[0];
                                $Tarjetapp = $numTarjeta->getNumeroTarjeta();
                                
                                //Registro numero autorizacion
                                $NumeroAutorizacion =  new PedidosOrdenCobro();
                                $NumeroAutorizacion -> setPedido($Cobromanual);
                                $Respuesta = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(1);
                                $NumeroAutorizacion -> setRespuesta($Respuesta);
                                $NumeroAutorizacion -> setComentarios("Cobro Manual exitoso");
                                $NumeroAutorizacion -> setNumeroAutorizacion($id['numAutorizacion']);
                                $NumeroAutorizacion -> setFechaCobro(new \DateTime());
                                $NumeroAutorizacion -> setNumeroTarjeta($Tarjetapp);
                                $NumeroAutorizacion ->setUsuario($user);
                                $em->persist($NumeroAutorizacion);

                                //Registramos en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("Cobranza");
                                $PedidoTotales = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($PedidoTotales);
                                $registroBitacora -> setDescripcionOperacion("Cobro manual del Pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['numAutorizacion']."</b> en terminal fisica <b>".$NuevaTerminal->getDescripcion()."</b>. Por un total de <b>".$Monto."</b>");
                                $registroBitacora -> setEstatusOperacion($Respuesta->getDescripcion());
                                $em->persist($registroBitacora);

                                $respuesta['estatus'] = 1;
                                $respuesta['mensaje'] = "Cobro manual exitoso Pedido:".$id['idPedido'].".\nNúmero de Autorización: ".$id['numAutorizacion'].".\nMonto: ".$Monto.".";
                                }
                                else
                                {
                                  $respuesta['mensaje'] = 'No se encontro la terminal';
                                }

                                
                                $em->flush();

                        }

                   

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/CancelacionCobro", name="cancelacion_reintento_cobro")
     * @Template()
     */
    public function CobroCanceladoAction(Request $request)
    {

        $ids = $request->get('AcancelacionPedido');
        $em = $this->getDoctrine()->getManager();
       	$respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $CancelCobro = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($CancelCobro)
            {
                try {
                    //ACTUALIZACION DE ESTATUS DEL PEDIDO                 		
                		$Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(5);
                		$CancelCobro -> setEstatus($Estatus);
                    	$em->persist($CancelCobro); 
                		$comentario = "El pedido ha sido <b>CANCELADO</b>. ".$id['Comentarios'].".";

                	//Registro en la tabla PedidosCancelados
                        $PedidosCancelados =  new PedidosCancelados();
                        $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $PedidosCancelados ->setPedido($Pedido);
                        $TipoCancelacion = $em->getRepository('MDRPuntoVentaBundle:TiposCancelacion')->find($id['Tcancelacion']);
                        $PedidosCancelados ->setTipoCancelacion($TipoCancelacion); 
                        $comentario = "El pedido ha sido <b>CANCELADO</b>. Por: ".$id['Comentarios'];                       
                        $PedidosCancelados -> setComentariosCancelacion($comentario);
                        $PedidosCancelados -> setPedidoSAP($CancelCobro -> getPedidoSAP());                        
                        $em->persist($PedidosCancelados);

                    //Registro en ComentariosPedido
                        $Comentarios = new ComentariosPedido();
                        $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['SubLlamada']);
                        $Comentarios -> setTipoLlamada($Tipollamada);
                        $Comentarios -> setPedido($CancelCobro);
                        $Comentarios -> setComentario($id['Comentarios']);
                        $Comentarios -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $Comentarios -> setUsuario($user);
                        $em->persist($Comentarios);
                   
                    //REGISTRO EN BITACORA
                        $registroBitacora = new Bitacora();
	                    $registroBitacora -> setFecha(new \DateTime());
	                    $user = $this->get('security.context')->getToken()->getUser();
	                    $registroBitacora -> setUsuario($user);
	                    $registroBitacora -> setAreaOperacion($id['TArea']);
	                    $PedidoTotales = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
	                    $registroBitacora -> setPedido($PedidoTotales);
                        if($id['blackList'] != 0){
                            $blackList = new ListaNegraTarjetas();
                            $blackList -> setNumeroTarjeta($id['numTarjeta']);
                            $blackList -> setActivo(1);
                            $comentario = $comentario.". La tarjeta a sido registrada en <b>LISTA NEGRA</b> El pedido esta en un estatus <b>".$Estatus->getDescripcion()."</b>";
                            $em->persist($blackList);
                        }else{
                            $comentario = $comentario.". El pedido esta en un estatus <b>".$Estatus->getDescripcion()."</b>";
                        }    
                        $registroBitacora -> setDescripcionOperacion($comentario);	                
	                    $registroBitacora -> setEstatusOperacion($Estatus->getDescripcion());
	                    $em->persist($registroBitacora);
    
                        $respuesta['estatus'] = 1;
                        $em->flush();

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/CancelacionCobroPedido", name="cancelacion_cobro24hrs")
     * @Template()
     */
    public function CancelacionCobroPedidoAction(Request $request)
    {

        $ids = $request->get('ApedidoCancelacionCobro');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $Cobromanual = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($Cobromanual)
            {                          

                try {
                        //Revisamos si no existe mas de un cobro sobre el pedido                        
                        $PedidoCobro =  $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByOrdenPedido($id['idPedido']); 
                    if(count($PedidoCobro)==1){
                       //Actualizo el estatus del pedido a pedido cancelado
                        $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(5);
                        $Cobromanual -> setEstatus($Estatus);
                        $pedidosap = $Cobromanual -> getPedidoSAP();
                        $em->persist($Cobromanual);
                       //Registro en la tabla PedidosCancelados
                        $PedidosCancelados =  new PedidosCancelados();
                        $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $PedidosCancelados ->setPedido($Pedido);
                        $TipoCancelacion = $em->getRepository('MDRPuntoVentaBundle:TiposCancelacion')->find($id['Tcancelacion']);
                        $PedidosCancelados ->setTipoCancelacion($TipoCancelacion); 
                        $comentario = $id['Comentarios']."El pedido ha sido <b>CANCELADO</b>.";                       
                        $PedidosCancelados -> setComentariosCancelacion($comentario);
                        $PedidosCancelados -> setPedidoSAP($pedidosap);                        
                        $em->persist($PedidosCancelados);
                       //Registro en bitacora                          
                        $registroBitacora = new Bitacora();
                        $registroBitacora -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                        $registroBitacora -> setUsuario($user);
                        $registroBitacora -> setAreaOperacion("Cobranza");
                        $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $registroBitacora -> setPedido($Pedido);                    
                        $registroBitacora -> setDescripcionOperacion("Cancelación del Pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['NumAuto']."</b> por motivos <b>".$id['Comentarios']."</b> El pedido esta en un estatus <b>CANCELADO</b>");                                     
                        $registroBitacora -> setEstatusOperacion("Cancelacion exitosa");
                        $em->persist($registroBitacora);
                        $respuesta['estatus'] = 1;
                    }else{
                        foreach($PedidoCobro as $valor){
                            if($valor->getNumeroAutorizacion() == $id['NumAuto']){
                                //Registro en bitacora                          
                                    $registroBitacora = new Bitacora();
                                    $registroBitacora -> setFecha(new \DateTime());
                                    $user = $this->get('security.context')->getToken()->getUser();
                                    //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                    $registroBitacora -> setUsuario($user);
                                    $registroBitacora -> setAreaOperacion("Cobranza");
                                    $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                    $registroBitacora -> setPedido($Pedido);                    
                                    $registroBitacora -> setDescripcionOperacion("Cancelación del Pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['NumAuto']."</b> por motivos <b>".$id['Comentarios']."</b> El pedido tiene mas de un cobro <b> COBRO CANCELADO</b>");                                     
                                    $registroBitacora -> setEstatusOperacion("Cancelacion exitosa");
                                    $em->persist($registroBitacora);
                                    //Actualizamos el cobro del pedido eliminamos el numero de cobro y lo dejamos referenciado 
                                    //en los comentarios
                                    $AntAutorizacion=$valor->getNumeroAutorizacion();
                                    $valor->setComentarios("Este cobro a sido cancelado ya que se cobro mas de una vez el numero de autorización fue <b>".$AntAutorizacion."</b>");
                                    $respuestacobro = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(6);  
                                    $valor->setRespuesta($respuestacobro);
                                    $em->persist($valor);
                                    $respuesta['mensaje'] = 'Se a eliminado el cobro sin afectar el estatus del pedido.';
                                    $respuesta['estatus'] = 1;
                            }
                        }
                    }
                    
                    $em->flush();

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    
    /**
     * @Route("/CCR", name="cierre_conciliacion")
     * @Template()
     */
    public function CCRAction(Request $request)
    {

        $ids = $request->get('ApedidosCCR1');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $PedidoSAP = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $CierreConciliacion = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($CierreConciliacion)
            {
                try {
                        if($id['type'] == "cierre"){
                            //Actualizo el estatus del pedido a cierre
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(3);
                                $CierreConciliacion -> setEstatus($Estatus);
                                $em->persist($CierreConciliacion);
                            //Registro en bitacora                          
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("Cobranza");
                                $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($Pedido);                    
                                $registroBitacora -> setDescripcionOperacion("<b>Cierre</b> del Pedido <b>".$id['idPedido']."</b>");                                     
                                $registroBitacora -> setEstatusOperacion("Cierre exitoso");
                                $em->persist($registroBitacora);
                                $respuesta['estatus'] = 1;
                                $em->flush();    
                        }else if($id['type'] == "conciliacion"){
                            $PedidoSAP = $this->crearPedido($CierreConciliacion);                                

                                if($PedidoSAP['code'] == 1){
                                    //Registro en bitacora                          
                                    $registroBitacora = new Bitacora();
                                    $registroBitacora -> setFecha(new \DateTime());
                                    $user = $this->get('security.context')->getToken()->getUser();
                                    //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                    $registroBitacora -> setUsuario($user);
                                    $registroBitacora -> setAreaOperacion("Cobranza");
                                    $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                    $registroBitacora -> setPedido($Pedido);                    
                                    $registroBitacora -> setDescripcionOperacion("<b>Conciliacion:</b> del Pedido <b>".$id['idPedido']."</b>, se genero en SAP con numero <b>".$PedidoSAP['pedidoSAP']."</b> y esta en un estatus de <b>POR SURTIR</b>");
                                    $registroBitacora -> setEstatusOperacion("Conciliacion exitosa");
                                    $em->persist($registroBitacora);
                                    $respuesta['estatus'] = 1;
                                    $respuesta['mensaje'] = "Pedido ".$id['idPedido']." generado en SAP con exito NumeroSAP: ".$PedidoSAP['pedidoSAP'];
                                    $em->flush(); 
                                }else if($PedidoSAP['code'] == 0){
                                        if($PedidoSAP['message'] == "El pedido ya se ha creado en SAP"){
                                            //Registro en bitacora                          
                                                $registroBitacora = new Bitacora();
                                                $registroBitacora -> setFecha(new \DateTime());
                                                $user = $this->get('security.context')->getToken()->getUser();
                                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                                $registroBitacora -> setUsuario($user);
                                                $registroBitacora -> setAreaOperacion("Cobranza");
                                                $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                                $registroBitacora -> setPedido($Pedido);                    
                                                $registroBitacora -> setDescripcionOperacion("<b>Conciliacion:</b> del Pedido <b>".$id['idPedido']."</b>, se genero en SAP con numero <b>".$CierreConciliacion->getPedidoSAP()."</b> y esta en un estatus de <b>POR SURTIR</b>");
                                                $registroBitacora -> setEstatusOperacion("Conciliacion exitosa");
                                                $em->persist($registroBitacora);
                                                $PedidoSAP['code'] = 1;
                                                $respuesta['estatus'] = 1;
                                                $respuesta['mensaje'] = "Pedido ".$id['idPedido']." generado en SAP con exito NumeroSAP: ".$CierreConciliacion->getPedidoSAP();
                                                $em->flush();
                                        }else{  
                                            //Cambiamos eel estatus del pedido a error de WS SAP                 
                                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(22);
                                                $CierreConciliacion -> setEstatus($Estatus);                         
                                            //Registro en bitacora                                                          
                                                $registroBitacora = new Bitacora();
                                                $registroBitacora -> setFecha(new \DateTime());
                                                $user = $this->get('security.context')->getToken()->getUser();
                                            //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                                $registroBitacora -> setUsuario($user);
                                                $registroBitacora -> setAreaOperacion("Cobranza");
                                                $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                                $registroBitacora -> setPedido($Pedido);                    
                                                $registroBitacora -> setDescripcionOperacion("<b>Conciliacion:</b> El Pedido <b>".$id['idPedido']."</b>, no se ha generado en SAP");
                                                $registroBitacora -> setEstatusOperacion("Conciliacion erronea");
                                                $em->persist($registroBitacora);
                                                $em->persist($CierreConciliacion);
                                                $respuesta['estatus'] = 0;
                                                $respuesta['mensaje'] = $PedidoSAP['message'];
                                                $em->flush(); 
                                        }
                                }
                           
                        }

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    

    /**
     * @Route("/ConfirmacionCobro", name="confirmacion_cobro")
     * @Template()
     */
    public function ConfirmacionCobroAction(Request $request)
    {

        $ids = $request->get('AcobroManual');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();
        $user = $this->get('security.context')->getToken()->getUser();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $ConfirmCobro = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($ConfirmCobro)
            {
                try {
                    //Cambio de estatus a COBRADO
                    $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(2);
                    $ConfirmCobro -> setEstatus($Estatus);
                    $em->persist($ConfirmCobro);
                    //Verificamos si existe el registro del cobro
                    $existPOC = $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByExistCobro($id['idPedido']);

                    if(count($existPOC)==1){
                                $autorizacion = $existPOC[0]-> getNumeroAutorizacion();
                            if($autorizacion == $id['numAutorizacion']){
                                //Registro en bitacora                          
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("Cobranza");
                                $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($Pedido);                    
                                $registroBitacora -> setDescripcionOperacion("Confirmación de cobro del pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['numAutorizacion']."</b>");                                     
                                $registroBitacora -> setEstatusOperacion("Confirmación Exitosa");
                                $em->persist($registroBitacora);
                                $respuesta['mensaje'] = "Confirmación de cobro del pedido: ".$id['idPedido'].".\nCon numero de autorización: ".$id['numAutorizacion'].".\nPor un monto de:".$Pedido->getTotalPago().".";
                                $respuesta['estatus'] = 1;
                                $em->flush(); 
                            }else{
                                $respuesta['mensaje'] = "El numero de autorización es erróneo";
                            }
                    }else if(count($existPOC)==0){
                            $numTarjeta = $ConfirmCobro->getPagosTarjeta();
                             if(count($numTarjeta) == 1)
                            {
                                $numTarjeta = $numTarjeta[0];
                            }
                            $Tarjetapp = $numTarjeta->getNumeroTarjeta();
                            //Registro numero autorizacion
                            $NumeroAutorizacion =  new PedidosOrdenCobro();
                            $NumeroAutorizacion -> setPedido($ConfirmCobro);
                            $Respuesta = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(1);
                            $NumeroAutorizacion -> setRespuesta($Respuesta);
                            $NumeroAutorizacion -> setComentarios("Confirmacion del Cobro exitoso");
                            $NumeroAutorizacion -> setNumeroAutorizacion($id['numAutorizacion']);
                            $NumeroAutorizacion -> setFechaCobro(new \DateTime());
                            $NumeroAutorizacion -> setNumeroTarjeta($Tarjetapp);
                            $NumeroAutorizacion ->setUsuario($user);
                            $em->persist($NumeroAutorizacion);
                            //Registro en bitacora                          
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());
                            
                            //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion("Cobranza");
                            $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                            $registroBitacora -> setPedido($Pedido);                    
                            $registroBitacora -> setDescripcionOperacion("Confirmación de cobro del pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['numAutorizacion']."</b>");                                     
                            $registroBitacora -> setEstatusOperacion("Confirmación Exitosa");
                            $em->persist($registroBitacora);
                            $respuesta['estatus'] = 1;
                            $respuesta['mensaje'] = "Confirmación de cobro del pedido: ".$id['idPedido'].".\nCon numero de autorización: ".$id['numAutorizacion'].".\nPor un monto de:".$Pedido->getTotalPago().".";
                            $em->flush(); 
                    }else{
                        $respuesta['mensaje'] = "Existe más de un registro de cobro con numero de Autorización para este pedido";
                    }

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    } 

   
    /**
     * @Route("/UpdateTarjetaCobro", name="Update_Tarjeta")
     * @Template()
    */
    public function UpdateTarjetaCobroAction(Request $request)
    {

        $ids = $request->get('AupTarjeta');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();
        $user = $this->get('security.context')->getToken()->getUser();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $TCupdateTarjeta = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($TCupdateTarjeta)
            {
                try {
                       //Actualizo la información de la tarjeta
                        $PagoTarjeta = $em->getRepository('MDRPuntoVentaBundle:PagoTarjeta')->findByPedido($id['idPedido']);
                        
                        $PagoTarjeta[0] -> setNumeroTarjeta($id['NTarjeta']);
                        $PagoTarjeta[0] -> setEsTitular($id['IsTitular']);
                        $PagoTarjeta[0] -> setTitular($id['NameTitular']);
                            $Vigencia = new \DateTime($id['Vigencia']);        
                        $PagoTarjeta[0] -> setVigencia($Vigencia);
                        $PagoTarjeta[0] -> setCodigoSeguridad($id['CSeguridad']);
                            $NuevoTipoTarjeta = $em->getRepository('MDRPuntoVentaBundle:TipoTarjeta')->find($id['TTarjeta']);  
                        $PagoTarjeta[0] -> setTipoTarjeta($NuevoTipoTarjeta);
                            $NuevaTerminal = $em->getRepository('MDRPuntoVentaBundle:Terminales')->find($id['TerminalCobro']); 
                        $PagoTarjeta[0] -> setTerminal($NuevaTerminal);
                            $NuevoBanco = $em->getRepository('MDRPuntoVentaBundle:Bancos')->find($id['Banco']); 
                        $PagoTarjeta[0] -> setBanco($NuevoBanco); 
                            $NuevaPromocion = $em->getRepository('MDRPuntoVentaBundle:Promociones')->find($id['Promocion']); 
                        $PagoTarjeta[0] -> setPromocion($NuevaPromocion);
                        $em->persist($PagoTarjeta[0]);

                       //Verificamos si realizo cobro manual 
                        if($id['NumAutorizacion']){
                            //Generamos el cobro manual
                            //Cambio a estatus de cobrado
                                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(2);
                                $TCupdateTarjeta -> setEstatus($Estatus);
                                $Monto = $TCupdateTarjeta-> getTotalPago();
                                $em->persist($TCupdateTarjeta);
                                                                
                                //Obtenemos el numero de tarjeta
                                $numTarjeta = $id['NTarjeta'];
                                
                                //Registro numero autorizacion
                                $NumeroAutorizacion =  new PedidosOrdenCobro();
                                $NumeroAutorizacion -> setPedido($TCupdateTarjeta);
                                $Respuesta = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(1);
                                $NumeroAutorizacion -> setRespuesta($Respuesta);
                                $NumeroAutorizacion -> setComentarios("Cobro Manual exitoso</br> ".$id['Comentarios']);
                                $NumeroAutorizacion -> setNumeroAutorizacion($id['NumAutorizacion']);
                                $NumeroAutorizacion -> setFechaCobro(new \DateTime());
                                $NumeroAutorizacion -> setNumeroTarjeta($numTarjeta);
                                $NumeroAutorizacion ->setUsuario($user);
                                $em->persist($NumeroAutorizacion);

                                //Registramos en bitacora
                                $registroBitacora = new Bitacora();
                                $registroBitacora -> setFecha(new \DateTime());
                                
                                $registroBitacora -> setUsuario($user);
                                $registroBitacora -> setAreaOperacion("Cobranza");
                                $PedidoTotales = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                                $registroBitacora -> setPedido($PedidoTotales);
                                $registroBitacora -> setDescripcionOperacion("Cobro manual del Pedido <b>".$id['idPedido']."</b> con numero de autorización <b>".$id['NumAutorizacion']."</b> en terminal fisica <b>".$NuevaTerminal->getDescripcion()."</b>. Por un total de <b>$".$Monto."</b>");
                                $registroBitacora -> setEstatusOperacion($Respuesta->getDescripcion());
                                $em->persist($registroBitacora);
                                //Registro de comentarios
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['TipoLlamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($TCupdateTarjeta);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                                $em->persist($Comentarios);                   
                                
                                $respuesta['estatus'] = 1;
                                $respuesta['mensaje'] = "Se actualizo la información del pedido:".$id['idPedido'].".\nCon cobro en la terminal fisica: ".$NuevaTerminal->getDescripcion().".\nPor un total de: $".$Monto.".\nCon Número de Autorización: ".$id['NumAutorizacion'];
                                $em->flush();
                                
                        }else{
                            $Monto = $TCupdateTarjeta-> getTotalPago();
                            $Cobrosrealizados = $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByIntentosCobro($id['idPedido'],$id['NTarjeta']);
                           
                            if(count($Cobrosrealizados) >= 3){
                                $respuesta['estatus'] = 2;
                                $respuesta['mensaje'] = "Ya se han realizado 3 intentos de cobro con la tarjeta: ".$id['NTarjeta'];
                                
                                 //Registro de comentarios
                                $Comentarios = new ComentariosPedido();
                                $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['TipoLlamada']);
                                $Comentarios -> setTipoLlamada($Tipollamada);
                                $Comentarios -> setPedido($TCupdateTarjeta);
                                $Comentarios -> setComentario($id['Comentarios']);
                                $Comentarios -> setFecha(new \DateTime());
                                $user = $this->get('security.context')->getToken()->getUser();
                                $Comentarios -> setUsuario($user);
                                $em->persist($Comentarios);
                                $em->flush();  

                            }else{
                                $em->flush();
                                $CobroVirtual = $this->cobrar($TCupdateTarjeta);
                                if($CobroVirtual['code']!=0){

                                    $respuesta['estatus'] = 1;
                                    $respuesta['mensaje'] = "Cobro realizado:</br>Pedido: ".$id['idPedido']."</br>Por un Monto de: $".$Monto."</br>Con Número de Autorización: ".$CobroVirtual['autorizacion'];
                                
                                     //Registro de comentarios
                                    $Comentarios = new ComentariosPedido();
                                    $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['TipoLlamada']);
                                    $Comentarios -> setTipoLlamada($Tipollamada);
                                    $Comentarios -> setPedido($TCupdateTarjeta);
                                    $Comentarios -> setComentario($id['Comentarios']);
                                    $Comentarios -> setFecha(new \DateTime());
                                    $user = $this->get('security.context')->getToken()->getUser();
                                    $Comentarios -> setUsuario($user);
                                    $em->persist($Comentarios);
                                    $em->flush(); 

                                }else{
                                    $respuesta['estatus'] = 2;
                                    $respuesta['mensaje'] = "Error de cobro:</br>".$CobroVirtual['message'];
                                
                                     //Registro de comentarios
                                    $Comentarios = new ComentariosPedido();
                                    $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['TipoLlamada']);
                                    $Comentarios -> setTipoLlamada($Tipollamada);
                                    $Comentarios -> setPedido($TCupdateTarjeta);
                                    $Comentarios -> setComentario($id['Comentarios']);
                                    $Comentarios -> setFecha(new \DateTime());
                                    $user = $this->get('security.context')->getToken()->getUser();
                                    $Comentarios -> setUsuario($user);                                    
                                    $em->persist($Comentarios);
                                    $em->flush(); 
                                }
                            }
                        }

                        

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }      

    /**
     * @Route("/UpdateTarjeta", name="Update_Tarjeta_posfechar")
     * @Template()
    */
    public function UpdateTarjetaAction(Request $request)
    {

        $ids = $request->get('AupTarjeta');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $TCupdateTarjeta = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($TCupdateTarjeta)
            {
                try {
                       //Actualizo la información de la tarjeta
                        $PagoTarjeta = $em->getRepository('MDRPuntoVentaBundle:PagoTarjeta')->findByPedido($id['idPedido']);
                        
                        $PagoTarjeta[0] -> setNumeroTarjeta($id['NTarjeta']);
                        $PagoTarjeta[0] -> setEsTitular($id['IsTitular']);
                        $PagoTarjeta[0] -> setTitular($id['NameTitular']);
                            $Vigencia = new \DateTime($id['Vigencia']);
                        $PagoTarjeta[0] -> setVigencia($Vigencia);
                        $PagoTarjeta[0] -> setCodigoSeguridad($id['CSeguridad']);
                            $NuevoTipoTarjeta = $em->getRepository('MDRPuntoVentaBundle:TipoTarjeta')->find($id['TTarjeta']);  
                        $PagoTarjeta[0] -> setTipoTarjeta($NuevoTipoTarjeta);
                            $NuevaTerminal = $em->getRepository('MDRPuntoVentaBundle:Terminales')->find($id['TerminalCobro']); 
                        $PagoTarjeta[0] -> setTerminal($NuevaTerminal);
                            $NuevoBanco = $em->getRepository('MDRPuntoVentaBundle:Bancos')->find($id['Banco']); 
                        $PagoTarjeta[0] -> setBanco($NuevoBanco); 
                            $NuevaPromocion = $em->getRepository('MDRPuntoVentaBundle:Promociones')->find($id['Promocion']); 
                        $PagoTarjeta[0] -> setPromocion($NuevaPromocion);                        
                            $Posfecho = date_create_from_format('d/m/Y', $id['Posfecho']);                        
                        $PagoTarjeta[0] -> setFechaCobro($Posfecho);
                           

                        $em->persist($PagoTarjeta[0]);
                            $Estatus =  $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(16); 
                        $TCupdateTarjeta -> setEstatus($Estatus);
                        $em->persist($TCupdateTarjeta);
                        
                         //Registro de comentarios
                        $Comentarios = new ComentariosPedido();
                        $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($id['TipoLlamada']);
                        $Comentarios -> setTipoLlamada($Tipollamada);
                        $Comentarios -> setPedido($TCupdateTarjeta);
                        $Comentarios -> setComentario($id['Comentarios']);
                        $Comentarios -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $Comentarios -> setUsuario($user);
                        $em->persist($Comentarios);

                        //Registramos en bitacora
                        $registroBitacora = new Bitacora();
                        $registroBitacora -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $registroBitacora -> setUsuario($user);
                        $registroBitacora -> setAreaOperacion("Cobranza");
                        $PedidoTotales = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                        $registroBitacora -> setPedido($PedidoTotales);
                            $registroBitacora -> setDescripcionOperacion("Actualizacion del pago del pedido <b>".$id['idPedido']."</b> El pedido ha sido postfechado para el dia".$id['Posfecho']);
                        
                        $Respuesta = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(1);
                        $registroBitacora -> setEstatusOperacion("Realizado con exito");
                        $em->persist($registroBitacora);
                        $em->flush();

                        $respuesta['estatus'] = 1;
                        $respuesta['mensaje'] = "Actualizacion del pago del pedido <b>".$id['idPedido']."</b> El pedido ha sido postfechado para el dia <b>".$id['Posfecho']."</b>";                          
                        
                } catch (\Exception $ex) {
                    $respuesta['mensaje'] = $ex->getMessage();
                }
            }else
            {
                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';
            }
            $respuestas[] = $respuesta;
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }      

   
    /**
     * @Route("/DenegarPedido", name="denegar_pedido")
     * @Template()
    */
    public function DenegarPedidoAction(Request $request)
    {

        $ids = $request->get('ApedidoDenegado');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $PedidoDenegado = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);          
           
            if($PedidoDenegado)
            {
                try {
                        //Cambiamos el estatus del pedido a "EN PROCESO DE COBRO"
                            $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(19);
                            $PedidoDenegado->setEstatus($Estatus);
                            $PedidoDenegado->setFecha(new \DateTime());
                            $em->persist($PedidoDenegado);
                        //Registramos en bitacora                       
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();
                            //$user = $em->getRepository('MDRPuntoVentaBundle:Usuario')->find(10);
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion("Cobranza");
                            $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idPedido']);
                            $registroBitacora -> setPedido($Pedido);                    
                            $registroBitacora -> setDescripcionOperacion("El pedido a sido <b>DENEGADO</b> por el banco y no puede ser <b>CONCILIADO</b> el pedido pasara a un estatus <b>CANCELADO POR BANCO</b>.");
                            $registroBitacora -> setEstatusOperacion("Pedido Denegado");
                            $em->persist($registroBitacora);
                        //Actualización de registro del cobro del pedido
                            $OrdenCobro = $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->findByOrdenPedido($id['idPedido']);                          
                            $NumOrden = $OrdenCobro[0]->getIdPedidoCobro();
                            $OrdenPedido =  $em->getRepository('MDRPuntoVentaBundle:PedidosOrdenCobro')->find($NumOrden);
                            $NumAutorizacion = $OrdenPedido -> getNumeroAutorizacion();
                            $OrdenPedido -> setComentarios("Pedido denegado por el banco, el pedido ha pasado a un estatus 'CANCELADO POR BANCO'.</br> El numero de autorización previo era: ".$NumAutorizacion);
                            $OrdenCobro[0] -> setNumeroAutorizacion("");
                            $em->persist($OrdenPedido);
                            $respuesta['estatus'] = 1;
                            $em->flush();

                } catch (\Exception $ex) {
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] = $ex->getMessage();
                }

            }else{

                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';

            }

            $respuestas[] = $respuesta;
        
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

//INFORMACIÓN PARA DIRECCIONES
 

     /**
     * @Route("/ColoniasJSON", name="colonias_json")
     * @Template()
     */
    public function ColoniasJSONAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $TipoBusqueda = $request->get('TP');
        $respuestas = array();

        if($TipoBusqueda == "C"){
            $CodigoPostal  = $request->get('CP'); 
            $respuesta = array();            
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';
            try {
                $entities = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByCp($CodigoPostal);
                if(count($entities)>0){
                    $respuesta['estatus'] = 1;
                    $respuesta['colonias'] = $entities;  
                }else{
                    $respuesta['mensaje'] = "Sin resultados";
                }                          
            }catch (\Exception $ex) {
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = $ex->getMessage();
            }
        }else if($TipoBusqueda == "M"){
            $Municipio  = $request->get('MS'); 
            $respuesta = array();            
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';
            try {
                $entities = $em->getRepository('MDRPuntoVentaBundle:Colonias')->findByMunicipio($Municipio);
                if(count($entities)>0){
                    $respuesta['estatus'] = 1;
                    $respuesta['colonias'] = $entities;  
                }else{
                    $respuesta['mensaje'] = "Sin resultados";
                }                          
            }catch (\Exception $ex) {
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] = $ex->getMessage();
            }
        }
        
        $respuestas[] = $respuesta;

        $response = new Response(json_encode(
                $respuestas
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

     /**
     * @Route("/MunicipiosJSON", name="municipios_json")
     * @Method("GET")
     */
    public function MunicipiosJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
       
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Municipios')->findall();

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $entities  = $ex->getMessage();
        }

        $response = new Response(json_encode(
                $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

     /**
     * @Route("/EstadosJSON", name="estados_json")
     * @Method("GET")
     */
    public function EstadosJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
       
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Estados')->findall();

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $entities  = $ex->getMessage();
        }

        $response = new Response(json_encode(
                $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
     /**
     * @Route("/tiposPagosJSON", name="tipos_pagos_json")
     * @Method("GET")
     */
    public function tiposPagosJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
       
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TipoPagos')->findall();

        } catch (\Exception $ex) {
            $this->get('logger')->error($ex->getMessage());
            $entities  = $ex->getMessage();
        }

        $response = new Response(json_encode(
                $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/tiposTarjetaJSON", name="tipos_tarjeta_json")
     * @Method("GET")
     */
    public function tiposTarjetaJSON()
    {
        
        $em = $this->getDoctrine()->getManager();        
       
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:TipoTarjeta')->findall();

        } catch (\Exception $ex) {
            $entities  = $ex->getMessage();
        }

        $response = new Response(json_encode(
                $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/bancosJSON", name="bancos_json")
     * @Method("GET")
     */
    public function BancosJSONAction()
    {
        
        $em = $this->getDoctrine()->getManager();        
       
        try {
            $entities = $em->getRepository('MDRPuntoVentaBundle:Bancos')->findall();

        } catch (\Exception $ex) {
            $entities  = $ex->getMessage();
        }

        $response = new Response(json_encode(
                $entities
            ));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/promocionesBinJSON",name="promociones_tarjeta_json")
     * @Template()
     */
    public function promocionesBinJSONAction(Request $request)
    {
    	$bin = $request->get('BIN');
    	$em = $this->getDoctrine()->getManager();
        $Promo = array();

        $Promo = $em->getRepository('MDRPuntoVentaBundle:BinTerminal')->findByIdBin($bin);

        $response = new Response(json_encode($Promo));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }	


    /**
     * @Route("/UpdateDireccionC",name="update_direccion")
     * @Template()
     */
    public function UpdateDireccionCAction(Request $request)
    {

        $operacion = $request->get('funcion');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

            if($operacion == "D"){

                    $pedidoOrigen = $request->get('pedido');                    
                    $dir = $request->get('direccion'); 
                    $area = $request->get('area');
                    $llamada = $request->get('stipollamada');
                    $coment = $request->get('comentarios');    

                    $respuesta = array();
                    $respuesta['id'] = $pedidoOrigen;
                    $respuesta['estatus'] = 0;    
                    $respuesta['mensaje'] = '';


                    $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedidoOrigen);
                    if(count($Pedido)>0){
                            $Direccion = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($dir);
                            $Pedido -> setDireccion($Direccion);
                            $em->persist($Pedido);
                        //Registro en bitacora
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion($area);
                            $registroBitacora -> setPedido($Pedido);
                            $registroBitacora -> setDescripcionOperacion("Se ha actualizado la dirección del pedido <b>".$pedidoOrigen."</b>");
                            $registroBitacora -> setEstatusOperacion("Realizado con exito");
                            $em->persist($registroBitacora);
                        //Registro de comentarios
                            $Comentarios = new ComentariosPedido();
                            $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($llamada);
                            $Comentarios -> setTipoLlamada($Tipollamada);
                            $Comentarios -> setPedido($Pedido);
                            $Comentarios -> setComentario($coment);
                            $Comentarios -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();
                            $Comentarios -> setUsuario($user);
                            $em->persist($Comentarios);                   
                            $respuesta['estatus'] = 1;
                            $respuesta['mensaje'] = 'Dirección Actualizada';
                            $em->flush();
                    }else{
                            $respuesta['mensaje'] = 'El pedido no existe';
                    }
                   
            }else if($operacion == "ND"){

                    $ClienteDireccion = $request->get('cliente');
                    $pedidoOrigen = $request->get('pedido');                     
                    $pais = $request->get('d_pais'); 
                    $cp = $request->get('d_cp'); 
                    $estado = $request->get('d_edo'); 
                    $municipio = $request->get('d_mun'); 
                    $colonia = $request->get('d_col'); 
                    $calle = $request->get('d_calle'); 
                    $entreCalles = $request->get('d_ecalles'); 
                    $noExt = $request->get('d_noExt'); 
                    $noInt = $request->get('d_noInt'); 
                    $referencia = $request->get('d_ref'); 
                    $telCasa = $request->get('d_casa'); 
                    $telOficina = $request->get('d_ofi'); 
                    $telCel = $request->get('d_cel');
                    $area = $request->get('area');
                    $llamada = $request->get('stipollamada');
                    $coment = $request->get('comentarios');     

                    $respuesta = array();
                    $respuesta['id'] = $ClienteDireccion;
                    $respuesta['estatus'] = 0;    
                    $respuesta['mensaje'] = '';

                    //Existe el cliente
                    $Cliente = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($ClienteDireccion);
                    if(count($Cliente)>0){
                        //Creo Nueva Direccion
                            $NDireccion = new Direcciones();
                            $NDireccion-> setPais($pais);
                                $nameEdo = $em->getRepository('MDRPuntoVentaBundle:Estados')->find($estado);
                            $NDireccion-> setEstado($nameEdo->getNombre());
                                $nameMunicipio = $em->getRepository('MDRPuntoVentaBundle:Municipios')->find($municipio); 
                            $NDireccion-> setMunicipio($nameMunicipio->getNombre());
                            $NDireccion-> setCp($cp);
                                $nameColonia = $em->getRepository('MDRPuntoVentaBundle:Colonias')->find($colonia); 
                            $NDireccion-> setColonia($nameColonia->getNombre());
                            $NDireccion-> setCalle($calle);
                            $NDireccion-> setEntreCalles($entreCalles);
                            $NDireccion-> setReferencia($referencia);
                            $NDireccion-> setNoExt($noExt);
                            $NDireccion-> setNoInt($noInt);
                            $NDireccion-> setTelCasa($telCasa);
                            $NDireccion-> setTelOficina($telOficina);
                            $NDireccion-> setTelCel($telCel);
                            $NDireccion-> setCliente($Cliente);
                            $em->persist($NDireccion);
                            $em->flush();
                        //Obtengo el id de la nueva direccion y sus datos para modificar el pedido
                            $IdDireccion = $NDireccion->getIdDireccion();   
                            $Direccion = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($IdDireccion);
                            $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedidoOrigen);

                    if(count($Pedido)>0){
                            $Pedido -> setDireccion($Direccion);
                            $em->persist($Pedido);
                        //Registro en bitacora
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion($area);
                            $registroBitacora -> setPedido($Pedido);
                            $registroBitacora -> setDescripcionOperacion("Se ha actualizado la dirección del pedido <b>".$pedidoOrigen."</b> y creado una nueva direccion");
                            $registroBitacora -> setEstatusOperacion("Realizado con exito");
                            $em->persist($registroBitacora);
                        //Registro de comentarios
                            $Comentarios = new ComentariosPedido();
                            $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($llamada);
                            $Comentarios -> setTipoLlamada($Tipollamada);
                            $Comentarios -> setPedido($Pedido);
                            $Comentarios -> setComentario($coment);
                            $Comentarios -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();
                            $Comentarios -> setUsuario($user);
                            $em->persist($Comentarios);                   
                            $respuesta['estatus'] = 1;
                            $respuesta['mensaje'] = 'Dirección Actualizada';
                            $em->flush();
                    }else{
                            $respuesta['mensaje'] = 'El pedido no existe';
                    }

            }else{
                 $respuesta['mensaje'] = 'El cliente no existe';
            }
        }    
            $respuestas[] = $respuesta;

        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/UpdateTipodePago",name="update_tipo_pago")
     * @Template()
     */
    public function UpdateTipodePagoAction(Request $request)
    {
        $pedidoOrigen = $request->get('idPedido');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();
            

        $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedidoOrigen);
            if(count($Pedido)>0){
                $respuesta = array();
                $respuesta['id'] = $pedidoOrigen;
                $respuesta['estatus'] = 0;    
                $respuesta['mensaje'] = '';

                $llamada = $request->get('stipollamada');
                $coment = $request->get('comentarios'); 
                //Actualizo el tipo de pago a COD
                $pago = $em->getRepository('MDRPuntoVentaBundle:TipoPagos')->find(2); 
                $Pedido -> setPago($pago);
                $em->persist($Pedido);
                $em->flush();
              //Crear pedido en sap
                $CrearSAP = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedidoOrigen);
                $PedidoSAP = $this->crearPedido($CrearSAP);                                

                    if($PedidoSAP['code'] == 1){
                      //Registro en bitacora
                        $registroBitacora = new Bitacora();
                        $registroBitacora -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $registroBitacora -> setUsuario($user);
                        $registroBitacora -> setAreaOperacion("Cobranza");
                        $registroBitacora -> setPedido($Pedido);
                        $registroBitacora -> setDescripcionOperacion("Se ha actualizado el pedido <b>".$pedidoOrigen."</b> Cambio su forma de pago a <b>COD</b>, Pedido creado en SAP con numero: <b>".$PedidoSAP['pedidoSAP']."</b>");
                        $registroBitacora -> setEstatusOperacion("Realizado con exito");
                        $em->persist($registroBitacora);
                      //Registro de comentarios
                        $Comentarios = new ComentariosPedido();
                        $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($llamada);
                        $Comentarios -> setTipoLlamada($Tipollamada);
                        $Comentarios -> setPedido($Pedido);
                        $Comentarios -> setComentario($coment);
                        $Comentarios -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $Comentarios -> setUsuario($user);
                        $em->persist($Comentarios);                   
                        $respuesta['estatus'] = 1;
                        $respuesta['mensaje'] = 'Tipo de pago Actualizado, Pedido creado en SAP con numero: <b>'.$PedidoSAP['pedidoSAP'].'</b>';
                        $em->flush();       
                    }else{
                      //Registro en bitacora
                        $registroBitacora = new Bitacora();
                        $registroBitacora -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $registroBitacora -> setUsuario($user);
                        $registroBitacora -> setAreaOperacion("Cobranza");
                        $registroBitacora -> setPedido($Pedido);
                        $registroBitacora -> setDescripcionOperacion("Se ha actualizado el pedido <b>".$pedidoOrigen."</b> Cambio su forma de pago a <b>COD</b> aún no se crea en SAP");
                        $registroBitacora -> setEstatusOperacion("Realizado con exito");
                        $em->persist($registroBitacora);
                      //Registro de comentarios
                        $Comentarios = new ComentariosPedido();
                        $Tipollamada = $em->getRepository('MDRPuntoVentaBundle:SubTipoLlamada')->find($llamada);
                        $Comentarios -> setTipoLlamada($Tipollamada);
                        $Comentarios -> setPedido($Pedido);
                        $Comentarios -> setComentario($coment);
                        $Comentarios -> setFecha(new \DateTime());
                        $user = $this->get('security.context')->getToken()->getUser();
                        $Comentarios -> setUsuario($user);
                        $em->persist($Comentarios);                   
                        $respuesta['estatus'] = 1;
                        $respuesta['mensaje'] = "Tipo de pago Actualizado, El pedido no se pudo crear en SAP\nVerificar en la pantalla de 'Reenvio SAP' para el pedido <b>".$pedidoOrigen."</b>";
                        $em->flush(); 
                    }    
                
            }else{
                $respuesta['mensaje'] = 'El pedido no Existe';
            }

             $respuestas[] = $respuesta;

        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * @Route("/ExistListaNegra",name="exist_lista_negra")
     * @Template()
     */
    public function ExistListaNegraAction(Request $request)
    {
        $NumTarjeta = $request->get('Numero_tarjeta');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();
        $TarjetaBancaria = new \MDR\PuntoVentaBundle\Controller\TarjetasBancariasController();
        $TarjetaBancaria->setContainer($this->container);
      
        $exist = $em->getRepository('MDRPuntoVentaBundle:ListaNegraTarjetas')->findBynumeroTarjeta($NumTarjeta);
        
            if(count($exist)>0){
                $activo = $exist[0] -> getActivo();
                if($activo){
                    $respuesta = array();
                    $respuesta['id'] = $NumTarjeta;
                    $respuesta['estatus'] = 0;    
                    $respuesta['mensaje'] = "Esta tarjeta esta en lista negra.\nIngrese una nueva tarjeta.";
                }else{
                    $datosbancarios = $TarjetaBancaria->validar($NumTarjeta);
                    $respuesta = array();

                    if($datosbancarios['code'] == 1){

                        $respuesta['id'] = $NumTarjeta;
                        $respuesta['estatus'] = $datosbancarios['code'];    
                        $respuesta['mensaje'] = 'La tarjeta es valida no existe en lista negra';
                        $respuesta['dTarjetas'] = $datosbancarios;

                    }else{

                        $respuesta['id'] = $NumTarjeta;
                        $respuesta['estatus'] = $datosbancarios['code'];    
                        $respuesta['mensaje'] = $datosbancarios['message'];

                    }
                }   

            }else{
                $datosbancarios = $TarjetaBancaria->validar($NumTarjeta);
                $respuesta = array();

                if($datosbancarios['code'] == 1){

                    $respuesta['id'] = $NumTarjeta;
                    $respuesta['estatus'] = $datosbancarios['code'];    
                    $respuesta['mensaje'] = 'La tarjeta es valida no existe en lista negra';
                    $respuesta['dTarjetas'] = $datosbancarios;

                }else{

                    $respuesta['id'] = $NumTarjeta;
                    $respuesta['estatus'] = $datosbancarios['code'];    
                    $respuesta['mensaje'] = $datosbancarios['message'];

                }


            }

        $respuestas[] = $respuesta;
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;    
    }

    /**
     * @Route("/UpdateMesualidadesPP", name="update_mensualidades")
     * @Template()
    */
    public function UpdateMesualidadesPPAction(Request $request)
    {

        $ids = $request->get('ApedidosUpdate');
        $em = $this->getDoctrine()->getManager();
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idpedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';


            $PedidoPromocion = $em->getRepository('MDRPuntoVentaBundle:PagoTarjeta')->find($id['PT']);          
           
            if($PedidoPromocion)
            {
                try {
                        //Cambiamos el estatus del pedido a "EN PROCESO DE COBRO"
                            $Promocion = $em->getRepository('MDRPuntoVentaBundle:Promociones')->find($id['Promo']);
                            $PedidoPromocion->setPromocion($Promocion);
                            $em->persist($PedidoPromocion);


                        //Registramos en bitacora                       
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());
                            $user = $this->get('security.context')->getToken()->getUser();    
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion("Cobranza");
                            $Pedido = $em ->getRepository('MDRPuntoVentaBundle:Pedidos')->find($id['idpedido']);
                            $registroBitacora -> setPedido($Pedido);                    
                            $registroBitacora -> setDescripcionOperacion("Se a modificado la promoción del pedido: <b>".$id['idpedido']."</b> a una promocion de <b>".$Promocion->getDescripcion()."</b>");
                            $registroBitacora -> setEstatusOperacion("Exitoso");
                            $em->persist($registroBitacora);
                            $respuesta['estatus'] = 1;
                            $respuesta['mensaje'] = 'Se realizo exitosamente la actualización';
                            $em->flush();


                } catch (\Exception $ex) {
                    $respuesta['mensaje'] = $ex->getMessage();
                }

            }else{

                $respuesta['mensaje'] = 'No se encontro el pedido '.$id["idPedido"].'.';

            }

            $respuestas[] = $respuesta;
        
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/detallepedidoJSON",name="detalle_pedido_json")
     * @Template()
     */
    public function detallepedidoJSONAction(Request $request)
    {
        $idPedido = $request->get('idPedido');
        $em = $this->getDoctrine()->getManager();
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['detalle'] = '';

        $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->findByIdPedido($idPedido);
        if(count($Pedido)>0){
            $respuesta['estatus'] = 1;
            $respuesta['detalle'] = $Pedido;

        }else{

            $respuesta['detalle'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



    /**
     * @Route("/terminalesFisicasLog",name="terminales_fisicas_log")
     * @Template()
     */
    public function terminalesFisicasLogAction(Request $request)
    {
        $idPedido = $request->get('pedido');
        $em = $this->getDoctrine()->getManager();
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

        $comentarios = $request->get('comentario');
        $tarjeta = $request->get('tarjeta');

            $user = $this->get('security.context')->getToken()->getUser();  

        $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($idPedido);
        if(count($Pedido)>0){
            //Cambiamos el estatus a REchazado 
                $Estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(17);
                $Pedido->setEstatus($Estatus);
                $em->persist($Pedido);
            //Registro numero autorizacion
                $pedidoCobro =  new PedidosOrdenCobro();
                $pedidoCobro -> setPedido($Pedido);
                $Respuesta = $em->getRepository('MDRPuntoVentaBundle:TipoRespuesta')->find(5);
                $pedidoCobro -> setRespuesta($Respuesta);
                $pedidoCobro -> setComentarios("<b>TERMINAL FISICA:</b> ".$comentarios);
                $pedidoCobro -> setNumeroAutorizacion("");
                $pedidoCobro -> setFechaCobro(new \DateTime());
                $pedidoCobro -> setNumeroTarjeta($tarjeta);
                $pedidoCobro ->setUsuario($user);
                $em->persist($pedidoCobro);

            //Registramos en bitacora                       
                $registroBitacora = new Bitacora();
                $registroBitacora -> setFecha(new \DateTime());                  
                $registroBitacora -> setUsuario($user);
                $registroBitacora -> setAreaOperacion("Cobranza");
                $registroBitacora -> setPedido($Pedido);                    
                $registroBitacora -> setDescripcionOperacion("Se a registrado un error en la <b>terminal fisica:<b>".$comentarios);
                $registroBitacora -> setEstatusOperacion("Exitoso");
                $em->persist($registroBitacora);
                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = 'Se realizo exitosamente el registro';
                $em->flush();

            

        }else{

            $respuesta['mensaje'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }     


    /**
     * @Route("/eLiminarProductoPedido",name="eliminar_producto_pedido")
     * @Template()
     */
    public function eLiminarProductoPedidoAction(Request $request)
    {
        $idPedido = $request->get('idp');
        $em = $this->getDoctrine()->getManager();
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

        $precio = $request->get('precioP');
        $cantidad = $request->get('canP');
        $descripcion = $request->get('descripcion');
        $detallePP = $request->get('iddetalle');

        $user = $this->get('security.context')->getToken()->getUser();  

        $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($idPedido);
        if(count($Pedido)>0){
                
                //$ProductoPedido = $em->getRepository('MDRPuntoVentaBundle:ProductosPedido')->find($detallePP);
                
                foreach ($Pedido->getProductos() as $producto) {
                    if($producto->getIdDetallePedido() == $detallePP){
                       $Pedido->removeProducto($producto);
                       $em->remove($producto);
                    }
                }
               
                $Pedido->calculaTotal();
                $em->persist($Pedido);
            //Registramos en bitacora                       
                $registroBitacora = new Bitacora();
                $registroBitacora -> setFecha(new \DateTime());                  
                $registroBitacora -> setUsuario($user);
                $registroBitacora -> setAreaOperacion("Cobranza");
                $registroBitacora -> setPedido($Pedido);                    
                $registroBitacora -> setDescripcionOperacion("Se elimina el producto <b>".$descripcion."</b> del pedido <b>".$idPedido."</b> con precio <b>$".$precio."</b> y la cantidad de <b>".$cantidad."pzas.</b>");
                $registroBitacora -> setEstatusOperacion("Exitoso");
                $em->persist($registroBitacora);
                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = 'Se realizo exitosamente el registro';
                $em->flush();

            

        }else{

            $respuesta['mensaje'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    } 

// PEDIDOS PHONEMART ******************************************************************************************

    /**
     * @Route("/phonemartJSON", name="phonemart_json")
     * @Method({"GET", "POST"})
     */
    public function pedidosPhonemartJSONAction(Request $request)
    {
        $ids = $request->get('ApedidoConsulta');
        $em = $this->getDoctrine()->getManager('phonemart');
        $respuestas = array();

        foreach ($ids as $id) {
            $respuesta = array();
            $respuesta['id'] = $id['idPedido'];
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';
          
            
                try {                        
                    $Pedidos = $em->getRepository('MDRLegacyBundle:PedidosPhonemart')->findByPedidosFiltro($id['idPedido'],$id['Cliente'],$id['NTarjeta'],$id['Tipo']);
                    if(count($Pedidos)>0){
                        $respuesta['estatus'] = 1;
                        $respuesta['mensaje'] = $Pedidos;
                    }else{
                        $respuesta['mensaje'] = 'SIN RESULTADOS';
                    }   

                } catch (\Exception $ex) {
                    $respuesta['mensaje'] = $ex->getMessage();
                }

            

            $respuestas[] = $respuesta;
        
        }
        
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

   /**
     * @Route("/updateEstatusPhonemart", name="estatus_phonemart")
     * @Method({"GET", "POST"})
     */
    public function updateEstatusPhonemartAction(Request $request)
    {
        $idPedido = $request->get('pedido');
        $llamada = $request->get('llamada');
        $sllamada = $request->get('sllamada');
        $estatus = $request->get('estatus');
        $comentarios = $request->get('comentarios');
        $user = $this->get('security.context')->getToken()->getUser()->__toString();
        $em = $this->getDoctrine()->getManager('phonemart');
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

        $Pedido = $em->getRepository('MDRLegacyBundle:PedidosPhonemart')->find($idPedido);
        if($Pedido){
            //Cambio de estatus
                $Pedido->setEstatus($estatus);
                $em->persist($Pedido);
            //Comentarios
                $ComentariosPhonemart = new ComentariosPhonemart();
                $ComentariosPhonemart -> setTipo($llamada);                  
                $ComentariosPhonemart -> setSubTipo($sllamada);
                $ComentariosPhonemart -> setComentario("<b>Cambia Estatus por:</b> ".$comentarios);
                $ComentariosPhonemart -> setUsuario($user);                    
                $ComentariosPhonemart -> setFAlta(new \DateTime());
                $ComentariosPhonemart -> setPedido($Pedido);
                $em->persist($ComentariosPhonemart);
                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = 'Se realizo exitosamente el registro';
                $em->flush();

        }else{

            $respuesta['mensaje'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route("/detallepedidoPhonemartJSON",name="detalle_pedido_phonemart_json")
     * @Template()
     */
    public function detallepedidoPhonemartJSONAction(Request $request)
    {
        $idPedido = $request->get('idPedido');
        $em = $this->getDoctrine()->getManager('phonemart');
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['detalle'] = '';

        $Pedido = $em->getRepository('MDRLegacyBundle:PedidosPhonemart')->findByPedido($idPedido);
        if(count($Pedido)>0){
            $respuesta['estatus'] = 1;
            $respuesta['detalle'] = $Pedido;

        }else{

            $respuesta['detalle'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    


   /**
     * @Route("/ComentariosPhonemart", name="comentarios_phonemart")
     * @Method({"GET", "POST"})
     */
    public function ComentariosPhonemartAction(Request $request)
    {
        $idPedido = $request->get('pedido');
        $llamada = $request->get('llamada');
        $sllamada = $request->get('sllamada');
        $comentarios = $request->get('comentarios');
        $user = $this->get('security.context')->getToken()->getUser()->__toString();
        $em = $this->getDoctrine()->getManager('phonemart');
            $respuesta = array();
            $respuesta['id'] = $idPedido;
            $respuesta['estatus'] = 0;
            $respuesta['mensaje'] = '';

        $Pedido = $em->getRepository('MDRLegacyBundle:PedidosPhonemart')->find($idPedido);
        if($Pedido){
            //Comentarios
                $ComentariosPhonemart = new ComentariosPhonemart();
                $ComentariosPhonemart -> setTipo($llamada);                  
                $ComentariosPhonemart -> setSubTipo($sllamada);
                $ComentariosPhonemart -> setComentario($comentarios);
                $ComentariosPhonemart -> setUsuario($user);                    
                $ComentariosPhonemart -> setFAlta(new \DateTime());
                $ComentariosPhonemart -> setPedido($Pedido);
                $em->persist($ComentariosPhonemart);
                $respuesta['estatus'] = 1;
                $respuesta['mensaje'] = 'Se realizo exitosamente el registro';
                $em->flush();

        }else{

            $respuesta['mensaje'] = 'No se encontro el pedido';
            
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

//FUNCIONALIDADES PARA PAUTA SECA ---------------------------------------------------------------------------------------------------------------------

    /**
     * @Route("/CancelacionPS", name="cancelacion_ps")
     * @Method({"GET", "POST"})
     */
    public function CancelacionPSAction(Request $request)
    {
        $idPedido = $request->get('ApedidosLC');
        $comentarios = $request->get('comentarios');
        $TCancelacion = $request->get('tipocancel');

        $user = $this->get('security.context')->getToken()->getUser();       
        $em = $this->getDoctrine()->getManager();
            $respuesta = array();

        foreach ($idPedido as $pedido) {
            
                $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedido);
            if($Pedido){
            //Cambio de estatus    
                $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(5);
                $Pedido->setEstatus($estatus);
                $em->persist($Pedido);

            //Registro en la tabla PedidosCancelados
                $PedidosCancelados =  new PedidosCancelados();
                $PedidosCancelados ->setPedido($Pedido);
                $TipoCancelacion = $em->getRepository('MDRPuntoVentaBundle:TiposCancelacion')->find($TCancelacion);
                $PedidosCancelados ->setTipoCancelacion($TipoCancelacion); 
                $comentario = "El pedido de pauta seca ha sido <b>CANCELADO</b>. Por: ".$comentarios;                       
                $PedidosCancelados -> setComentariosCancelacion($comentario);
                $PedidosCancelados -> setPedidoSAP($Pedido -> getPedidoSAP());                        
                $em->persist($PedidosCancelados);

            //Registramos en bitacora                       
                $registroBitacora = new Bitacora();
                $registroBitacora -> setFecha(new \DateTime());                  
                $registroBitacora -> setUsuario($user);
                $registroBitacora -> setAreaOperacion("Cobranza");
                $registroBitacora -> setPedido($Pedido);                    
                $registroBitacora -> setDescripcionOperacion("El pedido de <b>PAUTA SECA</b> ha sido <b>CANCELADO</b>. Por: ".$comentarios);
                $registroBitacora -> setEstatusOperacion("CANCELADO");
                $em->persist($registroBitacora);

                $respuesta[] = array('id' => $pedido, 'mensaje'=>'Pedido Cancelado'); 
                $em->flush();  
                     
            }else{
                $respuesta[] = array('id' => $pedido, 'mensaje'=>'El pedido no existe');   
            }
        }
        
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }



    /**
     * @Route("/LiberarPS", name="liberar_ps")
     * @Method({"GET", "POST"})
     */
    public function LiberarPSAction(Request $request)
    {
        $idPedido = $request->get('ApedidosLC');

        $user = $this->get('security.context')->getToken()->getUser();       
        $em = $this->getDoctrine()->getManager();
            $respuesta = array();
            $Items = array(); 

        foreach ($idPedido as $pedido) {
                
                $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($pedido);                
                $Fraudes = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findReglasActivas(true);

            if($Pedido){

                
                $Tpago = $Pedido->getPago();
                $tipoPago = $Pedido->getPago()->getIdTipoPago(); //id del pédido 
                $pedidoTelefono = $Pedido->getTelefono();
                //Loop para depurar reglas que se incluiran con respecto al tipo de pago
                foreach ($Fraudes as $valFraudes) {
                   if($valFraudes->getIdPago() == $tipoPago){
                        $Items[] = array('id' => $valFraudes->getIdReglas() , 'valor' => $valFraudes->getValor());
                   }else if($valFraudes->getIdPago() == 3){
                        $Items[] = array('id' => $valFraudes->getIdReglas() , 'valor' => $valFraudes->getValor());
                   }
                } 

                $resultadoInvestigacion = $this->RevisionContraFraudes($pedido,$Items);    

                    if($resultadoInvestigacion['id']==1){


                        $estatus = $em->getRepository("MDRPuntoVentaBundle:Estatus")->find(9);
                        $Pedido->setEstatus($estatus);
                        $em->persist($Pedido);
                        
                        $respuesta[] = array('id' => $pedido, 'mensaje'=>'Pedido enviado a Investigación');  
                        $EstatusTicket = $em->getRepository('MDRPuntoVentaBundle:EstatusTicket')->find(1);
                        $NuevoTicket = new PedidosTickets();
                        $NuevoTicket -> setComentarios($resultadoInvestigacion['respuesta']);
                        $NuevoTicket -> setUsuario($user);
                        $NuevoTicket -> setPedido($Pedido);
                        $NuevoTicket -> setEstatusTicket($EstatusTicket);
                        $NuevoTicket -> setFecha(new \DateTime());
                        $em->persist($NuevoTicket);
                        unset($Items);
                        $Items = array(); 
                        $em->flush();

                        $this->get('logger')->debug('PAUTA SECA: Investigacion dio 1');



                    }else{    

                        if($Tpago == "TDC"){
                            
                        //Cambio de estatus a "SIN COBRO"
                            $estatus = $em->getRepository('MDRPuntoVentaBundle:Estatus')->find(1);
                            $Pedido->setEstatus($estatus);
                            $em->persist($Pedido);

                        //Registramos en bitacora                       
                            $registroBitacora = new Bitacora();
                            $registroBitacora -> setFecha(new \DateTime());                  
                            $registroBitacora -> setUsuario($user);
                            $registroBitacora -> setAreaOperacion("Cobranza");
                            $registroBitacora -> setPedido($Pedido);                    
                            $registroBitacora -> setDescripcionOperacion("El pedido ".$pedido." de <b>PAUTA SECA</b> ha sido <b>LIBERADO TDC</b>");
                            $registroBitacora -> setEstatusOperacion("LIBERADO");
                            $em->persist($registroBitacora);

                            $respuesta[] = array('id' => $pedido, 'mensaje'=>'TDC LIBERADO');  
                            $em->flush(); 


                        }else if($Tpago == "COD"){
                            $PedidoSAP = $this->crearPedido($Pedido);                                

                                        if($PedidoSAP['code'] == 1){
                                            //Registro en bitacora                          
                                            $registroBitacora = new Bitacora();
                                            $registroBitacora -> setFecha(new \DateTime());                  
                                            $registroBitacora -> setUsuario($user);
                                            $registroBitacora -> setAreaOperacion("Cobranza");
                                            $registroBitacora -> setPedido($Pedido);                    
                                            $registroBitacora -> setDescripcionOperacion("El pedido ".$pedido." de <b>PAUTA SECA</b> ha sido <b>LIBERADO COD</b> con número ".$Pedido->getPedidoSAP());
                                            $registroBitacora -> setEstatusOperacion("LIBERADO");
                                            $em->persist($registroBitacora);

                                            $respuesta[] = array('id' => $pedido, 'mensaje'=>'COD LIBERADO CON NÚMERO '.$Pedido->getPedidoSAP()); 
                                            $em->flush(); 

                                        }else if($PedidoSAP['code'] == 0){
                                                if($PedidoSAP['message'] == "El pedido ya se ha creado en SAP"){
                                                    //Registro en bitacora                          
                                                        $registroBitacora = new Bitacora();
                                                        $registroBitacora -> setFecha(new \DateTime());                  
                                                        $registroBitacora -> setUsuario($user);
                                                        $registroBitacora -> setAreaOperacion("Cobranza");
                                                        $registroBitacora -> setPedido($Pedido);                    
                                                        $registroBitacora -> setDescripcionOperacion("El pedido ".$pedido." de <b>PAUTA SECA</b> ha sido <b>LIBERADO COD</b> con número ".$Pedido->getPedidoSAP());
                                                        $registroBitacora -> setEstatusOperacion("LIBERADO");

                                                        $respuesta[] = array('id' => $pedido, 'mensaje'=>'COD LIBERADO CON NÚMERO '.$Pedido->getPedidoSAP());     
                                                        $em->flush();
                                                }else{                                            
                                                    //Registro en bitacora                                                          
                                                        $registroBitacora = new Bitacora();
                                                        $registroBitacora -> setFecha(new \DateTime());                  
                                                        $registroBitacora -> setUsuario($user);
                                                        $registroBitacora -> setAreaOperacion("Cobranza");
                                                        $registroBitacora -> setPedido($Pedido);                        
                                                        $registroBitacora -> setDescripcionOperacion("El Pedido <b>".$pedido."</b> de <b>PAUTA SECA</b>, no se ha generado en SAP");
                                                        $registroBitacora -> setEstatusOperacion("ERROR");
                                                        $em->persist($registroBitacora);

                                                        $respuesta[] = array('id' => $pedido, 'mensaje'=>'COD LIBERADO PERO NO HA SIDO CREADO EN SAP');
                                                        $em->flush();  
                                                }
                                        }
                        }
                    }    
                         
            }else{
                $respuesta[] = array('id' => $pedido, 'mensaje'=>'El pedido no existe');   
            }
        }
        
        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    function RevisionContraFraudes($idPedido,$Items){

        $em = $this->getDoctrine()->getManager();
        
        $Pedido = $em->getRepository('MDRPuntoVentaBundle:Pedidos')->find($idPedido);

        $idCliente = $Pedido->getCliente()->getIdCliente();
        $Cliente = $Pedido->getCliente()->getNombre()." ".$Pedido->getCliente()->getApellidoPaterno();
        if($Pedido->getCliente()->getApellidoMaterno()){
            $Cliente." ".$Pedido->getCliente()->getApellidoMaterno();
        }
        $CP = $Pedido->getDireccion()->getCp();
        $Municipio = utf8_encode($Pedido->getDireccion()->getMunicipio());
        $Colonia = utf8_encode($Pedido->getDireccion()->getColonia());
        $Telefono = $Pedido->getTelefono();
        $esFraude = 0;
        $mensaje = "<b>EL PEDIDO PRESENTA POSIBLE FRAUDE POR:</b>";

        foreach ($Items as $regla) {

            if($regla['id'] == 1){

                //COTEJO DE LISTA NEGRA CLIENTES
                $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnClientes($Cliente);
                if(count($exist)>0){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>El cliente se encuentra en Lista Negra";
                }

            }else if($regla['id'] == 2){

                //COTEJO DE LISTA NEGRA DIRECCIONES y DIRECCIONES STAFF
                $lnDir = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnDirecciones($CP,$Municipio,$Colonia);

                if(count($lnDir)>0){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>La direccion se encuentra en Lista Negra";
                }else{
                    $Staff =  $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findStaffDirecciones($CP,$Municipio,$Colonia);
                    if(count($Staff)>0){
                        $esFraude = 1;
                        $mensaje = $mensaje."</br>La direccion tiene coincidencias con la de un miembro(s) de MDR";
                    }
                }



            }else if($regla['id'] == 3){

                //COTEJO PRODUCTOS C/U POR PEDIDO
                $Productos = $Pedido->getProductos();
                foreach ($Productos as $pindividual) {
                    $clvProducto = $pindividual->getProductoPrecio()->getProducto()->getCodigoSAP();
                    $cantidad = $pindividual->getCantidad();
                    $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findClaveProducto($clvProducto); 
                    if(count($exist)>0){
                        if($cantidad>$exist[0]->getMonto()){
                            $esFraude = 1;
                            $mensaje = $mensaje."</br>Se rebaso el limite de pzas. del producto <b>".$pindividual->getProductoPrecio()->getProducto()->getDescripcion()."</b>";
                        }
                    }
                }

            }else if($regla['id'] == 4){
                
                //COTEJO LISTA NEGRA TARJETAS               
                $Tarjeta = $Pedido->getPagosTarjeta();
                $numTarjeta = $Tarjeta[0]->getNumeroTarjeta();
                $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnTarjetas($numTarjeta); 
                if(count($exist)>0){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>La tarjeta se encuentra Lista Negra";
                }

            }else if($regla['id'] == 5){
                
                //COTEJO LISTA NEGRA TELEFONOS
                $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findLnTelefonos($Telefono); 
                if(count($exist)>0){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>El número Telefonico esta en Lista Negra";
                }

            }else if($regla['id'] == 7){

                //CANTIDAD DE PRODUCTOS POR PEDIDO
                $Productos = $Pedido->getProductos();
                $totalItems = 0;
                foreach ($Productos as $suma) {
                    $totalItems = $totalItems+$suma->getCantidad();
                }
                if($totalItems>$regla['valor']){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>Se ha rebazado el limite de productos por pedido";
                }
                
            }else if($regla['id'] == 8){
                
                //COTEJO PEDIDOS POR DIA                
                $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findPedidosDia($idCliente); 
                if(count($exist)>$regla['valor']){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>Se han excedido el maximo de <b>".$regla['valor']."</b> pedido(s) por dia del cliente";
                }
                

            }else if($regla['id'] == 12){

                //COTEJO ZONAS DE RIESGO
                $exist = $em->getRepository("MDRPuntoVentaBundle:ReglasFraude")->findZonaRiesgo($CP,$Municipio); 
                if(count($exist)>$regla['valor']){
                    $esFraude = 1;
                    $mensaje = $mensaje."</br>La direccion esta en una Zona de Riesgo";
                }

            }

        }

        return $respuesta[] = array('id' => $esFraude  , 'respuesta' => $mensaje );
    }

    /**
     * @Route("/listaNegraNumeros", name="lista_negra_numeros")
     * @Method({"GET", "POST"})
     */
    public function listaNegraNumerosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $tipo = $request->get('tipo');
        $numero = $request->get('idTel');
        
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';


        if($tipo == "B"){

            try{
                $Bloqueado = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->find($numero);
                //\Doctrine\Common\Util\Debug::dump($Bloqueado); 
                if(count($Bloqueado)>0){
                    //Cambio de estatus del numero a BLOQUEADO
                    $Bloqueado -> setEsListaNegra(true);
                    $em->persist($Bloqueado);
                    $em->flush();
                    $respuesta['estatus'] = 1;                            
                    $respuesta['mensaje'] ='Número Bloqueado';

                }else{
                    $respuesta['mensaje'] = 'El número telefonico no existe';
                }
            }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error LISTA NEGRA TELEFONICA(bloqueo):');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';

            }catch(\Exception $ex){   

                    $this->get('logger')->error('Error LISTA NEGRA TELEFONICA(bloqueo):');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';
            }

        }else if($tipo == "L"){

            try{
                $Liberado = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->find($numero);
                if(count($Liberado)>0){
                    //Cambio de estatus del numero a LIBERADO
                    $Liberado -> setEsListaNegra(false);
                    $em->persist($Liberado);
                    $em->flush();
                    $respuesta['estatus'] = 1;                            
                    $respuesta['mensaje'] ='Número Liberado';

                }else{
                    $respuesta['mensaje'] = 'El número telefonico no existe';
                }
            }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error LISTA NEGRA TELEFONICA(liberado):');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';

            }catch(\Exception $ex){   

                    $this->get('logger')->error('Error LISTA NEGRA TELEFONICA(liberado):');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';
            }
        }

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


/* ------------------------------- FLASH ---------------------------------------------------------------------------------*/

    /**
     * @Route("/ClientesFlash", name="clientes_flash")
     * @Method({"GET", "POST"})
     */
    public function clientesFlashJSONAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $nombre = $request->get('nombre');
        $app = $request->get('app');
        $apm = $request->get('apm');
        $sexo = $request->get('sex');
        $telefono = $request->get('tel');
        $slscliente = $request->get('slscliente');
        $DNIS = $request->get('DNIS');
        $telefono2 = $request->get('tel2');
        $comentarios = $request->get('coments');
        $user = $this->get('security.context')->getToken()->getUser();  
        
        $tel1 = null;
        $tel2 = null;
        $respuesta = array();
        $respuesta['estatus'] = 0;
        $respuesta['mensaje'] = '';


        if ($slscliente) {


                try{
                        $ClienteFlash = $em->getRepository('MDRPuntoVentaBundle:Clientes')->find($slscliente);
                        
                    //verificamos si existe el numero
                        $tel1 = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findOneByNumero($telefono);
                        if(!$tel1)
                        {
                            //Agregamos el telefono                            
                            $tel1 = new Telefonos();
                            $tel1 -> setNumero($telefono);
                            $tel1 -> setEsListaNegra(false);

                            $tel1 -> addCliente($ClienteFlash);
                            $ClienteFlash->addTelefono($tel1);

                            $em->persist($ClienteFlash);
                            $em->persist($tel1);
                            $em->flush();
                        }   

                        if($telefono2){
                             $tel2 = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findOneByNumero($telefono2);
                            if(!$tel2)
                                {
                                    //Agregamos el telefono                            
                                    $tel2 = new Telefonos();
                                    $tel2 -> setNumero($telefono2);
                                    $tel2 -> setEsListaNegra(false);
                                }
                            $tel2 -> addCliente($ClienteFlash);
                            $ClienteFlash->addTelefono($tel2);

                            $em->persist($ClienteFlash);
                            $em->persist($tel2);
                            $em->flush();
                        }          

                        $Flash = new ClienteFlash();
                        $Flash -> setFecha(new \Datetime());
                        $Flash -> setTelefono($tel1);
                        $Flash -> setCliente($ClienteFlash);
                        $Flash -> setDnis($DNIS);
                        $Flash -> setUsuario($user);
                        $Flash -> setComentarios($comentarios);
                            
                        $em->persist($Flash);

                        $em->flush();
                        $respuesta['estatus'] = 1;                            
                        $respuesta['mensaje'] ='Cliente registrado con éxito';


                }catch(\SoapFault $ex){

                    $this->get('logger')->error('Error EC SOAP de FLASH:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';

                }catch(\Exception $ex){   

                    $this->get('logger')->error('Error EC EXCEPTION de FLASH:');
                    $this->get('logger')->error($ex->getMessage());
                    $respuesta['mensaje'] ='No se ha registrado el cliente';

                }

        }else{
            try{

                //Guardamos al cliente solo sus datos referenciados
                $Clientes = new Clientes();
                $Clientes -> setNombre($nombre);
                $Clientes -> setApellidoPaterno($app);
                $Clientes -> setApellidoMaterno($apm);
                $Clientes -> setSexo($sexo);
                            
                //verificamos si existe el numero
                $tel1 = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findOneByNumero($telefono);
                    if(!$tel1)
                        {
                            //Agregamos el telefono                            
                            $tel1 = new Telefonos();
                            $tel1 -> setNumero($telefono);
                            $tel1 -> setEsListaNegra(false);
                        }

                //En caso de existir un segundo numero        
                if($telefono2){
                            $tel2 = $em->getRepository('MDRPuntoVentaBundle:Telefonos')->findOneByNumero($telefono2);
                            if(!$tel2)
                                {
                                    //Agregamos el telefono                            
                                    $tel2 = new Telefonos();
                                    $tel2 -> setNumero($telefono2);
                                    $tel2 -> setEsListaNegra(false);
                                }
                            $tel2 -> addCliente($Clientes);
                            $Clientes->addTelefono($tel2);

                            $em->persist($Clientes);
                            $em->persist($tel2);
                        }                                       

                $tel1 -> addCliente($Clientes);
                $Clientes->addTelefono($tel1);

                $Flash = new ClienteFlash();
                $Flash -> setFecha(new \Datetime());
                $Flash -> setTelefono($tel1);
                $Flash -> setCliente($Clientes);
                $Flash -> setDnis($DNIS);
                $Flash -> setUsuario($user);
                $Flash -> setComentarios($comentarios);


                $em->persist($Clientes);
                $em->persist($tel1);
                $em->persist($Flash);


                $em->flush();
                $respuesta['estatus'] = 1;                            
                $respuesta['mensaje'] ='Cliente registrado con éxito';
                

            }catch(\SoapFault $ex){

                $this->get('logger')->error('Error NE EXCEPTION de FLASH:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] ='No se ha registrado el cliente';

            }catch(\Exception $ex){   

                $this->get('logger')->error('Error NE EXCEPTION de FLASH:');
                $this->get('logger')->error($ex->getMessage());
                $respuesta['mensaje'] ='No se ha registrado el cliente';

            }
        }

        

        $response = new Response(json_encode($respuesta));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/tiempoEntrega", name="tiempo_entrega")
     * @Template()
     */
    public function gettiempoEntregaAction(Request $request)
    {
        $TipoPago = $request->get('pago');
        $Direccion = $request->get('direccion');
        $em = $this->getDoctrine()->getManager();

            $respuestas = array();
            $respuestas['mensaje'] = '';

       try{
               $Dir = $em->getRepository('MDRPuntoVentaBundle:Direcciones')->find($Direccion);
               $cp = $Dir->getCp();  
               $Pago = $em->getRepository('MDRPuntoVentaBundle:TipoPagos')->find($TipoPago);  
               $idPago = $Pago->getIdTipoPago();

               $exist = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findExisteCobertura($idPago,$cp);
               if(count($exist)>0){
                    $respuestas['mensaje'] = 'Tiempo de entrega para el codigo postal: <b>'.$cp.'</b> Con tipo de pago <b>'.$Pago->getDescripcion().'</b></br>Es de <b>'.$exist[0]->getTiempoEntregaMin().'</b> a <b>'.$exist[0]->getTiempoEntregaMax().'</b> días hábiles';
               }else{
                    $respuestas['mensaje'] = 'No hay tiempos de entrega para este CP';  
               }


        }catch(\SoapFault $ex){

            $this->get('logger')->error('Error ERROR EN CONSULTA DE TIEMPO DE ENTREGA PEDIDOS:');
            $this->get('logger')->error($ex->getMessage());
            $respuestas['mensaje'] = 'Error en consulta de tiempos de entrega intente más tarde';
        }catch(\Exception $ex){   

            $this->get('logger')->error('Error ERROR EN CONSULTA DE TIEMPO DE ENTREGA PEDIDOS:');
            $this->get('logger')->error($ex->getMessage());
            $respuestas['mensaje'] = 'Error en consulta de tiempos de entrega intente más tarde';
        }
       
        $response = new Response(json_encode($respuestas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }



}


