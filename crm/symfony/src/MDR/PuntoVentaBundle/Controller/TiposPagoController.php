<?php

namespace MDR\PuntoVentaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use MDR\PuntoVentaBundle\Entity\PaqueteriaFedex;

/**
 * Fedex controller.
 *
 * @Route("/TiposPago")
 */
class TiposPagoController extends Controller
{
    /**
     * @Route("/byCP", name="tipos_pago_by_cp")
     * @Method({"POST","GET"})
     */
    public function byCPAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();
    	$cp = $request->get('cp');

        $coberturas[1] = null;
        $coberturas[2] = null;
    	
        $entities = $em->getRepository('MDRPuntoVentaBundle:CoberturaEnvio')->findByCP($cp);
        foreach ($entities as $cobertura) {
            $idTipoPago = $cobertura->getTipoPago()->getIdTipoPago();
            if($coberturas[$idTipoPago] != null)
            {
                if($coberturas[$idTipoPago]->getPrioridad() > $cobertura->getPrioridad())
                {
                    $coberturas[$idTipoPago] = $cobertura;
                }
            }
            else
            {
                $coberturas[$idTipoPago] = $cobertura;
            }
        }

        $entities = array();
        foreach ($coberturas as $cobertura) {
            if($cobertura != null)
                $entities[] = $cobertura;
        }
    	$code = 1;
    	$message = '';

        $response = new Response(json_encode(array(
                'code' => $code,
                'message' => $message,
                'entities' => $entities
                )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
