<?php

namespace MDR\PuntoVentaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/productos")
 **/
class ProductosController extends Controller
{
    /**
     * @Route("/did/{canal}/{numero}", name="productos_by_id")
     * @Method("GET")
     */
    public function getByDidAction(Request $request, $canal, $numero)
    {
    	$em = $this->getDoctrine()->getManager();

        $did = null;
        $dids = $em->getRepository('MDRPuntoVentaBundle:Dids')->findByCanalNumero($canal, $numero);
        $descuentos = [];
        if(count($dids) > 0)
        {
            $did = $dids[0];
            $descuentos = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->findByCanalOficinaVentas($canal, $did->getOficinaVentas());
        }
		$entities = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->findByDid($numero, $canal);

        $response = new Response(json_encode(array(
            'code'          => 1,
            'message'       => "",
            'entities'      => $entities,
            'did'           => $did,
            'descuentos'    => $descuentos,
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/{canal}/{oficinaVentas}", name="productos_by_canal_oficinaVentas")
     * @Method("GET")
     */
    public function getByCanalOficinaVentasAction(Request $request, $canal, $oficinaVentas)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MDRPuntoVentaBundle:ProductoPrecios')->findByCanalOficinaVentas($canal, $oficinaVentas);
        $descuentos = $em->getRepository('MDRPuntoVentaBundle:Descuentos')->findByCanalOficinaVentas($canal, $oficinaVentas);

        $response = new Response(json_encode(array(
            'code'          => 1,
            'message'       => "",
            'entities'      => $entities,
            'descuentos'    => $descuentos,
            )));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
