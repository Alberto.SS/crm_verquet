<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BinTerminalType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
		->add('idBin', 'text', array(
				'label'     => 'BIN',
				'required' => true,
				'attr' => array('maxlength' => '6', 'minlength' => '6', 'onKeyUp'=> 'this.value = this.value.replace(no_letras, "")')
			))
            ->add('descripcion', null, array(
				'required' => true
			))		
            ->add('esdebito', null, array(
				'required' => false,
				'attr' => array('style' => 'margin-left: 12px;')
			))
            ->add('TipoTarjeta', null, array(
				'required' => true
			))
            ->add('terminal', null, array(
				'required' => true
			))
            ->add('banco', null, array(
				'required' => true
			))
            ->add('promociones', null, array(
				'required' => false,
				'attr' => array('style' => 'height:97px')
			))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\BinTerminal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_binterminal';
    }
}
