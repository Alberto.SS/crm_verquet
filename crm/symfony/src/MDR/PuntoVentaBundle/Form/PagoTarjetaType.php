<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PagoTarjetaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fecha = new \DateTime();
        $anio = intval($fecha->format('Y'));
        $anios = [];
        for($i=0; $i<10; $i++)
        {
            $anios[] = $anio++;
        }
        $builder
            ->add('esTitular', 'choice', array(
                'choices' => array(0 => 'No', 1 => 'Si'),
                'data' => 1
                ))
            ->add('titular',null,array(
                'required' => false,
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();'),
                ))
            ->add('numeroTarjeta')
            ->add('vigencia', 'date', [
                'widget' => 'choice',
                'years' => $anios
                ])
            ->add('codigoSeguridad')
            ->add('posFechar', 'choice', array(
                'choices' => array(0 => 'No', 1 => 'Si')
                ))
            ->add('fechaCobro', 'date', array(
                "read_only" => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
                ))
            ->add('tipoTarjeta')
            ->add('terminal', null, array(
                'attr'      => array('disabled'  => true)
                ))
            ->add('promocion')
            ->add('banco')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\PagoTarjeta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pagotarjeta';
    }
}
