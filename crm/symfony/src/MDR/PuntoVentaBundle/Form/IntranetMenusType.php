<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IntranetMenusType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('menuPadre', 'hidden', array(
                'mapped'    => false,
                'data'      => $options['menuPadre']
                ))
            ->add('tipoRequest', 'hidden', array(
                'mapped'    => false,
                'data'      => $options['tipoRequest']
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'MDR\PuntoVentaBundle\Entity\IntranetMenus',
            'menuPadre'     => 0,
            'tipoRequest'   => '',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'intranet_menus';
    }
}
