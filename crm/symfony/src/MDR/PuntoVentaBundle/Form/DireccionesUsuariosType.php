<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DireccionesUsuariosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pais', null, array(
                'data'  => 'MÉXICO',
                'attr'  => array('readonly' => ''),
                ))
            ->add('cp', null, array(
                'attr'     => array('maxlength' => 10,'onchange' => 'getInfo(1)')
                ))
            ->add('estado', 'choice', array( 
                'attr'     => array('onchange' => 'getInfo(2)'),
                ))
            ->add('municipio', 'choice', array( 
                'attr'     => array('onchange' => 'getInfo(3)'),
                ))
            ->add('colonia', 'choice', array(
                'attr'     => array('onchange' => 'getInfo(4)'),
                ))
            ->add('calle', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('entreCalles', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('referencia', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('noExt', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 10),
                ))
            ->add('noInt', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 10),
                ))
            ->add('telCasa', null, array(
                'label'     => 'Tel Casa',
                'required' => true,
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente']),
                ))
            /*->add('telOficina', null, array(
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente']),
                ))*/
            ->add('telCel', null, array(
                'label'     => 'Tel Móvil',
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente']),
                ))
          //->add('idDireccion')
           /* ->add('usuario', null, array(
                'required' => true,
                'attr'  => array('readonly' => ''),
                ))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\DireccionesUsuarios',
            'municipios'            => array(),
            'colonias'              => array(),
            'maxDigitosTelCliente'  => 18,
            'csrf_protection'       => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_direccionesusuarios';
    }
}
