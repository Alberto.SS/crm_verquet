<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GastosEnvioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('precio')
            ->add('tipoPago',null, array(
                    'required' => true,
                    'empty_value' => "-----------",
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\GastosEnvio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_gastosenvio';
    }
}
