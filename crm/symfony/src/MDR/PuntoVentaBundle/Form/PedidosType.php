<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PedidosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cliente', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Clientes',
                'choices' => $options['clientes'],
                'empty_value' => '----------'
            ))
            ->add('direccion', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Direcciones',
                'choices' => $options['direcciones'],
                'empty_value' => '----------'
            ))
            ->add('productos', 'collection', array(
                'type'          => new ProductosPedidoType(),
                'options'       => array(
                    'productoPrecios' => $options['productoPrecios'],
                    ),
                'allow_add'     => true,
                'prototype'     => true,
                'label'         => false
                ))
            ->add('descuentos', 'entity', array(
                'class'     => 'MDRPuntoVentaBundle:Descuentos',
                'mapped'    => false,
                'multiple'  => true,
                ))
            ->add('pago', null, array(
                'label'     => 'Tipo de Pago',
                'required'  => true
                ))
            ->add('subTotal', null, array(
                'attr' => array('readonly' => '')
                ))
            ->add('gastosEnvio', null, array(
                'required'  => true,
                'attr'      => array('disabled' => ''),
                'label'     => 'Gastos de envío '
                ))
            ->add('totalPago', null, array(
                'attr' => array('readonly' => '')
                ))
            ->add('factura', 'hidden', array(
                // 'label' => '¿Requiere factura?',
                // 'choices' => array(0 => 'No', 1 => 'Sí'),
                // 'empty_value' => false,
                'data' => 0,
                'required' => false,
                ))
            ->add('rfc', null, array(
                'required' => false,
                ))
            ->add('pagosTarjeta', 'collection', array(
                'type'      => new PagoTarjetaType(),
                'allow_add'     => false,
                'allow_delete'  => false,
                'prototype'     => false,
                'label'         => false
            ))
            ->add('did', 'hidden', array(
                'mapped'    => false,
                'required'  => false,
            ))
            ->add('telefono', 'hidden', array(
                'mapped'    => false,
                'required'  => false,
            ))
            ->add('esEntrante', 'hidden')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MDR\PuntoVentaBundle\Entity\Pedidos',
            'productoPrecios'       => array(),
            'clientes'              => array(),
            'direcciones'           => array(),
            'csrf_protection'       => false,
            'cascade_validation'    => true,
            'validation_groups'     => function(FormInterface $form) {
                // If the form is disabled, don't use any constraints
                if ($form->get('pago')->getData()->getIdTipoPago() != 1) {
                    return true;
                }

                // Otherwise, use the default validation group
                return 'Default';
            },
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_pedidos';
    }
}
