<?php

namespace MDR\PuntoVentaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DireccionesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cliente', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Clientes',
                'choices' => $options['clientes']
                ))
            ->add('pais', null, array(
                'data'  => 'MÉXICO',
                'attr'  => array('readonly' => ''),
                ))
            ->add('cp', null, array(
                'attr'     => array('maxlength' => 10)
                ))
            ->add('estado', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Estados',
                'empty_value' => '----------'
            ))
            ->add('municipio', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Municipios',
                'choices' => $options['municipios']
            ))
            ->add('colonia', 'entity', array(
                'class' => 'MDRPuntoVentaBundle:Colonias',
                'choices' => $options['colonias']
                ))
            ->add('calle', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('entreCalles', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('referencia', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 40),
                ))
            ->add('noExt', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 10),
                ))
            ->add('noInt', null, array(
                'attr' => array('onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength' => 10),
                ))
            ->add('telCasa', null, array(
                'required' => true,
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente'],'onkeypress' => 'return solonum(event)'),
                ))
            ->add('telOficina', null, array(
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente'],'onkeypress' => 'return solonum(event)'),
                ))
            ->add('telCel', null, array(
                'label'     => 'Tel Móvil',
                'attr'     => array('maxlength' => $options['maxDigitosTelCliente'],'onkeypress' => 'return solonum(event)'),
                ))
            ->add('tipoRequest', 'hidden', array(
                'mapped'    => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'MDR\PuntoVentaBundle\Entity\Direcciones',
            'clientes'              => array(),
            'municipios'            => array(),
            'colonias'              => array(),
            'maxDigitosTelCliente'  => 18,
            'csrf_protection'       => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mdr_puntoventabundle_direcciones';
    }
}
