<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * ListaNegraDirecciones
 */
class ListaNegraDirecciones implements JsonSerializable
{
    /**
     * @var string
     */
    private $Municipio;

    /**
     * @var string
     */
    private $Colonia;

    /**
     * @var string
     */
    private $CP;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idDireccion;


    /**
     * Set Municipio
     *
     * @param string $municipio
     * @return ListaNegraDirecciones
     */
    public function setMunicipio($municipio)
    {
        $this->Municipio = $municipio;

        return $this;
    }

    /**
     * Get Municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->Municipio;
    }

    /**
     * Set Colonia
     *
     * @param string $colonia
     * @return ListaNegraDirecciones
     */
    public function setColonia($colonia)
    {
        $this->Colonia = $colonia;

        return $this;
    }

    /**
     * Get Colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->Colonia;
    }

    /**
     * Set CP
     *
     * @param string $cP
     * @return ListaNegraDirecciones
     */
    public function setCP($cP)
    {
        $this->CP = $cP;

        return $this;
    }

    /**
     * Get CP
     *
     * @return string 
     */
    public function getCP()
    {
        return $this->CP;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return ListaNegraDirecciones
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idDireccion
     *
     * @return integer 
     */
    public function getIdDireccion()
    {
        return $this->idDireccion;
    }
    
    public function jsonSerialize()
    {
        return array(

            'Municipio'     => $this->Municipio,
            'Colonia'       => $this->Colonia,
            'CP'            => $this->CP,
            'Activo'        => $this->Activo,
            'idDireccion'   => $this->idDireccion,
                       
        );

    }
}
