<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ProductoPrecios
 */
class ProductoPrecios implements JsonSerializable
{
    /**
     * @var string
     */
    private $organizacionVentas;

    /**
     * @var string
     */
    private $canalDistribucion;

    /**
     * @var string
     */
    private $materialPrecio;

    /**
     * @var string
     */
    private $precio;

    /**
     * @var string
     */
    private $moneda;

    /**
     * @var boolean
     */
    private $activo;

    /**
     * @var integer
     */
    private $tipoPromocion;

    /**
     * @var integer
     */
    private $idProductoPrecio;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoPagos
     */
    private $tipoPago;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\OficinasVentas
     */
    private $oficinaVenta;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Productos
     */
    private $producto;


    /**
     * Set organizacionVentas
     *
     * @param string $organizacionVentas
     * @return ProductoPrecios
     */
    public function setOrganizacionVentas($organizacionVentas)
    {
        $this->organizacionVentas = $organizacionVentas;

        return $this;
    }

    /**
     * Get organizacionVentas
     *
     * @return string 
     */
    public function getOrganizacionVentas()
    {
        return $this->organizacionVentas;
    }

    /**
     * Set canalDistribucion
     *
     * @param string $canalDistribucion
     * @return ProductoPrecios
     */
    public function setCanalDistribucion($canalDistribucion)
    {
        $this->canalDistribucion = $canalDistribucion;

        return $this;
    }

    /**
     * Get canalDistribucion
     *
     * @return string 
     */
    public function getCanalDistribucion()
    {
        return $this->canalDistribucion;
    }


	public function getCanalDistribucionDesc()
    {
		if ($this->canalDistribucion == '20'){
		return 'Call Center';
		}
		elseif($this->canalDistribucion == '30'){
		return 'Commerce';	
		}
		else{
        return $this->canalDistribucion;
		}
    }
    /**
     * Set materialPrecio
     *
     * @param string $materialPrecio
     * @return ProductoPrecios
     */
    public function setMaterialPrecio($materialPrecio)
    {
        $this->materialPrecio = $materialPrecio;

        return $this;
    }

    /**
     * Get materialPrecio
     *
     * @return string 
     */
    public function getMaterialPrecio()
    {
        return $this->materialPrecio;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return ProductoPrecios
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     * @return ProductoPrecios
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string 
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return ProductoPrecios
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set tipoPromocion
     *
     * @param integer $tipoPromocion
     * @return ProductoPrecios
     */
    public function setTipoPromocion($tipoPromocion)
    {
        $this->tipoPromocion = $tipoPromocion;

        return $this;
    }

    /**
     * Get tipoPromocion
     *
     * @return integer 
     */
    public function getTipoPromocion()
    {
        return $this->tipoPromocion;
    }

    /**
     * Get idProductoPrecio
     *
     * @return integer 
     */
    public function getIdProductoPrecio()
    {
        return $this->idProductoPrecio;
    }

    /**
     * Set tipoPago
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago
     * @return ProductoPrecios
     */
    public function setTipoPago(\MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago = null)
    {
        $this->tipoPago = $tipoPago;

        return $this;
    }

    /**
     * Get tipoPago
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoPagos 
     */
    public function getTipoPago()
    {
        return $this->tipoPago;
    }

    /**
     * Set oficinaVenta
     *
     * @param \MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVenta
     * @return ProductoPrecios
     */
    public function setOficinaVenta(\MDR\PuntoVentaBundle\Entity\OficinasVentas $oficinaVenta = null)
    {
        $this->oficinaVenta = $oficinaVenta;

        return $this;
    }

    /**
     * Get oficinaVenta
     *
     * @return \MDR\PuntoVentaBundle\Entity\OficinasVentas 
     */
    public function getOficinaVenta()
    {
        return $this->oficinaVenta;
    }

    /**
     * Set producto
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $producto
     * @return ProductoPrecios
     */
    public function setProducto(\MDR\PuntoVentaBundle\Entity\Productos $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \MDR\PuntoVentaBundle\Entity\Productos 
     */
    public function getProducto()
    {
        return $this->producto;
    }

    public function __toString()
    {
        return $this->producto->getDescripcion(). ' - $ '.$this->precio;
    }

    public function jsonSerialize()
    {
        return array(
            'idProductoPrecio'      => $this->idProductoPrecio,
            'precio'                => $this->precio,
            'moneda'                => $this->moneda,
            'organizacionVentas'    => $this->organizacionVentas,
            'canalDistribucion'     => $this->canalDistribucion,
            'oficinaVenta'          => $this->oficinaVenta,
            'materialPrecio'        => $this->materialPrecio,
            'producto'              => $this->producto,
            'tipoPago'              => $this->tipoPago,
            'tipoPromocion'         => $this->tipoPromocion,
        );

    }
}
