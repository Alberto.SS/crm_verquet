<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Direcciones
 */
class Direcciones implements JsonSerializable
{
    /**
     * @var string
     */
    private $pais;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $municipio;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $calle;

    /**
     * @var string
     */
    private $entreCalles;

    /**
     * @var string
     */
    private $referencia;

    /**
     * @var string
     */
    private $noExt;

    /**
     * @var string
     */
    private $noInt;

    /**
     * @var string
     */
    private $telCasa;

    /**
     * @var string
     */
    private $telOficina;

    /**
     * @var string
     */
    private $telCel;

    /**
     * @var integer
     */
    private $idDireccion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Clientes
     */
    private $cliente;


    /**
     * Set pais
     *
     * @param string $pais
     * @return Direcciones
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Direcciones
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return Direcciones
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Direcciones
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     * @return Direcciones
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set calle
     *
     * @param string $calle
     * @return Direcciones
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set entreCalles
     *
     * @param string $entreCalles
     * @return Direcciones
     */
    public function setEntreCalles($entreCalles)
    {
        $this->entreCalles = $entreCalles;

        return $this;
    }

    /**
     * Get entreCalles
     *
     * @return string 
     */
    public function getEntreCalles()
    {
        return $this->entreCalles;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return Direcciones
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set noExt
     *
     * @param string $noExt
     * @return Direcciones
     */
    public function setNoExt($noExt)
    {
        $this->noExt = $noExt;

        return $this;
    }

    /**
     * Get noExt
     *
     * @return string 
     */
    public function getNoExt()
    {
        return $this->noExt;
    }

    /**
     * Set noInt
     *
     * @param string $noInt
     * @return Direcciones
     */
    public function setNoInt($noInt)
    {
        $this->noInt = $noInt;

        return $this;
    }

    /**
     * Get noInt
     *
     * @return string 
     */
    public function getNoInt()
    {
        return $this->noInt;
    }

    /**
     * Set telCasa
     *
     * @param string $telCasa
     * @return Direcciones
     */
    public function setTelCasa($telCasa)
    {
        $this->telCasa = $telCasa;

        return $this;
    }

    /**
     * Get telCasa
     *
     * @return string 
     */
    public function getTelCasa()
    {
        return $this->telCasa;
    }

    /**
     * Set telOficina
     *
     * @param string $telOficina
     * @return Direcciones
     */
    public function setTelOficina($telOficina)
    {
        $this->telOficina = $telOficina;

        return $this;
    }

    /**
     * Get telOficina
     *
     * @return string 
     */
    public function getTelOficina()
    {
        return $this->telOficina;
    }

    /**
     * Set telCel
     *
     * @param string $telCel
     * @return Direcciones
     */
    public function setTelCel($telCel)
    {
        $this->telCel = $telCel;

        return $this;
    }

    /**
     * Get telCel
     *
     * @return string 
     */
    public function getTelCel()
    {
        return $this->telCel;
    }

    /**
     * Get idDireccion
     *
     * @return integer 
     */
    public function getIdDireccion()
    {
        return $this->idDireccion;
    }

    /**
     * Set cliente
     *
     * @param \MDR\PuntoVentaBundle\Entity\Clientes $cliente
     * @return Direcciones
     */
    public function setCliente(\MDR\PuntoVentaBundle\Entity\Clientes $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \MDR\PuntoVentaBundle\Entity\Clientes 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

	public function __toString()
    {
        return $this->calle.', '.$this->noExt.', '.$this->noInt.', '.$this->colonia.', '.$this->cp.', '.$this->municipio.', '.$this->estado;
    }

    public function jsonSerialize()
    {
        return array(
            'idDireccion'   => $this->idDireccion,
            'pais'          => $this->pais,
            'estado'        => $this->estado,
            'municipio'     => $this->municipio,
            'cp'            => $this->cp,
            'colonia'       => $this->colonia,
            'calle'         => $this->calle,
            'entreCalles'   => $this->entreCalles,
            'referencia'    => $this->referencia,
            'noInt'         => $this->noInt,
            'noExt'         => $this->noExt,
            'telCasa'       => $this->telCasa,
            'telOficina'    => $this->telOficina,
            'telCel'        => $this->telCel,
            'toString'      => $this->__toString(),
        );

    }
}
