<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Municipios
 */
class Municipios implements JsonSerializable
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $CodigoSepomex;

    /**
     * @var integer
     */
    private $idMunicipio;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Estados
     */
    private $estado;


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Municipios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set CodigoSepomex
     *
     * @param string $codigoSepomex
     * @return Municipios
     */
    public function setCodigoSepomex($codigoSepomex)
    {
        $this->CodigoSepomex = $codigoSepomex;

        return $this;
    }

    /**
     * Get CodigoSepomex
     *
     * @return string 
     */
    public function getCodigoSepomex()
    {
        return $this->CodigoSepomex;
    }

    /**
     * Get idMunicipio
     *
     * @return integer 
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set estado
     *
     * @param \MDR\PuntoVentaBundle\Entity\Estados $estado
     * @return Municipios
     */
    public function setEstado(\MDR\PuntoVentaBundle\Entity\Estados $estado = null)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \MDR\PuntoVentaBundle\Entity\Estados 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function jsonSerialize()
    {
        return array(
            'idMunicipio'   => $this->idMunicipio,
            'nombre'        => $this->nombre,
            'CodigoSepomex' => $this->CodigoSepomex,
            'estado'        => $this->estado,
        );
    }
}