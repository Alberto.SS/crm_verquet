<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * TiposCancelacion
 */
class TiposCancelacion implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $idCancelacion;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TiposCancelacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idCancelacion
     *
     * @return string 
     */
    public function getIdCancelacion()
    {
        return $this->idCancelacion;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(
            'descripcion'       => $this->descripcion,
            'idCancelacion'     => $this->idCancelacion,                       
        );

    }
}
