<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * OficinasVentas
 */
class OficinasVentas implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dids;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productosPrecios;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dids = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productosPrecios = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OficinasVentas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }

    /**
     * Add dids
     *
     * @param \MDR\PuntoVentaBundle\Entity\Dids $dids
     * @return OficinasVentas
     */
    public function addDid(\MDR\PuntoVentaBundle\Entity\Dids $dids)
    {
        $this->dids[] = $dids;

        return $this;
    }

    /**
     * Remove dids
     *
     * @param \MDR\PuntoVentaBundle\Entity\Dids $dids
     */
    public function removeDid(\MDR\PuntoVentaBundle\Entity\Dids $dids)
    {
        $this->dids->removeElement($dids);
    }

    /**
     * Get dids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDids()
    {
        return $this->dids;
    }

    /**
     * Add productosPrecios
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductoPrecios $productosPrecios
     * @return OficinasVentas
     */
    public function addProductosPrecio(\MDR\PuntoVentaBundle\Entity\ProductoPrecios $productosPrecios)
    {
        $this->productosPrecios[] = $productosPrecios;

        return $this;
    }

    /**
     * Remove productosPrecios
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductoPrecios $productosPrecios
     */
    public function removeProductosPrecio(\MDR\PuntoVentaBundle\Entity\ProductoPrecios $productosPrecios)
    {
        $this->productosPrecios->removeElement($productosPrecios);
    }

    /**
     * Get productosPrecios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductosPrecios()
    {
        return $this->productosPrecios;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(
            'claveSAP'      => $this->claveSAP,
            'descripcion'   => $this->descripcion,
        );

    }
}
