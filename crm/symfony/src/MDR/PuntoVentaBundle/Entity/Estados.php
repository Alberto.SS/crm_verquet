<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Estados
 */
class Estados implements JsonSerializable
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var string
     */
    private $codigoSepomex;

    /**
     * @var integer
     */
    private $idEstado;


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Estados
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set claveSAP
     *
     * @param string $claveSAP
     * @return Estados
     */
    public function setClaveSAP($claveSAP)
    {
        $this->claveSAP = $claveSAP;

        return $this;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }

    /**
     * Set codigoSepomex
     *
     * @param string $codigoSepomex
     * @return Estados
     */
    public function setCodigoSepomex($codigoSepomex)
    {
        $this->codigoSepomex = $codigoSepomex;

        return $this;
    }

    /**
     * Get codigoSepomex
     *
     * @return string 
     */
    public function getCodigoSepomex()
    {
        return $this->codigoSepomex;
    }

    /**
     * Get idEstado
     *
     * @return integer 
     */
    public function getIdEstado()
    {
        return $this->idEstado;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function jsonSerialize()
    {
        return array(
            'idEstado'      => $this->idEstado,
            'nombre'        => $this->nombre,
            'claveSAP'      => $this->claveSAP,
            'codigoSepomex' => $this->codigoSepomex,
        );
    }
}