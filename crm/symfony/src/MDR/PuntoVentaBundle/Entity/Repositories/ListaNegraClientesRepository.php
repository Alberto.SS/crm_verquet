<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ListaNegraClientesRepository extends EntityRepository
{
	function findLNClientes($nombre,$app,$apm){
		$em = $this->getEntityManager();
        $totalname="";
        if($nombre){
            $totalname .= "%".$nombre."%";
        }
        if($app){
            $totalname .= "%".$app."%";
        }
        if($apm){
            $totalname .= "%".$apm."%";
        }
        
                $strQuery = "SELECT cl FROM MDRPuntoVentaBundle:ListaNegraClientes cl";

                if($nombre || $app || $apm){
                    $strQuery .= " WHERE cl.NombreCompleto LIKE :fullname";
                }          
                $query = $em->createQuery($strQuery);
                if($nombre || $app || $apm){
                        $query->setParameter(':fullname', $totalname);
                }
                $entities = $query->getResult();
                return $entities;
                                              
        
        
    }

    function findTotalName($nombre){
                $em = $this->getEntityManager();        
        
                $strQuery = "SELECT cl FROM MDRPuntoVentaBundle:ListaNegraClientes cl 
                            WHERE cl.NombreCompleto = :fullname";

                   
                $query = $em->createQuery($strQuery);
                $query->setParameter(':fullname', $nombre);
                
                $entities = $query->getResult();
                return $entities;
        
    }    

}
