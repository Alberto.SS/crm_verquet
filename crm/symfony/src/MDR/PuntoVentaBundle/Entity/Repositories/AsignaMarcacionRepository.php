<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * AsignaMarcacionRepository
 */
class AsignaMarcacionRepository extends EntityRepository
{
        public function findByUsuario($usuario, $canal)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT a FROM MDRPuntoVentaBundle:AsignaMarcacion a
                                WHERE a.usuario = :usuario AND a.canalDistribucion = :canal AND a.atendido = 0
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('usuario', $usuario);
                $query->setParameter('canal', $canal);
                $entities = $query->getResult();

                return $entities;
	}
}
