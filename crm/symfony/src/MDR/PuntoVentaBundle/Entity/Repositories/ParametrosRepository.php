<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ClientesRepository
 */
class ParametrosRepository extends EntityRepository
{
        public function getValor($clave)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Parametros p
                                WHERE p.clave = :clave
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('clave', $clave);
                $entities = $query->getResult();
                $valor = '';
                if(count($entities) == 1)
                {
                        $valor = $entities[0]->getValor();
                }

                return $valor;
	}

        public function getParametersValue()
        {
                $em = $this->getEntityManager();

                $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Parametros p
                                WHERE p.clave IN ('SWITCH_INBOX_ALL_USERS','SWITCH_ENVIO_SAP')";

                $query = $em->createQuery($strQuery);
                $entities = $query->getResult();
                return $entities;
        }
}
