<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class UsuarioRepository extends EntityRepository
{

	function findBySupervisor($user,$pass){
		$em = $this->getEntityManager();
        $pass = strtoupper($pass);
		$pass = md5($pass);

        $strQuery = 'SELECT us FROM MDRPuntoVentaBundle:Usuario us
        				WHERE us.uClave = :usuario AND us.uPassword = :password AND us.tipoUsuario IN (3,4,10,11)';

        $query = $em->createQuery($strQuery);
        $query->setParameter(':usuario', $user);
        $query->setParameter(':password', $pass);
        $entities = $query->getResult();
        return $entities;
	}

	function findByUsuario($user){
                $em = $this->getEntityManager();

        $strQuery = "SELECT us FROM MDRPuntoVentaBundle:Usuario us
                                        WHERE us.uClave = :usuario";

        
        $query = $em->createQuery($strQuery);
        $query->setParameter(':usuario', $user);
        $entities = $query->getResult();
        return $entities;
        }

    function findToInbox($user){
        $em = $this->getEntityManager();

        $strQuery = 'SELECT us FROM MDRPuntoVentaBundle:Usuario us
                     WHERE us.tipoUsuario IN (3,4,5) AND us.uNumero != :usuario AND us.activo = 1 ';

        $query = $em->createQuery($strQuery);
        $query->setParameter(':usuario', $user);
        $entities = $query->getResult();
        return $entities;
    }

    function findAllToInbox($user){
        $em = $this->getEntityManager();

        $strQuery = 'SELECT us FROM MDRPuntoVentaBundle:Usuario us
                     WHERE us.tipoUsuario != 26 AND us.uNumero != :usuario AND us.activo = 1 ';

        $query = $em->createQuery($strQuery);
        $query->setParameter(':usuario', $user);
        $entities = $query->getResult();
        return $entities;
    }

    function findUsuariosTalleres(){

        $em = $this->getEntityManager();

        $strQuery = 'SELECT us FROM MDRPuntoVentaBundle:Usuario us
                     WHERE us.tipoUsuario IN(3,4) ';

        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();
        return $entities;
    }

}
