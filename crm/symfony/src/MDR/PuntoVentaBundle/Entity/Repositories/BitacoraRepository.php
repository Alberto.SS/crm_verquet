<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class BitacoraRepository extends EntityRepository
{
	function findByOperaciones($fechaInicio, $fechaFin, $area, $pedido){
		$em = $this->getEntityManager();
                $fechaFin->add(new \DateInterval('P1D'));
 
		//Pedidos que no han sido cobrados y no estan posfechados
        if($pedido){
               $strQuery = "SELECT p, u FROM MDRPuntoVentaBundle:Bitacora p
                             INNER JOIN p.usuario u";
                if($area){
                $strQuery .= " WHERE p.areaOperacion = :area";
                }
                if($pedido){
                       $strQuery .= " AND p.pedido = :pedido"; 
                }                                
                $query = $em->createQuery($strQuery);
                if($area){
                        $query->setParameter(':area', $area);
                } 
                if($pedido){
                        $query->setParameter(':pedido', $pedido);
                } 
                $entities = $query->getResult();
                return $entities;
                                                                            

        }else{
                $strQuery = "SELECT p, u FROM MDRPuntoVentaBundle:Bitacora p
                             INNER JOIN p.usuario u
                             WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin";
                if($area){
                $strQuery .= " AND p.areaOperacion = :area";
                }                               
                $query = $em->createQuery($strQuery);
                $query->setParameter(':fechaInicio', $fechaInicio);
                $query->setParameter(':fechaFin', $fechaFin);
                if($area){
                        $query->setParameter(':area', $area);
                }
                $entities = $query->getResult();
                return $entities;
                                              
        }
        
}        

        function findByOperacionesATC($fechaInicio, $fechaFin, $pedido){
                $em = $this->getEntityManager();
                $fechaFin->add(new \DateInterval('P1D'));
 
                //Pedidos que no han sido cobrados y no estan posfechados
                if($pedido){
                       $strQuery = "SELECT p, u FROM MDRPuntoVentaBundle:Bitacora p
                                     INNER JOIN p.usuario u";
                        if($pedido){
                               $strQuery .= " WHERE p.pedido = :pedido"; 
                        }                                
                        $query = $em->createQuery($strQuery);
                        if($pedido){
                                $query->setParameter(':pedido', $pedido);
                        } 
                        $entities = $query->getResult();
                        return $entities;
                                                                                    

                }else{
                        $strQuery = "SELECT p, u FROM MDRPuntoVentaBundle:Bitacora p
                                     INNER JOIN p.usuario u
                                     WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':fechaInicio', $fechaInicio);
                        $query->setParameter(':fechaFin', $fechaFin);
                        
                        $entities = $query->getResult();
                        return $entities;
                                                      
                }
        }


}
