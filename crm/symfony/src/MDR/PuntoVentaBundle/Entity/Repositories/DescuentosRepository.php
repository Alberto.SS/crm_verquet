<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * DescuentosRepository
 */
class DescuentosRepository extends EntityRepository
{
	public function findByCanalOficinaVentas($canal, $oficinaVentas)
	{
		$em = $this->getEntityManager();

		$strQuery = 'SELECT d,o,tp FROM MDRPuntoVentaBundle:Descuentos d
						INNER JOIN d.origenes o INNER JOIN d.tipoPago tp
						WHERE o.canalDistribucion = :canal AND o.oficinaVentas = :oficinaVentas AND d.activo = 1
					';

		$query = $em->createQuery($strQuery);
        $query->setParameter('canal', $canal);
        $query->setParameter('oficinaVentas', $oficinaVentas);
        $entities = $query->getResult();

        return $entities;
	}
	
	// se Obtinenen los objetos de descuentos para aplicar la consulta que seleccionara los registros existenetesz
		public function findByClaveTipoPago($clave, $idDescuento, $tipoPago)
	{
		$em = $this->getEntityManager();

		$strQuery = 'SELECT d FROM MDRPuntoVentaBundle:Descuentos d
						WHERE d.clave = :clave AND d.tipoPago = :tipoPago AND d.idDescuento != :idDescuento
					';

		$query = $em->createQuery($strQuery);
        $query->setParameter('clave', $clave);
        $query->setParameter('idDescuento', $idDescuento);
		$query->setParameter('tipoPago', $tipoPago);
        $entities = $query->getResult();

        return $entities;
	}
}