<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ProductoPreciosRepository
 */
class ProductoPreciosRepository extends EntityRepository
{
        public function findByDid($did, $canalDistribucion)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT pp,p,ov,d,tp FROM MDRPuntoVentaBundle:ProductoPrecios pp
                                INNER JOIN pp.producto p INNER JOIN pp.oficinaVenta ov INNER JOIN ov.dids d
                                INNER join pp.tipoPago tp
                                WHERE d.numero = :did AND pp.canalDistribucion = :canal AND pp.activo = 1
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('did', $did);
                $query->setParameter('canal', $canalDistribucion);
                $entities = $query->getResult();

                return $entities;
	}

        public function findByCanalOficinaVentas($canal, $oficinaVentas)
        {
                $em = $this->getEntityManager();

                $strQuery = "SELECT pp,p,tp FROM MDRPuntoVentaBundle:ProductoPrecios pp
                                INNER JOIN pp.producto p INNER join pp.tipoPago tp
                                WHERE pp.canalDistribucion = :canal AND pp.oficinaVenta = :oficinaVentas AND pp.activo = 1
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('canal', $canal);
                $query->setParameter('oficinaVentas', $oficinaVentas);
                $entities = $query->getResult();

                return $entities;
        }

        public function findAllComplete()
        {
                $em = $this->getEntityManager();

                $strQuery = "SELECT pp, p FROM MDRPuntoVentaBundle:ProductoPrecios pp
                                INNER JOIN pp.producto p
                                ";

                $query = $em->createQuery($strQuery);
                $entities = $query->getResult();

                return $entities;
        }

        public function findByIds($ids)
        {
                $em = $this->getEntityManager();

                $strQuery = 'SELECT pp FROM MDRPuntoVentaBundle:ProductoPrecios pp WHERE pp IN (:ids)';

                $query = $em->createQuery($strQuery);
                $query->setParameter('ids', $ids);
                $entities = $query->getResult();

                return $entities;
        }

        public function findByPrecios($idProducto,$OfVtn,$CanDis,$OrgVtn,$detalle){

                $em = $this->getEntityManager();
                $Aprices = array();

                $strQuery = "SELECT pp.precio,pp.idProductoPrecio FROM MDRPuntoVentaBundle:ProductoPrecios pp
                                WHERE pp.producto = :Producto AND pp.oficinaVenta = :Ventas AND pp.canalDistribucion = :Distribucion 
                                AND pp.organizacionVentas = :Organizacion AND pp.idProductoPrecio != :idDetalle  AND pp.activo = 1
                                ORDER BY pp.precio ASC";

                $query = $em->createQuery($strQuery);
                $query->setParameter(':Producto', $idProducto);
                $query->setParameter(':Ventas', $OfVtn);
                $query->setParameter(':Distribucion', $CanDis);
                $query->setParameter(':Organizacion', $OrgVtn);
                $query->setParameter(':idDetalle', $detalle);
                $entities = $query->getResult();

                foreach ($entities as $key) {
                        $Aprices[] = array('value' => (double)$key['idProductoPrecio'], 'text' => "$".(double)$key['precio']);
                }

                return $Aprices;

        }
}
