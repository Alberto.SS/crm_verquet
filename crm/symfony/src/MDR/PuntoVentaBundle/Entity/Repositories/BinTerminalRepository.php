<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * BinTerminalRepository
 */
class BinTerminalRepository extends EntityRepository
{
        public function findAllComplete()
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT bt, b, t, tt FROM MDRPuntoVentaBundle:BinTerminal bt
                                INNER JOIN bt.banco b INNER JOIN bt.terminal t INNER JOIN bt.TipoTarjeta tt
                                ";

                $query = $em->createQuery($strQuery);
                $entities = $query->getResult();

                return $entities;
	}
}
