<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ZonasRiesgoRepository extends EntityRepository
{
	    function findZonasRiesgo($cp,$municipio){
                $em = $this->getEntityManager();
                
                $strQuery = "SELECT zr FROM MDRPuntoVentaBundle:ZonasRiesgo zr";
                        
                        if($cp || $municipio){
                            $strQuery .= " WHERE "; 
                        }          
                        if($cp){
                               $strQuery .= " zr.CP = :cp"; 
                        }
                        if($municipio){
                                if($cp){
                                    $strQuery .= " AND zr.Municipio LIKE :mun";                             
                                }else{
                                    $strQuery .= " zr.Municipio LIKE :mun"; 
                                }
                        } 
                                                      
                        $query = $em->createQuery($strQuery);

                        if($cp){
                               $query->setParameter(':cp', $cp);
                        } 
                        if($municipio){
                                $query->setParameter(':mun', '%'.$municipio.'%');
                        } 
                        
                        $entities = $query->getResult();
                        return $entities;
                                                      
               
        }


}
