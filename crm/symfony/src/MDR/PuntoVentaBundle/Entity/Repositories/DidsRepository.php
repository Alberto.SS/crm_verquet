<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

//Agregar inclusión en xml
class DidsRepository extends EntityRepository
{
	public function findByDescCanalDistribucion($descripcion)
	{
		$em = $this->getEntityManager();
        
        //Join para buscar por la descripción de la tabla canal de distribución
        $strQuery = "SELECT d,c,ov FROM MDRPuntoVentaBundle:Dids d
        				JOIN d.canalDistribucion c JOIN d.oficinaVentas ov
        				WHERE c.descripcion = :descripcion";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':descripcion', $descripcion);
        $entities = $query->getResult();
       	
        return $entities;

	}

    public function findByCanalNumero($canal, $numero)
    {
        $em = $this->getEntityManager();
        
        $strQuery = 'SELECT d FROM MDRPuntoVentaBundle:Dids d
                        INNER JOIN d.oficinaVentas ov INNER JOIN ov.productosPrecios pp
                        WHERE pp.canalDistribucion = d.canalDistribucion AND d.canalDistribucion = :canal AND d.numero = :numero';

        $query = $em->createQuery($strQuery);
        $query->setParameter(':canal', $canal);
        $query->setParameter(':numero', $numero);
        $entities = $query->getResult();
        
        return $entities;

    }

}
