<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class TelefonosRepository extends EntityRepository
{
    	function findByTelefonosListaNegra($numero, $estatus){
            
    		$em = $this->getEntityManager();
            
            $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Telefonos t";

            if($numero || $estatus == "1" || $estatus == "0"){            
                $strQuery .= " WHERE ";            
            }

             if($numero)
            {
                $strQuery .= ' t.numero = :numero';
            }
            if($estatus == "1" || $estatus == "0")
            {
                if($numero){

                    $strQuery .= ' AND t.esListaNegra = :estatus';

                }else{

                    $strQuery .= ' t.esListaNegra = :estatus';

                }

            }
            $strQuery .= ' ORDER BY t.numero';



            $query = $em->createQuery($strQuery);
            if($numero)
            {
                $query->setParameter(':numero', $numero);            
            }
            if($estatus == "1" || $estatus == "0")
            {
                $query->setParameter(':estatus', $estatus); 
            }

            $entities = $query->getResult();
            return $entities;        
            
        }        

        function findByesbloqueado($numero){
            
            $em = $this->getEntityManager();        
            $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Telefonos t WHERE t.numero = :numero AND t.esListaNegra = 1";

            $query = $em->createQuery($strQuery);
            $query->setParameter(':numero', $numero);
            $entities = $query->getResult();                
   
            return $entities;

        }


        function findByExcel($numero){
            
            $em = $this->getEntityManager();        
            $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Telefonos t WHERE t.numero = :numero";

            $query = $em->createQuery($strQuery);
            $query->setParameter(':numero', $numero);
            $entities = $query->getResult();                
   
            return $entities;

        }



}
