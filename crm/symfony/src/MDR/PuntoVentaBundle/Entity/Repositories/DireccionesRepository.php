<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class DireccionesRepository extends EntityRepository
{
	public function findByCliente($idCliente)
	{
		$em = $this->getEntityManager();

        $strQuery = "SELECT d FROM MDRPuntoVentaBundle:Direcciones d
                            where d.cliente = :cliente";
        $query = $em->createQuery($strQuery);
        $query->setParameter(':cliente', $idCliente);
        $entities = $query->getResult();

        return $entities;
	}
}