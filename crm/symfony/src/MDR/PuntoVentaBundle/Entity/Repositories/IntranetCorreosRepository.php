<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class IntranetCorreosRepository extends EntityRepository
{
	
        function findArea($area){
                $em = $this->getEntityManager();
                        $strQuery = "SELECT ic FROM MDRPuntoVentaBundle:IntranetCorreos ic
                                     WHERE ic.Area = :area";
                                                      
                        $query = $em->createQuery($strQuery);                        
                		$query->setParameter(':area', $area);
                        $entities = $query->getResult();
                        return $entities;
                                                      
                }
        


}
