<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class CoberturaEnvioRepository extends EntityRepository
{
	function findExisteCobertura($pago, $CP){
                $em = $this->getEntityManager();
                       
                        $strQuery = "SELECT ce FROM MDRPuntoVentaBundle:CoberturaEnvio ce
                                     WHERE ce.cP = :codigo AND ce.tipoPago = :pago";
                                                      
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':codigo', $CP);
                        $query->setParameter(':pago', $pago);
                        
                        $entities = $query->getResult();
                        return $entities;
        }

    function findTipoPagoDescripcion($tipoPago){
         $em = $this->getEntityManager();
                       
        $strQuery = "SELECT tp FROM MDRPuntoVentaBundle:TipoPagos tp
                    WHERE tp.descripcion = :pago";
                                                      
        $query = $em->createQuery($strQuery);
        $query->setParameter(':pago', $tipoPago);
                        
        $entities = $query->getResult();
        return $entities;

    }

    function findExisteCoberturaIntranet($pago, $CP){
                $em = $this->getEntityManager();
                       
                        $strQuery = "SELECT ce FROM MDRPuntoVentaBundle:CoberturaEnvio ce
                                     WHERE ce.cP = :codigo ";
                        if($pago){
                            $strQuery .= "AND ce.tipoPago = :pago";
                        }                              
                        $query = $em->createQuery($strQuery);
                        $query->setParameter(':codigo', $CP);
                        if($pago){
                            $query->setParameter(':pago', $pago);
                        }
                        
                        $entities = $query->getResult();
                        return $entities;
        }    


}
