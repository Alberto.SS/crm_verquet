<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * IntranetMenusRepository
 */
class IntranetMenusRepository extends EntityRepository
{
    public function findByUrls($urls)
	{
		$em = $this->getEntityManager();
        $menu = 0;

        for ($i=0; $i < count($urls); $i++) { 
            $url = $urls[$i];
            $strQuery = 'SELECT im FROM MDRPuntoVentaBundle:IntranetMenus im
                            LEFT JOIN im.menuPadre imp
                            WHERE im.url = :url AND imp = :menuPadre
                            ';
            $query = $em->createQuery($strQuery);
            $query->setParameter('url', $url);
            $query->setParameter('menuPadre', $menu);
            $entities = $query->getResult();
            if(count($entities) == 1)
            {
                $menu = $entities[0];
            } else {
                $menu = 0;
                $i = count($urls);
            }
            //\Doctrine\Common\Util\Debug::dump($menu);
        }

        return $menu;
	}

}
