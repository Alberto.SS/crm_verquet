<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class TerminalesRepository extends EntityRepository
{
	function findByTerminalesActualizacion(){
	$em = $this->getEntityManager();
 
		//Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Terminales t
        	WHERE t.idTerminal IN(1,2,3,4)";

        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();
        return $entities;
	}

	function findByTerminalesFisicas(){
		$em = $this->getEntityManager();
 
		//Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT t FROM MDRPuntoVentaBundle:Terminales t
        	WHERE t.idTerminal IN(7,8,9,10,11)";

        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();
        return $entities;
	}


}
