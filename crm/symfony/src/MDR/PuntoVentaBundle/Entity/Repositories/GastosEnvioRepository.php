<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class GastosEnvioRepository extends EntityRepository
{

	function findByGastosTDC(){
                $em = $this->getEntityManager();

        $strQuery = "SELECT gv FROM MDRPuntoVentaBundle:GastosEnvio gv WHERE gv.tipoPago = 1 AND gv.activo = true";

        
        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();
        return $entities;
        }


}
