<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ListaNegraMontosRepository extends EntityRepository
{   
        function findExistProducto($clvproducto){
                $em = $this->getEntityManager();
 
                
                        $strQuery = "SELECT lnm FROM MDRPuntoVentaBundle:ListaNegraMontos lnm";
                        if($clvproducto){
                            $strQuery .=" WHERE lnm.ClaveProducto = :clvproducto";
                        }                    
                        $query = $em->createQuery($strQuery);
                        if($clvproducto){
                            $query->setParameter(':clvproducto', $clvproducto);
                        }
                        $entities = $query->getResult();
                        return $entities;
        }


}
