<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class TipoRespuestaRepository extends EntityRepository
{
	function findByRespuestasCancelacion(){
	$em = $this->getEntityManager();
 
	//Tipo de respuesta para solucionar problemas del WS
        $strQuery = "SELECT tr FROM MDRPuntoVentaBundle:TipoRespuesta tr
        	WHERE tr.idTipoRespuesta in(3,4)";

        $query = $em->createQuery($strQuery);
        $entities = $query->getResult();
        return $entities;
	}


}
