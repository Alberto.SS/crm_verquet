<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

class ProductosPedidoRepository extends EntityRepository
{
	function findByOperaciones($fechaInicio, $fechaFin, $usuario){
		$em = $this->getEntityManager();
 
		//Pedidos que no han sido cobrados y no estan posfechados
        $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Bitacora p
        				WHERE p.fecha >= :fechaInicio AND p.fecha <= :fechaFin AND p.usuario = :usuario";

        $query = $em->createQuery($strQuery);
        $query->setParameter(':fechaInicio', $fechaInicio);
        $query->setParameter(':fechaFin', $fechaFin);
        $query->setParameter(':usuario', $usuario);
        $entities = $query->getResult();
        return $entities;
	}


}
