<?php

namespace MDR\PuntoVentaBundle\Entity\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * ProductosRepository
 */
class ProductosRepository extends EntityRepository
{
        public function findByDid($numero)
	{
		$em = $this->getEntityManager();

                $strQuery = "SELECT p FROM MDRPuntoVentaBundle:Productos p
                                INNER JOIN p.grupos g INNER JOIN g.especialidades e INNER JOIN e.dids d
                                WHERE d.numero = :numero
                                ";

                $query = $em->createQuery($strQuery);
                $query->setParameter('numero', $numero);
                $entities = $query->getResult();

                return $entities;
	}

        public function findByRepProductos($OficinaVta, $Producto, $Activo)
        {
                $em = $this->getEntityManager();

                $strQuery = "SELECT pp,ov FROM MDRPuntoVentaBundle:ProductoPrecios pp
                             INNER JOIN pp.oficinaVenta ov INNER JOIN pp.producto p";
                if($OficinaVta)
                {
                    $strQuery .= ' WHERE pp.oficinaVenta = :OficinaVta';
                }
                if($Producto)
                {
                   if($OficinaVta){
                        $strQuery .= ' AND pp.producto = :Producto';
                   }else{
                        $strQuery .= ' WHERE pp.producto = :Producto';
                   }       
                }
                if($Activo || $Activo == "0" )
                {
                    if($OficinaVta || $Producto){
                        $strQuery .= ' AND pp.activo = :Activo';
                    }else{
                        $strQuery .= ' WHERE pp.activo = :Activo';
                    } 
                }
                               
                $query = $em->createQuery($strQuery);
                if($OficinaVta)
                {
                    $query->setParameter('OficinaVta', $OficinaVta);
                }
                if($Producto)
                {
                     $query->setParameter('Producto', $Producto);    
                }
                if($Activo || $Activo == "0")
                {
                    $query->setParameter('Activo', $Activo);
                }
                
                $entities = $query->getResult();

                return $entities;
        }

        public function findByRepFiltro($fechaInicio, $OfiVentas, $Tpago)
        {
                $em = $this->getEntityManager();
                $fechaFin =  clone $fechaInicio;;
                $fechaFin ->add(new \DateInterval('P1D')); 

                $strQuery = "SELECT p,pp,ppc,ov FROM MDRPuntoVentaBundle:Pedidos p
                             INNER JOIN p.productos pp INNER JOIN pp.productoPrecio ppc INNER JOIN ppc.oficinaVenta ov 
                             WHERE p.fecha BETWEEN  :fechaInicio AND :fechaFin ";
                if($OfiVentas)
                {
                    $strQuery .= ' AND ppc.oficinaVenta = :OficinaVta';
                }
                if($Tpago)
                {
                    $strQuery .= ' AND p.pago = :Pago';                      
                }
                              
                $query = $em->createQuery($strQuery);
                $query->setParameter(':fechaInicio', $fechaInicio);
                $query->setParameter(':fechaFin', $fechaFin);
                if($OfiVentas)
                {
                    $query->setParameter(':OficinaVta', $OfiVentas);
                }
                if($Tpago)
                {
                     $query->setParameter(':Pago', $Tpago);    
                }

                
                $entities = $query->getResult();

                return $entities;
        }

}
