<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * IntranetNoticias
 */
class IntranetNoticias implements JsonSerializable
{
    /**
     * @var string
     */
    private $Imagen;

    /**
     * @var string
     */
    private $TituloNoticia;

    /**
     * @var string
     */
    private $Noticia;

    /**
     * @var string
     */
    private $URL;

    /**
     * @var \DateTime
     */
    private $FechaInicio;

    /**
     * @var \DateTime
     */
    private $FechaFin;

    /**
     * @var string
     */
    private $TipoNoticia;

    /**
     * @var integer
     */
    private $Usuario;

    /**
     * @var integer
     */
    private $idnoticia;


    /**
     * Set Imagen
     *
     * @param string $imagen
     * @return IntranetNoticias
     */
    public function setImagen($imagen)
    {
        $this->Imagen = $imagen;

        return $this;
    }

    /**
     * Get Imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->Imagen;
    }

    /**
     * Set TituloNoticia
     *
     * @param string $tituloNoticia
     * @return IntranetNoticias
     */
    public function setTituloNoticia($tituloNoticia)
    {
        $this->TituloNoticia = $tituloNoticia;

        return $this;
    }

    /**
     * Get TituloNoticia
     *
     * @return string 
     */
    public function getTituloNoticia()
    {
        return $this->TituloNoticia;
    }

    /**
     * Set Noticia
     *
     * @param string $noticia
     * @return IntranetNoticias
     */
    public function setNoticia($noticia)
    {
        $this->Noticia = $noticia;

        return $this;
    }

    /**
     * Get Noticia
     *
     * @return string 
     */
    public function getNoticia()
    {
        return $this->Noticia;
    }

    /**
     * Set URL
     *
     * @param string $uRL
     * @return IntranetNoticias
     */
    public function setURL($uRL)
    {
        $this->URL = $uRL;

        return $this;
    }

    /**
     * Get URL
     *
     * @return string 
     */
    public function getURL()
    {
        return $this->URL;
    }

    /**
     * Set FechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return IntranetNoticias
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->FechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get FechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->FechaInicio;
    }

    /**
     * Set FechaFin
     *
     * @param \DateTime $fechaFin
     * @return IntranetNoticias
     */
    public function setFechaFin($fechaFin)
    {
        $this->FechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get FechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->FechaFin;
    }

    /**
     * Set TipoNoticia
     *
     * @param string $tipoNoticia
     * @return IntranetNoticias
     */
    public function setTipoNoticia($tipoNoticia)
    {
        $this->TipoNoticia = $tipoNoticia;

        return $this;
    }

    /**
     * Get TipoNoticia
     *
     * @return string 
     */
    public function getTipoNoticia()
    {
        return $this->TipoNoticia;
    }

    /**
     * Set Usuario
     *
     * @param integer $usuario
     * @return IntranetNoticias
     */
    public function setUsuario($usuario)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return integer 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Get idnoticia
     *
     * @return integer 
     */
    public function getIdnoticia()
    {
        return $this->idnoticia;
    }

     public function __GregorianDateTimeInicio()
    {
        if($this->FechaInicio != null)
        {
            return date_format($this->FechaInicio,'d-m-Y');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y');
        }
    }

     public function __GregorianDateTimeFin()
    {
        if($this->FechaFin != null)
        {
            return date_format($this->FechaFin,'d-m-Y');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y');
        }
    }

    public function jsonSerialize()
    {
        return array(

            'Imagen'            => $this->Imagen,            
            'TituloNoticia'     => $this->TituloNoticia,
            'Noticia'           => $this->Noticia,
            'URL'               => $this->URL,
            'FechaInicio'       => $this->FechaInicio,
            'FechaFin'          => $this->FechaFin,
            'TipoNoticia'       => $this->TipoNoticia,
            'Usuario'           => $this->Usuario,
            'idNoticia'         => $this->idnoticia,
            'InicioGregorian'   => $this->__GregorianDateTimeInicio(),
            'FinGregorian'      => $this->__GregorianDateTimeFin(), 
        );

    }
    


    
}
