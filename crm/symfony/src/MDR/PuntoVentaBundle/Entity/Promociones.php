<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Promociones
 */
class Promociones implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var float
     */
    private $montoMinimo;

    /**
     * @var integer
     */
    private $idPromocion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bines;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Promociones
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Promociones
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set montoMinimo
     *
     * @param float $montoMinimo
     * @return Promociones
     */
    public function setMontoMinimo($montoMinimo)
    {
        $this->montoMinimo = $montoMinimo;

        return $this;
    }

    /**
     * Get montoMinimo
     *
     * @return float 
     */
    public function getMontoMinimo()
    {
        return $this->montoMinimo;
    }

    /**
     * Get idPromocion
     *
     * @return integer 
     */
    public function getIdPromocion()
    {
        return $this->idPromocion;
    }

    /**
     * Add bines
     *
     * @param \MDR\PuntoVentaBundle\Entity\BinTerminal $bines
     * @return Promociones
     */
    public function addBine(\MDR\PuntoVentaBundle\Entity\BinTerminal $bines)
    {
        $this->bines[] = $bines;

        return $this;
    }

    /**
     * Remove bines
     *
     * @param \MDR\PuntoVentaBundle\Entity\BinTerminal $bines
     */
    public function removeBine(\MDR\PuntoVentaBundle\Entity\BinTerminal $bines)
    {
        $this->bines->removeElement($bines);
    }

    /**
     * Get bines
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBines()
    {
        return $this->bines;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    public function jsonSerialize()
    {
        return array(
            'value'         => $this->idPromocion,
            'text'          => $this->descripcion,
            'idPromocion'   => $this->idPromocion,
            'montoMinimo'   => $this->montoMinimo,
            'descripcion'   => $this->descripcion,
            'toString'      => $this->__toString(),
        );

    }
}
