<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ComentariosPedido
 */
class ComentariosPedido implements JsonSerializable
{
    /**
     * @var string
     */
    private $comentario;

    /**
     * @var \Datetime
     */
    private $fecha;

    /**
     * @var integer
     */
    private $idComentario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\SubTipoLlamada
     */
    private $tipoLlamada;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return ComentariosPedido
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return ComentariosPedido
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fecha
     *
     * @param \Datetime $fecha
     * @return ComentariosPedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \Datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Get idComentario
     *
     * @return integer 
     */
    public function getIdComentario()
    {
        return $this->idComentario;
    }

    /**
     * Set tipoLlamada
     *
     * @param \MDR\PuntoVentaBundle\Entity\SubTipoLlamada $tipoLlamada
     * @return ComentariosPedido
     */
    public function setTipoLlamada(\MDR\PuntoVentaBundle\Entity\SubTipoLlamada $tipoLlamada = null)
    {
        $this->tipoLlamada = $tipoLlamada;

        return $this;
    }

    /**
     * Get tipoLlamada
     *
     * @return \MDR\PuntoVentaBundle\Entity\SubTipoLlamada 
     */
    public function getTipoLlamada()
    {
        return $this->tipoLlamada;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return ComentariosPedido
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

  public function __GregorianDateTime()
    {
        return date_format($this->fecha,'d-m-Y  H:i:s');
    }

    public function jsonSerialize()
    {
        return array(

            'comentario'        => $this->comentario,
            'fecha'             => $this->fecha,
            'fechaGregorian'    => $this->__GregorianDateTime(),
            'idComentario'      => $this->idComentario,
            'tipoLlamada'       => $this->tipoLlamada,
            'pedido'            => $this->pedido,
            'usuario'           => $this->usuario,
                       
        );

    }

}
