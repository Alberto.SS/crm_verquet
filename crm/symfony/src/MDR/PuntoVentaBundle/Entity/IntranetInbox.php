<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetInbox
 */
class IntranetInbox implements JsonSerializable
{
    /**
     * @var integer
     */
    private $idInbox;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetEstatusInbox
     */
    private $Estatus;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetMensajeInbox
     */
    private $mensaje;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $Receptor;

    /**
     * @var \DateTime
     */
    private $FechaEliminado;


    /**
     * Get idInbox
     *
     * @return integer 
     */
    public function getIdInbox()
    {
        return $this->idInbox;
    }

    /**
     * Set Estatus
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetEstatusInbox $estatus
     * @return IntranetInbox
     */
    public function setEstatus(\MDR\PuntoVentaBundle\Entity\Intranetestatusinbox $estatus = null)
    {
        $this->Estatus = $estatus;

        return $this;
    }

    /**
     * Get Estatus
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetEstatusInbox 
     */
    public function getEstatus()
    {
        return $this->Estatus;
    }

    /**
     * Set Mensaje
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetMensajeInbox $mensaje
     * @return IntranetInbox
     */
    public function setMensaje(\MDR\PuntoVentaBundle\Entity\Intranetmensajeinbox $mensaje = null)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get Mensaje
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetMensajeInbox 
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set Receptor
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $receptor
     * @return IntranetInbox
     */
    public function setReceptor(\MDR\PuntoVentaBundle\Entity\Usuario $receptor = null)
    {
        $this->Receptor = $receptor;

        return $this;
    }

    /**
     * Get Receptor
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getReceptor()
    {
        return $this->Receptor;
    }

    /**
     * Set FechaEliminado
     *
     * @param \DateTime $fechaEliminado
     * @return IntranetInbox
     */
    public function setFechaEliminado($fechaEliminado)
    {
        $this->FechaEliminado = $fechaEliminado;

        return $this;
    }

    /**
     * Get FechaEliminado
     *
     * @return \DateTime 
     */
    public function getFechaEliminado()
    {
        return $this->FechaEliminado;
    }

    public function jsonSerialize()
    {
        return array(

            'idInbox'           => $this->idInbox,
            'Estatus'           => $this->Estatus,
            //'Mensaje'   => $this->mensaje,
            'Receptor'          => $this->Receptor,
            'FechaEliminado'    => $this->FechaEliminado,
                       
        );

    }
}
