<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetRegistrosTalleres
 */
class IntranetRegistrosTalleres implements JsonSerializable
{
    /**
     * @var integer
     */
    private $idRegistro;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetTalleres
     */
    private $taller;


    /**
     * Get idRegistro
     *
     * @return integer 
     */
    public function getIdRegistro()
    {
        return $this->idRegistro;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return IntranetRegistrosTalleres
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set taller
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetTalleres $taller
     * @return IntranetRegistrosTalleres
     */
    public function setTaller(\MDR\PuntoVentaBundle\Entity\IntranetTalleres $taller = null)
    {
        $this->taller = $taller;

        return $this;
    }

    /**
     * Get taller
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetTalleres 
     */
    public function getTaller()
    {
        return $this->taller;
    }

    public function jsonSerialize()
    {
        return array(

            'idRegistro'    => $this->idRegistro,            
            'usuario'       => $this->usuario,
                       
        );

    }
}
