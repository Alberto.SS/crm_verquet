<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use JsonSerializable;
/**
 * Usuario
 */
class Usuario implements AdvancedUserInterface,JsonSerializable, \Serializable
{
    
    /**
     * @var string
     */
    private $uClave;

    /**
     * @var string
     */
    private $uPassword;

    /**
     * @var string
     */
    private $uNombre;

    /**
     * @var string
     */
    private $uApPaterno;

    /**
     * @var string
     */
    private $uApMaterno;

    /**
     * @var string
     */
    private $urfc;

    /**
     * @var integer
     */
    private $unumerosupervisor;


    /**
     * @var integer
     */
    private $idCampana;

    /**
     * @var integer
     */
    private $idCentro;

    /**
     * @var integer
     */
    private $activo;

    /**
     * @var string
     */
    private $eUser;

    /**
     * @var string
     */
    private $ePass;

    /**
     * @var string
     */
    private $eAgent;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $uNumero;

    /**
     * @var string
     */
    private $tokenSesion;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $direcciones;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoUsuario
     */
    private $tipoUsuario;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pedidosRecuperacion;

    /**
     * @var \DateTime
     */
    private $uFechaNac;

    /**
     * @var \DateTime
     */
    private $ufechaalta;

    /**
     * @var \DateTime
     */
    private $ufechamodificacion;
    
    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuarioModificacion;

    /**
     * @var string
     */
    
    private $temaIntranet;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetAvatar
     */
    private $avatarIntranet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->direcciones = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pedidosRecuperacion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set uClave
     *
     * @param string $uClave
     * @return Usuario
     */
    public function setUClave($uClave)
    {
        $this->uClave = $uClave;

        return $this;
    }

    /**
     * Get uClave
     *
     * @return string 
     */
    public function getUClave()
    {
        return $this->uClave;
    }

    /**
     * Set uPassword
     *
     * @param string $uPassword
     * @return Usuario
     */
    public function setUPassword($uPassword)
    {
        $this->uPassword = $uPassword;

        return $this;
    }

    /**
     * Get uPassword
     *
     * @return string 
     */
    public function getUPassword()
    {
        return $this->uPassword;
    }

    /**
     * Set uNombre
     *
     * @param string $uNombre
     * @return Usuario
     */
    public function setUNombre($uNombre)
    {
        $this->uNombre = $uNombre;

        return $this;
    }

    /**
     * Get uNombre
     *
     * @return string 
     */
    public function getUNombre()
    {
        return $this->uNombre;
    }

    /**
     * Set uApPaterno
     *
     * @param string $uApPaterno
     * @return Usuario
     */
    public function setUApPaterno($uApPaterno)
    {
        $this->uApPaterno = $uApPaterno;

        return $this;
    }

    /**
     * Get uApPaterno
     *
     * @return string 
     */
    public function getUApPaterno()
    {
        return $this->uApPaterno;
    }

    /**
     * Set uApMaterno
     *
     * @param string $uApMaterno
     * @return Usuario
     */
    public function setUApMaterno($uApMaterno)
    {
        $this->uApMaterno = $uApMaterno;

        return $this;
    }

    /**
     * Get uApMaterno
     *
     * @return string 
     */
    public function getUApMaterno()
    {
        return $this->uApMaterno;
    }

    /**
     * Set urfc
     *
     * @param string $urfc
     * @return Usuario
     */
    public function setUrfc($urfc)
    {
        $this->urfc = $urfc;

        return $this;
    }

    /**
     * Get urfc
     *
     * @return string 
     */
    public function getUrfc()
    {
        return $this->urfc;
    }

    /**
     * Set unumerosupervisor
     *
     * @param integer $unumerosupervisor
     * @return Usuario
     */
    public function setUnumerosupervisor($unumerosupervisor)
    {
        $this->unumerosupervisor = $unumerosupervisor;

        return $this;
    }

    /**
     * Get unumerosupervisor
     *
     * @return integer 
     */
    public function getUnumerosupervisor()
    {
        return $this->unumerosupervisor;
    }

    /**
     * Set tokenSesion
     *
     * @param string $tokenSesion
     * @return Usuario
     */
    public function setTokenSesion($tokenSesion)
    {
        $this->tokenSesion = $tokenSesion;

        return $this;
    }

    /**
     * Get tokenSesion
     *
     * @return string 
     */
    public function getTokenSesion()
    {
        return $this->tokenSesion;
    }
    


    /**
     * Set idCampana
     *
     * @param integer $idCampana
     * @return Usuario
     */
    public function setIdCampana($idCampana)
    {
        $this->idCampana = $idCampana;

        return $this;
    }

    /**
     * Get idCampana
     *
     * @return integer 
     */
    public function getIdCampana()
    {
        return $this->idCampana;
    }

    /**
     * Set idCentro
     *
     * @param integer $idCentro
     * @return Usuario
     */
    public function setIdCentro($idCentro)
    {
        $this->idCentro = $idCentro;

        return $this;
    }

    /**
     * Get idCentro
     *
     * @return integer 
     */
    public function getIdCentro()
    {
        return $this->idCentro;
    }

    /**
     * Set activo
     *
     * @param integer $activo
     * @return Usuario
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return integer 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set eUser
     *
     * @param string $eUser
     * @return Usuario
     */
    public function setEUser($eUser)
    {
        $this->eUser = $eUser;

        return $this;
    }

    /**
     * Get eUser
     *
     * @return string 
     */
    public function getEUser()
    {
        return $this->eUser;
    }

    /**
     * Set ePass
     *
     * @param string $ePass
     * @return Usuario
     */
    public function setEPass($ePass)
    {
        $this->ePass = $ePass;

        return $this;
    }

    /**
     * Get ePass
     *
     * @return string 
     */
    public function getEPass()
    {
        return $this->ePass;
    }

    /**
     * Set eAgent
     *
     * @param string $eAgent
     * @return Usuario
     */
    public function setEAgent($eAgent)
    {
        $this->eAgent = $eAgent;

        return $this;
    }

    /**
     * Get eAgent
     *
     * @return string 
     */
    public function getEAgent()
    {
        return $this->eAgent;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->uNumero;
    }

    /**
     * Get uNumero
     *
     * @return integer 
     */
    public function getUNumero()
    {
        return $this->uNumero;
    }

    /**
     * Add direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones
     * @return Usuario
     */
    public function addDireccion(\MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones)
    {
        $this->direcciones[] = $direcciones;

        return $this;
    }

    /**
     * Remove direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones
     */
    public function removeDireccion(\MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones)
    {
        $this->direcciones->removeElement($direcciones);
    }

    /**
     * Get direcciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDirecciones()
    {
        return $this->direcciones;
    }

    /**
     * Set tipoUsuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoUsuario $tipoUsuario
     * @return Usuario
     */
    public function setTipoUsuario(\MDR\PuntoVentaBundle\Entity\TipoUsuario $tipoUsuario = null)
    {
        $this->tipoUsuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get tipoUsuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoUsuario 
     */
    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }

    /**
     * Add pedidosRecuperacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedidosRecuperacion
     * @return Usuario
     */
    public function addPedidosRecuperacion(\MDR\PuntoVentaBundle\Entity\Pedidos $pedidosRecuperacion)
    {
        $this->pedidosRecuperacion[] = $pedidosRecuperacion;

        return $this;
    }

    /**
     * Remove pedidosRecuperacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedidosRecuperacion
     */
    public function removePedidosRecuperacion(\MDR\PuntoVentaBundle\Entity\Pedidos $pedidosRecuperacion)
    {
        $this->pedidosRecuperacion->removeElement($pedidosRecuperacion);
    }

    /**
     * Get pedidosRecuperacion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPedidosRecuperacion()
    {
        return $this->pedidosRecuperacion;
    }


    /**
     * Set uFechaNac
     *
     * @param \DateTime $uFechaNac
     * @return Usuario
     */
    public function setUFechaNac($uFechaNac)
    {
        $this->uFechaNac = $uFechaNac;

        return $this;
    }

    /**
     * Get uFechaNac
     *
     * @return \DateTime 
     */
    public function getUFechaNac()
    {
        return $this->uFechaNac;
    }

    /**
     * Set ufechaalta
     *
     * @param \DateTime $ufechaalta
     * @return Usuario
     */
    public function setUfechaalta($ufechaalta)
    {
        $this->ufechaalta = $ufechaalta;

        return $this;
    }

    /**
     * Get ufechaalta
     *
     * @return \DateTime 
     */
    public function getUfechaalta()
    {
        return $this->ufechaalta;
    }

    /**
     * Set ufechamodificacion
     *
     * @param \DateTime $ufechamodificacion
     * @return Usuario
     */
    public function setUfechamodificacion($ufechamodificacion)
    {
        $this->ufechamodificacion = $ufechamodificacion;

        return $this;
    }

    /**
     * Get ufechamodificacion
     *
     * @return \DateTime 
     */
    public function getUfechamodificacion()
    {
        return $this->ufechamodificacion;
    }

    /**
     * Add direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones
     * @return Usuario
     */
    public function addDireccione(\MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones)
    {
        $this->direcciones[] = $direcciones;

        return $this;
    }

    /**
     * Remove direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones
     */
    public function removeDireccione(\MDR\PuntoVentaBundle\Entity\DireccionesUsuarios $direcciones)
    {
        $this->direcciones->removeElement($direcciones);
    }
    
    /**
     * @var \MDR\PuntoVentaBundle\Entity\Horarios
     */
    private $horario;


    /**
     * Set horario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Horarios $horario
     * @return Usuario
     */
    public function setHorario(\MDR\PuntoVentaBundle\Entity\Horarios $horario = null)
    {
        $this->horario = $horario;

        return $this;
    }

    /**
     * Get horario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Horarios 
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * Set usuarioModificacion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuarioModificacion
     * @return Usuario
     */
    public function setUsuarioModificacion(\MDR\PuntoVentaBundle\Entity\Usuario $usuarioModificacion = null)
    {
        $this->usuarioModificacion = $usuarioModificacion;

        return $this;
    }

    /**
     * Get usuarioModificacion
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuarioModificacion()
    {
        return $this->usuarioModificacion;
    }

    
     /**
     * Set temaIntranet
     *
     * @param string $temaIntranet
     * @return Usuario
     */
    public function setTemaIntranet($temaIntranet)
    {
        $this->temaIntranet = $temaIntranet;

        return $this;
    }

    /**
     * Get temaIntranet
     *
     * @return string 
     */
    public function getTemaIntranet()
    {
        return $this->temaIntranet;
    }

    /**
     * Set avatarIntranet
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetAvatar $avatarIntranet
     * @return Usuario
     */
    public function setAvatarIntranet(\MDR\PuntoVentaBundle\Entity\IntranetAvatar $avatarIntranet = null)
    {
        $this->avatarIntranet = $avatarIntranet;

        return $this;
    }

    /**
     * Get avatarIntranet
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetAvatar 
     */
    public function getAvatarIntranet()
    {
        return $this->avatarIntranet;
    }

/* Implementacióin de metodos de UserInterface */
    public function getPassword()
    {
        return $this->uPassword;
    }
    public function getSalt()
    {
        return  '';
    }

    /* Implementacióin de metodos de AdvancedUserInterface */
    public function eraseCredentials() {
        
    }

    public function getRoles() {
        return array($this->tipoUsuario);
    }

    public function getUsername() {
        return $this->uClave;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function isEnabled() {
        return true;
    }

    public function __sleep(){
        return array('id');
    }
    
    public function serialize() {
        return serialize(array(
            $this->uNumero, $this->eUser
            ));
    }

    public function unserialize($serialized) {
        list(
        $this->uNumero, $this->uClave
        ) = unserialize($serialized);
    }

    public function name() {
        return $this->uNombre.' '.$this->uApPaterno.' '.$this->uApMaterno;
    }
    
    public function __toString(){
        return $this->uClave.' - '.$this->name();
    }


    public function jsonSerialize()
    {
        return array(

            'uClave'                => $this->uClave,
            'uPassword'             => $this->uPassword,
            'uNombre'               => $this->uNombre,
            'uApPaterno'            => $this->uApPaterno,
            'uApMaterno'            => $this->uApMaterno,
            'urfc'                  => $this->urfc,
            'unumerosupervisor'     => $this->unumerosupervisor,
            'horario'               => $this->horario,
            'idCampana'             => $this->idCampana,
            'idCentro'              => $this->idCentro,
            'activo'                => $this->activo,
            'eUser'                 => $this->eUser,
            'ePass'                 => $this->ePass,
            'eAgent'                => $this->eAgent,
            'email'                 => $this->email,
            'uNumero'               => $this->uNumero,
            'tipoUsuario'           => $this->tipoUsuario,
            'name'                  => $this->name(),
            'fullname'              => $this->__toString(),
            'direcciones'           => $this->direcciones->toArray(),
            'avatar'                => $this->avatarIntranet,            
        );

    }
}
