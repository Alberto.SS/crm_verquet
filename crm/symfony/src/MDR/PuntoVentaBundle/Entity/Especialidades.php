<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Especialidades
 */
class Especialidades
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idEspecialidad;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $grupos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dids;

    /**
	
	
     * Constructor
     */
    public function __construct()
    {
        $this->grupos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dids = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Especialidades
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idEspecialidad
     *
     * @return integer 
     */
    public function getIdEspecialidad()
    {
        return $this->idEspecialidad;
    }

    /**
     * Add grupos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Grupos $grupos
     * @return Especialidades
     */
    public function addGrupo(\MDR\PuntoVentaBundle\Entity\Grupos $grupos)
    {
        $this->grupos[] = $grupos;

        return $this;
    }

    /**
     * Remove grupos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Grupos $grupos
     */
    public function removeGrupo(\MDR\PuntoVentaBundle\Entity\Grupos $grupos)
    {
        $this->grupos->removeElement($grupos);
    }

    /**
     * Get grupos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrupos()
    {
        return $this->grupos;
    }

    /**
     * Add dids
     *
     * @param \MDR\PuntoVentaBundle\Entity\Dids $dids
     * @return Especialidades
     */
    public function addDid(\MDR\PuntoVentaBundle\Entity\Dids $dids)
    {
        $this->dids[] = $dids;

        return $this;
    }

    /**
     * Remove dids
     *
     * @param \MDR\PuntoVentaBundle\Entity\Dids $dids
     */
    public function removeDid(\MDR\PuntoVentaBundle\Entity\Dids $dids)
    {
        $this->dids->removeElement($dids);
    }

    /**
     * Get dids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDids()
    {
        return $this->dids;
    }
	
	    public function __toString()
    {
        return $this->descripcion;
    }
}
