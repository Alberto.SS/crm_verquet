<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetAvatar
 */
class IntranetAvatar implements JsonSerializable
{
    /**
     * @var string
     */
    private $URL;

    /**
     * @var integer
     */
    private $idAvatar;


    /**
     * Set URL
     *
     * @param string $uRL
     * @return IntranetAvatar
     */
    public function setURL($uRL)
    {
        $this->URL = $uRL;

        return $this;
    }

    /**
     * Get URL
     *
     * @return string 
     */
    public function getURL()
    {
        return $this->URL;
    }

    /**
     * Get idAvatar
     *
     * @return integer 
     */
    public function getIdAvatar()
    {
        return $this->idAvatar;
    }

    public function jsonSerialize()
    {
        return array(
            
            'URL'       => $this->URL,
            'idAvatar'  => $this->idAvatar,
                       
        );

    }
}
