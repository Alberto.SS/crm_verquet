<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Mensajeria
 */
class Mensajeria implements JsonSerializable
{
    /**
     * @var string
     */
    private $empresa;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var string
     */
    private $ruta;

    /**
     * @var integer
     */
    private $prioridad;

    /**
     * @var integer
     */
    private $idMensajeria;


    /**
     * Set empresa
     *
     * @param string $empresa
     * @return Mensajeria
     */
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Get empresa
     *
     * @return string 
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set claveSAP
     *
     * @param string $claveSAP
     * @return Mensajeria
     */
    public function setClaveSAP($claveSAP)
    {
        $this->claveSAP = $claveSAP;

        return $this;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Mensajeria
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set prioridad
     *
     * @param integer $prioridad
     * @return Mensajeria
     */
    public function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    /**
     * Get prioridad
     *
     * @return integer 
     */
    public function getPrioridad()
    {
        return $this->prioridad;
    }

    /**
     * Get idMensajeria
     *
     * @return integer 
     */
    public function getIdMensajeria()
    {
        return $this->idMensajeria;
    }

    public function __toString()
    {
        return $this->empresa;
    }

    public function jsonSerialize()
    {
        return array(
            'empresa'           => $this->empresa,
            'claveSAP'          => $this->claveSAP,
            'ruta'              => $this->ruta,
            'prioridad'         => $this->prioridad,
            'idMensajeria'      => $this->idMensajeria,
                       
        );

    }
}
