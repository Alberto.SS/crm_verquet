<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * CoberturaEnvio
 */
class CoberturaEnvio implements JsonSerializable
{
    /**
     * @var string
     */
    private $cP;

    /**
     * @var integer
     */
    private $idCobertura;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoPagos
     */
    private $tipoPago;

    /**
     * @var integer
     */
    private $idMensajeria;

    /**
     * @var string
     */
    private $tiempoEntregaMin;

    /**
     * @var string
     */
    private $tiempoEntregaMax;


    /**
     * Set CP
     *
     * @param string $cP
     * @return CoberturaEnvio
     */
    public function setCP($cP)
    {
        $this->cP = $cP;

        return $this;
    }

    /**
     * Get CP
     *
     * @return string 
     */
    public function getCP()
    {
        return $this->cP;
    }

    /**
     * Get idCobertura
     *
     * @return integer 
     */
    public function getIdCobertura()
    {
        return $this->idCobertura;
    }

    /**
     * Set tipoPago
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago
     * @return CoberturaEnvio
     */
    public function setTipoPago(\MDR\PuntoVentaBundle\Entity\TipoPagos $tipoPago = null)
    {
        $this->tipoPago = $tipoPago;

        return $this;
    }

    /**
     * Get tipoPago
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoPagos 
     */
    public function getTipoPago()
    {
        return $this->tipoPago;
    }

     public function __toString()
    {
        return $this->cP;
    }

    /**
     * Set idMensajeria
     *
     * @param integer $idMensajeria
     * @return CoberturaEnvio
     */
    public function setIdMensajeria($idMensajeria)
    {
        $this->idMensajeria = $idMensajeria;

        return $this;
    }

    /**
     * Get idMensajeria
     *
     * @return integer 
     */
    public function getIdMensajeria()
    {
        return $this->idMensajeria;
    }

    /**
     * Set tiempoEntregaMin
     *
     * @param string $tiempoEntregaMin
     * @return CoberturaEnvio
     */
    public function setTiempoEntregaMin($tiempoEntregaMin)
    {
        $this->tiempoEntregaMin = $tiempoEntregaMin;

        return $this;
    }

    /**
     * Get tiempoEntregaMin
     *
     * @return string 
     */
    public function getTiempoEntregaMin()
    {
        return $this->tiempoEntregaMin;
    }

    /**
     * Set tiempoEntregaMax
     *
     * @param string $tiempoEntregaMax
     * @return CoberturaEnvio
     */
    public function setTiempoEntregaMax($tiempoEntregaMax)
    {
        $this->tiempoEntregaMax = $tiempoEntregaMax;

        return $this;
    }

    /**
     * Get tiempoEntregaMax
     *
     * @return string 
     */
    public function getTiempoEntregaMax()
    {
        return $this->tiempoEntregaMax;
    }

    public function jsonSerialize()
    {
        return array(
            'cp'                => $this->cP,
            'tipoPago'          => $this->tipoPago,
            'idMensajeria'      => $this->idMensajeria,
            'tiempoEntregaMin'  => $this->tiempoEntregaMin,
            'tiempoEntregaMax'  => $this->tiempoEntregaMax,
        );

    }
	
    

}
