<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ListaNegraClientes
 */
class ListaNegraClientes implements JsonSerializable
{
    /**
     * @var string
     */
    private $NombreCompleto;

    /**
     * @var boolean
     */
    private $Activo;

    /**
     * @var integer
     */
    private $idCliente;


    /**
     * Set NombreCompleto
     *
     * @param string $nombreCompleto
     * @return ListaNegraClientes
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->NombreCompleto = $nombreCompleto;

        return $this;
    }

    /**
     * Get NombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->NombreCompleto;
    }

    /**
     * Set Activo
     *
     * @param boolean $activo
     * @return ListaNegraClientes
     */
    public function setActivo($activo)
    {
        $this->Activo = $activo;

        return $this;
    }

    /**
     * Get Activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->Activo;
    }

    /**
     * Get idCliente
     *
     * @return integer 
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }
    
    public function jsonSerialize()
    {
        return array(

            'NombreCompleto'    => $this->NombreCompleto,
            'Activo'            => $this->Activo,
            'idCliente'         => $this->idCliente,
                       
        );

    }
}
