<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Horarios
 */
class Horarios implements JsonSerializable
{
 
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var \DateTime
     */
    private $horaentrada;

    /**
     * @var \DateTime
     */
    private $horasalida;

    /**
     * @var string
     */
    private $activo;

    /**
     * @var integer
     */
    private $idhorario;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Horarios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set horaentrada
     *
     * @param \DateTime $horaentrada
     * @return Horarios
     */
    public function setHoraentrada($horaentrada)
    {
        $this->horaentrada = $horaentrada;

        return $this;
    }

    /**
     * Get horaentrada
     *
     * @return \DateTime 
     */
    public function getHoraentrada()
    {
        return $this->horaentrada;
    }

    /**
     * Set horasalida
     *
     * @param \DateTime $horasalida
     * @return Horarios
     */
    public function setHorasalida($horasalida)
    {
        $this->horasalida = $horasalida;

        return $this;
    }

    /**
     * Get horasalida
     *
     * @return \DateTime 
     */
    public function getHorasalida()
    {
        return $this->horasalida;
    }

    /**
     * Set activo
     *
     * @param string $activo
     * @return Horarios
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return string 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get idhorario
     *
     * @return integer 
     */
    public function getIdhorario()
    {
        return $this->idhorario;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $horario;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->horario = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add horario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Horarios $horario
     * @return Horarios
     */
    public function addHorario(\MDR\PuntoVentaBundle\Entity\Horarios $horario)
    {
        $this->horario[] = $horario;

        return $this;
    }

    /**
     * Remove horario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Horarios $horario
     */
    public function removeHorario(\MDR\PuntoVentaBundle\Entity\Horarios $horario)
    {
        $this->horario->removeElement($horario);
    }

    /**
     * Get horario
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHorario()
    {
        return $this->horario;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

     public function jsonSerialize()
    {
        return array(

            'descripcion'   => $this->descripcion,
            'horaentrada'   => $this->horaentrada,
            'horasalida'    => $this->horasalida,
            'activo'        => $this->activo,
            'idhorario'     => $this->idhorario,

        );

    }
}
