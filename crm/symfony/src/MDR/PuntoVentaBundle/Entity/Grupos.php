<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupos
 */
class Grupos
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idGrupo;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Productos
     */
    private $productoPrincipal;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $especialidades;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->especialidades = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Grupos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idGrupo
     *
     * @return integer 
     */
    public function getIdGrupo()
    {
        return $this->idGrupo;
    }

    /**
     * Set productoPrincipal
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productoPrincipal
     * @return Grupos
     */
    public function setProductoPrincipal(\MDR\PuntoVentaBundle\Entity\Productos $productoPrincipal = null)
    {
        $this->productoPrincipal = $productoPrincipal;

        return $this;
    }

    /**
     * Get productoPrincipal
     *
     * @return \MDR\PuntoVentaBundle\Entity\Productos 
     */
    public function getProductoPrincipal()
    {
        return $this->productoPrincipal;
    }

    /**
     * Add productos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productos
     * @return Grupos
     */
    public function addProducto(\MDR\PuntoVentaBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Productos $productos
     */
    public function removeProducto(\MDR\PuntoVentaBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Add especialidades
     *
     * @param \MDR\PuntoVentaBundle\Entity\Especialidades $especialidades
     * @return Grupos
     */
    public function addEspecialidade(\MDR\PuntoVentaBundle\Entity\Especialidades $especialidades)
    {
        $this->especialidades[] = $especialidades;

        return $this;
    }

    /**
     * Remove especialidades
     *
     * @param \MDR\PuntoVentaBundle\Entity\Especialidades $especialidades
     */
    public function removeEspecialidade(\MDR\PuntoVentaBundle\Entity\Especialidades $especialidades)
    {
        $this->especialidades->removeElement($especialidades);
    }

    /**
     * Get especialidades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEspecialidades()
    {
        return $this->especialidades;
    }

	
}
