<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Colonias
 */
class Colonias implements JsonSerializable
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var string
     */
    private $codigoSepomex;

    /**
     * @var integer
     */
    private $idColonia;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Municipios
     */
    private $municipio;


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Colonias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Colonias
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set codigoSepomex
     *
     * @param string $codigoSepomex
     * @return Colonias
     */
    public function setCodigoSepomex($codigoSepomex)
    {
        $this->codigoSepomex = $codigoSepomex;

        return $this;
    }

    /**
     * Get codigoSepomex
     *
     * @return string 
     */
    public function getCodigoSepomex()
    {
        return $this->codigoSepomex;
    }

    /**
     * Get idColonia
     *
     * @return integer 
     */
    public function getIdColonia()
    {
        return $this->idColonia;
    }

    /**
     * Set municipio
     *
     * @param \MDR\PuntoVentaBundle\Entity\Municipios $municipio
     * @return Colonias
     */
    public function setMunicipio(\MDR\PuntoVentaBundle\Entity\Municipios $municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return \MDR\PuntoVentaBundle\Entity\Municipios 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function jsonSerialize()
    {
        return array(
            'idColonia'     => $this->idColonia,
            'nombre'        => $this->nombre,
            'cp'            => $this->cp,
            'codigoSepomex' => $this->codigoSepomex,
            'municipio'     => $this->municipio,
        );
    }
}



