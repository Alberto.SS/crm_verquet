<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Clientes
 */
class Clientes implements JsonSerializable
{
    /**
     * @var string
     * 
     * @Assert\NotBlank(message="Escriba un nombre")
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $apellidoPaterno;

    /**
     * @var string
     */
    private $apellidoMaterno;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \DateTime
     */
    private $fechaNacimiento;

    /**
     * @var integer
     * @Assert\NotBlank()
     */
    private $sexo;

    /**
     * @var integer
     */
    private $idCliente;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $telefonos;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $direcciones;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->telefonos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->direcciones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Clientes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return Clientes
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string 
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return Clientes
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string 
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Clientes
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return Clientes
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set sexo
     *
     * @param integer $sexo
     * @return Clientes
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return integer 
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Get sexo string
     *
     * @return string 
     */
    public function getSexoString()
    {
        return $this->sexo == 1 ? 'Masculino' : 'Femenino';
    }

    /**
     * Get idCliente
     *
     * @return integer 
     */
    public function getIdCliente()
    {
        return $this->idCliente;
    }

    /**
     * Add telefonos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Telefonos $telefonos
     * @return Clientes
     */
    public function addTelefono(\MDR\PuntoVentaBundle\Entity\Telefonos $telefonos)
    {
        $this->telefonos[] = $telefonos;

        return $this;
    }

    /**
     * Remove telefonos
     *
     * @param \MDR\PuntoVentaBundle\Entity\Telefonos $telefonos
     */
    public function removeTelefono(\MDR\PuntoVentaBundle\Entity\Telefonos $telefonos)
    {
        $this->telefonos->removeElement($telefonos);
    }

    /**
     * Get telefonos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelefonos()
    {
        return $this->telefonos;
    }

    /**
     * Add direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\Direcciones $direcciones
     * @return Clientes
     */
    public function addDireccione(\MDR\PuntoVentaBundle\Entity\Direcciones $direcciones)
    {
        $this->direcciones[] = $direcciones;

        return $this;
    }

    /**
     * Remove direcciones
     *
     * @param \MDR\PuntoVentaBundle\Entity\Direcciones $direcciones
     */
    public function removeDireccione(\MDR\PuntoVentaBundle\Entity\Direcciones $direcciones)
    {
        $this->direcciones->removeElement($direcciones);
    }

    /**
     * Get direcciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDirecciones()
    {
        return $this->direcciones;
    }

	public function getNombreCompleto()
    {
        return $this->nombre.' '.$this->apellidoPaterno.' '.$this->apellidoMaterno;
    }

    public function __toString()
    {
        return $this->getNombreCompleto();
    }

    public function jsonSerialize()
    {
        return array(
            'idCliente'         => $this->idCliente,
            'nombre'            => $this->nombre,
            'apellidoPaterno'   => $this->apellidoPaterno,
            'apellidoMaterno'   => $this->apellidoMaterno,
            'email'             => $this->email,
            'fechaNacimiento'   => $this->fechaNacimiento,
            'sexo'              => $this->sexo,
            'sexoString'        => $this->getSexoString(),
            'toString'          => $this->__toString(),
            'id'                => $this->idCliente,
            'text'              => $this->__toString(),
        );
    }
}