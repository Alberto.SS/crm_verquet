<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * PedidosTickets
 */
class PedidosTickets implements JsonSerializable
{
    
    /**
     * @var string
     */
    private $Comentarios;

    /**
     * @var integer
     */
    private $idTicket;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $Usuario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\EstatusTicket
     */
    private $EstatusTicket;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $Pedido;

    /**
     * @var \DateTime
     */
    private $Fecha;

    /**
     * Set Comentarios
     *
     * @param string $comentarios
     * @return PedidosTickets
     */
    public function setComentarios($comentarios)
    {
        $this->Comentarios = $comentarios;

        return $this;
    }

    /**
     * Get Comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->Comentarios;
    }

    /**
     * Get idTicket
     *
     * @return integer 
     */
    public function getIdTicket()
    {
        return $this->idTicket;
    }

    /**
     * Set Usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return PedidosTickets
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->Usuario = $usuario;

        return $this;
    }

    /**
     * Get Usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->Usuario;
    }

    /**
     * Set EstatusTicket
     *
     * @param \MDR\PuntoVentaBundle\Entity\EstatusTicket $estatusTicket
     * @return PedidosTickets
     */
    public function setEstatusTicket(\MDR\PuntoVentaBundle\Entity\EstatusTicket $estatusTicket = null)
    {
        $this->EstatusTicket = $estatusTicket;

        return $this;
    }

    /**
     * Get EstatusTicket
     *
     * @return \MDR\PuntoVentaBundle\Entity\EstatusTicket 
     */
    public function getEstatusTicket()
    {
        return $this->EstatusTicket;
    }

    /**
     * Set Pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PedidosTickets
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->Pedido = $pedido;

        return $this;
    }

    /**
     * Get Pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->Pedido;
    }

    /**
     * Set Fecha
     *
     * @param \DateTime $fecha
     * @return PedidosTickets
     */
    public function setFecha($fecha)
    {
        $this->Fecha = $fecha;

        return $this;
    }

    /**
     * Get Fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->Fecha;

    }

    public function __GregorianDateTime()
    {
            return date_format($this->Fecha,'d-m-Y  H:i:s');
       
    }

    public function jsonSerialize()
    {
        return array(

            'Comentarios'       => $this->Comentarios,
            'idTicket'          => $this->idTicket,
            'Usuario'           => $this->Usuario,
            'EstatusTicket'     => $this->EstatusTicket,
            'Fecha'             => $this->Fecha,
            'FechaGregorian'    => $this->__GregorianDateTime(),
            //'Pedido'        => $this->Pedido,
        );
    }    
}
