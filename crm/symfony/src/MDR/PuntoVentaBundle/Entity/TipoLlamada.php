<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * TipoLlamada
 */
class TipoLlamada implements JsonSerializable
{
	
	
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idTipoLlamada;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoLlamada
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idTipoLlamada
     *
     * @return integer 
     */
    public function getIdTipoLlamada()
    {
        return $this->idTipoLlamada;
    }
	
	
	
	        public function __toString()
    {
        return $this->getdescripcion().'';
    }

    public function jsonSerialize()
    {
        return array(

            'descripcion'      => $this->descripcion,
            'idTipoLlamada'    => $this->idTipoLlamada,                       
        );

    }
}
