<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * PaqueteriaFedex
 */
class PaqueteriaFedex implements JsonSerializable
{
    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $municipio;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var boolean
     */
    private $prePago;

    /**
     * @var boolean
     */
    private $cod;

    /**
     * @var integer
     */
    private $idPaqueteria;


    /**
     * Set estado
     *
     * @param string $estado
     * @return PaqueteriaFedex
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return PaqueteriaFedex
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     * @return PaqueteriaFedex
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return PaqueteriaFedex
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set prePago
     *
     * @param boolean $prePago
     * @return PaqueteriaFedex
     */
    public function setPrePago($prePago)
    {
        $this->prePago = $prePago;

        return $this;
    }

    /**
     * Get prePago
     *
     * @return boolean 
     */
    public function getPrePago()
    {
        return $this->prePago;
    }

    /**
     * Set cod
     *
     * @param boolean $cod
     * @return PaqueteriaFedex
     */
    public function setCod($cod)
    {
        $this->cod = $cod;

        return $this;
    }

    /**
     * Get cod
     *
     * @return boolean 
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * Get idPaqueteria
     *
     * @return integer 
     */
    public function getIdPaqueteria()
    {
        return $this->idPaqueteria;
    }

    public function jsonSerialize()
    {
        return array(
            'idPaqueteria'  => $this->idPaqueteria,
            'estado'        => $this->estado,
            'municipio'     => $this->municipio,
            'colonia'       => $this->colonia,
            'colonia'       => $this->colonia,
            'cp'            => $this->cp,
            'prePago'       => $this->prePago,
            'cod'           => $this->cod,
        );
    }
}
