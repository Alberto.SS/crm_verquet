<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * CanalDistribucion
 */
class CanalDistribucion implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $claveSAP;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return CanalDistribucion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
	

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }


    public function jsonSerialize()
    {
        return array(
            'claveSAP'              => $this->claveSAP,
            'descripcion'           => $this->descripcion,

        );
    }

	
    public function __toString()
    {
        return $this->descripcion;

    }
}
