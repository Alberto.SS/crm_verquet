<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetContenidos
 */
class IntranetContenidos implements JsonSerializable
{
    /**
     * @var string
     */
    private $imagen;

    /**
     * @var string
     */
    private $html1;

    /**
     * @var string
     */
    private $html2;

    /**
     * @var string
     */
    private $html3;

    /**
     * @var string
     */
    private $html4;

    /**
     * @var boolean
     */
    private $comentariosActivo;

    /**
     * @var \DateTime
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     */
    private $fechaEdicion;

    /**
     * @var integer
     */
    private $idContenido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuarioEdita;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuarioCrea;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetPlantillas
     */
    private $plantilla;


    /**
     * Set imagen
     *
     * @param string $imagen
     * @return IntranetContenidos
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set html1
     *
     * @param string $html1
     * @return IntranetContenidos
     */
    public function setHtml1($html1)
    {
        $this->html1 = $html1;

        return $this;
    }

    /**
     * Get html1
     *
     * @return string 
     */
    public function getHtml1()
    {
        return $this->html1;
    }

    /**
     * Set html2
     *
     * @param string $html2
     * @return IntranetContenidos
     */
    public function setHtml2($html2)
    {
        $this->html2 = $html2;

        return $this;
    }

    /**
     * Get html2
     *
     * @return string 
     */
    public function getHtml2()
    {
        return $this->html2;
    }

    /**
     * Set html3
     *
     * @param string $html3
     * @return IntranetContenidos
     */
    public function setHtml3($html3)
    {
        $this->html3 = $html3;

        return $this;
    }

    /**
     * Get html3
     *
     * @return string 
     */
    public function getHtml3()
    {
        return $this->html3;
    }

    /**
     * Set html4
     *
     * @param string $html4
     * @return IntranetContenidos
     */
    public function setHtml4($html4)
    {
        $this->html4 = $html4;

        return $this;
    }

    /**
     * Get html4
     *
     * @return string 
     */
    public function getHtml4()
    {
        return $this->html4;
    }

    /**
     * Set comentariosActivo
     *
     * @param boolean $comentariosActivo
     * @return IntranetContenidos
     */
    public function setComentariosActivo($comentariosActivo)
    {
        $this->comentariosActivo = $comentariosActivo;

        return $this;
    }

    /**
     * Get comentariosActivo
     *
     * @return boolean 
     */
    public function getComentariosActivo()
    {
        return $this->comentariosActivo;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     * @return IntranetContenidos
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime 
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaEdicion
     *
     * @param \DateTime $fechaEdicion
     * @return IntranetContenidos
     */
    public function setFechaEdicion($fechaEdicion)
    {
        $this->fechaEdicion = $fechaEdicion;

        return $this;
    }

    /**
     * Get fechaEdicion
     *
     * @return \DateTime 
     */
    public function getFechaEdicion()
    {
        return $this->fechaEdicion;
    }

    /**
     * Get idContenido
     *
     * @return integer 
     */
    public function getIdContenido()
    {
        return $this->idContenido;
    }

    /**
     * Set usuarioEdita
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuarioEdita
     * @return IntranetContenidos
     */
    public function setUsuarioEdita(\MDR\PuntoVentaBundle\Entity\Usuario $usuarioEdita = null)
    {
        $this->usuarioEdita = $usuarioEdita;

        return $this;
    }

    /**
     * Get usuarioEdita
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuarioEdita()
    {
        return $this->usuarioEdita;
    }

    /**
     * Set usuarioCrea
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuarioCrea
     * @return IntranetContenidos
     */
    public function setUsuarioCrea(\MDR\PuntoVentaBundle\Entity\Usuario $usuarioCrea = null)
    {
        $this->usuarioCrea = $usuarioCrea;

        return $this;
    }

    /**
     * Get usuarioCrea
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuarioCrea()
    {
        return $this->usuarioCrea;
    }

    /**
     * Set plantilla
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetPlantillas $plantilla
     * @return IntranetContenidos
     */
    public function setPlantilla(\MDR\PuntoVentaBundle\Entity\IntranetPlantillas $plantilla = null)
    {
        $this->plantilla = $plantilla;

        return $this;
    }

    /**
     * Get plantilla
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetPlantillas 
     */
    public function getPlantilla()
    {
        return $this->plantilla;
    }

    public function jsonSerialize()
    {
        return array(
            'idContenido'       => $this->idContenido,
            'imagen'            => $this->imagen,
            'html1'             => $this->html1,
            'html2'             => $this->html2,
            'html3'             => $this->html3,
            'html4'             => $this->html4,
            'comentariosActivo' => $this->comentariosActivo,
            'fechaCreacion'     => $this->fechaCreacion,
            'fechaEdicion'      => $this->fechaEdicion,
            'usuarioCrea'       => $this->usuarioCrea,
            'usuarioEdita'      => $this->usuarioEdita,
            'plantilla'         => $this->plantilla,
        );
    }
}