<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Direccionesusuarios
 */
class DireccionesUsuarios implements JsonSerializable
{
  
    /**
     * @var string
     */
    private $pais;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $municipio;

    /**
     * @var string
     */
    private $cp;

    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $calle;

    /**
     * @var string
     */
    private $entreCalles;

    /**
     * @var string
     */
    private $referencia;

    /**
     * @var string
     */
    private $noExt;

    /**
     * @var string
     */
    private $noInt;

    /**
     * @var string
     */
    private $telcasa;

    /**
     * @var string
     */
    private $telOficina;

    /**
     * @var string
     */
    private $telCel;

    /**
     * @var integer
     */
    private $idDireccion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;


    /**
     * Set pais
     *
     * @param string $pais
     * @return DireccionesUsuarios
     */
    public function setPais($pais)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string 
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return DireccionesUsuarios
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return DireccionesUsuarios
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return DireccionesUsuarios
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set colonia
     *
     * @param string $colonia
     * @return DireccionesUsuarios
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set calle
     *
     * @param string $calle
     * @return DireccionesUsuarios
     */
    public function setCalle($calle)
    {
        $this->calle = $calle;

        return $this;
    }

    /**
     * Get calle
     *
     * @return string 
     */
    public function getCalle()
    {
        return $this->calle;
    }

    /**
     * Set entreCalles
     *
     * @param string $entreCalles
     * @return DireccionesUsuarios
     */
    public function setEntreCalles($entreCalles)
    {
        $this->entreCalles = $entreCalles;

        return $this;
    }

    /**
     * Get entreCalles
     *
     * @return string 
     */
    public function getEntreCalles()
    {
        return $this->entreCalles;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return DireccionesUsuarios
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set noExt
     *
     * @param string $noExt
     * @return DireccionesUsuarios
     */
    public function setNoExt($noExt)
    {
        $this->noExt = $noExt;

        return $this;
    }

    /**
     * Get noExt
     *
     * @return string 
     */
    public function getNoExt()
    {
        return $this->noExt;
    }

    /**
     * Set noInt
     *
     * @param string $noInt
     * @return DireccionesUsuarios
     */
    public function setNoInt($noInt)
    {
        $this->noInt = $noInt;

        return $this;
    }

    /**
     * Get noInt
     *
     * @return string 
     */
    public function getNoInt()
    {
        return $this->noInt;
    }

    /**
     * Set telcasa
     *
     * @param string $telcasa
     * @return DireccionesUsuarios
     */
    public function setTelcasa($telcasa)
    {
        $this->telcasa = $telcasa;

        return $this;
    }

    /**
     * Get telcasa
     *
     * @return string 
     */
    public function getTelcasa()
    {
        return $this->telcasa;
    }

    /**
     * Set telOficina
     *
     * @param string $telOficina
     * @return DireccionesUsuarios
     */
    public function setTelOficina($telOficina)
    {
        $this->telOficina = $telOficina;

        return $this;
    }

    /**
     * Get telOficina
     *
     * @return string 
     */
    public function getTelOficina()
    {
        return $this->telOficina;
    }

    /**
     * Set telCel
     *
     * @param string $telCel
     * @return DireccionesUsuarios
     */
    public function setTelCel($telCel)
    {
        $this->telCel = $telCel;

        return $this;
    }

    /**
     * Get telCel
     *
     * @return string 
     */
    public function getTelCel()
    {
        return $this->telCel;
    }

    /**
     * Set idDireccion
     *
     * @param integer $idDireccion
     * @return DireccionesUsuarios
     */
    public function setIdDireccion($idDireccion)
    {
        $this->idDireccion = $idDireccion;

        return $this;
    }

    /**
     * Get idDireccion
     *
     * @return integer 
     */
    public function getIdDireccion()
    {
        return $this->idDireccion;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return DireccionesUsuarios
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function __toString()
    {
        return $this->calle.', '.$this->noExt.', '.$this->noInt.', '.$this->colonia.', '.$this->cp.', '.$this->municipio.', '.$this->estado;
    }


    public function jsonSerialize()
    {
        return array(

            'pais'              => $this->pais,
            'estado'            => $this->estado,
            'municipio'         => $this->municipio,
            'cp'                => $this->cp,
            'colonia'           => $this->colonia,
            'calle'             => $this->calle,
            'entreCalles'       => $this->entreCalles,
            'referencia'        => $this->referencia,
            'noExt'             => $this->noExt,
            'noInt'             => $this->noInt,
            'telcasa'           => $this->telcasa,
            'telOficina'        => $this->telOficina,
            'telCel'            => $this->telCel,
            'idDireccion'       => $this->idDireccion,
            //'usuario'           => $this->usuario,
            'fulldireccion'    => $this->__toString(),

        );

    }
}
