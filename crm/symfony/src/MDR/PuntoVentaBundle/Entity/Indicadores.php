<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Indicadores
 */
class Indicadores
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idIndicador;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $pedido;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $modelo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pedido = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modelo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Indicadores
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idIndicador
     *
     * @return integer 
     */
    public function getIdIndicador()
    {
        return $this->idIndicador;
    }

    /**
     * Add pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return Indicadores
     */
    public function addPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido)
    {
        $this->pedido[] = $pedido;

        return $this;
    }

    /**
     * Remove pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     */
    public function removePedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido)
    {
        $this->pedido->removeElement($pedido);
    }

    /**
     * Get pedido
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Add modelo
     *
     * @param \MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo
     * @return Indicadores
     */
    public function addModelo(\MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo)
    {
        $this->modelo[] = $modelo;

        return $this;
    }

    /**
     * Remove modelo
     *
     * @param \MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo
     */
    public function removeModelo(\MDR\PuntoVentaBundle\Entity\ModeloCalificador $modelo)
    {
        $this->modelo->removeElement($modelo);
    }

    /**
     * Get modelo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModelo()
    {
        return $this->modelo;
    }
}
