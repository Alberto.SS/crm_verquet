<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * BinTerminal
 */
class BinTerminal implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var boolean
     */
    private $esdebito;

    /**
     * @var integer
     */
    private $idBin;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoTarjeta
     */
    private $TipoTarjeta;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Terminales
     */
    private $terminal;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Bancos
     */
    private $banco;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $promociones;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->promociones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return BinTerminal
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set esdebito
     *
     * @param boolean $esdebito
     * @return BinTerminal
     */
    public function setEsdebito($esdebito)
    {
        $this->esdebito = $esdebito;

        return $this;
    }

    /**
     * Get esdebito
     *
     * @return boolean 
     */
    public function getEsdebito()
    {
        return $this->esdebito;
    }

    /**
     * Set idBin
     *
     * @param integer 
     */
    public function setIdBin($idBin)
    {
        $this->idBin = $idBin;
    }

    /**
     * Get idBin
     *
     * @return integer 
     */
    public function getIdBin()
    {
        return $this->idBin;
    }

    /**
     * Set TipoTarjeta
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta
     * @return BinTerminal
     */
    public function setTipoTarjeta(\MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta = null)
    {
        $this->TipoTarjeta = $tipoTarjeta;

        return $this;
    }

    /**
     * Get TipoTarjeta
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoTarjeta 
     */
    public function getTipoTarjeta()
    {
        return $this->TipoTarjeta;
    }

    /**
     * Set terminal
     *
     * @param \MDR\PuntoVentaBundle\Entity\Terminales $terminal
     * @return BinTerminal
     */
    public function setTerminal(\MDR\PuntoVentaBundle\Entity\Terminales $terminal = null)
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * Get terminal
     *
     * @return \MDR\PuntoVentaBundle\Entity\Terminales 
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * Set banco
     *
     * @param \MDR\PuntoVentaBundle\Entity\Bancos $banco
     * @return BinTerminal
     */
    public function setBanco(\MDR\PuntoVentaBundle\Entity\Bancos $banco = null)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \MDR\PuntoVentaBundle\Entity\Bancos 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Add promociones
     *
     * @param \MDR\PuntoVentaBundle\Entity\Promociones $promociones
     * @return BinTerminal
     */
    public function addPromocione(\MDR\PuntoVentaBundle\Entity\Promociones $promociones)
    {
        $this->promociones[] = $promociones;

        return $this;
    }

    /**
     * Remove promociones
     *
     * @param \MDR\PuntoVentaBundle\Entity\Promociones $promociones
     */
    public function removePromocione(\MDR\PuntoVentaBundle\Entity\Promociones $promociones)
    {
        $this->promociones->removeElement($promociones);
    }

    /**
     * Get promociones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPromociones()
    {
        return $this->promociones;
    }

    public function jsonSerialize()
    {
        return array(

            'descripcion'   => $this->descripcion,
            'esdebito'      => $this->esdebito,
            'idBin'         => $this->idBin,
            'TipoTarjeta'   => $this->TipoTarjeta,
            'terminal'      => $this->terminal,
            'banco'         => $this->banco,
            'promociones'   => $this->promociones->toArray(),
        );

    }


}
