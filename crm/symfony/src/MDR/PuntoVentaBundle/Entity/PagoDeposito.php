<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PagoDeposito
 */
class PagoDeposito
{
    /**
     * @var integer
     */
    private $clave;

    /**
     * @var string
     */
    private $referencia;

    /**
     * @var integer
     */
    private $idDepositoPago;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Bancos
     */
    private $banco;


    /**
     * Set clave
     *
     * @param integer $clave
     * @return PagoDeposito
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return integer 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return PagoDeposito
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Get idDepositoPago
     *
     * @return integer 
     */
    public function getIdDepositoPago()
    {
        return $this->idDepositoPago;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PagoDeposito
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set banco
     *
     * @param \MDR\PuntoVentaBundle\Entity\Bancos $banco
     * @return PagoDeposito
     */
    public function setBanco(\MDR\PuntoVentaBundle\Entity\Bancos $banco = null)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \MDR\PuntoVentaBundle\Entity\Bancos 
     */
    public function getBanco()
    {
        return $this->banco;
    }
}
