<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Bancos
 */
class Bancos implements JsonSerializable
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idBanco;



    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Bancos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idBanco
     *
     * @return integer 
     */
    public function getIdBanco()
    {
        return $this->idBanco;
    }

    public function __toString()
    {
        return $this->nombre;
    }
    
    public function jsonSerialize()
    {
        return array(
            'idBanco'   => $this->idBanco,
            'nombre'    => $this->nombre,
            'toString'  => $this->__toString(),
        );

    }
}
