<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeloCalificador
 */
class ModeloCalificador
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var integer
     */
    private $idModelo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $indicador;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->indicador = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return ModeloCalificador
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idModelo
     *
     * @return integer 
     */
    public function getIdModelo()
    {
        return $this->idModelo;
    }

    /**
     * Add indicador
     *
     * @param \MDR\PuntoVentaBundle\Entity\Indicadores $indicador
     * @return ModeloCalificador
     */
    public function addIndicador(\MDR\PuntoVentaBundle\Entity\Indicadores $indicador)
    {
        $this->indicador[] = $indicador;

        return $this;
    }

    /**
     * Remove indicador
     *
     * @param \MDR\PuntoVentaBundle\Entity\Indicadores $indicador
     */
    public function removeIndicador(\MDR\PuntoVentaBundle\Entity\Indicadores $indicador)
    {
        $this->indicador->removeElement($indicador);
    }

    /**
     * Get indicador
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndicador()
    {
        return $this->indicador;
    }
}
