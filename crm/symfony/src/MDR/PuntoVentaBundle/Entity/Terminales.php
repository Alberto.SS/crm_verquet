<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Terminales
 */
class Terminales implements JsonSerializable
{
    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $claveSAP;

    /**
     * @var integer
     */
    private $idTerminal;


    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Terminales
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get idTerminal
     *
     * @return integer 
     */
    public function getIdTerminal()
    {
        return $this->idTerminal;
    }

    public function __toString()
    {
        return $this->descripcion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Terminales
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set claveSAP
     *
     * @param string $claveSAP
     * @return Terminales
     */
    public function setClaveSAP($claveSAP)
    {
        $this->claveSAP = $claveSAP;

        return $this;
    }

    /**
     * Get claveSAP
     *
     * @return string 
     */
    public function getClaveSAP()
    {
        return $this->claveSAP;
    }



    public function jsonSerialize()
    {
        return array(
            'idTerminal'    => $this->idTerminal,
            'descripcion'   => $this->descripcion,
            'value'         => $this->idTerminal,
            'text'          => $this->descripcion,
            'codigo'        => $this->codigo,
            'claveSAP'      => $this->claveSAP,
        );

    }




}
