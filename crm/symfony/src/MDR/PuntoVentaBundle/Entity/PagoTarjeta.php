<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * PagoTarjeta
 * @Assert\Callback(methods={"estaCompleto", "esValido"})
 */
class PagoTarjeta implements JsonSerializable
{
    /**
     * @var string
     */
    private $titular;

    /**
     * @var integer
     */
    private $esTitular;

    /**
     * @var string
     *
     */
    private $numeroTarjeta;

    /**
     * @var \DateTime
     */
    private $vigencia;

    /**
     * @var integer
     */
    private $codigoSeguridad;

    /**
     * @var integer
     */
    private $posFechar;

    /**
     * @var \DateTime
     */
    private $fechaCobro;

    /**
     * @var integer
     */
    private $idPago;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\TipoTarjeta
     */
    private $tipoTarjeta;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Terminales
     */
    private $terminal;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Promociones
     */
    private $promocion;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Bancos
     */
    private $banco;


    /**
     * Set titular
     *
     * @param string $titular
     * @return PagoTarjeta
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return string 
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set esTitular
     *
     * @param integer $esTitular
     * @return PagoTarjeta
     */
    public function setEsTitular($esTitular)
    {
        $this->esTitular = $esTitular;

        return $this;
    }

    /**
     * Get esTitular
     *
     * @return integer 
     */
    public function getEsTitular()
    {
        return $this->esTitular;
    }

    /**
     * Set numeroTarjeta
     *
     * @param string $numeroTarjeta
     * @return PagoTarjeta
     */
    public function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;

        return $this;
    }

    /**
     * Get numeroTarjeta
     *
     * @return string 
     */
    public function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    /**
     * Set vigencia
     *
     * @param \DateTime $vigencia
     * @return PagoTarjeta
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;

        return $this;
    }

    /**
     * Get vigencia
     *
     * @return \DateTime 
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }

    /**
     * Set codigoSeguridad
     *
     * @param integer $codigoSeguridad
     * @return PagoTarjeta
     */
    public function setCodigoSeguridad($codigoSeguridad)
    {
        $this->codigoSeguridad = $codigoSeguridad;

        return $this;
    }

    /**
     * Get codigoSeguridad
     *
     * @return integer 
     */
    public function getCodigoSeguridad()
    {
        return $this->codigoSeguridad;
    }

    /**
     * Set posFechar
     *
     * @param integer $posFechar
     * @return PagoTarjeta
     */
    public function setPosFechar($posFechar)
    {
        $this->posFechar = $posFechar;

        return $this;
    }

    /**
     * Get posFechar
     *
     * @return integer 
     */
    public function getPosFechar()
    {
        return $this->posFechar;
    }

    /**
     * Set fechaCobro
     *
     * @param \DateTime $fechaCobro
     * @return PagoTarjeta
     */
    public function setFechaCobro($fechaCobro)
    {
        $this->fechaCobro = $fechaCobro;

        return $this;
    }

    /**
     * Get fechaCobro
     *
     * @return \DateTime 
     */
    public function getFechaCobro()
    {
        return $this->fechaCobro;
    }

    /**
     * Get idPago
     *
     * @return integer 
     */
    public function getIdPago()
    {
        return $this->idPago;
    }

    /**
     * Set tipoTarjeta
     *
     * @param \MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta
     * @return PagoTarjeta
     */
    public function setTipoTarjeta(\MDR\PuntoVentaBundle\Entity\TipoTarjeta $tipoTarjeta = null)
    {
        $this->tipoTarjeta = $tipoTarjeta;

        return $this;
    }

    /**
     * Get tipoTarjeta
     *
     * @return \MDR\PuntoVentaBundle\Entity\TipoTarjeta 
     */
    public function getTipoTarjeta()
    {
        return $this->tipoTarjeta;
    }

    /**
     * Set terminal
     *
     * @param \MDR\PuntoVentaBundle\Entity\Terminales $terminal
     * @return PagoTarjeta
     */
    public function setTerminal(\MDR\PuntoVentaBundle\Entity\Terminales $terminal = null)
    {
        $this->terminal = $terminal;

        return $this;
    }

    /**
     * Get terminal
     *
     * @return \MDR\PuntoVentaBundle\Entity\Terminales 
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * Set promocion
     *
     * @param \MDR\PuntoVentaBundle\Entity\Promociones $promocion
     * @return PagoTarjeta
     */
    public function setPromocion(\MDR\PuntoVentaBundle\Entity\Promociones $promocion = null)
    {
        $this->promocion = $promocion;

        return $this;
    }

    /**
     * Get promocion
     *
     * @return \MDR\PuntoVentaBundle\Entity\Promociones 
     */
    public function getPromocion()
    {
        return $this->promocion;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PagoTarjeta
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set banco
     *
     * @param \MDR\PuntoVentaBundle\Entity\Bancos $banco
     * @return PagoTarjeta
     */
    public function setBanco(\MDR\PuntoVentaBundle\Entity\Bancos $banco = null)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \MDR\PuntoVentaBundle\Entity\Bancos 
     */
    public function getBanco()
    {
        return $this->banco;
    }

    public function estaCompleto(ExecutionContextInterface $context)
    {
        if($this->terminal == null)
            $context->addViolationAt('terminal', 'Debe seleccionar una terminal');
        if($this->promocion == null)
            $context->addViolationAt('promocion', 'Debe seleccionar una promocion');
        if($this->banco == null)
            $context->addViolationAt('banco', 'Debe seleccionar un banco');
        if($this->tipoTarjeta == null)
            $context->addViolationAt('tipo de tarjeta', 'Debe seleccionar un tipo de tarjeta');
    }
    public function esValido(ExecutionContextInterface $context)
    {
        if($this->fechaCobro != null)
        {
            $fechaMin = new \DateTime('-2 hour');
            $fechaMin = new \DateTime($fechaMin->format('Y-m-d'));
            $fechaMax = new \DateTime('5 days + 2 hour');
            $fechaMax = new \DateTime($fechaMax->format('Y-m-d'));
            if($this->fechaCobro < $fechaMin)
            {
                $context->addViolationAt('Fecha de cobro', 'La fecha debe ser mayor o igual a la fecha actual.');
            }elseif($this->fechaCobro > $fechaMax)
            {
                $context->addViolationAt('Fecha de cobro', 'La fecha debe ser no debe ser mayor a 5 días posteriores.');
            }
        }
    }

    public function __GregorianDateTime()
    {
        if($this->fechaCobro != null)
        {
            return date_format($this->fechaCobro,'d-m-Y  H:i:s');
        }
        else
        {
            return date_format(new \DateTime(),'d-m-Y  H:i:s');
        }
    }

    public function __vigenciaJS()
    {
        if($this->vigencia != null)
        {
            return date_format($this->vigencia,'m-Y');
        }
        else
        {
            return date_format(new \DateTime(),'m-Y');
        }
    }



    public function jsonSerialize()
    {
        return array(

            'titular'                  => $this->titular,
            'esTitular'                => $this->esTitular,
            'numeroTarjeta'            => $this->numeroTarjeta,
            'vigencia'                 => $this->vigencia,
            'codigoSeguridad'          => $this->codigoSeguridad,
            'posFechar'                => $this->posFechar,
            'fechaCobro'               => $this->fechaCobro,
            'fechaCobroGregorian'      => $this->__GregorianDateTime(),
            'vigenciaJS'               => $this->__vigenciaJS(),
            'idPago'                   => $this->idPago,
            'tipoTarjeta'              => $this->tipoTarjeta,
            'terminal'                 => $this->terminal,
            'promocion'                => $this->promocion,
            'banco'                    => $this->banco,
        );

    }
}
