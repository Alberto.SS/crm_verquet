<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetMain
 */
class IntranetMain implements JsonSerializable
{
    /**
     * @var string
     */
    private $Descripcion;

    /**
     * @var string
     */
    private $Valor;

    /**
     * @var integer
     */
    private $idItem;


    /**
     * Set Descripcion
     *
     * @param string $descripcion
     * @return IntranetMain
     */
    public function setDescripcion($descripcion)
    {
        $this->Descripcion = $descripcion;

        return $this;
    }

    /**
     * Get Descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Set Valor
     *
     * @param string $valor
     * @return IntranetMain
     */
    public function setValor($valor)
    {
        $this->Valor = $valor;

        return $this;
    }

    /**
     * Get Valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->Valor;
    }

    /**
     * Get idItem
     *
     * @return integer 
     */
    public function getIdItem()
    {
        return $this->idItem;
    }

    public function jsonSerialize()
    {
        return array(

            'Descripcion'   => $this->Descripcion,
            'Valor'         => $this->Valor,
            'idItem'        => $this->idItem,
                       
        );

    }
}
