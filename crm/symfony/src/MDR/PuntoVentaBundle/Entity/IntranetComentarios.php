<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetComentarios
 */
class IntranetComentarios implements JsonSerializable
{
    /**
     * @var string
     */
    private $texto;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var integer
     */
    private $calificacion;

    /**
     * @var integer
     */
    private $idComentario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Usuario
     */
    private $usuario;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\IntranetContenidos
     */
    private $contenido;


    /**
     * Set texto
     *
     * @param string $texto
     * @return IntranetComentarios
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return IntranetComentarios
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set calificacion
     *
     * @param integer $calificacion
     * @return IntranetComentarios
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;

        return $this;
    }

    /**
     * Get calificacion
     *
     * @return integer 
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Get idComentario
     *
     * @return integer 
     */
    public function getIdComentario()
    {
        return $this->idComentario;
    }

    /**
     * Set usuario
     *
     * @param \MDR\PuntoVentaBundle\Entity\Usuario $usuario
     * @return IntranetComentarios
     */
    public function setUsuario(\MDR\PuntoVentaBundle\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \MDR\PuntoVentaBundle\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set contenido
     *
     * @param \MDR\PuntoVentaBundle\Entity\IntranetContenidos $contenido
     * @return IntranetComentarios
     */
    public function setContenido(\MDR\PuntoVentaBundle\Entity\IntranetContenidos $contenido = null)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return \MDR\PuntoVentaBundle\Entity\IntranetContenidos 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    public function jsonSerialize()
    {
        return array(
            'idComentario'      => $this->idComentario,
            'texto'             => $this->texto,
            'fecha'             => $this->fecha,
            'calificacion'      => $this->calificacion,
            'usuario'           => $this->usuario,
        );
    }
}