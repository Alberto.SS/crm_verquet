<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetEstatusInbox
 */
class IntranetEstatusInbox implements JsonSerializable
{
    /**
     * @var string
     */
    private $Descripcion;

    /**
     * @var integer
     */
    private $idEstatus;


    /**
     * Set Descripcion
     *
     * @param string $descripcion
     * @return IntranetEstatusInbox
     */
    public function setDescripcion($descripcion)
    {
        $this->Descripcion = $descripcion;

        return $this;
    }

    /**
     * Get Descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->Descripcion;
    }

    /**
     * Get idEstatus
     *
     * @return integer 
     */
    public function getIdEstatus()
    {
        return $this->idEstatus;
    }

    public function jsonSerialize()
    {
        return array(

            'Descripcion'   => $this->Descripcion,
            'idEstatus'     => $this->idEstatus,
                       
        );

    }
}
