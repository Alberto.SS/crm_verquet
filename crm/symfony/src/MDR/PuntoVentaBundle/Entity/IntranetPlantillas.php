<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IntranetPlantillas
 */
class IntranetPlantillas implements JsonSerializable
{
    /**
     * @var integer
     */
    private $tipo;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $idPlantilla;


    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return IntranetPlantillas
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return IntranetPlantillas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get idPlantilla
     *
     * @return integer 
     */
    public function getIdPlantilla()
    {
        return $this->idPlantilla;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    public function jsonSerialize()
    {
        return array(
            'tipo'          => $this->tipo,
            'nombre'        => $this->nombre,
            'idPlantilla'   => $this->idPlantilla,
        );
    }
}