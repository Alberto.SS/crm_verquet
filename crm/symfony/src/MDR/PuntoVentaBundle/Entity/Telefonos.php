<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
/**
 * Telefonos
 */
class Telefonos implements JsonSerializable
{
    /**
     * @var string
     */
    private $numero;

    /**
     * @var boolean
     */
    private $esListaNegra;

    /**
     * @var integer
     */
    private $idTelefono;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $clientes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Telefonos
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set esListaNegra
     *
     * @param boolean $esListaNegra
     * @return Telefonos
     */
    public function setEsListaNegra($esListaNegra)
    {
        $this->esListaNegra = $esListaNegra;

        return $this;
    }

    /**
     * Get esListaNegra
     *
     * @return boolean 
     */
    public function getEsListaNegra()
    {
        return $this->esListaNegra;
    }

    /**
     * Get idTelefono
     *
     * @return integer 
     */
    public function getIdTelefono()
    {
        return $this->idTelefono;
    }

    /**
     * Add clientes
     *
     * @param \MDR\PuntoVentaBundle\Entity\Clientes $cliente
     * @return Telefonos
     */
    public function addCliente(\MDR\PuntoVentaBundle\Entity\Clientes $cliente)
    {
        $this->clientes[] = $cliente;

        return $this;
    }

    /**
     * Remove cliente
     *
     * @param \MDR\PuntoVentaBundle\Entity\Clientes $cliente
     */
    public function removeCliente(\MDR\PuntoVentaBundle\Entity\Clientes $cliente)
    {
        $this->clientes->removeElement($cliente);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    public function jsonSerialize()
    {
        return array(
            
            'numero'                 => $this->numero,
            'esListaNegra'           => $this->esListaNegra,
            'idTelefono'             => $this->idTelefono,
            //'clientes'              => $this->subTotal,
                       
        );

    }
}

