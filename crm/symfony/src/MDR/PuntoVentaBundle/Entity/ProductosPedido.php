<?php

namespace MDR\PuntoVentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ProductosPedido
 */
class ProductosPedido implements JsonSerializable
{
    /**
     * @var integer
     */
    private $cantidad;

    /**
     * @var string
     */
    private $posicion;

    /**
     * @var integer
     */
    private $idDetallePedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\ProductoPrecios
     */
    private $productoPrecio;


    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return ProductosPedido
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set posicion
     *
     * @param string $posicion
     * @return ProductosPedido
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return string 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Get idDetallePedido
     *
     * @return integer 
     */
    public function getIdDetallePedido()
    {
        return $this->idDetallePedido;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return ProductosPedido
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

    /**
     * Set productoPrecio
     *
     * @param \MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecio
     * @return ProductosPedido
     */
    public function setProductoPrecio(\MDR\PuntoVentaBundle\Entity\ProductoPrecios $productoPrecio = null)
    {
        $this->productoPrecio = $productoPrecio;

        return $this;
    }

    /**
     * Get productoPrecio
     *
     * @return \MDR\PuntoVentaBundle\Entity\ProductoPrecios 
     */
    public function getProductoPrecio()
    {
        return $this->productoPrecio;
    }

	public function jsonSerialize()
    {
        return array(
            'cantidad'          => $this->cantidad,
            'idDetallePedido'   => $this->idDetallePedido,
            'productoPrecio'    => $this->productoPrecio,

        );

    }
}
