<?php

namespace MDR\PuntoVentaBundle\Entity;
use JsonSerializable;
use Doctrine\ORM\Mapping as ORM;

/**
 * PedidoDescuentos
 */
class PedidoDescuentos implements JsonSerializable
{
    /**
     * @var string
     */
    private $clave;

    /**
     * @var float
     */
    private $importe;

    /**
     * @var integer
     */
    private $idPedidoDescuento;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Descuentos
     */
    private $descuento;

    /**
     * @var \MDR\PuntoVentaBundle\Entity\Pedidos
     */
    private $pedido;


    /**
     * Set clave
     *
     * @param string $clave
     * @return PedidoDescuentos
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return PedidoDescuentos
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporteSinIva()
    {
        $importe = floatval($this->importe);
        if($importe == 0) return 0;
        return number_format(($importe/1.16), 5, '.', '');
    }

    /**
     * Get idPedidoDescuento
     *
     * @return integer 
     */
    public function getIdPedidoDescuento()
    {
        return $this->idPedidoDescuento;
    }

    /**
     * Set descuento
     *
     * @param \MDR\PuntoVentaBundle\Entity\Descuentos $descuento
     * @return PedidoDescuentos
     */
    public function setDescuento(\MDR\PuntoVentaBundle\Entity\Descuentos $descuento = null)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return \MDR\PuntoVentaBundle\Entity\Descuentos 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set pedido
     *
     * @param \MDR\PuntoVentaBundle\Entity\Pedidos $pedido
     * @return PedidoDescuentos
     */
    public function setPedido(\MDR\PuntoVentaBundle\Entity\Pedidos $pedido = null)
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * Get pedido
     *
     * @return \MDR\PuntoVentaBundle\Entity\Pedidos 
     */
    public function getPedido()
    {
        return $this->pedido;
    }

     public function jsonSerialize()
    {
        return array(
            'clave'                 => $this->clave,
            'importe'               => $this->importe,
            'idPedidoDescuento'     => $this->idPedidoDescuento,
            'descuento'             => $this->descuento,
                       
        );

    }
}
