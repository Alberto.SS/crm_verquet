<?php

namespace MDR\SepomexBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MDRSepomexBundle:Default:index.html.twig', array('name' => $name));
    }
}
