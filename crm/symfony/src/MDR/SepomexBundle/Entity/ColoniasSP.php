<?php

namespace MDR\SepomexBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ColoniasSP
 */
class ColoniasSP implements JsonSerializable
{
    /**
     * @var string
     */
    private $colonia;

    /**
     * @var string
     */
    private $ciudad;

    /**
     * @var string
     */
    private $estado;

    /**
     * @var string
     */
    private $delegacion;

    /**
     * @var string
     */
    private $codigo;

    /**
     * @var string
     */
    private $numero;


    /**
     * Set colonia
     *
     * @param string $colonia
     * @return ColoniasSP
     */
    public function setColonia($colonia)
    {
        $this->colonia = $colonia;

        return $this;
    }

    /**
     * Get colonia
     *
     * @return string 
     */
    public function getColonia()
    {
        return $this->colonia;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     * @return ColoniasSP
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string 
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return ColoniasSP
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set delegacion
     *
     * @param string $delegacion
     * @return ColoniasSP
     */
    public function setDelegacion($delegacion)
    {
        $this->delegacion = $delegacion;

        return $this;
    }

    /**
     * Get delegacion
     *
     * @return string 
     */
    public function getDelegacion()
    {
        return $this->delegacion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return ColoniasSP
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    public function jsonSerialize()
    {
        return array(
            'numero'        => $this->numero,
            'colonia'       => $this->colonia,
            'ciudad'        => $this->ciudad,
            'estado'        => $this->estado,
            'delegacion'    => $this->delegacion,
            'codigo'        => $this->codigo,
        );
    }
}
