var AEstatusTickets = new Array();
var detalles_pedidoinv = new Array();
var cancelacion_pedidoinv = new Array();
//Select para cancelacion
var ATCancelacion = new Array();



  $(document).ready(function() { 

    table  = $('#tbl_operaciones').DataTable({
                      "language":DataTables.languaje.es          
                    });


        $( "select" ).addClass( "form-control" );
        $( "input[type='text']" ).addClass( "form-control" );
        $( "input[type='search']").addClass( "form-control" );
        $( "#tbl_operaciones_length" ).addClass( "form-inline" );
        $( "#tbl_operaciones_filter" ).addClass( "form-inline" );  

    $.getJSON("../estatusTicketJSON",function(result){
        $.each(result, function(index, val) {
           AEstatusTickets.push({id:val.idEstatusTicket,text:val.Descripcion});
        });
    });

    $.getJSON( "../respuestascancelacionJSON", function(result) {
        $.each(result, function(index, val) {
             ATCancelacion.push({id:val.idCancelacion,text:val.descripcion});
          });       
    }); 

  });

//validar fechas  
  function vd_date(){
    
    var date1 = createdate($('#form_fechaInicio').val());
    var date2 = createdate($('#form_fechaFin').val());

    if (date1 <= date2) {
      $('#form_Consultar').attr("disabled", false);     
    }else if (date1 > date2){
      alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
      $('#form_Consultar').attr("disabled", true);  
    }

  }


function loadPedidos(){
  
    showLoading('Cargando Pedidos ...</h3>');
    
}

function operaciones(idPedido,mnj){

  showLoading(mnj);
  showOperaciones(idPedido,function(res){
          hideLoading();
          detalles_pedidoinv[0] = res.Pedido[0];
          detalles_pedidoinv[1] = res.Tickets;
          detalles_pedidoinv[2] = AEstatusTickets;
            var template = _.template(document.getElementById("previewBodyTemplate").textContent);
            var html_info = template(detalles_pedidoinv);
            $("#popup_inv").modal("show");
            $("#body_inv").html(html_info);
    });

}


function ShowCancel(idPedido){
  cancelacion_pedidoinv[0] = idPedido;
  cancelacion_pedidoinv[1] = ATCancelacion;
            var template = _.template(document.getElementById("CancelacionPedido").textContent);
            var html_info = template(cancelacion_pedidoinv);
            $("#popup_cancel").modal("show");
            $("#body_cancel").html(html_info);
}


function showOperaciones(idPedido,callback){

      $.post("../DtlPedidoInvestigacion",{
        idPedido:idPedido
      },function(respuesta){
        var res = respuesta;
        callback(res);
      });

}

function saveComentarios(idPedido){

  var Testatus = $('#testatus').val();
  var Comentarios = $('#comentarios').val();
  showLoading("Generando Ticket");
  if(Testatus != "0" && Comentarios){
      setComentarios(idPedido,Testatus,Comentarios,function(res){

        console.log(res);
        hideLoading();
        if(res.estatus == 1){
            alert(res.mensaje);            
            operaciones(res.Pedido,'Actualizando Pedido...');
        }else{
            alert(res.mensaje);
        }

      });
  }else{
    hideLoading();
    alert("Debe indicar</br>-> Estatus del Ticket</br>-> Comentarios");
  }

}


function setComentarios(idPedido,ticket,comentarios,callback){

  $.post("../CrearTickets",{
        idPedido:idPedido,
        ticket:ticket,
        comentarios:comentarios
      },function(respuesta){
        var res = respuesta;
        callback(res);
      });

}


function cierrePedido(){
  var Testatus = $('#testatus').val();
  if(Testatus == 4){
      $("#btn_liberar").css("display", "block");
      $("#btn_cancelar").css("display", "block");
      $("#savecoments").css("display", "none");
  }else{
      $("#btn_liberar").css("display", "none");
      $("#btn_cancelar").css("display", "none");
      $("#savecoments").css("display", "block");

  }

}


function CierreInv(idPedido,Operacion){
  var Testatus = $('#testatus').val();
  var Comentarios = $('#comentarios').val();
  var Motivo = null;

  if(Operacion == 'LP'){
      showLoading("Liberando Pedido");
      if(Testatus && Comentarios){
          setCierre(idPedido,Testatus,Comentarios,Operacion,Motivo,function(res){

            console.log(res);
            hideLoading();
            if(res.estatus == 1){
                alert(res.mensaje);
                showLoading("Actualizando Pedidos");
                $('#form_Consultar').click();
            }else{
                alert(res.mensaje);
            }

          });
      }else{
        hideLoading();
        alert("Debe indicar</br>-> Estatus del Ticket</br>-> Comentarios");
      }
  }else if(Operacion == 'DL'){

    Motivo = $('#sls_cancelacion').val();
    showLoading("Cancelando Pedido");
      if(Testatus && Comentarios && Motivo){
          setCierre(idPedido,Testatus,Comentarios,Operacion,Motivo,function(res){

            console.log(res);
            hideLoading();
            if(res.estatus == 1){
                alert(res.mensaje);
                showLoading("Actualizando Pedidos");
                $('#form_Consultar').click();
            }else{
                alert(res.mensaje);
            }

          });
      }else{
        hideLoading();
        alert("Debe indicar</br>-> Estatus del Ticket</br>-> Comentarios</br>-> Motivo de cancelación");
      }

  }

}


function setCierre(idPedido,Testatus,Comentarios,Operacion,Motivo,callback){

  $.post("../CierreInvestigacion",{
        idPedido:idPedido,
        Testatus:Testatus,
        Comentarios:Comentarios,
        Operacion:Operacion,
        Motivo:Motivo
      },function(respuesta){
        var res = respuesta;
        callback(res);
      });


}