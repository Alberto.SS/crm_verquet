var nCliente = document.getElementById("mdr_puntoventabundle_pedidos_cliente");
var nDireccion = document.getElementById("mdr_puntoventabundle_pedidos_direccion");
var nFactura = document.getElementById("mdr_puntoventabundle_pedidos_factura");
var nRfc = document.getElementById("mdr_puntoventabundle_pedidos_rfc");

var idCliente = null;
var direcciones = [];
var clientes = [];
var cliente = null;
var direccionCliente = null;

window.onbeforeunload = validarSalida;

document.addEventListener('DOMContentLoaded', function () {
	nCliente.addEventListener("change", changeCliente);
	nDireccion.addEventListener("change", changeDireccion);
	nFactura.addEventListener("change", changeFactura);
	
	nRfc.parentNode.hidden = true;
});

function changeCliente()
{
	idCliente = nCliente.value;
	getDirecciones(idCliente);
	cliente = null;
	carrito.showCliente(cliente);
	for (var i = 0; i < clientes.length; i++) {
		if(clientes[i].idCliente == idCliente)
		{
			cliente = clientes[i];
			carrito.showCliente(cliente);
		}
	}
	direccionCliente = null;
	carrito.showDireccion(direccionCliente);
}
function changeDireccion()
{
	direccionCliente = null;
	carrito.showDireccion(direccionCliente);
	for (var i = 0; i < direcciones.length; i++) {
		if(direcciones[i].idDireccion == nDireccion.value)
		{
			direccionCliente = direcciones[i];
			carrito.showDireccion(direccionCliente);
			i = direcciones.length;
			setTiposPago(direccionCliente);
		}
	};
}
function changeFactura()
{
	nRfc.parentNode.hidden = (this.value == 0);
}

function getDirecciones(idCliente)
{
	showLoading("Obteniendo direcciones", "#tabDireccion");
	$.post(urls.cliente.direcciones, {idCliente: idCliente}, function(data, textStatus, xhr) {
		if(data.code == 1)
		{
			clearSelect(nDireccion);
			direcciones = data.value;
			addOption("", "", nDireccion);
			for (var i = 0; i < direcciones.length; i++) {
				addOption(direcciones[i].toString, direcciones[i].idDireccion, nDireccion);
			};
			if(direcciones.length == 1)
			{
				nDireccion.value = direcciones[0].idDireccion;
				fireEvent(nDireccion, "change");
			}
		}
	}).always(function(){
    	hideLoading("#tabDireccion");
    });
}