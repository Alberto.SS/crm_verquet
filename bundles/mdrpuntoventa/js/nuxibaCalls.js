var EstatusNuxibaCall = {atendida:1, posponer: 2};

var NuxibaCall = Backbone.Model.extend({
	initialize: function(){
	},
	defaults: {
		DNIS: '',
		phoneNumber: '',
		esEntrante: 0,
		callId: 0
	}
});

var NuxibaCallCollection = Backbone.Collection.extend({
	model: NuxibaCall,
	localStorage: new Backbone.LocalStorage("NuxibaCallsBackbone"),
	byEstatus: function(estatus) {
		filtered = this.filter(function(nuxibaCall) {
			return nuxibaCall.get('estatus') == estatus;
		});
		return new NuxibaCallCollection(filtered);
	},
	byCallId: function(callId)
	{
		filtered = this.filter(function(nuxibaCall) {
			return nuxibaCall.get('callId') == callId;
		});
		return new NuxibaCallCollection(filtered);
	},
	byPhoneNumberType: function(number, esEntrante)
	{
		number = quitarLada(number);
		filtered = this.filter(function(nuxibaCall) {
			return quitarLada(nuxibaCall.get('phoneNumber')) == number && nuxibaCall.get('esEntrante') == esEntrante;
		});
		return new NuxibaCallCollection(filtered);
	}
});

var nuxibaCalls = new NuxibaCallCollection();