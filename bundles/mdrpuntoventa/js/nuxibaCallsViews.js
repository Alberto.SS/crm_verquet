var CallView = Backbone.View.extend({
	tagName: "tr",
	template: _.template($('#nuxibaCallTemplate').html()),
	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
		this.render();
	},
	render: function(){
		this.$el.html(this.template(this.model.toJSON()));

		return this;
	},
	events: {
		'click button.tomar'	: 'tomar'
	},
	tomar: function(){
		localStorage.setItem("nuxibaRetCall", "");
		localStorage.setItem("nuxibaRetCall", JSON.stringify(this.model.toJSON()));
	}
})

var NuxibaView = Backbone.View.extend({
	el: $("#mainContainer"),
	initialize: function() {			
		this.nuxibaCalls = this.$(".nuxibaCalls");

		this.listenTo(nuxibaCalls, 'add', this.addCall);
		this.listenTo(nuxibaCalls, 'remove', this.render);
		nuxibaCalls.fetch();
	},

	render: function() {
		var childs = this.nuxibaCalls.children();
		for (var i = childs.length - 1; i >= 0; i--) {
			childs[i].remove();
		};

		this.addAllCalls(nuxibaCalls);
	},
	addCall: function(item)
	{
		var view = new CallView({model: item});
		this.nuxibaCalls.prepend(view.render().el);
	},
	addAllCalls: function(items)
	{
		items.each(this.addCall, this);
	},
});