//Variables a utilizar
var ApedidosLC = new Array(); //----> variable que contiene los pedidos seleccionados
//Select para cancelacion
var atcancelacion = '<select id="sls_type_cancel" class="form-control"><option>----------------------------------</option>';

//ESTILO DE LA TABLA Y FUNCION DROPDOWN MENU RESPONSIVE
$(document).ready(function() {
						var table  = $('#tbl_operaciones').DataTable({
        					"language":DataTables.languaje.es
										 					
        				});	
						


						$( "select" ).addClass( "form-control" );
			        	$( "input[type='text']" ).addClass( "form-control" );
			        	$( "input[type='search']").addClass( "form-control" );			        	
			        	$( "#tbl_operaciones_length" ).addClass( "form-inline" );
			        	$( "#tbl_operaciones_filter" ).addClass( "form-inline" );

			

			var tiposrespuesta = $.getJSON( "../respuestascancelacionJSON", function() {
  				for(var x=0;x<=tiposrespuesta.responseJSON.length-1;x++){
  					atcancelacion = atcancelacion +'<option value="'+tiposrespuesta.responseJSON[x].idCancelacion+'">'+tiposrespuesta.responseJSON[x].descripcion+'</option>';	  			
  				}        	
  			});	


    	});


function loadPedidos(){
		showLoading('Cargando Pedidos ...</h3>');		
}

//validar fechas	
	function vd_date(){
		
		var date1 = createdate($('#form_fechaInicio').val());
		var date2 = createdate($('#form_fechaFin').val());

		if (date1 <= date2) {
			$('#form_Consultar').attr("disabled", false);			
		}else if (date1 > date2){
			alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
			$('#form_Consultar').attr("disabled", true);	
		}

	}


//Botones de seleccion todos o deselecionar todos

		$(function(){
				marcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", true);
				ApedidosLC.length = 0;
				for(x=0;x<=pautaSecaJSON.length-1;x++){
						ApedidosLC.push(pautaSecaJSON[x].idPedido);	
				}
			}
		

			desmarcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", false);
				ApedidosLC.length=0;
			}
		});



//Captura de checks de pedidos
		
	function check_pf(idP){
		idpedidos = parseInt(idP);
		var posicion = ApedidosLC.indexOf(idpedidos);
		if (posicion == -1) {
			ApedidosLC.push(idpedidos);
		}else{
			ApedidosLC.splice(posicion,1);
		}
	}			


//Ver detalle del pedido

var detalles_pedidocrm;
	function showDtl(pedido){
	showLoading("Cargando Datos");
	var idPedido = pedido;
		loaddetalles(idPedido,function(res){
				if(res.estatus == 1){
					hideLoading();
					detalles_pedidocrm = res.detalle[0];
					var template = _.template(document.getElementById("previewBodyTemplate").textContent);
				    var html_info = template(res.detalle[0]);
				    $("#popup_up_detalle").modal("show");
					$("#body_detalle").html(html_info);
				}else{
					hideLoading();
					alert(res.detalle);
				}
		});	


		
	}

	function loaddetalles(idPedido,callback){

		$.post("../detallepedidoJSON",{
				idPedido:idPedido
			},function(respuesta){
			var res = respuesta;
				callback(res);
			});

	}


function liberarPedidos(){

	

	if(ApedidosLC.length>0){
		okLiberate(ApedidosLC,function(respuesta){
			hideLoading();
			showlog(respuesta);			
		});
	}else{
		alert("Aún no selecciona ningún pedido.");
	}
}

function okLiberate(ApedidosLC,callback){
	showLoading("Liberando Pedido(s)...");
		$.post("../LiberarPS",{
				ApedidosLC,ApedidosLC
			},function(respuesta){
				callback(respuesta);
			});
}


function opencancel(){

	if(ApedidosLC.length>0){

				var cuerpo ='<div class="row">'+
							'<div class="col-xs-12">'+
								'<label>Motivo de cancelacion</label>'+atcancelacion+'</select>'+
							'</div>'+
							'<div class="col-xs-12">'+
								'<label>Comentarios</label></br>'+
								'<textarea maxlength="100" rows="5" class="form-control" style="width:100%; resize: none;" id="comment_log" placeholder="Describa el motivo de la cancelacion del pedido(s)"></textarea>'+
							'</div>'+
						'</div>';
				$('#body_cancel').html(cuerpo);				
				$('#popup_cancel').modal('show');

	}else{
		alert("Aún no selecciona ningún pedido.");
	}

}




function cancelarPedidos(){
	var tipocancel = $('#sls_type_cancel').val();
	var comentarios =  $('#comment_log').val();

	if(tipocancel && comentarios){
		okCancel(tipocancel,comentarios,ApedidosLC,function(respuesta){
			$('#popup_cancel').modal('hide');
			hideLoading();
			showlog(respuesta);			
		});
	}else{
		alert("Debe indicar: </br>->Motivo de Cancelacion </br>->Comentarios");
	}

}

function okCancel(tipocancel,comentarios,ApedidosLC,callback){
		
		showLoading("Cancelando Pedidos...");
		$.post("../CancelacionPS",{
				tipocancel:tipocancel,
				comentarios:comentarios,
				ApedidosLC,ApedidosLC
			},function(respuesta){
				callback(respuesta);
			});
}


function showlog(respuesta){

	showLoading('Cargando resultados');

	var logbody = '<div class="row">';

	for(var z=0; z<= respuesta.length-1;z++){
		logbody = logbody+'<div class="col-md-12"><div class="col-md-4"><label>Pedido:</label>'+respuesta[z].id+'</div><div class="col-md-8">'+respuesta[z].mensaje+'</div></div>';
		if(z == respuesta.length){
			logbody = '</div>';
		}
	}

		hideLoading();
		$('#body_log').html(logbody);
		$('#popup_log').modal({
      			backdrop: 'static',
      			keyboard: false
    	});


}

function reload(){
	$('#form_Consultar').click();
}