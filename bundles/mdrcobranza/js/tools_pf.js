//variables para la seleccion general o indivudual de pedidos para cobro
	var rows = "";
	var ApedidosCobro = new Array();

//variables para habilitar deshabilitar la edicion de terminal o precio producto
	var clave = "";
	var idedit = "";
	var typeedit = "";
	var detallePP = "";
  
//Arreglo de precios
	var Aprices = new Array();
//Arreglos de costos de envio
  	var AcostosEnvi = new Array();  
//Arreglo para las promociones
	var Apromos = new Array();  	 	

  
//Formato Gregoriano de fecha
function gregoriandate(date){
	var annio = date.substring(0,4);
	date = date.substr(5);
	var mes = date.substr(2,4);
	mes = mes.substr(1);
	var dia = date.substr(0,2);
	date = mes+"-"+dia+"-"+annio;
	return date;
}

var ASubTipoLllamada = new Array();

//ESTILO DE LA TABLA Y FUNCION DROPDOWN MENU RESPONSIVE
$(document).ready(function() {
						var table  = $('#tbl_pf').DataTable({
        					"language":DataTables.languaje.es
										 					
        				});	
						


						$( "select" ).addClass( "form-control" );
			        	$( "input[type='text']" ).addClass( "form-control" );
			        	$( "input[type='search']").addClass( "form-control" );			        	
			        	$( "#tbl_pf_length" ).addClass( "form-inline" );
			        	$( "#tbl_pf_filter" ).addClass( "form-inline" );
			        	$( "#tbl_pf_filter>label" ).html('Buscar: <input type="text" id="search_pedido" placeholder="Numero de pedido" class="form-control">');				



						$('#search_pedido').on( 'keyup', function () {
						    table
						        .columns( 1 )
						        .search( this.value )
						        .draw();
						} );



			    $('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                    	$target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });	       

 $.getJSON("../GastosEnvioJSON",function(result){
        $.each(result, function(index, val) {
           AcostosEnvi.push({precio:val.precio,idGastosEnvio:val.idGastosEnvio,tipoPago:val.tipoPago});
        });
    });

    	});



function loadPedidos(){
	
		showLoading('Cargando Pedidos ...</h3>');
		
}


//validar fechas	
	function vd_date(){
		
		var date1 = createdate($('#form_fechaInicio').val());
		var date2 = createdate($('#form_fechaFin').val());

		if (date1 <= date2) {
			$('#form_Consultar').attr("disabled", false);			
		}else if (date1 > date2){
			alert("La fecha de fin no puede ser una fecha anterior a la fecha de Inicio");
			$('#form_Consultar').attr("disabled", true);	
		}

	}

//Botones de seleccion todos o deselecionar todos

		$(function(){
				marcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", true);
				ApedidosCobro.length = 0;
				for(x=0;x<=Apedidos1.length-1;x++){
						ApedidosCobro.push(Apedidos1[x].id);	
				}
			}
		

			desmarcar = function(elemento){
				elemento = $(elemento);
				elemento.prop("checked", false);
				ApedidosCobro.length=0;
			}
		});

//Captura de checks de pedidos
		
	function check_pf(idP){
		idpedidos = parseInt(idP);
		var posicion = ApedidosCobro.indexOf(idpedidos);
		if (posicion == -1) {
			ApedidosCobro.push(idpedidos);
		}else{
			ApedidosCobro.splice(posicion,1);
		}
	}			
	
//Envio de pedidos que seran cobrados
var ApedidosCobrados = new Array();
var contador = 0;
		function sendPf(){
			if (ApedidosCobro.length != 0) {
				showLoading('Realizando cobro de Pedido(s) ...');
				Cobrarpedidos(ApedidosCobro,function(respuesta,x){
					contador++;
					if(contador == x){
						contador = 0;
						$('#text_load').html("Actualizando pedidos...");
						$('#form_Consultar').click();
					}
				});	
			}else{
				alert("No se ha seleccionado ningun pedido");
			}
		}


//Cobrar pedido
function Cobrarpedidos(detalle,callback){
	for(var x=0;x<=detalle.length-1;x++){
		idPedido = detalle[x];
	$.post("../cobro",{
			idPedido:idPedido
	},function(respuesta){
			callback(respuesta,x);
		});

	}
}

//REINTENTO DEL COBRO
function Recobro(numpedido){
	idPedido = numpedido;

	$('#popup_operacion').modal('hide'); 
	showLoading('Realizando reintento de cobro del Pedido '+idPedido);
	$.post("../cobro",{
			idPedido:idPedido
	},function(respuesta){
			ApedidosCobrados.push(respuesta);
			if(ApedidosCobrados[0].code == 1){
				alert(ApedidosCobrados[0].message);
				$('#txt_recobro').html("Actualizando Pedidos");
				$('#form_Consultar').click();
			}else{
				alert("ERROR:\n"+ApedidosCobrados[0].message);
				$('#txt_recobro').html("Actualizando Pedidos");
				$('#form_Consultar').click();
			}
		});
}

//Editar terminal y precio

function edit(ident,id){

	if (ident == "T") {
		$("#"+id).editable({
	        type: 'select',
	    	title: 'Terminal',
	    	mode: 'inline',   
	        source:Abancos.concat(),
	        validate: function(value){
	        	if(value == null){
	        		return "Este campo no puede estar vacio";
	    	    }else{
	    	    	validatechange(ident,id,value,detallePP,function(res){
	    	    		if(res == 1){
	    	    			alert("Actualización exitosa");	 
	    	    			$('#text_load').text("Actualizando Pedidos");   	    			
							$('#form_Consultar').click();
							$("#"+id).css({ color: "#428bca"});
	    	    			cleanedit();
	    	    		}else{
	    	    			alert("Error Actualizacion TERMINAL:\n"+res);
	    	    			$("#"+id).css({ color: "#FF0000"});
	    	    		}
	    	    	});
	    	    }
	    	},
	    });		    
	}else if (ident == "P") {
				$("#"+id).editable({
		    	type: 'select',
		    	title: 'Nuevo Precio',
		    	mode: 'inline', 
		        source: Aprices.concat(),
		       	validate: function(value){
			        if(value == null || value == 0){
			        	return "No puede dejar vacio este campo"
			        }else{
			        	validatechange(ident,id,value,detallePP,function(res){
	    	    		if(res == 1){
	    	    			$("#"+id).css({ color: "#428bca"});
	    	    			cleanedit();
							precios_cagados=0;
	    	    			alert("Actualización exitosa");
	    	    			$('#text_load').text("Actualizando Pedidos");
	    	    			$('#form_Consultar').click();	    	    			
	    	    		}else{
	    	    			for(var x=0;x<=Apedidosproductos.length-1;x++){
	    	    					if(res[0].id == Apedidosproductos[x].idPedido){
	    	    						$("#"+id).html("$"+Apedidosproductos[x].precio);
	    	    						$("#"+id).css({ color: "#FF0000"});
	    	    					}
	    	    				}
	    	    			alert("Error Actualizacion PRECIO:\n"+res[0].mensaje);	    	    				
	    	    			hideLoading();
	    	    			
	    	    		}
	    	    	});
	    	    }		
	        }, 
		});
	}else if(ident == "G"){
		$("#"+id).editable({
		    	type: 'select',
		    	title: 'Nuevo costo de envio',
		    	mode: 'inline', 
		        source: function() {
		        			
		        			var AGastosEnvio = new Array();
					        
					        for(var y=0;y<=AcostosEnvi.length-1;y++){
					        	if(AcostosEnvi[y].tipoPago.idTipoPago == 1){
					        		AGastosEnvio.push({value:AcostosEnvi[y].idGastosEnvio,text:"$"+parseFloat(AcostosEnvi[y].precio)});
					        	}
					        }

					        return AGastosEnvio;
					    },
		       	validate: function(value){
			        if(value == null){
			        	return "No puede dejar vacio este campo"
			        }else{
			        	validatechange(ident,id,value,detallePP,function(res){
	    	    		if(res == 1){
	    	    			$("#"+id).css({ color: "#428bca"});
	    	    			cleanedit();
	    	    			alert("Actualización exitosa");
	    	    			$('#text_load').text("Actualizando Pedidos");
	    	    			$('#form_Consultar').click();	    	    			
	    	    		}else{
	    	    			$("#"+id).css({ color: "#FF0000"});
	    	    			alert("Error Actualizacion GASTOS DE ENVIO:\n"+res);
	    	    			hideLoading();
	    	    			
	    	    		}
	    	    	});
	    	    }		
	        }, 
		});

	}else if(ident == "C"){
		$("#"+id).editable({
	        type: 'number',
	        min:1,
	        max:999,
	    	title: 'Cantidad',
	    	mode: 'inline', 
	        validate: function(value){
	        	if(value == 0 || value == "Empty"){
	        		return "Este campo no puede estar vacio";
	    	    }else{
	    	    	validatechange(ident,id,value,detallePP,function(res){
	    	    			if(res == 1){
		    	    			$("#"+id).css({ color: "#428bca"});
		    	    			cleanedit();
		    	    			alert("Actualización exitosa");
		    	    			$('#text_load').text("Actualizando Pedidos");
		    	    			$('#form_Consultar').click();
	    	    			}else{
	    	    				$("#"+id).css({ color: "#FF0000"});
		    	    			alert("Error Actualizacion CANTIDAD DE PRODUCTO(s):\n"+res);
		    	    			hideLoading();
	    	    			}
	    	    	});	
	    	    }
	    	},
	    });		    
	}else if(ident == "M"){
		$("#"+id).editable({
			type: 'select',
		    	title: 'Nueva promoción',
		    	mode: 'inline', 
		        source: Apromos.concat(),
		       	validate: function(value){
			        if(value == 0 || value == "Empty" || value == null){
			        	return "No puede dejar vacio este campo"
			        }else{
			        	validatechange(ident,id,value,detallePP,function(res){
	    	    		if(res == 1){
	    	    			$("#"+id).css({ color: "#428bca"});
	    	    			cleanedit();
	    	    			alert("Actualización exitosa");
	    	    			$('#text_load').text("Actualizando Pedidos");
	    	    			$('#form_Consultar').click();	    	    			
	    	    		}else{
	    	    			$("#"+id).css({ color: "#FF0000"});
	    	    			alert("Error Actualización PROMOCIONES:\n"+res);
	    	    			hideLoading();
	    	    			
	    	    		}
	    	    	});
	    	    }	
	        }, 
		});	
	}
}
 

// Aprobar cambios clave del supervisor
$(document).ready(function(){
	$("#btn_aceptar_sup").click(function(){
		valclave(typeedit,idedit);
	});
});

//el parametro detalle es para el cambio de precio es el detalle de la relacion pedido/producto
function aprovechanges(ident,id,detalle){
	
  if(clave == 1 && idedit == id){
  		  	edit(ident,id); 
//24 es perfil cobranza y 3 es supervisor
  }else if(usuario == 24 || usuario == 3){

  			idedit = id;
			typeedit = ident;
			detallePP = detalle;
  		  	edit(ident,id); 

  }else{

  		idedit = id;
		typeedit = ident;
		detallePP = detalle;
		$('#pf_supervisor').modal('show');
		
 }
}

function valclave(ident,id){
	usuario = $('#user_sup').val();
	contrasena = $('#pass_sup').val();
	bioSupervisor = new Array();
	

	if (contrasena != "" && usuario != "") {
		$("#super_loading").html("<img src="+img_loding.src+">&nbsp;&nbsp;&nbsp;<label>Validando...</label>");
		$('#btn_cancel_sup').attr("disabled", true);
		$('#btn_aceptar_sup').attr("disabled", true);

		bioSupervisor.push({user:usuario, pass:contrasena});
		$.post("../Supervisor",{
			bioSupervisor:bioSupervisor
		},function(respuesta){
		 	if(respuesta[0].estatus == 1){
		 		clave = 1;
				$('#user_sup').val("");
				$('#pass_sup').val("");
				$("#super_loading").html("");
				alert("Usuario correcto");
				$('#btn_cancel_sup').attr("disabled", false);	
				$('#btn_aceptar_sup').attr("disabled", false);				
				$('#pf_supervisor').modal('hide');
				edit(ident,id);
		 	}else{
		 		$('#user_sup').val("");
				$('#pass_sup').val("");
				$("#super_loading").html("");				
		 		alert(respuesta[0].mensaje);
		 		$('#btn_cancel_sup').attr("disabled", false);	
		 		$('#btn_aceptar_sup').attr("disabled", false);	
		 	}
			}
		);
		
	}else{
		alert("Es necesario indicar Usuario y contraseña de Supervisor de area");
	}
}

function cleanedit(){
	clave = "";
	idedit = "";
	detallePP = "";
	precios_cagados=0;
	promociones_cargadas = 0;
}
// Guardamos y validamos cambios

function validatechange(idents,ids,values,detalle,callback){

	if(idents == "T"){ 
		var ApedidosUpdate = new Array();
		ApedidosUpdate.length = 0;
		var id1 = ids.replace("terminal","");
		for(var x=0;x<=pedidosJSON.length-1;x++){
			if(pedidosJSON[x].idPedido == id1){
				if (pedidosJSON[x].pagosTarjeta[0].terminal.idTerminal != values) {
					showLoading('Actualizando Pedido '+id1.replace("terminal",""));
					ApedidosUpdate.push({id:id1,terminal:values});
					var idArrPedidos = x;
					$.post("../UpdateTerminal",{
		                ApedidosUpdate:ApedidosUpdate
		            },function(respuesta){
		                	var res = respuesta[0].estatus;
		                	if(res == 1){
		                		callback(res);
		                		pedidosJSON[idArrPedidos].pagosTarjeta[0].terminal.idTerminal;
		                		ApedidosUpdate.length = 0;
		                	}else{
		                		res = respuesta[0].mensaje;
		                		callback(res);
		                	}
		             }
		            );
				}else{
					callback("La terminal seleccionada es la misma");
				}
			}
		}

	}else if(idents == "P"){
		
		var ApedidosUpdate = new Array();		
		ApedidosUpdate.length = 0;

		for(var x=0;x<=Apedidosproductos.length-1;x++){
			if(Apedidosproductos[x].idDetallePedido == detalle){
				showLoading('Actualizando Pedido '+Apedidosproductos[x].idPedido);
				

						ApedidosUpdate.push({Pedido:Apedidosproductos[x].idPedido,
											idDetallePedido:detalle,
											Aproducto:Apedidosproductos[x].ProdPrecio,
											Nproducto:values,
											cantidad:Apedidosproductos[x].cantidad,
											descripcion:Apedidosproductos[x].descripcion,area:"Cobranza"});
							$.post("../UpdatePrecioPP",{
							                ApedidosUpdate:ApedidosUpdate
							            },function(respuesta){
							                	var res = respuesta[0].estatus;
							                	if(res == 1){
							                		callback(res);
							                	}else{
							                		res = respuesta;
							                		callback(res);
							                	}
							             }
							            );				
			}
		}

	}else if(idents == "G"){

		var ApedidosUpdate = new Array();
		ApedidosUpdate.length = 0;
		var idPedido = detalle;
		var AntPrice = 0;
		var newPrice = 0;
		var totalPago = 0;
		var idGastoEnvio = values;


		showLoading('Actualizando Pedido '+idPedido);

		for(var z=0;z<=AcostosEnvi.length-1;z++){
			if(AcostosEnvi[z].idGastosEnvio == idGastoEnvio){
				newPrice = parseFloat(AcostosEnvi[z].precio);
			}
		}

		for(var x=0;x<=pedidosJSON.length-1;x++){
			if(pedidosJSON[x].idPedido == idPedido){
				AntPrice = parseFloat(pedidosJSON[x].gastosEnvio.precio);
				totalPago = parseFloat(pedidosJSON[x].totalPago);
			}
		}

		
			ApedidosUpdate.push({idpedido:idPedido,nGastosEnvio:newPrice,OGastosEnvio:AntPrice,Totalpago:totalPago,idGN:idGastoEnvio});
			
			$.post("../UpdateGastosEnvioPP",{
					    ApedidosUpdate:ApedidosUpdate
					},function(respuesta){
					    	var res = respuesta[0].estatus;
					    	if(res == 1){
					    		callback(res);
					    	}else{
					    		res = respuesta[0].mensaje;
					    		callback(res);
					    	}
					 }
					);
			
		
	}else if(idents == "C"){
		

		var ApedidosUpdate = new Array();
		ApedidosUpdate.length = 0;
		var idDetallePedido = detalle;
		var Subtotal = 0;
		var gastosEnvio = 0;
		var Total = 0;
		var AntCant = 0;
		var NewCant = values;
		var PrecioU = 0;
		var idPedido = 0;

		for(var x=0;x<=Apedidosproductos.length-1;x++){
			if(Apedidosproductos[x].idDetallePedido == idDetallePedido){
				Subtotal = Apedidosproductos[x].subTotal;
				gastosEnvio = Apedidosproductos[x].gastosEnvio;
				AntCant = Apedidosproductos[x].cantidad;
				PrecioU = Apedidosproductos[x].precio;
				idPedido = Apedidosproductos[x].idPedido;

			}
		}
		showLoading('Actualizando Pedido '+idPedido);

		if(NewCant == AntCant){
			callback("La cantidad es la misma: "+AntCant);
		}else{


			Precioproducto = AntCant*PrecioU;
			Subtotal = Subtotal-Precioproducto;
			Subtotal = Subtotal + (NewCant*PrecioU);
			Total = Subtotal + gastosEnvio;

				ApedidosUpdate.push({idPedido:idPedido,Detalle:idDetallePedido,Sub:Subtotal,total:Total,Cant:NewCant,ACant:AntCant,area:"Cobranza"});

				$.post("../UpdateCantProductosPedido",{
					    ApedidosUpdate:ApedidosUpdate
					},function(respuesta){
					    	var res = respuesta[0].estatus;
					    	if(res == 1){
					    		callback(res);
					    	}else{
					    		res = respuesta[0].mensaje;
					    		callback(res);
					    	}
					 }
					);

			


		}

	}else if(idents == "M"){
		var ApedidosUpdate = new Array();
		ApedidosUpdate.length = 0;
		var idPedido = parseInt(ids.substr(5));		
		var pagoTarjeta;
		var promocion = values;


		showLoading('Actualizando Pedido '+idPedido);
		for(var x=0;x<=pedidosJSON.length-1;x++){
			if(pedidosJSON[x].idPedido == idPedido){
				pagoTarjeta = pedidosJSON[x].pagosTarjeta[0].idPago;
			}
		}
						
		ApedidosUpdate.push({idpedido:idPedido,PT:pagoTarjeta,Promo:promocion});
			
			$.post("../UpdateMesualidadesPP",{
					    ApedidosUpdate:ApedidosUpdate
					},function(respuesta){
					    	var res = respuesta[0].estatus;
					    	if(res == 1){
					    		callback(res);
					    	}else{
					    		res = respuesta[0].mensaje;
					    		callback(res);
					    	}
					 }
					);
	}

}

//Carga de popup de operaciones

function open_popupoperaciones(ident,pedido){

showLoading('Cargando operaciones del Pedido '+pedido);

var AdatosTarjeta = new Array();
var txt_error = "";
var count = 1;
var selects_tipollamada = "<option value=''>-------------------------</option>";
var selects_tipocancelacion = "<option value=''>-------------------------</option>";
var selects_terminales = "<option value=''>-------------------------</option>";


    $.getJSON("../subtipollamadasJSON",function(result){
           ASubTipoLllamada = result;
    });

preload_cancelaciones(selects_tipocancelacion,selects_terminales,selects_tipollamada);



//tipos de cancelacion
	function preload_cancelaciones(atcancelacion,terminales,llamadas){			
		var tiposrespuesta = $.getJSON( "../respuestascancelacionJSON", function() {
  			for(x=0;x<=tiposrespuesta.responseJSON.length-1;x++){
  				atcancelacion = atcancelacion +"<option value='"+tiposrespuesta.responseJSON[x].idCancelacion+"'>"+tiposrespuesta.responseJSON[x].descripcion+"</option>" ;  				
	  				if(x == tiposrespuesta.responseJSON.length-1){
	  					preload_llamadas(atcancelacion,terminales,llamadas);
	  				}
  			}   								
		});
	}
//tipos de llamadas
	function preload_llamadas(atcancelacion,terminales,llamadas){			
		var tiposrespuesta = $.getJSON( "../tipollamadasJSON", function() {
  			for(x=0;x<=tiposrespuesta.responseJSON.length-1;x++){
  				llamadas = llamadas +"<option value='"+tiposrespuesta.responseJSON[x].idTipoLlamada+"'>"+tiposrespuesta.responseJSON[x].descripcion+"</option>" ;  				
	  				if(x == tiposrespuesta.responseJSON.length-1){
	  					preload_terminales(atcancelacion,terminales,llamadas);
	  				}
  			}   								
		});
	}	
	
	function preload_terminales(atcancelacion,terminales,llamadas){	
		var terminales_fisicas = $.getJSON( "../terminalesFisicasJSON", function() {
  			for(x=0;x<=terminales_fisicas.responseJSON.length-1;x++){
  				terminales = terminales +"<option value='"+terminales_fisicas.responseJSON[x].idTerminal+"'>"+terminales_fisicas.responseJSON[x].descripcion+"</option>" ;  				
	  				if(x == terminales_fisicas.responseJSON.length-1){
	  					load_info(atcancelacion,terminales,llamadas);
	  				}
	  		}		
  		});	
	}

	function load_info(atcancelacion,terminales,llamadas){
	//Fecha de hoy
	var today = new Date(); 
	var dd = today.getDate(); 
	var mm = today.getMonth()+1;//January is 0! 
	var yyyy = today.getFullYear(); 
	if(dd<10){dd='0'+dd} 
	if(mm<10){mm='0'+mm} 	
	//datos de la tarjeta
		for(var x=0;x<=pedidosJSON.length-1;x++){
			if(pedidosJSON[x].idPedido == pedido ){
								
					AdatosTarjeta.push({num:pedidosJSON[x].pagosTarjeta[0].numeroTarjeta,
										type:pedidosJSON[x].pagosTarjeta[0].tipoTarjeta.descripcion,
										promocion:pedidosJSON[x].pagosTarjeta[0].promocion.descripcion,
										banco:pedidosJSON[x].pagosTarjeta[0].banco.nombre,
										nombretitular:pedidosJSON[x].pagosTarjeta[0].titular,
										esTitular:pedidosJSON[x].pagosTarjeta[0].esTitular,
										vigencia:pedidosJSON[x].pagosTarjeta[0].vigenciaJS,
										seguridad:pedidosJSON[x].pagosTarjeta[0].codigoSeguridad,
										promocion:pedidosJSON[x].pagosTarjeta[0].promocion.descripcion,
										TotalPagar:parseFloat(pedidosJSON[x].totalPago)});	
	//Datos de errores existentes				
					if(pedidosJSON[x].cobros.length > 0){
						var inverse = pedidosJSON[x].cobros.length-1;
						var logerror = pedidosJSON[x].cobros.length;
						var intentos=0;
						for(var y=0;y<=pedidosJSON[x].cobros.length-1;y++){
								txt_error = txt_error+"<label id='lbl_text_error'> ERROR "+logerror+":"+pedidosJSON[x].cobros[inverse].comentarios+"</label></br>";																
								if(pedidosJSON[x].cobros[inverse].numeroTarjeta == pedidosJSON[x].pagosTarjeta[0].numeroTarjeta && pedidosJSON[x].cobros[inverse].fechaCobroGregorian.substring(0,10) == (dd+"-"+mm+"-"+yyyy) ){
									intentos++;
								}
								inverse--;
								logerror--;
								count++;
								if(y == pedidosJSON[x].cobros.length-1 && inverse == -1){
									if(intentos == 0){
										intentos = 0;
									}
									openpopup(AdatosTarjeta,txt_error,atcancelacion,terminales,ident,intentos,pedido,llamadas);
								}
							}	
					}else{
						var intentos=0;
						txt_error = "SIN ERRORES";
						openpopup(AdatosTarjeta,txt_error,atcancelacion,terminales,ident,intentos,pedido,llamadas);	
					}
											
			}
		}
	}
}

// APERTURA DEL POPUP DE OPERACIONES

function openpopup(DatosTarjeta,txt_error,tiposcancelacion,terminales,ident,intentos,pedido,llamadas){
	    $('#popup_operacion').modal({
      backdrop: 'static',
      keyboard: false
    	});
		if(ident == "NAM"){
			$('#title_operacion').html("Operaciones del Pedido "+pedido+"</br>Intentos de cobro hoy: "+intentos);
			$('#footer_operacion').html('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
			var cuerpo = 	'<div class="row" id="space_oper">'+

							'<div class="panel panel-danger row" id="space_oper" style="margin-left: 10px;margin-right: 10px;">'+  
								'<div class="panel-heading col-md-12" id="heads_oper">Errores de Cobro</div>'+
								'<div class="col-md-12" id="log_errores">'+txt_error+'</div>'+
							'</div>'+

							  '<div class="col-md-6">'+
							  	'<div class="panel panel-success col-md-12" id="space_oper" style="margin-bottom: 10px;">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Cobro en terminal fisica</div>'+
							  		'<div class="col-md-6" id="titles_oper">Terminal:</div>'+
							  		'<div class="col-md-6"><select id="selectterminal" class="form-control">'+terminales+'</select></div>'+
							  		'<div class="col-md-12" id="heads_oper">Datos de tarjeta</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de tarjeta:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].num+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de seguridad:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].seguridad+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Vigencia:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].vigencia+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Monto:</div>'+
							  		'<div class="col-md-6">$'+DatosTarjeta[0].TotalPagar+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Promoción:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].promocion+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de Autorización:</div>'+
							  		'<div class="col-md-6"><input type="text" id="num_autorizacion" class="form-control"></div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-success" id="btn_cobro_manual" onclick=cobro_manual("","'+pedido+'")>Registrar Cobro</button>     <button type="button" class="btn btn-danger" id="btn_cobro_manual" onclick=logError("NAM","'+pedido+'")>Error de Cobro</button></div>'+
							  	'</div>'+
							  	'<div class="panel panel-success col-md-12" id="space_oper" style="margin-bottom: 10px;">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Reintento de cobro online</div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-success" onclick=Recobro('+pedido+') id="btn_cobrows">Reintentar</button></div>'+
							  	'</div>'+
							  	'<div class="panel panel-success col-md-12" id="space_oper">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Confirmacion del cobro</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de Autorización:</div>'+
							  		'<div class="col-md-6"><input type="text" id="confirm_num_autorizacion" class="form-control"></div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-success" id="btn_confirmacion" onclick=confirm_cobro('+pedido+')>Registrar</button></div>'+
							  	'</div>'+	
							  '</div>'+
							  '<div class="col-md-6">'+
							  	'<div class="panel panel-warning col-md-12" id="space_oper">'+
								  	'<div class="panel-heading col-md-12" id="heads_oper">Cancelacion de pedido</div>'+
								  	'<div class="col-md-5" id="titles_oper">Tipo de llamada:</div>'+
								  	'<div class="col-md-7"><select id="s_tipollamada" style="margin-bottom: 10px;" class="form-control" onchange="update_stllamada()">'+llamadas+'</select></div>'+
								  	'<div class="col-md-5" id="titles_oper">Subtipo de llamada:</div>'+
								  	'<div class="col-md-7"><select id="s_subtipollamada" style="margin-bottom: 10px;" class="form-control"></select></div>'+
								  	'<div class="col-md-12" id="titles_oper"  style="margin-bottom: 10px;">Motivo Cancelacion:</div>'+
								  	'<div class="col-md-12"><select id="selectcancel" class="form-control" style="margin-bottom: 10px;">'+tiposcancelacion+'</select></div>'+
								  	'<div class="col-md-12" id="titles_oper">Comentarios:</div>'+
								  	'<div class="col-md-12">'+
								  	'<textarea maxlength="100" rows="5" class="form-control" style="width:100%; resize: none;" id="comment_cancel"></textarea>'+
								  	'</div>'+
								  	'<div class="col-md-10" id="titles_oper" style="padding-top: 13px;">Enviar tarjeta a lista negra:</div>'+
								  	'<div class="col-md-2"><input type="checkbox" id="black_list_t" value="1"></div>'+
								  	'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-warning" id="btn_cancelacion_pedido" onclick=cancelacion_pedido('+pedido+')>Cancelar Pedido</button></div>'+
								'</div>'+  	
							  	'<div class="panel panel-info col-md-12" id="space_oper">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Recuperar Pedido</div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-info" onclick=Recuperacion('+pedido+') id="btn_recuperacion">Recuperar</button></div>'+
							  	'</div>'+
							  '</div>'+						  							  	
							'</div>'+
							'</div>';

			$('#body_operacion').html(cuerpo);
			hideLoading();
				if(intentos == 0 || intentos == 1 ){
					$('#popup_operacion').modal('show');
				}else if(intentos == 2){
					alert("Se realizara el día de hoy el tercer intento de cobro con esta tarjeta.");
					$('#popup_operacion').modal('show');
				}else if(intentos == 3){
					alert("Se han realizado los tres intentos del cobro con la misma tarjeta, debera esperar al dia de mañana para realizar el cobro");
					$('#popup_operacion').modal('show');					
					$('#btn_cobrows').attr("disabled", true);
				}	



		}else if(ident == "AM"){

			$('#title_operacion').html("Operaciones del Pedido "+pedido+"</br>Intentos de cobro: "+intentos);
			$('#footer_operacion').html('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>');
			var cuerpo = 	'<div class="row" id="space_oper">'+

							'<div class="panel panel-danger row" id="space_oper" style="margin-left: 10px;margin-right: 10px;">'+  
								'<div class="panel-heading col-md-12" id="heads_oper">Errores de Cobro</div>'+
								'<div class="col-md-12" id="log_errores">'+txt_error+'</div>'+
							'</div>'+	

							  '<div class="col-md-6" id="space_oper">'+
							  	'<div class="panel panel-success col-md-12" id="space_oper" style="margin-bottom: 10px;">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Cobro en terminal fisica</div>'+
									'<div class="col-md-12" id="heads_oper">Datos de tarjeta</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de tarjeta:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].num+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de seguridad:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].seguridad+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Vigencia:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].vigencia+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Monto:</div>'+
							  		'<div class="col-md-6">$'+DatosTarjeta[0].TotalPagar+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Promoción:</div>'+
							  		'<div class="col-md-6">'+DatosTarjeta[0].promocion+'</div>'+
							  		'<div class="col-md-6" id="titles_oper">Numero de Autorización:</div>'+
							  		'<div class="col-md-6"><input type="text" id="num_autorizacion" class="form-control"></div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-success" id="btn_cobro_manual" onclick=cobro_manual("5","'+pedido+'")>Registrar Cobro</button>     <button type="button" class="btn btn-danger" id="btn_cobro_manual" onclick=logError("AM","'+pedido+'")>Error de Cobro</button></div>'+
							  	'</div>'+							  	  	
							  	'<div class="panel panel-info col-md-12" id="space_oper">'+
							  		'<div class="panel-heading col-md-12" id="heads_oper">Recuperar Pedido</div>'+
							  		'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-info" onclick=Recuperacion('+pedido+') id="btn_recuperacion">Recuperar</button></div>'+
							  	'</div>'+
							  '</div>'+
							  '<div class="panel panel-warning col-md-6" id="space_oper">'+
							  	'<div class="panel-heading col-md-12" id="heads_oper">Cancelacion de pedido</div>'+
							  	'<div class="col-md-5" id="titles_oper">Tipo de llamada:</div>'+
							  	'<div class="col-md-7"><select id="s_tipollamada" style="margin-bottom: 10px;" class="form-control" onchange="update_stllamada()">'+llamadas+'</select></div>'+
							  	'<div class="col-md-5" id="titles_oper">Subtipo de llamada:</div>'+
							  	'<div class="col-md-7"><select id="s_subtipollamada" style="margin-bottom: 10px;" class="form-control"></select></div>'+
							  	'<div class="col-md-12" id="titles_oper"  style="margin-bottom: 10px;">Motivo Cancelacion:</div>'+
							  	'<div class="col-md-12"><select id="selectcancel" class="form-control" style="margin-bottom: 10px;">'+tiposcancelacion+'</select></div>'+
							  	'<div class="col-md-12" id="titles_oper">Comentarios:</div>'+
							  	'<div class="col-md-12">'+
							  	'<textarea maxlength="100" rows="5" class="form-control" style="width:100%; resize: none;" id="comment_cancel"></textarea>'+
							  	'</div>'+
							  	'<div class="col-md-10" id="titles_oper" style="padding-top: 13px;">Enviar tarjeta a lista negra:</div>'+
							  	'<div class="col-md-2"><input type="checkbox" id="black_list_t" value="1"></div>'+
							  	'<div class="col-md-12" id="btn_oper"><button type="button" class="btn btn-success" id="btn_cancelacion_pedido" onclick=cancelacion_pedido('+pedido+')>Cancelar Pedido</button></div>'+
							  '</div>'+							  
							'</div>'+
							'</div>';

			$('#body_operacion').html(cuerpo);
			hideLoading();
				if(intentos == 0 || intentos == 1 ){
					$('#popup_operacion').modal('show');
				}else if(intentos == 2){
					alert("Esta por realizar el tercer intento de cobro con la misma tarjeta,una vez realizados los 3 intentos se solicitara que cambie de tarjeta esto implicara contactar al cliente para notificarle que su tarjeta no pasa el cobro");
					$('#popup_operacion').modal('show');
				}else if(intentos >= 3){
					alert("Se han realizado los tres intentos del cobro con la misma tarjeta, debera esperar al dia de mañana para realizar el cobro");
					$('#popup_operacion').modal('show');					
					$('#btn_cobrows').attr("disabled", true);
				}
		}
}
//SUBTIPO DE LLAMADA
  function update_stllamada(){
    var tllamada = $('#s_tipollamada').val();
    $('option','#s_subtipollamada').remove();
      for(var x =0;x<=ASubTipoLllamada.length-1;x++){
        if (ASubTipoLllamada[x].tipoLlamada.idTipoLlamada == tllamada) {
            $('#s_subtipollamada').append('<option value="'+ASubTipoLllamada[x].idSubTipo+'" selected="selected">'+ASubTipoLllamada[x].descripcionSubTipo+'</option>');

        }
      }  

  }

//COBRO MANUAL
function cobro_manual(terminalesp,numpedido){

	if(terminalesp == ""){
		var terminal = $('#selectterminal').val();
	}else{
		var terminal = terminalesp;
	}

	var numautorizacion = $('#num_autorizacion').val();	

	if(numautorizacion != "" && terminal != ""){
		var AcobroManual = new Array();
		AcobroManual.push({idPedido:numpedido,numAutorizacion: numautorizacion,Nterminal:terminal});
		$('#popup_operacion').modal('hide')
			showLoading('Realizando el cobro del Pedido '+numpedido);	

		$.post("../CobroManual",{
				AcobroManual:AcobroManual
			},function(respuesta){
				var res = respuesta[0].estatus;
				if(res == 1){
					alert(respuesta[0].mensaje);
					$('#text_load').text("Actualizando Pedidos");
					$('#form_Consultar').click();
			  	}else{
					alert("Error:\n"+respuesta[0].mensaje);
					hideLoading();
			   	}
			});
	}else{
		alert("Es necesario indicar la terminal donde se realizo el cobro y el número de autorización obtenido.");
	}

}

//CONFIRMACIÓN DEL COBRO

function confirm_cobro(pedido){
	var numautorizacion = $('#confirm_num_autorizacion').val();
	if(numautorizacion != ""){
		var AcobroManual = new Array();
		AcobroManual.push({idPedido:pedido,numAutorizacion:numautorizacion});
		$('#popup_operacion').modal('hide')
			showLoading('Confirmando el cobro del Pedido '+pedido);	

		$.post("../ConfirmacionCobro",{
				AcobroManual:AcobroManual
			},function(respuesta){
				var res = respuesta[0].estatus;
				if(res == 1){
					alert(respuesta[0].mensaje);
					$('#text_load').text("Actualizando Pedidos");
					$('#form_Consultar').click();
			  	}else{
					alert("Error:\n"+respuesta[0].mensaje);
					hideLoading();
			   	}
			});
	}else{
		alert('Es necesario ingresar el numero de autorización');
	}

}

//CANCELACION DEL PEDIDO

function cancelacion_pedido(pedido){
	var tipoLlamada = $('#s_tipollamada').val();
	var subllamada = $('#s_subtipollamada').val();
	var tipocancelacion = $('#selectcancel').val();
	var comentarioscancelacion = $('#comment_cancel').val();
	var blacklist = $('#black_list_t:checked').val();
	var num_tarjeta;
	if(blacklist != 1){
		blacklist = 0;
	}

		for(var x=0;x<=pedidosJSON.length-1;x++){
		if(pedidosJSON[x].idPedido == pedido ){
			num_tarjeta = pedidosJSON[x].pagosTarjeta[0].numeroTarjeta;
		}

	}

	if(tipocancelacion && comentarioscancelacion && tipoLlamada && subllamada){

		var AcancelacionPedido = new Array();
		AcancelacionPedido.push({idPedido:pedido,Tcancelacion: tipocancelacion, Comentarios:comentarioscancelacion, blackList:blacklist , numTarjeta:num_tarjeta, llamada:tipoLlamada, SubLlamada:subllamada, TArea: "Cobranza"});
		$('#popup_operacion').modal('hide')
			showLoading('Realizando la cancelacion del Pedido '+pedido);	

		$.post("../CancelacionCobro",{
				AcancelacionPedido:AcancelacionPedido
			},function(respuesta){
				var res = respuesta[0].estatus;
				if(res == 1){
					alert("Se ha realizado la cancelación del pedido"+pedido);
					$('#text_load').text("Actualizando Pedidos");
					$('#form_Consultar').click();
			  	}else{
					alert("Error:\n"+respuesta[0].mensaje);
					hideLoading();
			   	}
			});

	}else{
		alert("Es necesario indicar:\n->Tipo de llamada\n->Subtipo de llamada\n->Motivo de la cancelación\n->Comentarios de la cancelación (con fines informativos)");
	}
}

//REGISTRO DE LOG DE ERRORESS POR TERMINAL FISICA 
function logError(ident,pedido){
	$('#popup_operacion').modal('hide');
	$('#title_log').html("Error en terminal virtual del pedido: "+pedido);
	$('#footer_log').html('<button type="button" class="btn btn-success" onclick=savelog('+pedido+')>Guardar</button>&nbsp;&nbsp;<button type="button" class="btn btn-default" data-dismiss="modal" onclick=open_popupoperaciones("'+ident+'","'+pedido+'")>Cerrar</button>');
	$('#popup_log').modal('show');
}

function savelog(pedido){
var comentario = $('#comment_log').val();
var tarjeta="";
	if(comentario){
		
		for(var t=0;t<=pedidosJSON.length-1;t++){
			if(pedidosJSON[t].idPedido == pedido){
				tarjeta = pedidosJSON[t].pagosTarjeta[0].numeroTarjeta;
			}
		}	
			loadlog(pedido,comentario,tarjeta,function(res){
							if(res != 0){
			    	    			alert(res);
			    	    			$('#form_Consultar').click();
			    	    			$('#text_load').html("Actualizando pedidos...");	    	    			
			    	    		}else{
			    	    			alert("Registro Incorrecto, intente nuevamente");
			    	    			hideLoading();	    	    			
			    	    		}
				});	
		
	}else{
		alert("Debe ingresar la descripcion del error obtenido en la terminal fisica");
	}
}

function loadlog(pedido,comentario,tarjeta,callback){
	showLoading("Registrando...");
		$.post("../terminalesFisicasLog",{
				pedido:pedido,
				comentario:comentario,
				tarjeta,tarjeta
			},function(respuesta){
				var res = respuesta.estatus;
					if(res == 1){
						res = respuesta.mensaje;
						callback(res);
					}else{
						callback(res);
					}
			});
}

//MOSTRAR VENTANA DE RECUPERACION.
function Recuperacion(pedido){
	url = "../cobranza/"+pedido+"/editDireccionTarjeta";
	window.location.replace(url);
	return false;
}

//CARGA DE LOS PRECIOS RELACIONADOS AL PRODUCTO
var precios_cagados = 0;

function getprices(producto,ventas,distribucion,organizacion,detalle,prodP,ident,ida){	
	
	if(precios_cagados == 0){
		Aprices.length=0;
		showLoading('Cargando precios del producto');
		loadprices(producto,ventas,distribucion,organizacion,prodP,function(res){
					if(res != 0){
	    	    			Aprices=res.concat();
	    	    			hideLoading();
	    	    			precios_cagados = 1;
	    	    			aprovechanges(ident,ida,detalle);
	    	    		}else{
	    	    			Aprices.push({value: '0', text: 'No hay más precios para este producto'});
	    	    			hideLoading();
	    	    			precios_cagados = 1;
	    	    			aprovechanges(ident,ida,detalle);	    	    			
	    	    		}
		});
	}else{
		aprovechanges(ident,ida,detalle);
	}
	
}


function loadprices(producto,ventas,distribucion,organizacion,prodP,callback){
	var ApedidotoPrice = new Array();
	ApedidotoPrice.push({idProducto:producto,OfVtn:ventas,CanDistri:distribucion,OrgVtn:organizacion,Pdetalle:prodP});

	$.post("../preciosJSON",{
				ApedidotoPrice:ApedidotoPrice
			},function(respuesta){
				var res = respuesta[0].estatus;
					if(res == 1){
						res = respuesta[0].mensaje;
						callback(res);
					}else{
						callback(res);
					}
			});

}

//CARGA LAS PROMOCIONES RESPECTO AL DID
var promociones_cargadas = 0;

function getPromociones(ident,ida,bin){	

	if(promociones_cargadas == 0){
		Apromos.length=0;
		showLoading('Cargando Promociones');
		loadpromociones(bin,function(res){

					if(res.length-1){
	    	    			Apromos=res.concat();
	    	    			hideLoading();
	    	    			promociones_cargadas = 1;
	    	    			aprovechanges(ident,ida,bin);
	    	    		}else{
	    	    			Apromos.push({value: '0', text: 'No más promociones'});
	    	    			hideLoading();
	    	    			promociones_cargadas = 1;
	    	    			aprovechanges(ident,ida,bin);	    	    			
	    	    		}
		});
	}else{
		aprovechanges(ident,ida,bin);
	}
	
}

function loadpromociones(BIN,callback){
	

	$.post("../promocionesBinJSON",{
				BIN:BIN
			},function(respuesta){
				var res;
				if(respuesta.length>0){
					if(respuesta[0].promociones.length>0){
						res = respuesta[0].promociones;
						callback(res);
					}else{
						res = 0;
						callback(res);
					}
				}else{
					res = 0;
					callback(res);
				}
			});

}

function deleteProducto(idp,iddetalle,precio,cantidad,descripcion){

	var precioP = precio;
	var canP = cantidad;
	confirmar=confirm("¿Decea eliminar el producto "+descripcion+" ?"); 
    if(confirmar){
	 
	eliminarProd(idp,precioP,canP,descripcion,iddetalle,function(res){
					if(res.estatus){
						alert(res.mensaje);
						$('#text_load').html("Actualizando pedidos...");
						$('#form_Consultar').click();
					}else{
						hideLoading();
						alert("Error al eliminar producto intente nuevamente o actualice los pedidos");
					}
		});
	}
}

function eliminarProd(idp,precioP,canP,descripcion,iddetalle,callback){
	showLoading("Eliminando producto </br>"+descripcion+"</br>del Pedido: "+idp);
	$.post("../eLiminarProductoPedido",{
				idp:idp,
				precioP:precioP,
				canP:canP,
				descripcion:descripcion,
				iddetalle:iddetalle
			},function(respuesta){
				var res = respuesta;				
					callback(res);
			});
}

function canceledit(){
	clave = "";
	idedit = "";
	precios_cagados=0;
	promociones_cargadas = 0;
}