	$(document).ready(function() {
        	var table = $('#tbl_cancelaciones').DataTable({
        					"language":DataTables.languaje.es					
        				});

        	$( "select" ).addClass( "form-control" );
        	$( "input[type='text']" ).addClass( "form-control" );        	
			$( "input[type='search']").addClass( "form-control" );
        	$( "#tbl_cancelaciones_length" ).addClass( "form-inline" ); 
        	$( "#tbl_cancelaciones_filter" ).addClass( "form-inline" );
        	$( "#tbl_cancelaciones_filter>label" ).html('Buscar: <input type="text" id="search_pedido" placeholder="Numero de pedido" class="form-control">');				

				$('#search_pedido').on( 'keyup', function () {
						    table
						        .columns(0)
						        .search( this.value )
						        .draw();
						} );

        	$('.navbar-toggle').click(function(){
                    var $target = $('.navbar-collapse');
                    if($target.hasClass('in')){
                        $target.toggle("linear").height(0).css('overflow','hidden');                         
                    }else{
                    	$target.toggle("linear").height(190).css('overflow','hidden'); 
                    }
                });

$('.dropdown').click(function(){
                    var $target = $('.dropdown');
                    if($target.hasClass('open')){
                        $target.removeClass( "dropdown open" ).addClass( "dropdown" );                         
                    }else{
                      $target.addClass("dropdown open"); 
                    }
                });   	       
  	});

//Loading para consulta de operaciones
var img = new Image();
img.src = "../../vendor/img/bubbles_load.gif";

function loadPedidos(){
	
		showLoading("Cargando Pedidos ...");

}

		if(canceladosJSON.length != 0){
			
				var table_cancelados;
				var tam_canceladosJSON = canceladosJSON.length-1;
				var next_row=0;

			for(var x=0;x<=canceladosJSON[next_row].cobros.length-1;){
					if(canceladosJSON[next_row].cobros[x].numeroAutorizacion && canceladosJSON[next_row].cobros[x].respuesta.idTipoRespuesta == 1 ){
						table_cancelados = table_cancelados+"<tr><td>"+canceladosJSON[next_row].idPedido+"</td><td class='cc_cobrados'>"+canceladosJSON[next_row].estatus.descripcion+"</td><td>"+canceladosJSON[next_row].pagosTarjeta[0].terminal.descripcion+"</td><td>"+canceladosJSON[next_row].cobros[x].numeroAutorizacion +"</td><td>"+canceladosJSON[next_row].cobros[x].fechaCobroGregorian+"</td><td> $ "+canceladosJSON[next_row].totalPago+"</td><td><button type='button' class='btn btn-danger' id='btn_cancelpedido' onclick=cancel_pedido('"+canceladosJSON[next_row].idPedido+"','"+canceladosJSON[next_row].cobros[x].numeroAutorizacion+"')><span class='icon-x-altx-alt'></span></button></td></tr>";
					}	
					
					if(x==canceladosJSON[next_row].cobros.length-1 && next_row != tam_canceladosJSON){
						next_row++;
						x=0;
					}else{
						x++;
					}
			}						
			
			if(next_row == canceladosJSON.length-1){
				$('#body_tbl_cancelaciones').html(table_cancelados);
			}
		}

var ok_cancel = 0;


function cancel_pedido(idpedido,autorizacion){

	if(ok_cancel == 0){
		$('#pf_supervisor').modal('show');
	}else if(ok_cancel == 1){
		popup_cancelacion(idpedido,autorizacion);
	}
}



function validate_supervisor(){

	usuario = $('#user_sup').val();
	contrasena = $('#pass_sup').val();
	bioSupervisor = new Array();
	

	if (contrasena != "" && usuario != "") {
		$("#super_loading").html("<img src="+img_loding.src+">&nbsp;&nbsp;&nbsp;<label>Validando...</label>");
		$('#btn_cancel_sup').attr("disabled", true);
		$('#btn_aceptar_sup').attr("disabled", true);

		bioSupervisor.push({user:usuario, pass:contrasena});
		$.post("../Supervisor",{
			bioSupervisor:bioSupervisor
		},function(respuesta){
		 	if(respuesta[0].estatus == 1){
		 		clave = 1;
				$('#user_sup').val("");
				$('#pass_sup').val("");
				$("#super_loading").html("");
				clave = 1;
				ok_cancel = 1;
				alert("Usuario correcto");
				$('#btn_cancel_sup').attr("disabled", false);	
				$('#btn_aceptar_sup').attr("disabled", false);				
				$('#pf_supervisor').modal('hide');
		 	}else{
		 		$('#user_sup').val("");
				$('#pass_sup').val("");
				$("#super_loading").html("");				
		 		alert(respuesta[0].mensaje);
		 		$('#btn_cancel_sup').attr("disabled", false);	
		 		$('#btn_aceptar_sup').attr("disabled", false);	
		 	}
			}
		);
		
	}else{
		alert("Es necesario indicar Usuario y contraseña de Supervisor de area");
	}
}


function popup_cancelacion(idpedido,autorizacion){
var selects_tipocancelacion="<option value=''>-------------------------</option>";
preload_options(selects_tipocancelacion,idpedido,autorizacion);

function preload_options(atcancelacion,pedido,autorizacion){
		showLoading("Cargando ventana de cancelacion del Pedido "+pedido);	
		var tiposrespuesta = $.getJSON( "../respuestascancelacionJSON", function() {

  			for(x=0;x<=tiposrespuesta.responseJSON.length-1;x++){
  				atcancelacion = atcancelacion +"<option value='"+tiposrespuesta.responseJSON[x].idCancelacion+"'>"+tiposrespuesta.responseJSON[x].descripcion+"</option>" ;
  				if(x == tiposrespuesta.responseJSON.length-1){
  					$.unblockUI();
  					open_cancelacion(atcancelacion,pedido,autorizacion);
  				}
  			} 					
		});

	}
function open_cancelacion(atcancelacion,pedido,autorizacion){

	var cuerpo = "<div class='row'>"+
				"<div class='col-xs-8 col-sm-6' style='font-size: 16px;font-weight: 700;'>Motivo de cancelación:</div>"+
				"<div class='col-xs-8 col-sm-6' style='font-size: 16px;'>"+
				"<select id='type_cancelacion' class='form-control'>"+
				atcancelacion+
				"</select></div>"+
				"<div class='col-xs-8 col-sm-12' style='font-size: 16px;font-weight: 700;'>Comentarios:</div>"+
				"<div class='col-xs-8 col-sm-12' style='font-size: 16px;'>"+
				"<textarea maxlength='80' rows='10' class='form-control' style='width:100%; resize: none;' id='comment_cancel' placeholder='Motivos de cancelacion'></textarea>"+
				"</div></div>";

	$('#title_cancelacion').text("Cancelacion del Pedido "+idpedido);
	$('#body_cancelacion').html(cuerpo);
	$('#footer_cancelacion').html('<button type="button" class="btn btn-danger" onclick="cancelacion_pedido('+idpedido+','+autorizacion+');">Cancelar Pedido</button><button type="button" class="btn btn-default" data-dismiss="modal" onclick="negative_cancel()">Cerrar</button>');
	$('#popup_cancelacion').modal('show');

}
		
}


function cancelacion_pedido(pedido,autorizacion){

	comentarios = $('#comment_cancel').val();
	motivocancel = $('#type_cancelacion').val();

	if(comentarios != "" && motivocancel != ""){

		var ApedidoCancelacionCobro = new Array();
		ApedidoCancelacionCobro.push({idPedido:pedido,Tcancelacion: motivocancel, Comentarios:comentarios,NumAuto:autorizacion});
		$('#popup_cancelacion').modal('hide')
			showLoading("Realizando la cancelacion del Pedido "+pedido);	

		$.post("../CancelacionCobroPedido",{
				ApedidoCancelacionCobro:ApedidoCancelacionCobro
			},function(respuesta){
				var res = respuesta[0].estatus;
				if(res == 1){
					alert("Sa ha realizado la cancelación del pedido");
					$('#text_load').text("Actualizando Pedidos");
					$('#form_Consultar').click();
			  	}else{
					alert("Error\n"+respuesta[0].mensaje);
					$('#form_Consultar').click();
					$('#text_load').text("Actualizando Pedidos");
			   	}
			});


	}else{
		alert("Es necesario indicar el motivo de la cancelación, y agregar comentarios solo como observación");
	}

}

function negative_cancel(){
	clave = 0;
	ok_cancel = 0;
}