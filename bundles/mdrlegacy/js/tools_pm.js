var ApedidoConsulta = new Array();
var AresultadosBusqueda = new Array(); 
var ATipoLlamada = new Array();
var ASubTipoLllamada = new Array();
var AEstatus = new Array();
var detalles_pedidocrm = new Array();
var table;

  $(document).ready(function() { 

    $.getJSON("../tipollamadasJSON",function(result){
      $.each(result, function(index, val) {
           ATipoLlamada.push({id:val.descripcion,text:val.descripcion});
        });
    });


    $.getJSON("../subtipollamadasJSON",function(result){
           ASubTipoLllamada = result;
    });  

 $.getJSON("../estatusJSON",function(result){
           AEstatus = result;
    });  



    table  = $('#tbl_consultas_atc').DataTable({
                      "language":DataTables.languaje.es           
                    });


        $( "select" ).addClass( "form-control" );
        $( "input[type='text']" ).addClass( "form-control" );
        $( "input[type='search']").addClass( "form-control" );
        $( "#tbl_consultas_atc_length" ).addClass( "form-inline" );
        $( "#tbl_consultas_atc_filter" ).addClass( "form-inline" );





  });

function consulta_pedido(){
  table.clear().draw(); 
  var nclie = $('#txt_ncliente').val(); 
  var app = $('#txt_appcliente').val(); 
  var apm =  $('#txt_apmcliente').val(); 
  var pedido = $('#txt_pedido').val(); 
  var tajeta = $('#txt_tarjeta').val();  
  var compra = $('#txt_tipocompra').val();  

  if(nclie || app || apm || pedido || tajeta){
    if((nclie && app) || pedido || tajeta){
      if((nclie.length >= 3 && app.length >= 3)|| pedido || tajeta ){
           showLoading("Cargando Pedidos ...");
              consultar(nclie,app,apm,pedido,tajeta,compra,function(respuesta){
                if(respuesta[0].estatus !=0){
                  AresultadosBusqueda = respuesta[0].mensaje;
                  make_table(AresultadosBusqueda);            
                }else{
                  alert("No hay resultados");
                  hideLoading();
                }
                
              });
      }else{
        alert("El nombre del cliente y el apellido parterno debe tener minimo 3 letras");
      }        
    }else{
      alert("El nombre del cliente y el apellido paterno son obligatorios");
    }      
  } else{
    alert("Debe ingresar almenos:</br>->Nombre del cliente</br>->Apellido Paterno</br>->Apellido Materno</br>->Apellido Materno</br>-># Pedido</br>-># Tarjeta");
  }
}


function consultar(nclie,app,apm,pedido,tajeta,compra,callback){
      var cliente="";
      if(nclie){
        cliente = cliente+nclie+"%";
      }
      if(app){
        cliente = cliente+app+"%";
      }
      if(apm){
        cliente = cliente+apm+"%";
      }

      ApedidoConsulta.length = 0;
      ApedidoConsulta.push({Cliente:cliente,idPedido:pedido,NTarjeta:tajeta,Tipo:compra});

        $.post("../phonemartJSON",{
          ApedidoConsulta:ApedidoConsulta
      },function(respuesta){
          callback(respuesta);
        });
}
function gregoriandate(date){
                var annio = date.substring(0,4);
                date = date.substr(5);
                var mes = date.substr(2,4);
                mes = mes.substr(1);
                var dia = date.substr(0,2);
                date = mes+"/"+dia+"/"+annio;
                return date;
            }

 function make_table(AresultadosBusqueda){
    var bodytable=new Array();
    var Productos="";

//NÚMERO DEL PEDIDO, NOMBRE DEL CLIENTE Y FECHA DEL PEDIDO
    for(var i =0;i<= AresultadosBusqueda.length - 1;i++){

      bodytable.length=0;
      Productos="";

      bodytable.push(AresultadosBusqueda[i].pedido);
      bodytable.push(AresultadosBusqueda[i].nombre);
      bodytable.push(gregoriandate(AresultadosBusqueda[i].fAlta.date.substring(0,10)));
 
//TIPO DE PAGO Y NUMERO DE TARJETA
        bodytable.push(AresultadosBusqueda[i].Pago);
        if(AresultadosBusqueda[i].Pago == "TARJETA"){
          if(AresultadosBusqueda[i].cobros.length > 0){
            bodytable.push(hidenumber(AresultadosBusqueda[i].cobros[0].tarjeta));
          }else{
            bodytable.push("");
          }  
        }
        else
        {
        bodytable.push("");
        }
var subtotal = 0;
//PRODUCTOS DEL PEDIDO
if(AresultadosBusqueda[i].cant1){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod1+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant1+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec1);
}
if(AresultadosBusqueda[i].cant2){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod2+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant2+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec2);  
}
if(AresultadosBusqueda[i].cant3){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod3+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant3+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec3);  
}
if(AresultadosBusqueda[i].cant4){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod4+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant4+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec4);  
}
if(AresultadosBusqueda[i].cant5){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod5+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant5+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec5);  
}
if(AresultadosBusqueda[i].cant6){
  Productos = Productos+ '<tr><td style="font-size: 14px;">'+AresultadosBusqueda[i].prod6+'</td></tr><tr><td style="font-size: 14px; text-align: center;"> Cant. '+AresultadosBusqueda[i].cant6+'</td></tr>';
  subtotal = subtotal+parseFloat(AresultadosBusqueda[i].prec6);  
}
             
          bodytable.push("<table>"+Productos+"</table>");                  
//GASTOS ENVIO, SUBTOTAL, TOTAL
        bodytable.push("$"+parseFloat(AresultadosBusqueda[i].gEnvio));
        bodytable.push("$"+parseFloat(subtotal));
        bodytable.push("$"+parseFloat(AresultadosBusqueda[i].total));
        var estat = AresultadosBusqueda[i].estatus;
        bodytable.push('<a style="cursor:pointer;" onclick=changeEstatus('+AresultadosBusqueda[i].pedido+',"'+estat.replace(/ /g,'_')+'")>'+AresultadosBusqueda[i].estatus+"</a>");

//VALIDAMOS EL ESTATUS            
       bodytable.push('<button type="button" class="btn btn-info" onclick=showDtl('+AresultadosBusqueda[i].pedido+');><span class="icon-pencil"></span></button>');
    
            
           
      table.row.add(bodytable).draw();    
    }
    $.unblockUI();
  }


    function hidenumber(num){
    var nnum ="";
    for(x=0;x<=num.length-1;x++){

      if(x>3 && x<12){
        nnum = nnum + '&#8226;';
      }else{
        nnum = nnum+num[x];
      }      
    }
    return nnum;
  }


  function changeEstatus(pedido,estatus){

     //Selects TipoLlamada
    var StipoLlamada="<option>-------------------------</option>";
    for(var x=0;x<=ATipoLlamada.length-1;x++){
      StipoLlamada = StipoLlamada+"<option value='"+ATipoLlamada[x].text+"'>"+ATipoLlamada[x].text+"</option>";
      //console.log(StipoLlamada);
    }

         //Selects TipoLlamada
    var NEstatus="<option>-------------------------</option>";
    for(var x=0;x<=AEstatus.length-1;x++){
      NEstatus = NEstatus+"<option value='"+AEstatus[x].descripcion+"'>"+AEstatus[x].descripcion+"</option>";
      //console.log(StipoLlamada);
    }

    $('#title_atc').html('PHONEMART: Pedido '+pedido+'<label style="float: right;">Cambio de Estatus</label>');
    var cuerpo= '<div class="row" style="padding-top: 15px;">'+
                    '<div class="col-md-6">'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<label style="margin-bottom: 10px;font-weight: 700;">ESTATUS ACTUAL:</label>          '+estatus+                              
                          '</div>'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<div class="col-md-4" style="margin-bottom: 10px;font-weight: 700;">*Tipo Llamada:</div>'+
                              '<div class="col-md-8" style="margin-bottom: 10px;"><select id="s_tllamada" class="form-control" onchange="update_stllamada()">'+StipoLlamada+'</select></div>'+
                          '</div>'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<div class="col-md-4" style="margin-bottom: 10px;font-weight: 700;">*Sub llamada:</div>'+
                              '<div class="col-md-8" style="margin-bottom: 10px;"><select id="s_stllamada" class="form-control"></select></div>'+
                          '</div>'+
                          '<div class="col-md-12" style="padding: 0px !important;">'+
                              '<div class="col-md-4" style="margin-bottom: 10px;font-weight: 700;">*Nuevo Estatus:</div>'+
                              '<div class="col-md-8" style="margin-bottom: 10px;"><select id="s_estatus" class="form-control">'+NEstatus+'</select></div>'+
                          '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="col-md-12" style="margin-top: 15px;font-weight: 700;">*Comentarios:</div>'+
                          '<div class="col-md-12"><textarea id="txt_comentarios" maxlength="150" style="resize:none;height: 200px;" class="form-control"></textarea></div>'+
                    '</div>'+
                '</div>';
    $('#body_atc').html(cuerpo);            
    $('#footer_atc').html('<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button><button type="button" class="btn btn-primary" onclick="saveEstatus('+pedido+')">Guardar</button>');

    $('#popup_atc').modal({
      backdrop: 'static',
      keyboard: false
    });

  }


function update_stllamada(){
    var tllamada = $('#s_tllamada').val();
    $('option','#s_stllamada').remove();
      for(var x =0;x<=ASubTipoLllamada.length-1;x++){
        if (ASubTipoLllamada[x].tipoLlamada.descripcion == tllamada) {
            $('#s_stllamada').append('<option value="'+ASubTipoLllamada[x].descripcionSubTipo+'" selected="selected">'+ASubTipoLllamada[x].descripcionSubTipo+'</option>');

        }
      }  

  }

function update_stllamada2(){
    var tllamada = $('#tllamada').val();
    $('option','#stllamada').remove();
      for(var x =0;x<=ASubTipoLllamada.length-1;x++){
        if (ASubTipoLllamada[x].tipoLlamada.descripcion == tllamada) {
            $('#stllamada').append('<option value="'+ASubTipoLllamada[x].descripcionSubTipo+'" selected="selected">'+ASubTipoLllamada[x].descripcionSubTipo+'</option>');

        }
      }  

  }  


  function saveEstatus(pedido){
    var llamada = $('#s_tllamada').val(); 
    var sllamada = $('#s_stllamada').val();
    var estatus = $('#s_estatus').val();
    var comentarios = $('#txt_comentarios').val();
    if( llamada && sllamada && estatus && comentarios){
       ExEstatus(pedido,llamada,sllamada,estatus,comentarios,function(res){
        if(res.estatus == 1){
            alert("Se a actualizado con exito el estatus del pedido "+pedido);
            $('#popup_atc').modal('hide');
            hideLoading();
            showLoading("Actualizando Pedidos");
            $('#btn_consulta').click();
        }else{
          hideLoading();
          alert("No se a podido cambiar el estatus intente nuevamente mas tarde");
        }
      }); 
    }else{
      alert("Debe ingresar:</br>->Tipo de LLamada</br>->Subtipo de Llamada</br>->El nuevo Estatus</br>->Comentarios");
    }
  }

function ExEstatus(pedido,llamada,sllamada,estatus,comentarios,callback){
  $.post("../updateEstatusPhonemart",{
        pedido:pedido,
        llamada:llamada,
        sllamada:sllamada,
        estatus:estatus,
        comentarios:comentarios,
      },function(respuesta){
      var res = respuesta;
        callback(res);
      });
}



function showDtl(pedido){
  showLoading("Cargando Datos");
  var idPedido = pedido;
    loaddetalles(idPedido,function(res){
        if(res.estatus == 1){
          hideLoading();
          detalles_pedidocrm[0] = res.detalle[0];
          detalles_pedidocrm[1] = ATipoLlamada; 
          detalles_pedidocrm[2] = AEstatus;
            var template = _.template(document.getElementById("previewBodyTemplate").textContent);
            var html_info = template(detalles_pedidocrm);
            $("#popup_atc").modal("show");
            $("#body_atc").html(html_info);
        }else{
          hideLoading();
          alert(res.detalle);
        }
    }); 


    
  }

  function loaddetalles(idPedido,callback){

    $.post("../detallepedidoPhonemartJSON",{
        idPedido:idPedido
      },function(respuesta){
      var res = respuesta;
        callback(res);
      });

  }


function saveComentarios(pedido){
    var llamada = $('#tllamada').val(); 
    var sllamada = $('#stllamada').val();
    var comentarios = $('#comentarios').val();
    if( llamada && sllamada && comentarios){
       ExComentarios(pedido,llamada,sllamada,comentarios,function(res){
        if(res.estatus == 1){
            alert("Guardando comentarios del pedido "+pedido);
            $('#popup_atc').modal('hide');
            hideLoading();
            showLoading("Actualizando Pedidos");
            $('#btn_consulta').click();
        }else{
          hideLoading();
          alert("No se a podido guardar los comentarios intente nuevamente mas tarde");
        }
      }); 
    }else{
      alert("Debe ingresar:</br>->Tipo de LLamada</br>->Subtipo de Llamada</br>->Comentarios");
    }
  }


function ExComentarios(pedido,llamada,sllamada,comentarios,callback){
  $.post("../ComentariosPhonemart",{
        pedido:pedido,
        llamada:llamada,
        sllamada:sllamada,
        comentarios:comentarios,
      },function(respuesta){
      var res = respuesta;
        callback(res);
      });
}  